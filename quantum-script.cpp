//
// Quantum Script
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>
#endif

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-licence.hpp"
#include "quantum-script-copyright.hpp"
#ifndef QUANTUM_SCRIPT_NO_VERSION
#include "quantum-script-version.hpp"
#endif

#include "libquantum-script.hpp"

using namespace XYO;
using namespace XYO::XY;
using namespace XYO::XO;
using namespace Quantum::Script;

class Application :
	public virtual IMain {
		XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Application);
	protected:

		static void initExecutive(Executive *);

		void showUsage();
		void showLicence();

	public:

		inline Application() {};

		int main(int cmdN, char *cmdS[]);
};

void Application::initExecutive(Executive *executive) {
};

void Application::showUsage() {
#ifdef QUANTUM_SCRIPT_INTERNAL
	printf("Quantum Script\n");
#else
	printf("Quantum Script - version %s build %s [%s]\n", Quantum::Script::Version::getVersion(), Quantum::Script::Version::getBuild(), Quantum::Script::Version::getDatetime());
#endif
	printf("%s\n\n", Quantum::Script::Copyright::fullCopyright());

	printf("%s",
	       "options:\n"
	       "    --licence           show licence\n"
	       "    script.js           execute script\n"
	      );
	printf("\n");
};

void Application::showLicence() {
	printf("%s", Quantum::Script::Licence::content());
};

int Application::main(int cmdN, char *cmdS[]) {
	int i;
	char *opt;
	char *fileIn;
	bool executionTime=false;
	qword beginTimestampInMilliseconds;
	qword endTimestampInMilliseconds;
	qword intervalTimestampInMilliseconds;
	fileIn = NULL;
	for (i = 1; i < cmdN; ++i) {
		if (strncmp(cmdS[i], "--", 2) == 0) {
			opt = &cmdS[i][2];
			if (strcmp(opt, "licence") == 0) {
				showLicence();
				if (cmdN == 2) {
					return 0;
				};
			};
			if (strcmp(opt, "execution-time") == 0) {
				executionTime=true;
			};
			continue;
		};
		if (!fileIn) {
			fileIn = cmdS[i];
		};
	};

	if(fileIn==NULL) {
		showUsage();
		return 0;
	};

	if(executionTime) {
		beginTimestampInMilliseconds=DateTime::timestampInMilliseconds();
	};

	if(ExecutiveX::initExecutive(cmdN,cmdS,initExecutive)) {
		if(ExecutiveX::executeFile(fileIn)) {
			ExecutiveX::executeEnd();
			if(executionTime) {
				endTimestampInMilliseconds=DateTime::timestampInMilliseconds();
				intervalTimestampInMilliseconds=endTimestampInMilliseconds-beginTimestampInMilliseconds;
				printf("Execution time: " XYO_FORMAT_SIZET " ms\n",(size_t)intervalTimestampInMilliseconds);
			};
			return 0;
		};
	};

	fflush(stdout);
	printf("%s\n",(ExecutiveX::getError()).value());
	printf("%s",(ExecutiveX::getStackTrace()).value());
	fflush(stdout);

	ExecutiveX::executeEnd();
	return -1;
};


XYO_XY_MAIN_STD(Application);

#ifdef QUANTUM_SCRIPT_AMALGAM
#include "quantum-script-amalgam.cpp"
#endif

