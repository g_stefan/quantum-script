//
// Quantum Script
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_LICENCE_HPP
#define QUANTUM_SCRIPT_LICENCE_HPP

namespace Quantum {
	namespace Script {

		class Licence {
			public:
				static const char *content();
				static const char *shortContent();
		};

	};
};

#endif
