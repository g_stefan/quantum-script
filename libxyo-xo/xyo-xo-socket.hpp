//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_SOCKET_HPP
#define XYO_XO_SOCKET_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

#ifndef XYO_XO_NET_HPP
#include "xyo-xo-net.hpp"
#endif

#ifndef XYO_XO_IREAD_HPP
#include "xyo-xo-iread.hpp"
#endif

#ifndef XYO_XO_IWRITE_HPP
#include "xyo-xo-iwrite.hpp"
#endif

#ifndef XYO_XO_IPADRESS4_HPP
#include "xyo-xo-ipaddress4.hpp"
#endif

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class Socket:
			public virtual IRead,
			public virtual IWrite {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Socket);
			protected:
#ifdef XYO_OS_TYPE_WIN
				void *this_;
#else
				int this_;
#endif
				Socket *linkNext_;
				Socket *linkBack_;
				IPAddress4 address;
				bool aquireOwner_;
			public:
				XYO_XO_EXPORT Socket();
				XYO_XO_EXPORT ~Socket();
				XYO_XO_EXPORT bool isValid();

				XYO_XO_EXPORT bool openClient(IPAddress4 &adr_);
				XYO_XO_EXPORT bool openServer(IPAddress4 &adr_);
				XYO_XO_EXPORT bool listen(word queue_);
				XYO_XO_EXPORT bool accept(Socket &socket_);
				XYO_XO_EXPORT void close();

				XYO_XO_EXPORT size_t read(void *output, size_t lungime);
				XYO_XO_EXPORT size_t write(void *input, size_t lungime);
				XYO_XO_EXPORT int waitToWrite(word seconds,word micro);
				XYO_XO_EXPORT int waitToRead(word seconds,word micro);

				XYO_XO_EXPORT bool openClientX(String adr_);
				XYO_XO_EXPORT bool openServerX(String adr_);

				XYO_XO_EXPORT void getIPAddress4(IPAddress4 &);

				XYO_XO_EXPORT void aquireOwner();
				XYO_XO_EXPORT bool becomeOwner(Socket &socket_);
				XYO_XO_EXPORT void releaseOwner();
		};

	};
};

#endif

