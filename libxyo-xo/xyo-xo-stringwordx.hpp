//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGWORDX_HPP
#define XYO_XO_STRINGWORDX_HPP

#ifndef XYO_XO_STRINGWORD_HPP
#include "xyo-xo-stringword.hpp"
#endif

namespace XYO {
	namespace XO {

		class StringWordX {
			public:
				XYO_XO_EXPORT static StringWord trimWithElement(StringWord, StringWord);
				XYO_XO_EXPORT static StringWord replace(StringWord, StringWord, StringWord);
				XYO_XO_EXPORT static StringWord nilAtFirstFromEnd(StringWord, StringWord);
				XYO_XO_EXPORT static StringWord substring(StringWord, size_t start, size_t length);
				XYO_XO_EXPORT static StringWord substring(StringWord, size_t start);

				inline  static bool indexOf(StringWord a, StringWord b, size_t start, size_t &index) {
					return StringBaseWord::indexOf(a, a.length(), b, b.length(), start, index);
				};

				inline static bool indexOfFromEnd(StringWord a, StringWord b, size_t start, size_t &index) {
					return StringBaseWord::indexOfFromEnd(a, a.length(), b, b.length(), start, index);
				};

				XYO_XO_EXPORT static StringWord toLowerCaseAscii(StringWord value);
				XYO_XO_EXPORT static StringWord toUpperCaseAscii(StringWord value);
				XYO_XO_EXPORT static bool matchAscii(StringWord text, StringWord sig);
				XYO_XO_EXPORT static bool split2FromBegin(StringWord text, StringWord sig,StringWord &firstPart,StringWord &secondPart);

		};


	};
};

#endif

