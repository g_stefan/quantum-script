//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_IPADDRESS4_HPP
#define XYO_XO_IPADDRESS4_HPP

#ifndef XYO_XO_IP4_HPP
#include "xyo-xo-ip4.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class IPAddress4:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(IPAddress4);
			public:
				IP4 ip;
				word port;

				XYO_XO_EXPORT IPAddress4();
				XYO_XO_EXPORT void copy(IPAddress4 &addr);
		};

	};
};

#endif

