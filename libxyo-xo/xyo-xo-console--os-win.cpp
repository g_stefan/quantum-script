//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <windows.h>
#include <direct.h>
#include <io.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

#include "xyo-xo-console.hpp"

namespace XYO {
	namespace XO {

		bool Console::keyHit() {
			return _kbhit();
		};

		char Console::getChar() {
			return _getch();
		};

	};
};

#endif

