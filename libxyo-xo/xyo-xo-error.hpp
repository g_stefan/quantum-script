//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_ERROR_HPP
#define XYO_XO_ERROR_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		class Error {
			protected:
				String message;
			public:
				XYO_XO_EXPORT Error();
				XYO_XO_EXPORT Error(const String message_);
				XYO_XO_EXPORT Error(const Error &e);
				XYO_XO_EXPORT Error(Error &&e);

				inline String getMessage() {
					return message;
				};

				inline static void memoryInit() {
					TMemory<String>::memoryInit();
				};
		};

	};
};

#endif
