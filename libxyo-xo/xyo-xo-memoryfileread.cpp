//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-memoryfileread.hpp"

namespace XYO {
	namespace XO {

		MemoryFileRead::MemoryFileRead() {
			data_ = NULL;
			size_ = 0;
			pos_ = 0;
		};

		MemoryFileRead::~MemoryFileRead() {
		};

		bool MemoryFileRead::isValid() {
			return (data_ != NULL);
		};

		bool MemoryFileRead::openRead(const byte *data, size_t size) {
			close();
			data_ = data;
			size_ = size;
			return true;
		};

		size_t MemoryFileRead::read(void *output, size_t lungime) {
			size_t npos;
			size_t nlen;
			npos = pos_ + lungime;
			if (npos > size_) {
				npos = size_;
			};
			nlen = npos - pos_;
			if (nlen == 0) {
				return 0;
			};
			memcpy(output, &data_[pos_], nlen);
			pos_ = npos;
			return nlen;
		};

		bool MemoryFileRead::seekFromBegin(long int cantitate) {
			pos_ = cantitate;
			if (pos_ < 0) {
				pos_ = 0;
				return false;
			} else if (pos_ > size_) {
				pos_ = size_;
				return false;
			};
			return true;
		};

		bool MemoryFileRead::seek(long int cantitate) {
			pos_ += cantitate;
			if (pos_ < 0) {
				pos_ = 0;
				return false;
			} else if (pos_ > size_) {
				pos_ = size_;
				return false;
			};
			return true;
		};

		bool MemoryFileRead::seekFromEnd(long int cantitate) {
			pos_ = size_ - cantitate;
			if (pos_ < 0) {
				pos_ = 0;
				return false;
			} else if (pos_ > size_) {
				pos_ = size_;
				return false;
			};
			return true;
		};

		long int MemoryFileRead::seekTell() {
			return pos_;
		};

		void MemoryFileRead::close() {
			data_ = NULL;
			pos_ = 0;
			size_ = 0;
		};

	};
};

