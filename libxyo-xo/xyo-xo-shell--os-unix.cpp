//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_UNIX

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <spawn.h>
#include <sys/wait.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cstdio>

#include "xyo-xo-shell.hpp"
#include "xyo-xo-string.hpp"

#define DT_DIR__ 0x04

namespace XYO {
	namespace XO {

		struct SShellFind_ {
			struct dirent *finddata;
			DIR *hfind;

			String match_;
		};

		ShellFind::ShellFind() {
			isFile = false;
			isDirectory = false;
			isReadOnly = false;
			isValid_ = false;

			shellFind_ = new SShellFind_();

			shellFind_->hfind = NULL;
			shellFind_->finddata = NULL;
		};

		ShellFind::~ShellFind() {
			close();
			delete shellFind_;
		};

		bool ShellFind::findNext() {

			shellFind_->finddata = readdir(shellFind_->hfind);
			if (shellFind_->finddata == NULL) {
				close();
				return false;
			} else {

				while (!StringX::matchAscii(shellFind_->finddata->d_name, shellFind_->match_.value())) {
					shellFind_->finddata = readdir(shellFind_->hfind);
					if (shellFind_->finddata != NULL) {
					} else {
						close();
						return false;
					};
				};


				isValid_ = true;
				name = shellFind_->finddata->d_name;
				isDirectory = (shellFind_->finddata->d_type == DT_DIR__);
				isFile = !isDirectory;

				struct stat attrib;
				stat(shellFind_->finddata->d_name, &attrib);

				isReadOnly = (attrib.st_mode & S_IWUSR == 0);
				return true;
			};


			close();
			return false;
		};

		void ShellFind::close() {
			if (shellFind_->hfind != NULL) {
				closedir(shellFind_->hfind);
				shellFind_->hfind = NULL;
			};
			isValid_ = false;
		};

		bool ShellFind::find(const char *name__) {
			String name_;
			String path_;
			size_t pos;

			if (StringBase::indexOfFromEnd(name__, "/", pos)) {
				if (pos == 0) {
					path_ = ".";
				} else {
					path_ = StringX::substring(name__, 0, pos);
				};
				name_ = StringX::substring(name__, pos + 1);
			} else {
				name_ = "*";
				path_ = ".";
			};

			shellFind_->match_ = name_;
			shellFind_->hfind = opendir(path_);
			if (shellFind_->hfind != NULL) {
				shellFind_->finddata = readdir(shellFind_->hfind);
				if (shellFind_->finddata == NULL) {
					return false;
				} else {

					while (!StringX::matchAscii(shellFind_->finddata->d_name, shellFind_->match_)) {
						shellFind_->finddata = readdir(shellFind_->hfind);
						if (shellFind_->finddata != NULL) {
						} else {
							return false;
						};
					};

					isValid_ = true;
					name = shellFind_->finddata->d_name;
					isDirectory = (shellFind_->finddata->d_type == DT_DIR__);
					isFile = !isDirectory;

					struct stat attrib;
					stat(shellFind_->finddata->d_name, &attrib);

					isReadOnly = (attrib.st_mode & S_IWUSR == 0);
					return true;
				};
			};
			return false;
		};

		bool Shell::chdir(const char *path) {
			return (::chdir(path) == 0);
		};

		bool Shell::rmdir(const char *path) {
			return (::rmdir(path) == 0);
		};

		bool Shell::mkdir(const char *path) {
			int retV;
			mode_t process_mask = umask(0);
			retV=::mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO);
			umask(process_mask);
			return (retV==0);
		};

		bool Shell::getcwd(char *buffer, size_t bufferSize) {
			return (::getcwd(buffer, bufferSize) != NULL);
		};

		bool Shell::copyFile(const char *source, const char *destination) {
			int readFd;
			int writeFd;
			struct stat statBuf;
			off_t offset;
			unsigned char buf[4096];
			size_t rd;
			size_t wd;
			FILE *fIn;
			FILE *fOut;
			bool retV;

			retV = false;
			offset = 0;
			readFd = open(source, O_RDONLY);
			if (readFd) {
				fstat(readFd, &statBuf);
				writeFd = open(destination, O_WRONLY | O_CREAT, statBuf.st_mode);
				if (writeFd) {
					fIn = fdopen(readFd, "rb");
					if (fIn != NULL) {
						fOut = fdopen(writeFd, "wb");
						if (fOut != NULL) {
							wd = 0;
							while (rd = fread(buf, 1, 4096, fIn)) {
								wd = fwrite(buf, 1, rd, fOut);
								if (rd < 1024 * 4) {
									break;
								};
							};
							if (wd == rd) {
								retV = true;
							};
							fclose(fOut);
						};
						fclose(fIn);
					};
					close(writeFd);
				};
				close(readFd);
			};
			return retV;
		};

		bool Shell::rename(const char *source, const char *destination) {
			return (::rename(source, destination) == 0);
		};

		bool Shell::remove(const char *file) {
			return (::remove(file) == 0);
		};

		int Shell::compareLastWriteTime(const char *fileA, const char *fileB) {
			struct stat attribA;
			struct stat attribB;

			stat(fileA, &attribA);
			stat(fileB, &attribB);
			return (attribA.st_mtime - attribB.st_mtime);
		};

		int Shell::system(const char *cmd) {
			return ::system(cmd);
		};

		bool Shell::touch(const char *file) {
			FILE *in;
			char ch;

			in = fopen(file, "rb+");
			if (in != NULL) {
				if (fread(&ch, 1, 1, in) == 1) {
					fseek(in, 0, SEEK_SET);
					fwrite(&ch, 1, 1, in);
				};
				fclose(in);
				return true;
			} else {
				in = fopen(file, "wb+");
				if (in != NULL) {
					fclose(in);
					return true;
				};
			};
			return false;
		};

		bool Shell::fileExists(const char *file) {
			struct stat status;
			if (access(file, 0) == 0) {
				stat(file, &status);
				if (status.st_mode & S_IFREG) {
					return true;
				};
			};
			return false;
		};

		bool Shell::directoryExists(const char *directory) {
			struct stat status;
			if (access(directory, 0) == 0) {
				stat(directory, &status);
				if (status.st_mode & S_IFDIR) {
					return true;
				};
			};
			return false;
		};

		char *Shell::getenv(const char *name) {
			return ::getenv(name);
		};

		bool Shell::putenv(const char *env) {
			return (::putenv((char *)env) == 0);
		};

		bool Shell::realpath(const char *fileNameIn,char *fileNameOut,long int filenameOutSize) {
			return (::realpath(fileNameIn,fileNameOut)!=NULL);
		};

		dword Shell::execute(const char *cmd) {
			return ::system(cmd);
		};

		dword Shell::executeHidden(const char *cmd) {
			return ::system(cmd);
		};

		static void shellArgsFilter__(char *cmdX) {
			char *result;
			char *scan;
			char *check;
			result=cmdX;
			scan=cmdX;
			while(*scan!='\0') {
				if(*scan=='\\') {
					check=scan+1;
					while(*check!='\0') {
						if(*check=='\\') {
							++check;
							continue;
						};
						break;
					};
					if(*check=='"') {

						while(*scan!='\0') {
							if(*scan=='\\') {
								++scan;
								*result=*scan;
								++result;
								++scan;
								continue;
							};
							break;
						};
						continue;
					};
					while(*scan!='\0') {
						if(*scan=='\\') {
							*result=*scan;
							++result;
							++scan;
							continue;
						};
						break;
					};
					continue;
				};
				if(*scan=='"') {
					++scan;
					continue;
				};
				*result=*scan;
				++result;
				++scan;
			};
			*result='\0';
		};

		static void shellArgsParse__(bool commit,const char *cmdLine,int &cmdN,char ** &cmdS,int startIndex) {
			const char *cmdLineScan;
			const char *cmdLastLineScan;
			int cmdSize;

			cmdN=startIndex;

			cmdLineScan=cmdLine;
			// ignore first spaces
			while(*cmdLineScan!='\0') {
				if(*cmdLineScan==' '||*cmdLineScan=='\t') {
					while(*cmdLineScan==' '||*cmdLineScan=='\t') {
						++cmdLineScan;
						if(*cmdLineScan=='\0') {
							break;
						};
					};
					if(*cmdLineScan=='\0') {
						break;
					};
					continue;
				};
				break;
			};
			//
			cmdLastLineScan=cmdLineScan;
			cmdSize=0;
			while(*cmdLineScan!='\0') {
				if(*cmdLineScan==' '||*cmdLineScan=='\t') {
					if(cmdSize>0) {
						if(commit) {
							cmdS[cmdN]=new char[cmdSize+1];
							memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
							cmdS[cmdN][cmdSize]='\0';
						};
						++cmdN;
					};
					while(*cmdLineScan==' '||*cmdLineScan=='\t') {
						++cmdLineScan;
						if(*cmdLineScan=='\0') {
							break;
						};
					};
					cmdLastLineScan=cmdLineScan;
					cmdSize=0;
					if(*cmdLineScan=='\0') {
						break;
					};
					continue;
				};
				if(*cmdLineScan=='\\') {
					++cmdSize;
					++cmdLineScan;
					if(*cmdLineScan!='\0') {
						++cmdSize;
						++cmdLineScan;
					};
					continue;
				};
				if(*cmdLineScan=='\"') {
					if(cmdSize==0) {
						cmdLastLineScan=cmdLineScan;
						++cmdSize;
						++cmdLineScan;
						while(*cmdLineScan!='\0') {
							if(*cmdLineScan=='\\') {
								++cmdSize;
								++cmdLineScan;
								if(*cmdLineScan!='\0') {
									++cmdSize;
									++cmdLineScan;
								};
								continue;
							};
							if(*cmdLineScan=='\"') {
								++cmdSize;
								++cmdLineScan;
								break;
							};
							++cmdSize;
							++cmdLineScan;
						};

						if(commit) {
							cmdS[cmdN]=new char[cmdSize+1];
							memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
							cmdS[cmdN][cmdSize]='\0';
						};
						++cmdN;

						cmdLastLineScan=cmdLineScan;
						cmdSize=0;
						continue;
					} else {
						++cmdSize;
						++cmdLineScan;
						while(*cmdLineScan!='\0') {
							if(*cmdLineScan=='\\') {
								++cmdSize;
								++cmdLineScan;
								if(*cmdLineScan!='\0') {
									++cmdSize;
									++cmdLineScan;
								};
								continue;
							};
							if(*cmdLineScan=='\"') {
								++cmdSize;
								++cmdLineScan;
								break;
							};
							++cmdSize;
							++cmdLineScan;
						};
						if(*cmdLineScan=='\0') {
							if(commit) {
								cmdS[cmdN]=new char[cmdSize+1];
								memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
								cmdS[cmdN][cmdSize]='\0';
							};
							++cmdN;

							cmdLastLineScan=cmdLineScan;
							cmdSize=0;

							break;
						};
						continue;
					};
				};
				++cmdSize;
				++cmdLineScan;
				if(*cmdLineScan=='\0') {
					if(commit) {
						cmdS[cmdN]=new char[cmdSize+1];
						memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
						cmdS[cmdN][cmdSize]='\0';
					};
					++cmdN;
					break;
				};
			};
		};


		static void shellArgsSet__(const char *cmdLine,int &cmdN,char ** &cmdS,int startIndex) {
			int k;
			shellArgsParse__(false,cmdLine,cmdN,cmdS,startIndex);

			cmdS=new char *[cmdN+1+startIndex];

			shellArgsParse__(true,cmdLine,cmdN,cmdS,startIndex);

			for(k=startIndex; k<cmdN; ++k) {
				shellArgsFilter__(cmdS[k]);
			};

			cmdS[cmdN]=NULL;
		};

		static void shellArgsDelete__(int cmdN,char **cmdS,int startIndex) {
			int k;
			for(k=startIndex; k<cmdN; ++k) {
				delete[] cmdS[k];
			};
			delete[] cmdS;
		};

		Shell::ProcessId Shell::executeNoWait(const char *cmd) {
			int cmdN;
			char **cmdS;
			pid_t pid;
			int status;

			shellArgsSet__(cmd,cmdN,cmdS,0);

			if(cmdN==0) {
				shellArgsDelete__(cmdN,cmdS,0);
				return 0;
			};

			status=posix_spawn(&pid, cmdS[0], NULL, NULL, cmdS, ::environ);
			shellArgsDelete__(cmdN,cmdS,0);

			if(status==0) {
				return (ProcessId)pid;
			};
			return 0;
		};

		Shell::ProcessId Shell::executeNoWait(const char *cmd,const char *cmdline) {
			int cmdN;
			char **cmdS;
			pid_t pid;
			int status;

			shellArgsSet__(cmd,cmdN,cmdS,1);
			cmdS[0]=(char *)cmd;
			status=posix_spawn(&pid, cmd, NULL, NULL, cmdS, ::environ);
			shellArgsDelete__(cmdN,cmdS,1);

			if(status==0) {
				return (ProcessId)pid;
			};

			return 0;
		};

		Shell::ProcessId Shell::executeHiddenNoWait(const char *cmd) {
			return executeNoWait(cmd);
		};

		bool Shell::isProcessTerminated(const Shell::ProcessId processId) {
			pid_t retV;
			int status;

			retV=waitpid(processId,&status,WNOHANG);
			if(retV==0) {
				return false;
			};

			return true;
		};

		bool Shell::terminateProcess(const Shell::ProcessId processId,const dword wait_) {
			int counter=1000;

			kill(processId, SIGTERM);
			while(counter>0) {
				if(isProcessTerminated(processId)) {
					return true;
				};
				usleep(wait_*100);
				counter-=100;
			};

			kill(processId, SIGKILL);
		};

	};
};


#endif

