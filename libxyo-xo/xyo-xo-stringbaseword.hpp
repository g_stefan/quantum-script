//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGBASEWORD_HPP
#define XYO_XO_STRINGBASEWORD_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringBaseWord {
			public:

				XYO_XO_EXPORT static word empty[];
				XYO_XO_EXPORT static size_t length(const word *o);
				XYO_XO_EXPORT static void copy(word *o, const word *v);
				XYO_XO_EXPORT static void copyN(word *o, const word *v, size_t k);
				XYO_XO_EXPORT static void copyNMemory(word *o, const word *v, size_t k);
				XYO_XO_EXPORT static int compare(const word *o, const word *v);
				XYO_XO_EXPORT static int compareN(const word *o, const word *v, size_t k);

				inline static bool isEqual(const word *o, const word *v) {
					return (compare(o,v)==0);
				};

				inline static bool isEqualN(const word *o, const word *v, size_t k) {
					return (compareN(o,v,k)==0);
				};

				inline static bool beginWith(const word *o, const word *v) {
					return (compareN(o, v, length(v)) == 0);
				};

				XYO_XO_EXPORT static void concatenate(word *o, const word *v);
				XYO_XO_EXPORT static void concatenateN(word *o, const word *v, size_t k);
				XYO_XO_EXPORT static bool indexOf(const word *o, size_t o_ln, const word *v, size_t v_ln, size_t pos, size_t &retV);
				XYO_XO_EXPORT static bool indexOfFromEnd(const word *o, size_t o_ln, const word *v, size_t v_ln, size_t pos, size_t &retV);

				inline static bool indexOf(const word *o, const word *v, size_t &retV) {
					return indexOf(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOf(const word *o, const word *v, size_t pos, size_t &retV) {
					return indexOf(o, length(o), v, length(v), pos, retV);
				};

				inline static bool indexOfFromEnd(const word *o, const word *v, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOfFromEnd(const word *o, const word *v, size_t pos, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), pos, retV);
				};

				XYO_XO_EXPORT static bool nilAt(word *o, const word *v);
				XYO_XO_EXPORT static bool nilAtFromEnd(word *o, const word *v);
				XYO_XO_EXPORT static bool skipTo(word *o, const word *v);
				XYO_XO_EXPORT static bool skipToFromEnd(word *o, const word *v);
				XYO_XO_EXPORT static bool hasOnlyElement(const word *o, const word *v);
				XYO_XO_EXPORT static const word *toNotInElement(const word *o, const word *v);
				XYO_XO_EXPORT static const word *toNotInFromEndElement(const word *o, const word *v);

				inline static word chrToLowerCaseAscii(word in) {
					if(in<0x41||in>0x5A) {
						return in;
					};
					return in+0x20;
				};

				inline static word chrToUpperCaseAscii(word in) {
					if(in<0x61||in>0x7A) {
						return in;
					};
					return in-0x20;
				};

				XYO_XO_EXPORT static int  compareIgnoreCaseAscii(const word *x, const word *y);
				XYO_XO_EXPORT static int  compareIgnoreCaseNAscii(const word *x, const word *y, size_t ln);
				XYO_XO_EXPORT static bool indexOfIgnoreCaseAscii(const word *the_string, size_t length, const word *patern, size_t patlength, size_t pos, size_t *result);
				XYO_XO_EXPORT static bool indexOfFromEndIgnoreCaseAscii(const word *the_string, size_t length, const word *patern, size_t patlength, size_t pos, size_t *result);

		};
	};
};

#endif


