//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-utfx.hpp"

namespace XYO {
	namespace XO {

		size_t UtfX::chrUtf32FromUtf8(utf32 *out, utf8 *in) {
			size_t in_sz;
			in_sz = Utf8X::memChrSize(in);
			if (in_sz == 0) {
				return 0;
			}
			if (!Utf8X::chrIsValid(in)) {
				return 0;
			}
			switch (in_sz) {
				case 1:
					*out = (utf32) * in;
					break;
				case 2:
					*out = (utf32) (*in & 0x1F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					break;
				case 3:
					*out = (utf32) (*in & 0x0F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					break;
				case 4:
					*out = (utf32) (*in & 0x07);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					break;
				case 5:
					*out = (utf32) (*in & 0x03);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					break;
				case 6:
					*out = (utf32) (*in & 0x01);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					*out <<= 6;
					++in;
					*out |= (utf32) (*in & 0x3F);
					break;
			};
			if (Utf32X::chrIsValid(out)) {
				return true;
			}
			return false;
		};

		size_t UtfX::memChrUtf8FromUtf32Size(utf32 in) {
			if (!Utf32X::chrIsValid(&in)) {
				return 0;
			}

			//ISO-10646-UTF-8
			if ((in & 0xFFFFFF80) == 0x00000000) {
				return 1;
			};
			if ((in & 0xFFFFF800) == 0x00000000) {
				return 2;
			};
			if ((in & 0xFFFF0000) == 0x00000000) {
				return 3;
			};
			if ((in & 0xFFE00000) == 0x00000000) { // <-- never here ...
				return 4;
			};
			if ((in & 0xFC000000) == 0x00000000) {
				return 5;
			};
			if ((in & 0x80000000) == 0x00000000) {
				return 6;
			};
			return 0;
		};

		size_t UtfX::chrUtf8FromUtf32(utf8 *out, utf32 in) {
			size_t sz;
			sz = memChrUtf8FromUtf32Size(in);
			if (sz == 0) {
				return 0;
			}
			switch (sz) {
				case 1:
					*out = (utf8) in;
					break;
				case 2:
					out += 1;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x1F | 0xC0);
					break;
				case 3:
					out += 2;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x0F | 0xE0);
					break;
				case 4:
					out += 3;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x07 | 0xF0);
					break;
				case 5:
					out += 4;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x03 | 0xF8);
					break;
				case 6:
					out += 5;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x3F | 0x80);
					--out;
					in >>= 6;
					*out = (utf8) (in & 0x01 | 0xFC);
					break;
			};
			return sz;
		};

		size_t UtfX::memChrUtf16FromUtf32Size(utf32 in) {
			//ISO-10646-UTF-16
			if (!Utf32X::chrIsValid(&in)) {
				return 0;
			}
			if (in <= 0x0000FFFF) {
				return 1;
			}
			return 2;
		};

		size_t UtfX::chrUtf16FromUtf32(utf16 *out, utf32 in) {
			size_t sz;
			sz = memChrUtf16FromUtf32Size(in);
			if (sz == 0) {
				return 0;
			}
			switch (sz) {
				case 1:
					*out = (utf16) in;
					break;
				case 2:
					in -= 0x00010000;
					++out;
					*out = (utf16) ((in % 0x0400) + 0xDC00);
					--out;
					*out = (utf16) ((in / 0x0400) + 0xD800);
					break;
			};
			return sz;
		};

		bool UtfX::chrUtf32FromUtf16(utf32 *out, utf16 *in) {
			size_t sz;
			sz = Utf16X::memChrSize(in);
			if (sz == 0) {
				return 0;
			}
			if (!Utf16X::chrIsValid(in)) {
				return false;
			}
			switch (sz) {
				case 1:
					*out = (utf32) * in;
					break;
				case 2:
					*out = (*in - 0xD800);
					*out *= 0x400;
					++in;
					*out += (*in - 0xDC00);
					*out += 0x0010000;
					break;
			};
			if (Utf32X::chrIsValid(out)) {
				return true;
			}
			return false;
		};

		size_t UtfX::memChrUtf8FromUtf16Size(utf16 *in) {
			if ((*in & 0xFC00) == 0xD800) {
				++in;
				if ((*in & 0xFF80) == 0x0000) {
					return 3;
				};
				if ((*in & 0xF800) == 0x0000) {
					return 4;
				};
				return 5;
			};

			if ((*in & 0xFC00) == 0xDC00) {
				return 0;
			}
			if (*in >= 0xFFFE) {
				return 0;
			}

			if ((*in & 0xFF80) == 0x0000) {
				return 1;
			};
			if ((*in & 0xF800) == 0x0000) {
				return 2;
			};
			return 3;
		};

		StringUtf8 UtfX::utf8FromUtf16X(StringUtf16 in_, StringUtf8 err_) {
			StringUtf8 retV;
			utf8 chr[8];
			utf16 *x;
			utf32 tmp;
			size_t sz;
			for (x = in_; *x;) {
				sz = Utf16X::memChrSize(x);
				if (sz) {
					if (chrUtf32FromUtf16(&tmp, x)) {
						chr[chrUtf8FromUtf32(chr, tmp)] = 0;
						retV += chr;
					} else {
						retV += err_;
					};

					x += sz;
				} else {
					retV += err_;
					++x;
				};
			};
			return retV;
		};

		StringUtf8 UtfX::utf8FromUtf32X(StringUtf32 in_, StringUtf8 err_) {
			StringUtf8 retV;
			utf8 chr[8];
			utf32 *x;
			size_t sz;
			for (x = in_; *x;) {
				sz = Utf32X::memChrSize(x);
				if (sz) {
					chr[chrUtf8FromUtf32(chr, *x)] = 0;
					retV += chr;

					x += sz;
				} else {
					retV += err_;
					++x;
				};
			};
			return retV;
		};

		StringUtf16 UtfX::utf16FromUtf8X(StringUtf8 in_, StringUtf16 err_) {
			StringUtf16 retV;
			utf16 chr[4];
			utf8 *x;
			utf32 tmp;
			size_t sz;
			for (x = in_; *x;) {
				sz = Utf8X::memChrSize(x);
				if (sz) {
					if (chrUtf32FromUtf8(&tmp, x)) {
						chr[chrUtf16FromUtf32(chr, tmp)] = 0;
						retV += chr;
					} else {
						retV += err_;
					};

					x += sz;
				} else {
					retV += err_;
					++x;
				};
			};
			return retV;
		};

		StringUtf16 UtfX::utf16FromUtf32X(StringUtf32 in_, StringUtf16 err_) {
			StringUtf16 retV;
			utf16 chr[4];
			utf32 *x;
			size_t sz;
			for (x = in_; *x;) {
				sz = Utf32X::memChrSize(x);
				if (sz) {
					chr[chrUtf16FromUtf32(chr, *x)] = 0;
					retV += chr;

					x += sz;
				} else {
					retV += err_;
					++x;
				};
			};
			return retV;
		};

		StringUtf32 UtfX::utf32FromUtf8X(StringUtf8 in_, StringUtf32 err_) {
			StringUtf32 retV;
			utf32 chr[2];
			utf8 *x;
			size_t sz;
			for (x = in_; *x;) {
				sz = Utf8X::memChrSize(x);
				if (sz) {
					chr[chrUtf32FromUtf8(chr, x)] = 0;
					retV += chr;

					x += sz;
				} else {
					retV += err_;
					++x;
				};
			};
			return retV;
		};

		StringUtf32 UtfX::utf32FromUtf16X(StringUtf16 in_, StringUtf32 err_) {
			StringUtf32 retV;
			utf32 chr[2];
			utf16 *x;
			size_t sz;
			for (x = in_; *x;) {
				sz = Utf16X::memChrSize(x);
				if (sz) {
					chr[chrUtf32FromUtf16(chr, x)] = 0;
					retV += chr;

					x += sz;
				} else {
					retV += err_;
					++x;
				};
			};
			return retV;
		};

		static utf8 utf8StrQuestionMarkValue[] = {'?', 0};
		static utf16 utf16StrQuestionMarkValue[] = {'?', 0};
		static utf32 utf32StrQuestionMarkValue[] = {'?', 0};


		StringUtf8 UtfX::utf8FromUtf16(StringUtf16 in_) {
			return utf8FromUtf16X(in_, utf8StrQuestionMarkValue);
		};

		StringUtf8 UtfX::utf8FromUtf32(StringUtf32 in_) {
			return utf8FromUtf32X(in_, utf8StrQuestionMarkValue);
		};

		StringUtf16 UtfX::utf16FromUtf8(StringUtf8 in_) {
			return utf16FromUtf8X(in_, utf16StrQuestionMarkValue);
		};

		StringUtf16 UtfX::utf16FromUtf32(StringUtf32 in_) {
			return utf16FromUtf32X(in_, utf16StrQuestionMarkValue);
		};

		StringUtf32 UtfX::utf32FromUtf8(StringUtf8 in_) {
			return utf32FromUtf8X(in_, utf32StrQuestionMarkValue);
		};

		StringUtf32 UtfX::utf32FromUtf16(StringUtf16 in_) {
			return utf32FromUtf16X(in_, utf32StrQuestionMarkValue);
		};


	};
};

