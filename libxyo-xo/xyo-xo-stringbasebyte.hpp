//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGBASEBYTE_HPP
#define XYO_XO_STRINGBASEBYTE_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		class StringBaseByte {
			public:

				XYO_XO_EXPORT static byte empty[];

				inline static size_t length(const byte *o) {
					return strlen((char *)o);
				};

				inline static void copy(byte *o, const byte *v) {
					strcpy((char *)o, (const char *)v);
				};

				inline static void copyN(byte *o, const byte *v, size_t k) {
					strncpy((char *)o, (const char *)v, k);
					o[k] = 0;
				};

				inline static void copyNMemory(byte *o, const byte *v, size_t k) {
					memcpy((char *)o, (const char *)v, k);
					o[k] = 0;
				};

				inline static int compare(const byte *o, const byte *v) {
					return strcmp((const char *)o, (const char *)v);
				};

				inline static int compareN(const byte *o, const byte *v, size_t k) {
					return strncmp((const char *)o, (const char *)v, k);
				};

				inline static bool isEqual(const byte *o, const byte *v) {
					return (strcmp((const char *)o, (const char *)v) == 0);
				};

				inline static bool isEqualN(const byte *o, const byte *v, size_t k) {
					return (strncmp((const char *)o, (const char *)v, k) == 0);
				};

				inline static bool beginWith(const byte *o, const byte *v) {
					return (strncmp((const char *)o, (const char *)v, strlen((const char *)v)) == 0);
				};

				inline static void concatenate(byte *o, const byte *v) {
					strcat((char *)o, (const char *)v);
				};

				inline static void concatenateN(byte *o, const byte *v, size_t k) {
					strncat((char *)o, (const char *)v, k);
				};

				XYO_XO_EXPORT static bool indexOf(const byte *o, size_t o_ln, const byte *v, size_t v_ln, size_t pos, size_t &retV);
				XYO_XO_EXPORT static bool indexOfFromEnd(const byte *o, size_t o_ln, const byte *v, size_t v_ln, size_t pos, size_t &retV);

				inline static bool indexOf(const byte *o, const byte *v, size_t &retV) {
					return indexOf(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOf(const byte *o, const byte *v, size_t pos, size_t &retV) {
					return indexOf(o, length(o), v, length(v), pos, retV);
				};

				inline static bool indexOfFromEnd(const byte *o, const byte *v, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOfFromEnd(const byte *o, const byte *v, size_t pos, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), pos, retV);
				};

				XYO_XO_EXPORT static bool nilAt(byte *o, const byte *v);
				XYO_XO_EXPORT static bool nilAtFromEnd(byte *o, const byte *v);
				XYO_XO_EXPORT static bool skipTo(byte *o, const byte *v);
				XYO_XO_EXPORT static bool skipToFromEnd(byte *o, const byte *v);
				XYO_XO_EXPORT static bool hasOnlyElement(const byte *o, const byte *v);
				XYO_XO_EXPORT static const byte *toNotInElement(const byte *o, const byte *v);
				XYO_XO_EXPORT static const byte *toNotInFromEndElement(const byte *o, const byte *v);

				inline static byte chrToLowerCaseAscii(byte in) {
					if(in<0x41||in>0x5A) {
						return in;
					};
					return in+0x20;
				};

				inline static byte chrToUpperCaseAscii(byte in) {
					if(in<0x61||in>0x7A) {
						return in;
					};
					return in-0x20;
				};

				XYO_XO_EXPORT static int  compareIgnoreCaseAscii(const byte *x, const byte *y);
				XYO_XO_EXPORT static int  compareIgnoreCaseNAscii(const byte *x, const byte *y, size_t ln);
				XYO_XO_EXPORT static bool indexOfIgnoreCaseAscii(const byte *the_string, size_t length, const byte *patern, size_t patlength, size_t pos, size_t *result);
				XYO_XO_EXPORT static bool indexOfFromEndIgnoreCaseAscii(const byte *the_string, size_t length, const byte *patern, size_t patlength, size_t pos, size_t *result);

				XYO_XO_EXPORT static void xorBuffer(byte *data, size_t dataLn, const byte *key, size_t keyLn);
				XYO_XO_EXPORT static void xorAvalancheBufferEncode(byte *data, size_t dataLn);
				XYO_XO_EXPORT static void xorAvalancheBufferDecode(byte *data, size_t dataLn);

		};


	};
};

#endif


