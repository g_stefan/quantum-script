//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringbasebyte.hpp"

namespace XYO {
	namespace XO {

		byte StringBaseByte::empty[]= {0};

		bool StringBaseByte::indexOf(const byte *o, size_t o_ln, const byte *v, size_t v_ln, size_t pos, size_t &retV) {
			if (pos > o_ln) {
				return false;
			}
			if (v_ln > o_ln) {
				return false;
			}
			if ((pos + v_ln) > o_ln) {
				return false;
			}
			for (retV = pos; retV <= o_ln - v_ln; ++retV) {
				if (isEqualN(&o[retV], v, v_ln)) {
					return true;
				}
			};
			return false;
		};

		bool StringBaseByte::indexOfFromEnd(const byte *o, size_t o_ln, const byte *v, size_t v_ln, size_t pos, size_t &retV) {
			if (pos > o_ln) {
				return false;
			}
			if (v_ln > o_ln) {
				return false;
			}
			if ((pos + v_ln) > o_ln) {
				return false;
			}
			for (retV = o_ln - pos - v_ln + 1; retV; --retV) {
				if (isEqualN(&o[retV - 1], v, v_ln)) {
					--retV;
					return true;
				};
			};
			return false;
		};

		bool StringBaseByte::nilAt(byte *o, const byte *v) {
			size_t idx;
			if (indexOf(o, v, idx)) {
				o[idx] = 0;
				return true;
			};
			return false;
		};

		bool StringBaseByte::nilAtFromEnd(byte *o, const byte *v) {
			size_t idx;
			if (indexOfFromEnd(o, v, idx)) {
				o[idx] = 0;
				return true;
			};
			return false;
		};

		bool StringBaseByte::skipTo(byte *o, const byte *v) {
			size_t idx;
			if (indexOf(o, v, idx)) {
				copy(o, &o[idx + length(v)]);
				return true;
			};
			*o = 0;
			return false;
		};

		bool StringBaseByte::skipToFromEnd(byte *o, const byte *v) {
			size_t idx;
			if (indexOfFromEnd(o, v, idx)) {
				copy(o, &o[idx + length(v)]);
				return true;
			};
			*o = 0;
			return false;
		};

		bool StringBaseByte::hasOnlyElement(const byte *o, const byte *v) {
			const byte *m;
			while (*o) {
				for (m = v; *m; ++m) {
					if (*o != *m) {
						return false;
					}
				};
				++o;
			};
			return true;
		};

		const byte *StringBaseByte::toNotInElement(const byte *o, const byte *v) {
			const byte *m;
			bool found;
			while (*o) {
				found = false;
				for (m = v; *m; ++m) {
					if (*o == *m) {
						found = true;
					}
				};
				if (!found) {
					return o;
				}
				++o;
			};
			return o;
		};

		const byte *StringBaseByte::toNotInFromEndElement(const byte *o, const byte *v) {
			const byte *m;
			const byte *xo;
			const byte *xo2;
			size_t k;
			bool found;
			k = length(o);
			xo = o + k;
			xo2 = xo;
			--xo;
			while (k > 0) {
				found = false;
				for (m = v; *m; ++m) {
					if (*xo == *m) {
						found = true;
					}
				};
				if (!found) {
					return xo;
				}
				--xo;
				--k;
			};
			return xo2;
		};

		int StringBaseByte::compareIgnoreCaseAscii(const byte *x, const byte *y) {
			byte chrx;
			byte chry;

			for (;;) {

				if ((*x) == 0) {
					if ((*y) == 0) {
						return 0;
					};
					return -1;
				}
				if ((*y) == 0) {
					return 1;
				}

				chrx = chrToLowerCaseAscii(*x);
				chry = chrToLowerCaseAscii(*y);

				if(chrx!=chry) {
					return chrx - chry;
				};

				++x;
				++y;
			};

		};

		int StringBaseByte::compareIgnoreCaseNAscii(const byte *x, const byte *y, size_t ln) {
			byte chrx;
			byte chry;

			for (;;) {
				if (ln == 0) {
					return 0;
				}
				if ((*x) == 0) {
					if ((*y) == 0) {
						return 0;
					};
					return -1;
				}
				if ((*y) == 0) {
					return 1;
				}

				chrx = chrToLowerCaseAscii(*x);
				chry = chrToLowerCaseAscii(*y);

				if(chrx!=chry) {
					return chrx - chry;
				};

				++x;
				++y;
				--ln;
			};

		};

		bool StringBaseByte::indexOfIgnoreCaseAscii(const byte *the_string, size_t length, const byte *patern, size_t patlength, size_t pos, size_t *result) {
			if (pos > length) {
				return false;
			}
			if (patlength > length) {
				return false;
			}
			for (*result = pos; *result <= length - patlength; *result = *result + 1) {
				if (compareIgnoreCaseNAscii(&the_string[*result], patern, patlength) == 0) {
					return true;
				}
			};
			return true;
		};

		bool StringBaseByte::indexOfFromEndIgnoreCaseAscii(const byte *the_string, size_t length, const byte *patern, size_t patlength, size_t pos, size_t *result) {
			if (pos > length) {
				return false;
			}
			if (pos < patlength) {
				pos = patlength;
			}
			for (*result = length - pos; *result >= 0; *result = *result - 1) {
				if (compareIgnoreCaseNAscii(&the_string[*result], patern, patlength) == 0) {
					return true;
				}
			};
			return false;
		};

		void StringBaseByte::xorBuffer(byte *data, size_t dataLn, const byte *key, size_t keyLn) {
			if(dataLn==0 || keyLn==0) {
				return;
			};
			size_t index=0;
			while(dataLn>0) {
				*data^=key[index];
				++index;
				if(index==keyLn) {
					index=0;
				};
				--dataLn;
				++data;
			};
		};

		void StringBaseByte::xorAvalancheBufferEncode(byte *data, size_t dataLn) {
			if(dataLn==0) {
				return;
			};
			while(dataLn>0) {
				if(dataLn-1>0) {
					*(data+1)^=*data;
				};
				--dataLn;
				++data;
			};
		};

		void StringBaseByte::xorAvalancheBufferDecode(byte *data, size_t dataLn) {
			if(dataLn==0) {
				return;
			};
			byte lastByte=*data;
			byte nextByte;
			while(dataLn>0) {
				if(dataLn-1>0) {
					nextByte=*(data+1);
					*(data+1)^=lastByte;
					lastByte=nextByte;
				};
				--dataLn;
				++data;
			};
		};


	};
};


