//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "xyo-xo-randommt.hpp"

//
// http://en.wikipedia.org/wiki/Mersenne_twister
//

namespace XYO {
	namespace XO {

		RandomMT::RandomMT() {
			seed(time(NULL));
		};

		void RandomMT::seed(dword seed_) {
			int i;
			if(seed_==0) {
				seed_=time(NULL);
			};
			mt[0]=seed_;
			for (i = 1; i < 623; ++i) {
				mt[i] = (1812433253UL * (mt[i-1] ^ (mt[i-1] >> 30)) + i);
			};
			index = 0;
			value = seed_;
		};

		dword RandomMT::nextRandom() {
			dword y;
			if(index==0) {
				int i;
				for (i = 1; i < 623; ++i) {
					y = (mt[i] & 0x80000000UL) + (mt[(i+1) % 624] & 0x7fffffffUL);
					mt[i] = mt[(i + 397) % 624] ^ (y >> 1);
					if (y & 0x00000001UL) {
						mt[i] = mt[i] ^ (2567483615UL);
					};
				};
			};

			y = mt[index];
			y = y ^ (y>>11);
			y = y ^ ((y<<7) & 2636928640UL);
			y = y ^ ((y<<15) & 4022730752UL);
			y = y ^ (y>>18);

			index = (index + 1) % 624;
			value=y;
			return y;
		};

		dword RandomMT::getValue() {
			return value;
		};

		void RandomMT::copy(RandomMT &value_) {
			index=value_.index;
			value=value_.value;
			memcpy(mt,value_.mt,624*sizeof(dword));
		};

	};
};

