//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_SHELLX_HPP
#define XYO_XO_SHELLX_HPP

#ifndef XYO_XO_SHELL_HPP
#include "xyo-xo-shell.hpp"
#endif

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

#ifndef XYO_XO_STRINGWORD_HPP
#include "xyo-xo-stringword.hpp"
#endif

#ifndef XYO_XO_STRINGDWORD_HPP
#include "xyo-xo-stringdword.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class ShellX {
			public:

				XYO_XO_EXPORT static String getFileName(String fileName);
				XYO_XO_EXPORT static String getFileExtension(String fileName);
				XYO_XO_EXPORT static String getFileBasename(String fileName);
				XYO_XO_EXPORT static String getFilePath(String fileName);
				XYO_XO_EXPORT static String getFilePathX(String fileName);
				XYO_XO_EXPORT static bool removeEmptyDir(String dirName);
				XYO_XO_EXPORT static bool removeFile(String file);
				XYO_XO_EXPORT static bool removeFileAndDirectoryIfEmpty(String target);
				XYO_XO_EXPORT static bool touchIfExists(String file);
				XYO_XO_EXPORT static bool isEnv(String name, String value);
				XYO_XO_EXPORT static bool fileGetContents(String fileName,String &output);
				XYO_XO_EXPORT static bool fileGetContents(String fileName,StringWord &output);
				XYO_XO_EXPORT static bool fileGetContents(String fileName,StringDWord &output);
				XYO_XO_EXPORT static bool filePutContents(String fileName,String value);
				XYO_XO_EXPORT static bool filePutContents(String fileName,StringWord value);
				XYO_XO_EXPORT static bool filePutContents(String fileName,StringDWord value);
				XYO_XO_EXPORT static String getEnv(String name);
				XYO_XO_EXPORT static String getCwd();

				inline static void memoryInit() {
					TMemory<String>::memoryInit();
					TMemory<StringWord>::memoryInit();
					TMemory<StringDWord>::memoryInit();
					TMemory<Shell>::memoryInit();
				};
		};

	};
};

#endif

