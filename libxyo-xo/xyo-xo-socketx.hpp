//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_SOCKETX_HPP
#define XYO_XO_SOCKETX_HPP

#ifndef XYO_XO_SOCKET_HPP
#include "xyo-xo-socket.hpp"
#endif

#ifndef XYO_XO_BUFFER_HPP
#include "xyo-xo-buffer.hpp"
#endif

namespace XYO {
	namespace XO {

		class SocketX {
			public:
				XYO_XO_EXPORT bool read(Socket &file,String &out,size_t size);
				XYO_XO_EXPORT bool readLn(Socket &file,String &out,size_t size);
				XYO_XO_EXPORT size_t write(Socket &file,String data);
				XYO_XO_EXPORT size_t writeLn(Socket &file,String data);
				XYO_XO_EXPORT size_t readToBuffer(Socket &file,Buffer &buffer,size_t ln);
				XYO_XO_EXPORT size_t writeFromBuffer(Socket &file,Buffer &buffer);
		};

	};
};

#endif
