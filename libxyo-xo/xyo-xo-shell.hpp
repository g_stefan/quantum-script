//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_SHELL_HPP
#define XYO_XO_SHELL_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		typedef struct SShellFind_ ShellFind_;

		class ShellFind :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ShellFind);
			protected:
				ShellFind_ *shellFind_;
				bool isValid_;
			public:
				XYO_XO_EXPORT ShellFind();
				XYO_XO_EXPORT ~ShellFind();

				char *name;
				bool isFile;
				bool isDirectory;
				bool isReadOnly;

				inline bool isValid() {
					return isValid_;
				};

				XYO_XO_EXPORT bool find(const char *name_);
				XYO_XO_EXPORT bool findNext();
				XYO_XO_EXPORT void close();
		};

		class Shell {
			public:

				typedef dword ProcessId;

				XYO_XO_EXPORT static bool chdir(const char *path);
				XYO_XO_EXPORT static bool rmdir(const char *path);
				XYO_XO_EXPORT static bool mkdir(const char *path);
				XYO_XO_EXPORT static bool getcwd(char *buffer, size_t bufferSize);
				XYO_XO_EXPORT static bool copyFile(const char *source, const char *destination);
				XYO_XO_EXPORT static bool rename(const char *source, const char *destination);
				XYO_XO_EXPORT static bool remove(const char *file);
				XYO_XO_EXPORT static int compareLastWriteTime(const char *fileA, const char *fileB);
				XYO_XO_EXPORT static int system(const char *cmd);
				XYO_XO_EXPORT static bool touch(const char *file);
				XYO_XO_EXPORT static bool fileExists(const char *file);
				XYO_XO_EXPORT static bool directoryExists(const char *directory);
				XYO_XO_EXPORT static char *getenv(const char *name);
				XYO_XO_EXPORT static bool putenv(const char *env);
				XYO_XO_EXPORT static bool realpath(const char *fileNameIn,char *fileNameOut,long int fileNameOutSize);

				XYO_XO_EXPORT static dword execute(const char *cmd);
				XYO_XO_EXPORT static dword executeHidden(const char *cmd);
				XYO_XO_EXPORT static ProcessId executeNoWait(const char *cmd);
				XYO_XO_EXPORT static ProcessId executeNoWait(const char *cmd,const char *cmdline);
				XYO_XO_EXPORT static ProcessId executeHiddenNoWait(const char *cmd);
				XYO_XO_EXPORT static bool isProcessTerminated(const ProcessId processId);
				XYO_XO_EXPORT static bool terminateProcess(const ProcessId processId,const dword wait_);

#ifdef XYO_OS_TYPE_WIN
				XYO_XO_EXPORT static dword executeWriteOutputToFile(const char *cmd,const char *out);
#endif

				inline static void memoryInit() {
					TMemory<ShellFind>::memoryInit();
				};
		};

	};
};

#endif

