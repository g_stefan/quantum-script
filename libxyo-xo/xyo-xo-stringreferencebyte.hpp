//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGREFERENCEBYTE_HPP
#define XYO_XO_STRINGREFERENCEBYTE_HPP

#ifndef XYO_XO_STRINGBASEBYTE_HPP
#include "xyo-xo-stringbasebyte.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringReferenceByte;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::StringReferenceByte>:
			public TMemoryObjectPoolActive<XO::StringReferenceByte> {};

	};
};

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class StringReferenceByte:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(StringReferenceByte);
			protected:
				byte *value_;
				size_t length_;
				size_t size_;
				size_t chunk_;
				bool mode_;

				XYO_XO_EXPORT void memoryDelete_();
				XYO_XO_EXPORT void memoryNew_(size_t size);
				XYO_XO_EXPORT void memoryResize_(size_t size);
			public:

				static const size_t defaultChunk_ = 32;

				XYO_XO_EXPORT StringReferenceByte();
				XYO_XO_EXPORT ~StringReferenceByte();
				XYO_XO_EXPORT byte *value();
				XYO_XO_EXPORT size_t length();
				XYO_XO_EXPORT size_t getSize();
				XYO_XO_EXPORT size_t getChunk();
				XYO_XO_EXPORT void setChunk(size_t x);
				XYO_XO_EXPORT void setLength(size_t x);
				XYO_XO_EXPORT void updateLength();
				XYO_XO_EXPORT static TPointer<StringReferenceByte> init();
				XYO_XO_EXPORT static TPointer<StringReferenceByte> init2(size_t initialSize, size_t chunk);
				XYO_XO_EXPORT static TPointer<StringReferenceByte> from(const byte *o);
				XYO_XO_EXPORT static TPointer<StringReferenceByte> concatenate(StringReferenceByte *o, const byte *x);
				XYO_XO_EXPORT static TPointer<StringReferenceByte> concatenate2(StringReferenceByte *o, StringReferenceByte *x);
				XYO_XO_EXPORT static TPointer<StringReferenceByte> concatenate3(StringReferenceByte *o, const byte *x, size_t x_ln);
				XYO_XO_EXPORT static TPointer<StringReferenceByte> from2(const byte *o, size_t o_ln);
				XYO_XO_EXPORT static TPointer<StringReferenceByte> concatenate_one(StringReferenceByte *o, const byte x);
				XYO_XO_EXPORT void activeDestructor();
				XYO_XO_EXPORT static void memoryInit();
		};

	};
};

#endif

