//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <string.h>

#include "xyo-xo-utf16x.hpp"

namespace XYO {
	namespace XO {

		size_t Utf16X::memChrSize(utf16 *x) {
			if ((*x & 0xFC00) == 0xD800) {
				return 2;
			}
			if ((*x & 0xFC00) == 0xDC00) {
				return 0;
			}
			if (*x >= 0xFFFE) {
				return 0;
			}
			return 1;
		};

		bool Utf16X::chrIsValid(utf16 *x) {
			size_t sz;
			sz = memChrSize(x);
			if (sz == 0) {
				return false;
			}
			if (sz == 1) {
				return true;
			}
			++x;
			if ((*x & 0xFC00) == 0xDC00) {
				return true;
			}
			return false;
		};

	};
};

