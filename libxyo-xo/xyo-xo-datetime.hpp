//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_DATETIME_HPP
#define XYO_XO_DATETIME_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class DateTime :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(DateTime);
			protected:
				void *value_;
			public:
				XYO_XO_EXPORT DateTime();
				XYO_XO_EXPORT ~DateTime();

				XYO_XO_EXPORT word getYear();
				XYO_XO_EXPORT word getMonth();
				XYO_XO_EXPORT word getDay();
				XYO_XO_EXPORT word getDayOfWeek();
				XYO_XO_EXPORT word getHour();
				XYO_XO_EXPORT word getMinute();
				XYO_XO_EXPORT word getSecond();
				XYO_XO_EXPORT word getMilliseconds();
				XYO_XO_EXPORT void setLocalTime();
				XYO_XO_EXPORT void setYear(word value);
				XYO_XO_EXPORT void setMonth(word value);
				XYO_XO_EXPORT void setDay(word value);
				XYO_XO_EXPORT void setDayOfWeek(word value);
				XYO_XO_EXPORT void setHour(word value);
				XYO_XO_EXPORT void setMinute(word value);
				XYO_XO_EXPORT void setSecond(word value);
				XYO_XO_EXPORT void setMilliseconds(word value);
				XYO_XO_EXPORT void copy(DateTime &in);
				XYO_XO_EXPORT void plus(DateTime &in);
				XYO_XO_EXPORT void minus(DateTime &in);
				XYO_XO_EXPORT int compare(DateTime &in);
				XYO_XO_EXPORT qword toUnixTime();
				XYO_XO_EXPORT void fromUnixTime(qword);
				static XYO_XO_EXPORT qword timestampInMilliseconds();
		};

	};
};

#endif

