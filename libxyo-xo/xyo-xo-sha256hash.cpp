//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-sha256hash.hpp"
#include "xyo-xo-bytex.hpp"

//
// http://en.wikipedia.org/wiki/SHA-2
//

namespace XYO {
	namespace XO {

		dword SHA256Hash::k_[]= {
			0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
			0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
			0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
			0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
			0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
			0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
			0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
			0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
		};

		SHA256Hash::SHA256Hash() {
			processInit();
		};

		void SHA256Hash::processInit() {
			h0 = 0x6a09e667;
			h1 = 0xbb67ae85;
			h2 = 0x3c6ef372;
			h3 = 0xa54ff53a;
			h4 = 0x510e527f;
			h5 = 0x9b05688c;
			h6 = 0x1f83d9ab;
			h7 = 0x5be0cd19;

			length=0;
			stateM=0;
			lastData[0]=0;
			lastData[1]=0;
			lastData[2]=0;
			lastData[3]=0;
		};

		void SHA256Hash::hashBlock(dword *w_) {
			int k;
			dword s0,s1,temp1,temp2;
			dword maj,ch;
			dword a,b,c,d,e,f,g,h;
			dword w[64];

			for(k=0; k<16; ++k) {
				w[k]=w_[k];
			};

			for(k=16; k<64; ++k) {
				s0=ByteX::dwordRightRotate(w[k-15],7) ^ ByteX::dwordRightRotate(w[k-15], 18) ^  (w[k-15] >> 3 );
				s1=ByteX::dwordRightRotate(w[k-2],17) ^ ByteX::dwordRightRotate(w[k-2], 19) ^  (w[k-2] >> 10 );
				w[k]=w[k-16]+s0+w[k-7]+s1;
			};

			a=h0;
			b=h1;
			c=h2;
			d=h3;
			e=h4;
			f=h5;
			g=h6;
			h=h7;

			for(k=0; k<64; ++k) {

				s1=ByteX::dwordRightRotate(e,6) ^ ByteX::dwordRightRotate(e,11) ^ ByteX::dwordRightRotate(e,25);
				ch=(e & f) ^ ((~e) & g);
				temp1=h+s1+ch+k_[k]+w[k];
				s0=ByteX::dwordRightRotate(a,2) ^ ByteX::dwordRightRotate(a,13) ^ ByteX::dwordRightRotate(a,22);
				maj = (a & b) ^ (a & c) ^ (b & c);
				temp2=s0+maj;
				h=g;
				g=f;
				f=e;
				e=d+temp1;
				d=c;
				c=b;
				b=a;
				a=temp1+temp2;

			};

			h0+=a;
			h1+=b;
			h2+=c;
			h3+=d;
			h4+=e;
			h5+=f;
			h6+=g;
			h7+=h;
		};



		void SHA256Hash::processBytes(const byte *toHash,size_t length_) {
			size_t k,z;
			size_t m=length%4;
			size_t length__=length_&(~((size_t)(0x03)));

			length+=length_;

			if(m) {

				for(k=m,z=0; z<length_;) {
					lastData[k]=*toHash;
					++toHash;
					++z;
					++k;
					if(k==4) {
						break;
					};
				};
				if(k!=4) {
					return;
				};
				length_-=z;
				process[stateM]=ByteX::dwordFromByteReversed(lastData);

				++stateM;
				if(stateM==16) {
					hashBlock(process);
					stateM=0;
				};
			};


			for(k=0; k<length__; k+=4) {
				process[stateM]=ByteX::dwordFromByteReversed(toHash);
				toHash+=4;
				++stateM;
				if(stateM==16) {
					hashBlock(process);
					stateM=0;
				};
			};

			m=length_%4;
			if(m) {
				for(k=0; k<m; ++k) {
					lastData[k]=*toHash;
					++toHash;
				};
			};
		};

		void SHA256Hash::processDone() {
			size_t m=length%64;
			qword finalLength=length*8;
			byte data[8];
			if(m<56) {
				data[0]=0x80;
				processBytes(data,1);
				++m;
				data[0]=0;
				for(; m<56; ++m) {
					processBytes(data,1);
				};
				ByteX::qwordToByteReversed(finalLength,data);
				processBytes(data,8);
				return;
			};
			if(m<63) {
				data[0]=0x80;
				processBytes(data,1);
				++m;
				data[0]=0;
				for(; m<64+56; ++m) {
					processBytes(data,1);
				};
				ByteX::qwordToByteReversed(finalLength,data);
				processBytes(data,8);
				return;
			};
			data[0]=0x80;
			processBytes(data,1);
			++m;
			data[0]=0;
			for(; m<56; ++m) {
				processBytes(data,1);
			};
			ByteX::qwordToByteReversed(finalLength,data);
			processBytes(data,8);
		};


		String SHA256Hash::getHashAsHex() {
			String retV(128, 0);
			byte result[32];

			toBytes(result);

			sprintf((char *) retV.value(),
				"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"
				"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				result[0], result[1], result[2], result[3],
				result[4], result[5], result[6], result[7],
				result[8], result[9], result[10], result[11],
				result[12], result[13], result[14], result[15],
				result[16], result[17], result[18], result[19],
				result[20], result[21], result[22], result[23],
				result[24], result[25], result[26], result[27],
				result[28], result[29], result[30], result[31]
			       );

			retV.setLength(64);
			return retV;
		};

		String SHA256Hash::getHashString(String toHash) {
			SHA256Hash hash;
			hash.processBytes((byte *)toHash.value(), toHash.length());
			hash.processDone();
			return hash.getHashAsHex();
		};

		void SHA256Hash::hashStringToBytes(String toHash,byte *buffer) {
			SHA256Hash hash;
			hash.processBytes((byte *)toHash.value(), toHash.length());
			hash.processDone();
			hash.toBytes(buffer);
		};

		void SHA256Hash::toBytes(byte *buffer) {
			ByteX::dwordToByteReversed(h0, buffer);
			ByteX::dwordToByteReversed(h1, buffer + 4);
			ByteX::dwordToByteReversed(h2, buffer + 8);
			ByteX::dwordToByteReversed(h3, buffer + 12);
			ByteX::dwordToByteReversed(h4, buffer + 16);
			ByteX::dwordToByteReversed(h5, buffer + 20);
			ByteX::dwordToByteReversed(h6, buffer + 24);
			ByteX::dwordToByteReversed(h7, buffer + 28);
		};

	};
};

