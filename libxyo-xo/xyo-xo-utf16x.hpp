//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_UTF16X_HPP
#define XYO_XO_UTF16X_HPP

#ifndef XYO_XO_STRINGWORD_HPP
#include "xyo-xo-stringword.hpp"
#endif

namespace XYO {
	namespace XO {

		typedef word utf16;
		typedef StringWord StringUtf16;

		class Utf16X {
			public:

				XYO_XO_EXPORT static size_t memChrSize(utf16 *x);
				XYO_XO_EXPORT static bool chrIsValid(utf16 *x);

		};

	};
};

#endif

