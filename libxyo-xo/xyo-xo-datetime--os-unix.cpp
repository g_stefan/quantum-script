//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_UNIX

#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "xyo-xo-datetime.hpp"

namespace XYO {
	namespace XO {

		DateTime::DateTime() {
			time_t valuex;
			value_ = new struct tm;
			valuex=time(NULL);
			localtime_r(&valuex,(struct tm *)value_);
		};

		DateTime::~DateTime() {
			delete ((struct tm *)value_);
		};

		word DateTime::getYear() {
			return ((struct tm *)value_)->tm_year+1900;
		};

		word DateTime::getMonth() {
			return ((struct tm *)value_)->tm_mon+1;
		};

		word DateTime::getDay() {
			return ((struct tm *)value_)->tm_mday;
		};

		word DateTime::getDayOfWeek() {
			return ((struct tm *)value_)->tm_wday;
		};

		word DateTime::getHour() {
			return ((struct tm *)value_)->tm_hour;
		};

		word DateTime::getMinute() {
			return ((struct tm *)value_)->tm_min;
		};

		word DateTime::getSecond() {
			return ((struct tm *)value_)->tm_sec;
		};

		word DateTime::getMilliseconds() {
			return 0;
		};

		void DateTime::setLocalTime() {
		};

		void DateTime::setYear(word value) {
			((struct tm *)value_)->tm_year=value;
		};

		void DateTime::setMonth(word value) {
			((struct tm *)value_)->tm_mon=value;
		};

		void DateTime::setDay(word value) {
			((struct tm *)value_)->tm_mday=value;
		};

		void DateTime::setDayOfWeek(word value) {
			((struct tm *)value_)->tm_wday=value;
		};

		void DateTime::setHour(word value) {
			((struct tm *)value_)->tm_hour=value;
		};

		void DateTime::setMinute(word value) {
			((struct tm *)value_)->tm_min=value;
		};

		void DateTime::setSecond(word value) {
			((struct tm *)value_)->tm_sec=value;
		};

		void DateTime::setMilliseconds(word value) {
		};

		void DateTime::copy(DateTime &in) {
			memcpy(value_,in.value_,sizeof(struct tm));
		};

		void DateTime::plus(DateTime &in) {
			time_t a;
			time_t b;
			a=mktime((struct tm *)value_);
			b=mktime((struct tm *)in.value_);
			a+=b;
			localtime_r(&a,(struct tm *)value_);
		};

		void DateTime::minus(DateTime &in) {
			time_t a;
			time_t b;
			a=mktime((struct tm *)value_);
			b=mktime((struct tm *)in.value_);
			a-=b;
			localtime_r(&a,(struct tm *)value_);
		};

		int DateTime::compare(DateTime &in) {
			time_t a;
			time_t b;
			a=mktime((struct tm *)value_);
			b=mktime((struct tm *)in.value_);
			if(a<b) {
				return -1;
			};
			if(a==b) {
				return 0;
			};
			return 1;
		};

		qword DateTime::toUnixTime() {
			return (qword)mktime((struct tm *)value_);
		};

		void DateTime::fromUnixTime(qword t) {
			time_t x=(time_t)t;
			localtime_r(&x,(struct tm *)value_);
		};

		qword DateTime::timestampInMilliseconds() {
			struct timeval tmp;
			gettimeofday(&tmp, NULL);
			return ((qword)tmp.tv_sec)*((qword)1000)+(((qword)tmp.tv_usec)/((qword)1000));
		};

	};
};


#endif
