//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "xyo-xo-url.hpp"
#include "xyo-xo-string.hpp"

namespace XYO {
	namespace XO {

		using namespace XYO::XY;


		String URL::decodeComponent(String value) {
			String out;
			size_t k;
			unsigned int valueX;
			char buf[3];
			buf[2]=0;
			for(k=0; k<value.length(); ++k) {
				if(value[k]=='%') {
					if(k+1<value.length()) {
						++k;
						buf[0]=value[k];
						if(k+1<value.length()) {
							++k;
							buf[1]=value[k];
							if(sscanf(buf,"%02X",&valueX)==1) {
								out<<(char)valueX;
							};
						};
					};
					continue;
				};
				out<<value[k];
			};
			return out;
		};

		String URL::encodeComponent(String value) {
			String out;
			size_t k;
			char buf[4];
			for(k=0; k<value.length(); ++k) {
				if(
					(value[k]>='A'&&value[k]<='Z')
					||(value[k]>='a'&&value[k]<='z')
					||(value[k]>='0'&&value[k]<='9')
					||(value[k]=='_')
					||(value[k]=='.')
					||(value[k]=='!')
					||(value[k]=='~')
					||(value[k]=='*')
					||(value[k]=='\'')
					||(value[k]=='(')
					||(value[k]==')')
				) {
					out<<value[k];
					continue;
				};
				out<<'%';
				sprintf(buf,"%02X",(unsigned char)value[k]);
				out<<buf;
			};
			return out;
		};

		bool URL::getSchemeName(String url,String &out) {
			size_t index;
			if(StringBase::indexOf(url,"://",index)) {
				out=StringX::substring(url,0,index);
				return true;
			};
			return false;
		};

		bool URL::getHostNameAndPort(String url,String &out) {
			size_t index;
			size_t part;
			String firstPart;
			String secondPart;
			if(StringBase::indexOf(url,"://",index)) {
				if(StringBase::indexOf(url,"/",index+3,part)) {
					if(StringX::split2FromBegin(StringX::substring(url,index+3,part-(index+3)), "@",firstPart,secondPart)) {
						out=secondPart;
						return true;
					};
					out=StringX::substring(url,index+3,part-(index+3));
					return true;
				};
				out=StringX::substring(url,index+3);
				return true;
			};
			return false;
		};

		bool URL::getUsernameAndPassword(String url,String &out) {
			size_t index;
			size_t part;
			String firstPart;
			String secondPart;
			if(StringBase::indexOf(url,"://",index)) {
				if(StringBase::indexOf(url,"/",index+3,part)) {
					if(StringX::split2FromBegin(StringX::substring(url,index+3,part-(index+3)), "@",firstPart,secondPart)) {
						out=firstPart;
						return true;
					};
				};
			};
			return false;
		};

		bool URL::getPathAndFileName(String url,String &out) {
			size_t index;
			size_t part;
			String firstPart;
			String secondPart;
			if(StringBase::indexOf(url,"://",index)) {
				if(StringBase::indexOf(url,"/",index+3,part)) {
					if(StringX::split2FromBegin(StringX::substring(url,part), "?",firstPart,secondPart)) {
						out=firstPart;
						return true;
					};
					out=StringX::substring(url,part);
					return true;
				};
				out="/";
				return true;
			};
			return false;
		};

		bool URL::getQuery(String url,String &out) {
			size_t index;
			size_t part;
			String firstPart;
			String secondPart;
			String thirdPart;
			if(StringX::indexOf(url,"://",0,index)) {
				if(StringBase::indexOf(url,"/",index+3,part)) {
					if(StringX::split2FromBegin(StringX::substring(url,part+1), "?",firstPart,secondPart)) {
						if(StringX::split2FromBegin(secondPart, "#",firstPart,thirdPart)) {
							out=firstPart;
							return true;
						};
					};
				};
			};
			return false;
		};

	};
};

