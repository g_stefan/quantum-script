//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBXYO_XO_COPYRIGHT_HPP
#define LIBXYO_XO_COPYRIGHT_HPP

#define LIBXYO_XO_COPYRIGHT            "Copyright (C) Grigore Stefan."
#define LIBXYO_XO_PUBLISHER            "Grigore Stefan"
#define LIBXYO_XO_COMPANY              LIBXYO_XO_PUBLISHER
#define LIBXYO_XO_CONTACT              "g_stefan@yahoo.com"
#define LIBXYO_XO_FULL_COPYRIGHT       LIBXYO_XO_COPYRIGHT " <" LIBXYO_XO_CONTACT ">"

#ifndef XYO_RC

#ifndef XYO_XO__EXPORT_HPP
#include "xyo-xo--export.hpp"
#endif

namespace Lib {
	namespace XYO {
		namespace XO {

			class Copyright {
				public:
					XYO_XO_EXPORT static const char *copyright();
					XYO_XO_EXPORT static const char *publisher();
					XYO_XO_EXPORT static const char *company();
					XYO_XO_EXPORT static const char *contact();
					XYO_XO_EXPORT static const char *fullCopyright();

			};

		};
	};
};

#endif
#endif
