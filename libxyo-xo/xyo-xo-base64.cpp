//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-base64.hpp"

//
// http://en.wikipedia.org/wiki/Base64
//

namespace XYO {
	namespace XO {

		static const char *code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

		String Base64::encode(String toEncode) {
			String retV;
			dword c;
			dword cx;
			dword co;
			dword cz;
			dword n;
			dword n0, n1, n2, n3;
			String buf(2, 0);
			buf[1] = 0;
			buf.setLength(1);
			co = toEncode.length();
			for (c = 0; c < co; c += 3) {
				n = (((dword) toEncode[c]) << 16);
				if (c + 1 < co) {
					n += (((dword) toEncode[c + 1]) << 8);
				};
				if (c + 2 < co) {
					n += (((dword) toEncode[c + 2]));
				};

				n0 = (n >> 18)&0x3F;
				n1 = (n >> 12)&0x3F;
				n2 = (n >> 6)&0x3F;
				n3 = (n)&0x3F;

				buf[0] = code[n0];
				retV << buf;
				buf[0] = code[n1];
				retV << buf;
				if (c + 1 < co) {
					buf[0] = code[n2];
					retV << buf;
				};
				if (c + 2 < co) {
					buf[0] = code[n3];
					retV << buf;
				};

			};
			cx = co % 3;
			if (cx > 0) {
				for (cz = cx; cz < 3; cz++) {
					retV << "=";
				};
			};
			return retV;
		};

		bool Base64::decode(String toDecode, String &out) {

			String retV;
			dword c;
			dword co;
			byte n0, n1, n2, n3;
			byte c0, c1, c2, ch;
			byte z;
			co = toDecode.length();
			out = "";

			for (c = 0; c < co; c += 4) {

				ch = toDecode[c];
				for (z = 0; z < 0x40; ++z) {
					if (code[z] == ch) {
						break;
					};
				};
				if (z == 0x40) {
					return false;
				};
				n0 = z;

				ch = toDecode[c + 1];
				for (z = 0; z < 0x40; ++z) {
					if (code[z] == ch) {
						break;
					};
				};
				if (z == 0x40) {
					return false;
				};
				n1 = z;

				ch = toDecode[c + 2];
				for (z = 0; z < 0x40; ++z) {
					if (code[z] == ch) {
						break;
					};
				};
				if (z == 0x40) {
					if (ch != '=') {
						return false;
					};
				};

				n2 = z;


				ch = toDecode[c + 3];
				for (z = 0; z < 0x40; ++z) {
					if (code[z] == ch) {
						break;
					};
				};
				if (z == 0x40) {
					if (ch != '=') {
						return false;
					};
				};
				n3 = z;

				c0 = (((n0) << 2) | ((n1 & 0x30) >> 4));
				c1 = (((n1 & 0x0F) << 4) | ((n2 & 0x3C) >> 2));
				c2 = (((n2 & 0x03) << 6) | (n3));

				out << c0;
				if (n2 != 0x40) {
					out << c1;
				};
				if (n3 != 0x40) {
					out << c2;
				};
			};
			return true;
		};

	};
};

