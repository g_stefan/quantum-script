//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_RANDOMMT_HPP
#define XYO_XO_RANDOMMT_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		class RandomMT :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(RandomMT);
			protected:
				int index;
				dword mt[624];
				dword value;
			public:

				XYO_XO_EXPORT RandomMT();
				XYO_XO_EXPORT void seed(dword);
				XYO_XO_EXPORT dword nextRandom();
				XYO_XO_EXPORT dword getValue();
				XYO_XO_EXPORT void copy(RandomMT &value);
		};

	};
};

#endif
