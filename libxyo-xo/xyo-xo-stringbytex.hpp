//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGBYTEX_HPP
#define XYO_XO_STRINGBYTEX_HPP

#ifndef XYO_XO_STRINGBYTE_HPP
#include "xyo-xo-stringbyte.hpp"
#endif

namespace XYO {
	namespace XO {

		class StringByteX {
			public:
				XYO_XO_EXPORT static StringByte trimWithElement(StringByte, StringByte);
				XYO_XO_EXPORT static StringByte replace(StringByte, StringByte, StringByte);
				XYO_XO_EXPORT static StringByte nilAtFirstFromEnd(StringByte, StringByte);
				XYO_XO_EXPORT static StringByte substring(StringByte, size_t start, size_t length);
				XYO_XO_EXPORT static StringByte substring(StringByte, size_t start);

				inline  static bool indexOf(StringByte a, StringByte b, size_t start, size_t &index) {
					return StringBaseByte::indexOf(a, a.length(), b, b.length(), start, index);
				};

				inline static bool indexOfFromEnd(StringByte a, StringByte b, size_t start, size_t &index) {
					return StringBaseByte::indexOfFromEnd(a, a.length(), b, b.length(), start, index);
				};

				XYO_XO_EXPORT static StringByte toLowerCaseAscii(StringByte value);
				XYO_XO_EXPORT static StringByte toUpperCaseAscii(StringByte value);
				XYO_XO_EXPORT static bool matchAscii(StringByte text, StringByte sig);
				XYO_XO_EXPORT static bool split2FromBegin(StringByte text, StringByte sig,StringByte &firstPart,StringByte &secondPart);
				XYO_XO_EXPORT static StringByte encodeC(StringByte in);
				XYO_XO_EXPORT static StringByte fromHex(StringByte in);
				XYO_XO_EXPORT static StringByte toHex(StringByte in);
		};


	};
};

#endif

