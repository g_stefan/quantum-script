//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <string.h>

#include "xyo-xo-utf32x.hpp"

namespace XYO {
	namespace XO {

		size_t Utf32X::memChrSize(utf32 *x) {
			return chrIsValid(x);
		};

		bool Utf32X::chrIsValid(utf32 *x) {
			if (*x <= 0x0000D7FF) {
				return true;
			}
			if ((*x >= 0x0000D800) && (*x < 0x0000E000)) {
				return false;
			}
			if ((*x >= 0x0000FFFE) && (*x <= 0x0000FFFF)) {
				return false;
			}
			if (*x >= 0x00110000) {
				return false;
			}
			return true;
		};

	};
};

