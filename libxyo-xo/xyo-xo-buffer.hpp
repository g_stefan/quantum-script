//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_BUFFER_HPP
#define XYO_XO_BUFFER_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {
		class Buffer;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::Buffer>:
			public TMemoryObjectPoolActive<XO::Buffer> {};

	};
};

namespace XYO {
	namespace XO {
		using namespace XYO::XY;

		class Buffer :
			public Object {
			public:

				XYO::byte *buffer;
				size_t length;
				size_t size;

				XYO_XO_EXPORT Buffer();
				XYO_XO_EXPORT ~Buffer();

				XYO_XO_EXPORT void setSize(size_t);

				XYO_XO_EXPORT void activeDestructor();
				XYO_XO_EXPORT static void memoryInit();

				XYO_XO_EXPORT String toString();

				XYO_XO_EXPORT void load(XYO::byte *buf_,size_t size_);
				XYO_XO_EXPORT void opXOR(XYO::byte *buf_,size_t size_);
				XYO_XO_EXPORT void fromString(String &);
		};
	};
};

#endif

