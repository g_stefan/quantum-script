//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRING_HPP
#define XYO_XO_STRING_HPP

#ifndef XYO_XO_STRINGBYTEX_HPP
#include "xyo-xo-stringbytex.hpp"
#endif

namespace XYO {
	namespace XO {

		typedef StringReferenceByte StringReference;
		typedef StringByte String;
		typedef StringByteX StringX;

		class StringBase {
			public:

				XYO_XO_EXPORT static char empty[];

				inline static size_t length(const char *o) {
					return strlen(o);
				};

				inline static void copy(char *o, const char *v) {
					strcpy(o,v);
				};

				inline static void copyN(char *o, const char *v, size_t k) {
					strncpy(o,v, k);
					o[k] = 0;
				};

				inline static int compare(const char *o, const char *v) {
					return strcmp(o,v);
				};

				inline static int compareN(const char *o, const char *v, size_t k) {
					return strncmp(o, v, k);
				};

				inline static bool isEqual(const char *o, const char *v) {
					return (strcmp(o,v) == 0);
				};

				inline static bool isEqualN(const char *o, const char *v, size_t k) {
					return (strncmp(o,v, k) == 0);
				};

				inline static bool beginWith(const char *o, const char *v) {
					return (strncmp(o,v, strlen(v)) == 0);
				};

				inline static void concatenate(char *o, const char *v) {
					strcat(o,v);
				};

				inline static void concatenateN(char *o, const char *v, size_t k) {
					strncat(o,v,k);
				};

				inline static bool indexOf(const char *o, size_t o_ln, const char *v, size_t v_ln, size_t pos, size_t &retV) {
					return StringBaseByte::indexOf((const byte *)o,o_ln,(const byte *)v,v_ln,pos,retV);
				};

				inline static bool indexOfFromEnd(const char *o, size_t o_ln, const char *v, size_t v_ln, size_t pos, size_t &retV) {
					return StringBaseByte::indexOfFromEnd((const byte *)o,o_ln,(const byte *)v,v_ln,pos,retV);
				};

				inline static bool indexOf(const char *o, const char *v, size_t &retV) {
					return indexOf(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOf(const char *o, const char *v, size_t pos, size_t &retV) {
					return indexOf(o, length(o), v, length(v), pos, retV);
				};

				inline static bool indexOfFromEnd(const char *o, const char *v, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOfFromEnd(const char *o, const char *v, size_t pos, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), pos, retV);
				};

				inline static bool nilAt(char *o, const char *v) {
					return StringBaseByte::nilAt((byte *)o, (const byte *)v);
				};

				inline static bool nilAtFromEnd(char *o, const char *v) {
					return StringBaseByte::nilAtFromEnd((byte *)o, (const byte *)v);
				};

				inline static bool skipTo(char *o, const char *v) {
					return StringBaseByte::skipTo((byte *)o,(const byte *)v);
				};

				inline static bool skipToFromEnd(char *o, const char *v) {
					return StringBaseByte::skipToFromEnd((byte *)o, (const byte *)v);
				};

				inline static bool hasOnlyElement(const char *o, const char *v) {
					return StringBaseByte::hasOnlyElement((const byte *)o, (const byte *)v);
				};

				inline static const char *toNotInElement(const char *o, const char *v) {
					return (const char *)StringBaseByte::toNotInElement((const byte *)o, (const byte *)v);
				};

				inline static const char *toNotInFromEndElement(const char *o, const char *v) {
					return (const char *)StringBaseByte::toNotInFromEndElement((const byte *)o, (const byte *)v);
				};

				inline static char chrToLowerCaseAscii(char in) {
					if(in<0x41||in>0x5A) {
						return in;
					};
					return in+0x20;
				};

				inline static char chrToUpperCaseAscii(char in) {
					if(in<0x61||in>0x7A) {
						return in;
					};
					return in-0x20;
				};

				inline static int  compareIgnoreCaseAscii(const char *x, const char *y) {
					return StringBaseByte::compareIgnoreCaseAscii((const byte *)x, (const byte *)y);
				};

				inline static int  compareIgnoreCaseNAscii(const char *x, const char *y, size_t ln) {
					return StringBaseByte::compareIgnoreCaseNAscii((const byte *)x, (const byte *)y, ln);
				};

				inline static bool indexOfIgnoreCaseAscii(const char *the_string, size_t length, const char *patern, size_t patlength, size_t pos, size_t *result) {
					return StringBaseByte::indexOfIgnoreCaseAscii((const byte *)the_string, length, (const byte *)patern, patlength, pos, result);
				};

				inline static bool indexOfFromEndIgnoreCaseAscii(const char *the_string, size_t length, const char *patern, size_t patlength, size_t pos, size_t *result) {
					return StringBaseByte::indexOfFromEndIgnoreCaseAscii((const byte *)the_string,length, (const byte *)patern, patlength, pos, result);
				};

		};



	};
};

#endif
