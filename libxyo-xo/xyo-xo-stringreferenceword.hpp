//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGREFERENCEWORD_HPP
#define XYO_XO_STRINGREFERENCEWORD_HPP

#ifndef XYO_XO_STRINGBASEWORD_HPP
#include "xyo-xo-stringbaseword.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringReferenceWord;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::StringReferenceWord>:
			public TMemoryObjectPoolActive<XO::StringReferenceWord> {};

	};
};

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class StringReferenceWord:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(StringReferenceWord);
			protected:
				word *value_;
				size_t length_;
				size_t size_;
				size_t chunk_;
				bool mode_;

				XYO_XO_EXPORT void memoryDelete_();
				XYO_XO_EXPORT void memoryNew_(size_t size);
				XYO_XO_EXPORT void memoryResize_(size_t size);
			public:

				static const size_t defaultChunk_ = 32;

				XYO_XO_EXPORT StringReferenceWord();
				XYO_XO_EXPORT ~StringReferenceWord();
				XYO_XO_EXPORT word *value();
				XYO_XO_EXPORT size_t length();
				XYO_XO_EXPORT size_t getSize();
				XYO_XO_EXPORT size_t getChunk();
				XYO_XO_EXPORT void setChunk(size_t x);
				XYO_XO_EXPORT void setLength(size_t x);
				XYO_XO_EXPORT void updateLength();
				XYO_XO_EXPORT static TPointer<StringReferenceWord> init();
				XYO_XO_EXPORT static TPointer<StringReferenceWord> init2(size_t initialSize, size_t chunk);
				XYO_XO_EXPORT static TPointer<StringReferenceWord> from(const word *o);
				XYO_XO_EXPORT static TPointer<StringReferenceWord> concatenate(StringReferenceWord *o, const word *x);
				XYO_XO_EXPORT static TPointer<StringReferenceWord> concatenate2(StringReferenceWord *o, StringReferenceWord *x);
				XYO_XO_EXPORT static TPointer<StringReferenceWord> concatenate3(StringReferenceWord *o, const word *x, size_t x_ln);
				XYO_XO_EXPORT static TPointer<StringReferenceWord> from2(const word *o, size_t o_ln);
				XYO_XO_EXPORT static TPointer<StringReferenceWord> concatenate_one(StringReferenceWord *o, const word x);
				XYO_XO_EXPORT void activeDestructor();
				XYO_XO_EXPORT static void memoryInit();
		};

	};
};

#endif

