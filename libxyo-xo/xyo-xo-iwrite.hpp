//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_IWRITE_HPP
#define XYO_XO_IWRITE_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class IWrite :
			public virtual Object {
				XYO_XY_INTERFACE(IWrite);

			public:
				virtual size_t write(void *output, size_t length) = 0;
		};

	};
};

#endif

