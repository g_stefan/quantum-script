//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringreferencedword.hpp"

//
// Accelerated memory allocation for fixed size of
// 32,64,96,128,160,192,224,256
//

namespace XYO {
	namespace XO {

		template<size_t sizeOfMemory>
		class TXStringReferenceDWordMemory {
			public:
				dword memory[sizeOfMemory];
		};

		StringReferenceDWord::StringReferenceDWord() {
			mode_ = false;
			value_ = NULL;
		};

		StringReferenceDWord::~StringReferenceDWord() {
			memoryDelete_();
		};

		void StringReferenceDWord::activeDestructor() {
			memoryDelete_();
			value_=NULL;
			mode_=false;
		};

		void StringReferenceDWord::memoryInit() {
			TMemory<TXStringReferenceDWordMemory<32> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<64> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<96> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<128> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<160> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<192> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<224> >::memoryInit();
			TMemory<TXStringReferenceDWordMemory<256> >::memoryInit();
		};

		void StringReferenceDWord::memoryDelete_() {
			if (value_ != NULL) {
				if(mode_) {
					switch(size_) {
						case 32:
							TMemory<TXStringReferenceDWordMemory<32> >::memoryDelete((TXStringReferenceDWordMemory<32> *)((void *)value_));
							break;
						case 64:
							TMemory<TXStringReferenceDWordMemory<64> >::memoryDelete((TXStringReferenceDWordMemory<64> *)((void *)value_));
							break;
						case 96:
							TMemory<TXStringReferenceDWordMemory<96> >::memoryDelete((TXStringReferenceDWordMemory<96> *)((void *)value_));
							break;
						case 128:
							TMemory<TXStringReferenceDWordMemory<128> >::memoryDelete((TXStringReferenceDWordMemory<128> *)((void *)value_));
							break;
						case 160:
							TMemory<TXStringReferenceDWordMemory<160> >::memoryDelete((TXStringReferenceDWordMemory<160> *)((void *)value_));
							break;
						case 192:
							TMemory<TXStringReferenceDWordMemory<192> >::memoryDelete((TXStringReferenceDWordMemory<192> *)((void *)value_));
							break;
						case 224:
							TMemory<TXStringReferenceDWordMemory<224> >::memoryDelete((TXStringReferenceDWordMemory<224> *)((void *)value_));
							break;
						case 256:
							TMemory<TXStringReferenceDWordMemory<256> >::memoryDelete((TXStringReferenceDWordMemory<256> *)((void *)value_));
							break;
					};
				} else {
					delete[] value_;
				};
			};
			mode_=false;
		};


		void StringReferenceDWord::memoryNew_(size_t size) {
			size_=size;
			switch(size) {
				case 32:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<32> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 64:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<64> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 96:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<96> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 128:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<128> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 160:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<160> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 192:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<192> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 224:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<224> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 256:
					value_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<256> >::memoryNew()));
					mode_=true;
					return;
					break;
				default:
					break;
			};
			value_ = new dword[size];
			mode_=false;
		};

		void StringReferenceDWord::memoryResize_(size_t size) {
			dword *newValue_;
			bool  newMode_;
			newMode_=false;
			switch(size) {
				case 32:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<32> >::memoryNew()));
					newMode_=true;
					break;
				case 64:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<64> >::memoryNew()));
					newMode_=true;
					break;
				case 96:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<96> >::memoryNew()));
					newMode_=true;
					break;
				case 128:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<128> >::memoryNew()));
					newMode_=true;
					break;
				case 160:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<160> >::memoryNew()));
					newMode_=true;
					break;
				case 192:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<192> >::memoryNew()));
					newMode_=true;
					break;
				case 224:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<224> >::memoryNew()));
					newMode_=true;
					break;
				case 256:
					newValue_=(dword *)((void *)(TMemory<TXStringReferenceDWordMemory<256> >::memoryNew()));
					newMode_=true;
					break;
				default:
					newValue_ = new dword[size];
					break;
			};
			StringBaseDWord::copyNMemory(newValue_,value_,length_);
			memoryDelete_();
			value_=newValue_;
			mode_=newMode_;
			size_=size;
		};


		dword *StringReferenceDWord::value() {
			return value_;
		};

		size_t StringReferenceDWord::length() {
			return length_;
		};

		size_t StringReferenceDWord::getSize() {
			return size_;
		};

		size_t StringReferenceDWord::getChunk() {
			return chunk_;
		};

		void StringReferenceDWord::setChunk(size_t x) {
			chunk_ = x;
		};

		void StringReferenceDWord::setLength(size_t x) {
			if(x<size_) {
				length_ = x;
				return;
			};
			length_=size_-1;
		};

		void StringReferenceDWord::updateLength() {
			length_ = StringBaseDWord::length(value_);
		};

		TPointer<StringReferenceDWord > StringReferenceDWord::init() {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->memoryNew_(defaultChunk_);
			retV->value_[0] = StringBaseDWord::empty[0];
			retV->length_ = 0;
			return retV;
		};

		TPointer<StringReferenceDWord > StringReferenceDWord::init2(size_t initialSize, size_t chunk) {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = (chunk) ? chunk : defaultChunk_;
			retV->length_ = 0;
			retV->memoryNew_(initialSize);
			retV->value_[0] = StringBaseDWord::empty[0];
			return retV;
		};

		TPointer<StringReferenceDWord> StringReferenceDWord::from(const dword *o) {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = StringBaseDWord::length(o);
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseDWord::copy(retV->value_, o);
			return retV;
		};

		TPointer<StringReferenceDWord> StringReferenceDWord::concatenate(StringReferenceDWord *o, const dword *x) {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + StringBaseDWord::length(x);
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseDWord::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseDWord::copy(&retV->value_[o->length_], x);
			return retV;
		};

		TPointer<StringReferenceDWord> StringReferenceDWord::concatenate2(StringReferenceDWord *o, StringReferenceDWord *x) {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + x->length_;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseDWord::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseDWord::copyNMemory(&retV->value_[o->length_], x->value_,x->length_);
			return retV;
		};

		TPointer<StringReferenceDWord> StringReferenceDWord::concatenate3(StringReferenceDWord *o, const dword *x, size_t x_ln) {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + x_ln;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};

			StringBaseDWord::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseDWord::copyNMemory(&retV->value_[o->length_], x, x_ln);
			return retV;
		};

		TPointer<StringReferenceDWord> StringReferenceDWord::from2(const dword *o, size_t o_ln) {
			TPointer<StringReferenceDWord> retV;
			retV.setObject(TMemory<StringReferenceDWord>::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o_ln;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};

			StringBaseDWord::copyNMemory(retV->value_, o, o_ln);
			return retV;
		};

		TPointer<StringReferenceDWord> StringReferenceDWord::concatenate_one(StringReferenceDWord *o, const dword x) {
			TPointer<StringReferenceDWord > retV;
			retV.setObject(TMemory<StringReferenceDWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + 1;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseDWord::copyNMemory(retV->value_, o->value_, o->length_);
			retV->value_[o->length_]= x;
			retV->value_[o->length_+1]= 0;
			return retV;
		};

	};
};


