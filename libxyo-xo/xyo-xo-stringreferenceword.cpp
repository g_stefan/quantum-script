//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringreferenceword.hpp"

//
// Accelerated memory allocation for fixed size of
// 32,64,96,128,160,192,224,256
//

namespace XYO {
	namespace XO {

		template<size_t sizeOfMemory>
		class TXStringReferenceWordMemory {
			public:
				word memory[sizeOfMemory];
		};

		StringReferenceWord::StringReferenceWord() {
			mode_ = false;
			value_ = NULL;
		};

		StringReferenceWord::~StringReferenceWord() {
			memoryDelete_();
		};

		void StringReferenceWord::activeDestructor() {
			memoryDelete_();
			value_=NULL;
			mode_=false;
		};

		void StringReferenceWord::memoryInit() {
			TMemory<TXStringReferenceWordMemory<32> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<64> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<96> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<128> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<160> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<192> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<224> >::memoryInit();
			TMemory<TXStringReferenceWordMemory<256> >::memoryInit();
		};

		void StringReferenceWord::memoryDelete_() {
			if (value_ != NULL) {
				if(mode_) {
					switch(size_) {
						case 32:
							TMemory<TXStringReferenceWordMemory<32> >::memoryDelete((TXStringReferenceWordMemory<32> *)((void *)value_));
							break;
						case 64:
							TMemory<TXStringReferenceWordMemory<64> >::memoryDelete((TXStringReferenceWordMemory<64> *)((void *)value_));
							break;
						case 96:
							TMemory<TXStringReferenceWordMemory<96> >::memoryDelete((TXStringReferenceWordMemory<96> *)((void *)value_));
							break;
						case 128:
							TMemory<TXStringReferenceWordMemory<128> >::memoryDelete((TXStringReferenceWordMemory<128> *)((void *)value_));
							break;
						case 160:
							TMemory<TXStringReferenceWordMemory<160> >::memoryDelete((TXStringReferenceWordMemory<160> *)((void *)value_));
							break;
						case 192:
							TMemory<TXStringReferenceWordMemory<192> >::memoryDelete((TXStringReferenceWordMemory<192> *)((void *)value_));
							break;
						case 224:
							TMemory<TXStringReferenceWordMemory<224> >::memoryDelete((TXStringReferenceWordMemory<224> *)((void *)value_));
							break;
						case 256:
							TMemory<TXStringReferenceWordMemory<256> >::memoryDelete((TXStringReferenceWordMemory<256> *)((void *)value_));
							break;
					};
				} else {
					delete[] value_;
				};
			};
			mode_=false;
		};


		void StringReferenceWord::memoryNew_(size_t size) {
			size_=size;
			switch(size) {
				case 32:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<32> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 64:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<64> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 96:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<96> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 128:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<128> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 160:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<160> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 192:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<192> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 224:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<224> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 256:
					value_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<256> >::memoryNew()));
					mode_=true;
					return;
					break;
				default:
					break;
			};
			value_ = new word[size];
			mode_=false;
		};

		void StringReferenceWord::memoryResize_(size_t size) {
			word *newValue_;
			bool  newMode_;
			newMode_=false;
			switch(size) {
				case 32:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<32> >::memoryNew()));
					newMode_=true;
					break;
				case 64:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<64> >::memoryNew()));
					newMode_=true;
					break;
				case 96:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<96> >::memoryNew()));
					newMode_=true;
					break;
				case 128:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<128> >::memoryNew()));
					newMode_=true;
					break;
				case 160:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<160> >::memoryNew()));
					newMode_=true;
					break;
				case 192:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<192> >::memoryNew()));
					newMode_=true;
					break;
				case 224:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<224> >::memoryNew()));
					newMode_=true;
					break;
				case 256:
					newValue_=(word *)((void *)(TMemory<TXStringReferenceWordMemory<256> >::memoryNew()));
					newMode_=true;
					break;
				default:
					newValue_ = new word[size];
					break;
			};
			StringBaseWord::copyNMemory(newValue_,value_,length_);
			memoryDelete_();
			value_=newValue_;
			mode_=newMode_;
			size_=size;
		};


		word *StringReferenceWord::value() {
			return value_;
		};

		size_t StringReferenceWord::length() {
			return length_;
		};

		size_t StringReferenceWord::getSize() {
			return size_;
		};

		size_t StringReferenceWord::getChunk() {
			return chunk_;
		};

		void StringReferenceWord::setChunk(size_t x) {
			chunk_ = x;
		};

		void StringReferenceWord::setLength(size_t x) {
			if(x<size_) {
				length_ = x;
				return;
			};
			length_=size_-1;
		};

		void StringReferenceWord::updateLength() {
			length_ = StringBaseWord::length(value_);
		};

		TPointer<StringReferenceWord > StringReferenceWord::init() {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->memoryNew_(defaultChunk_);
			retV->value_[0] = StringBaseWord::empty[0];
			retV->length_ = 0;
			return retV;
		};

		TPointer<StringReferenceWord > StringReferenceWord::init2(size_t initialSize, size_t chunk) {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = (chunk) ? chunk : defaultChunk_;
			retV->length_ = 0;
			retV->memoryNew_(initialSize);
			retV->value_[0] = StringBaseWord::empty[0];
			return retV;
		};

		TPointer<StringReferenceWord> StringReferenceWord::from(const word *o) {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = StringBaseWord::length(o);
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseWord::copy(retV->value_, o);
			return retV;
		};

		TPointer<StringReferenceWord> StringReferenceWord::concatenate(StringReferenceWord *o, const word *x) {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + StringBaseWord::length(x);
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseWord::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseWord::copy(&retV->value_[o->length_], x);
			return retV;
		};

		TPointer<StringReferenceWord> StringReferenceWord::concatenate2(StringReferenceWord *o, StringReferenceWord *x) {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + x->length_;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseWord::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseWord::copyNMemory(&retV->value_[o->length_], x->value_,x->length_);
			return retV;
		};

		TPointer<StringReferenceWord> StringReferenceWord::concatenate3(StringReferenceWord *o, const word *x, size_t x_ln) {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + x_ln;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};

			StringBaseWord::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseWord::copyNMemory(&retV->value_[o->length_], x, x_ln);
			return retV;
		};

		TPointer<StringReferenceWord> StringReferenceWord::from2(const word *o, size_t o_ln) {
			TPointer<StringReferenceWord> retV;
			retV.setObject(TMemory<StringReferenceWord>::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o_ln;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};

			StringBaseWord::copyNMemory(retV->value_, o, o_ln);
			return retV;
		};

		TPointer<StringReferenceWord> StringReferenceWord::concatenate_one(StringReferenceWord *o, const word x) {
			TPointer<StringReferenceWord > retV;
			retV.setObject(TMemory<StringReferenceWord >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + 1;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseWord::copyNMemory(retV->value_, o->value_, o->length_);
			retV->value_[o->length_]= x;
			retV->value_[o->length_+1]= 0;
			return retV;
		};

	};
};


