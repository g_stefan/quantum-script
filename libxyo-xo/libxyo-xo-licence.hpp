//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBXYO_XO_LICENCE_HPP
#define LIBXYO_XO_LICENCE_HPP

#ifndef XYO_XO__EXPORT_HPP
#include "xyo-xo--export.hpp"
#endif

namespace Lib {
	namespace XYO {
		namespace XO {

			class Licence {
				public:
					XYO_XO_EXPORT static const char *content();
					XYO_XO_EXPORT static const char *shortContent();
			};

		};
	};
};

#endif
