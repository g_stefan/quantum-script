//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-sha512hash.hpp"
#include "xyo-xo-bytex.hpp"

//
// http://en.wikipedia.org/wiki/SHA-2
//

namespace XYO {
	namespace XO {

		qword SHA512Hash::k_[]= {
			0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc, 0x3956c25bf348b538,
			0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118, 0xd807aa98a3030242, 0x12835b0145706fbe,
			0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2, 0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235,
			0xc19bf174cf692694, 0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
			0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5, 0x983e5152ee66dfab,
			0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4, 0xc6e00bf33da88fc2, 0xd5a79147930aa725,
			0x06ca6351e003826f, 0x142929670a0e6e70, 0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed,
			0x53380d139d95b3df, 0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
			0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30, 0xd192e819d6ef5218,
			0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8, 0x19a4c116b8d2d0c8, 0x1e376c085141ab53,
			0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8, 0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373,
			0x682e6ff3d6b2b8a3, 0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
			0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b, 0xca273eceea26619c,
			0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178, 0x06f067aa72176fba, 0x0a637dc5a2c898a6,
			0x113f9804bef90dae, 0x1b710b35131c471b, 0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc,
			0x431d67c49c100d4c, 0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
		};

		SHA512Hash::SHA512Hash() {
			processInit();
		};

		void SHA512Hash::processInit() {
			h0 = 0x6a09e667f3bcc908;
			h1 = 0xbb67ae8584caa73b;
			h2 = 0x3c6ef372fe94f82b;
			h3 = 0xa54ff53a5f1d36f1;
			h4 = 0x510e527fade682d1;
			h5 = 0x9b05688c2b3e6c1f;
			h6 = 0x1f83d9abfb41bd6b;
			h7 = 0x5be0cd19137e2179;

			length0=0;
			length1=0;
			stateM=0;
			lastData[0]=0;
			lastData[1]=0;
			lastData[2]=0;
			lastData[3]=0;
			lastData[4]=0;
			lastData[5]=0;
			lastData[6]=0;
			lastData[7]=0;
		};

		void SHA512Hash::hashBlock(qword *w_) {
			int k;
			qword s0,s1,temp1,temp2;
			qword maj,ch;
			qword a,b,c,d,e,f,g,h;
			qword w[80];

			for(k=0; k<16; ++k) {
				w[k]=w_[k];
			};

			for(k=16; k<80; ++k) {
				s0=ByteX::qwordRightRotate(w[k-15],1) ^ ByteX::qwordRightRotate(w[k-15], 8) ^  (w[k-15] >> 7);
				s1=ByteX::qwordRightRotate(w[k-2],19) ^ ByteX::qwordRightRotate(w[k-2], 61) ^  (w[k-2] >> 6);
				w[k]=w[k-16]+s0+w[k-7]+s1;
			};

			a=h0;
			b=h1;
			c=h2;
			d=h3;
			e=h4;
			f=h5;
			g=h6;
			h=h7;

			for(k=0; k<80; ++k) {

				s1=ByteX::qwordRightRotate(e,14) ^ ByteX::qwordRightRotate(e,18) ^ ByteX::qwordRightRotate(e,41);
				ch=(e & f) ^ ((~e) & g);
				temp1=h+s1+ch+k_[k]+w[k];
				s0=ByteX::qwordRightRotate(a,28) ^ ByteX::qwordRightRotate(a,34) ^ ByteX::qwordRightRotate(a,39);
				maj = (a & b) ^ (a & c) ^ (b & c);
				temp2=s0+maj;

				h=g;
				g=f;
				f=e;
				e=d+temp1;
				d=c;
				c=b;
				b=a;
				a=temp1+temp2;

			};

			h0+=a;
			h1+=b;
			h2+=c;
			h3+=d;
			h4+=e;
			h5+=f;
			h6+=g;
			h7+=h;
		};

		void SHA512Hash::processBytes(const byte *toHash,size_t length_) {
			size_t k,z;
			size_t m=length0%8;
			size_t length__=length_&(~((size_t)(0x07)));

			if(((qword)0xFFFFFFFFFFFFFFFF)-((qword)length_)>=length0) {
				length0+=((qword)length_);
			} else {
				length0+=((qword)length_);
				length1++;
			};

			if(m) {

				for(k=m,z=0; z<length_;) {
					lastData[k]=*toHash;
					++toHash;
					++z;
					++k;
					if(k==8) {
						break;
					};
				};
				if(k!=8) {
					return;
				};
				length_-=z;
				process[stateM]=ByteX::qwordFromByteReversed(lastData);

				++stateM;
				if(stateM==16) {
					hashBlock(process);
					stateM=0;
				};
			};


			for(k=0; k<length__; k+=8) {
				process[stateM]=ByteX::qwordFromByteReversed(toHash);
				toHash+=8;
				++stateM;
				if(stateM==16) {
					hashBlock(process);
					stateM=0;
				};
			};

			m=length_%8;
			if(m) {
				for(k=0; k<m; ++k) {
					lastData[k]=*toHash;
					++toHash;
				};
			};
		};

		void SHA512Hash::processDone() {
			size_t m=length0%128;
			qword finalLength1=(length1<<3)|(length0>>61);
			qword finalLength0=length0<<3;
			byte data[8];
			if(m<112) {
				data[0]=0x80;
				processBytes(data,1);
				++m;
				data[0]=0;
				for(; m<112; ++m) {
					processBytes(data,1);
				};
				ByteX::qwordToByteReversed(finalLength1,data);
				processBytes(data,8);
				ByteX::qwordToByteReversed(finalLength0,data);
				processBytes(data,8);
				return;
			};
			if(m<127) {
				data[0]=0x80;
				processBytes(data,1);
				++m;
				data[0]=0;
				for(; m<128+112; ++m) {
					processBytes(data,1);
				};
				ByteX::qwordToByteReversed(finalLength1,data);
				processBytes(data,8);
				ByteX::qwordToByteReversed(finalLength0,data);
				processBytes(data,8);
				return;
			};
			data[0]=0x80;
			processBytes(data,1);
			++m;
			data[0]=0;
			for(; m<112; ++m) {
				processBytes(data,1);
			};
			ByteX::qwordToByteReversed(finalLength1,data);
			processBytes(data,8);
			ByteX::qwordToByteReversed(finalLength0,data);
			processBytes(data,8);
		};

		String SHA512Hash::getHashAsHex() {
			String retV(256, 0);
			byte result[64];

			toBytes(result);

			sprintf((char *) retV.value(),
				"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"
				"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"
				"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"
				"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				result[0], result[1], result[2], result[3],
				result[4], result[5], result[6], result[7],
				result[8], result[9], result[10], result[11],
				result[12], result[13], result[14], result[15],
				result[16], result[17], result[18], result[19],
				result[20], result[21], result[22], result[23],
				result[24], result[25], result[26], result[27],
				result[28], result[29], result[30], result[31],
				result[32], result[33], result[34], result[35],
				result[36], result[37], result[38], result[39],
				result[40], result[41], result[42], result[43],
				result[44], result[45], result[46], result[47],
				result[48], result[49], result[50], result[51],
				result[52], result[53], result[54], result[55],
				result[56], result[57], result[58], result[59],
				result[60], result[61], result[62], result[63]
			       );

			retV.setLength(128);
			return retV;
		};

		String SHA512Hash::getHashString(String toHash) {
			SHA512Hash hash;
			hash.processBytes((byte *)toHash.value(), toHash.length());
			hash.processDone();
			return hash.getHashAsHex();
		};

		void SHA512Hash::hashStringToBytes(String toHash,byte *buffer) {
			SHA512Hash hash;
			hash.processBytes((byte *)toHash.value(), toHash.length());
			hash.processDone();
			hash.toBytes(buffer);
		};

		void SHA512Hash::toBytes(byte *buffer) {
			ByteX::qwordToByteReversed(h0, buffer);
			ByteX::qwordToByteReversed(h1, buffer + 8);
			ByteX::qwordToByteReversed(h2, buffer + 16);
			ByteX::qwordToByteReversed(h3, buffer + 24);
			ByteX::qwordToByteReversed(h4, buffer + 32);
			ByteX::qwordToByteReversed(h5, buffer + 40);
			ByteX::qwordToByteReversed(h6, buffer + 48);
			ByteX::qwordToByteReversed(h7, buffer + 56);
		};

	};
};

