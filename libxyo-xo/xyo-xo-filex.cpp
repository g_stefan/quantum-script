//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "xyo-xo-filex.hpp"

namespace XYO {
	namespace XO {

		bool FileX::read(File &file,String &out,size_t size) {
			size_t readLn;
			size_t readX;
			size_t readTotal;
			char buffer[1024];

			if(size==0) {
				return true;
			};

			readTotal=0;
			readX=1024;
			if(size<readX) {
				readX=size;
			};
			for(;;) {
				readLn=file.read(buffer, readX);
				if(readLn>0) {
					out.concatenate(buffer,readLn);
				};
				if(readLn==0) {
					if(readTotal==0) {
						return false;
					};
				};
				//end of file
				if(readLn<readX) {
					break;
				};
				//end of read
				readTotal+=readLn;
				if(readTotal>=size) {
					break;
				};
				readX=size-readTotal;
				if(readX>1024) {
					readX=1024;
				};
			};

			return true;
		};

		bool FileX::readLn(File &file,String &out,size_t size) {
			size_t readLn;
			size_t readTotal;
			char buffer[2];

			buffer[1]=0;

			readTotal=0;
			if(size==0) {
				return true;
			};
			for(;;) {
				readLn=file.read(buffer, 1);
				if(readLn>0) {

					if(buffer[0]=='\r') {
						if(readTotal+1>=size) {
							out.concatenate("\r",1);
							return true;
						};

						readLn=file.read(buffer, 1);
						if(readLn>0) {
							if(buffer[0]=='\n') {
								return true;
							};
							out.concatenate(buffer,1);
							readTotal+=2;
							if(readTotal>=size) {
								return true;
							};
							continue;
						};

						out.concatenate("\r",1);
						//end of file
						return true;
					};

					if(buffer[0]=='\n') {
						return true;
					};


					out.concatenate(buffer,1);
					readTotal++;
					if(readTotal>=size) {
						return true;
					};
					continue;
				};
				// end
				if(readTotal==0) {
					break;
				};
				//end of file
				return true;
			};

			return false;
		};

		size_t FileX::write(File &file,String data) {
			return file.write(data.value(),data.length());
		};

		size_t FileX::writeLn(File &file,String data) {
			data<<"\r\n";
			return file.write(data.value(),data.length());
		};

		size_t FileX::readToBuffer(File &file,Buffer &buffer,size_t ln) {
			if(ln>buffer.size) {
				ln=buffer.size;
			};
			buffer.length=file.read(buffer.buffer,ln);
			return buffer.length;
		};

		size_t FileX::writeFromBuffer(File &file,Buffer &buffer) {
			return file.write(buffer.buffer,buffer.length);
		};

	};
};

