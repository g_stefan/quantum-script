//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_BASE64_HPP
#define XYO_XO_BASE64_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		class Base64 {
			public:
				XYO_XO_EXPORT static String encode(String toEncode);
				XYO_XO_EXPORT static bool decode(String toDecode, String &out);

				inline static void memoryInit() {
					TMemory<String>::memoryInit();
				};
		};

	};
};

#endif
