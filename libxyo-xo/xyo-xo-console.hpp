//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_CONSOLE_HPP
#define XYO_XO_CONSOLE_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		class Console {
			public:

				XYO_XO_EXPORT static bool keyHit();
				XYO_XO_EXPORT static char getChar();
		};

	};
};

#endif

