//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringbaseword.hpp"

namespace XYO {
	namespace XO {

		word StringBaseWord::empty[]= {0};

		size_t StringBaseWord::length(const word *o) {
			size_t k=0;
			while(*o) {
				++k;
				++o;
			};
			return k;
		};

		void StringBaseWord::copy(word *o, const word *v) {
			while(*v) {
				*o=*v;
				++o;
				++v;
			};
			*o=*v;
		};

		void StringBaseWord::copyN(word *o, const word *v, size_t k) {
			while((*v)&&(k>0)) {
				*o=*v;
				++o;
				++v;
				--k;
			};
			*o=0;
		};

		void StringBaseWord::copyNMemory(word *o, const word *v, size_t k) {
			while(k>0) {
				*o=*v;
				++o;
				++v;
				--k;
			};
			*o=0;
		};

		int StringBaseWord::compare(const word *o, const word *v) {
			while((*v)&&(*o)) {
				if(*o!=*v) {
					break;
				};
				++o;
				++v;
			};
			return (*v)-(*o);
		};

		int StringBaseWord::compareN(const word *o, const word *v, size_t k) {
			while((*v)&&(*o)&&(k>0)) {
				if(*o!=*v) {
					break;
				};
				++o;
				++v;
				--k;
			};
			return (*v)-(*o);
		};

		void StringBaseWord::concatenate(word *o, const word *v) {
			while(*o) {
				++o;
			};
			copy(o,v);
		};

		void StringBaseWord::concatenateN(word *o, const word *v, size_t k) {
			while(*o) {
				++o;
			};
			copyN(o,v,k);
		};

		bool StringBaseWord::indexOf(const word *o, size_t o_ln, const word *v, size_t v_ln, size_t pos, size_t &retV) {
			if (pos > o_ln) {
				return false;
			}
			if (v_ln > o_ln) {
				return false;
			}
			if ((pos + v_ln) > o_ln) {
				return false;
			}
			for (retV = pos; retV <= o_ln - v_ln; ++retV) {
				if (isEqualN(&o[retV], v, v_ln)) {
					return true;
				}
			};
			return false;
		};

		bool StringBaseWord::indexOfFromEnd(const word *o, size_t o_ln, const word *v, size_t v_ln, size_t pos, size_t &retV) {
			if (pos > o_ln) {
				return false;
			}
			if (v_ln > o_ln) {
				return false;
			}
			if ((pos + v_ln) > o_ln) {
				return false;
			}
			for (retV = o_ln - pos - v_ln + 1; retV; --retV) {
				if (isEqualN(&o[retV - 1], v, v_ln)) {
					--retV;
					return true;
				};
			};
			return false;
		};

		bool StringBaseWord::nilAt(word *o, const word *v) {
			size_t idx;
			if (indexOf(o, v, idx)) {
				o[idx] = 0;
				return true;
			};
			return false;
		};

		bool StringBaseWord::nilAtFromEnd(word *o, const word *v) {
			size_t idx;
			if (indexOfFromEnd(o, v, idx)) {
				o[idx] = 0;
				return true;
			};
			return false;
		};

		bool StringBaseWord::skipTo(word *o, const word *v) {
			size_t idx;
			if (indexOf(o, v, idx)) {
				copy(o, &o[idx + length(v)]);
				return true;
			};
			*o = 0;
			return false;
		};

		bool StringBaseWord::skipToFromEnd(word *o, const word *v) {
			size_t idx;
			if (indexOfFromEnd(o, v, idx)) {
				copy(o, &o[idx + length(v)]);
				return true;
			};
			*o = 0;
			return false;
		};

		bool StringBaseWord::hasOnlyElement(const word *o, const word *v) {
			const word *m;
			while (*o) {
				for (m = v; *m; ++m) {
					if (*o != *m) {
						return false;
					}
				};
				++o;
			};
			return true;
		};

		const word *StringBaseWord::toNotInElement(const word *o, const word *v) {
			const word *m;
			bool found;
			while (*o) {
				found = false;
				for (m = v; *m; ++m) {
					if (*o == *m) {
						found = true;
					}
				};
				if (!found) {
					return o;
				}
				++o;
			};
			return o;
		};

		const word *StringBaseWord::toNotInFromEndElement(const word *o, const word *v) {
			const word *m;
			const word *xo;
			const word *xo2;
			size_t k;
			bool found;
			k = length(o);
			xo = o + k;
			xo2 = xo;
			--xo;
			while (k > 0) {
				found = false;
				for (m = v; *m; ++m) {
					if (*xo == *m) {
						found = true;
					}
				};
				if (!found) {
					return xo;
				}
				--xo;
				--k;
			};
			return xo2;
		};

		int StringBaseWord::compareIgnoreCaseAscii(const word *x, const word *y) {
			word chrx;
			word chry;

			for (;;) {

				if ((*x) == 0) {
					if ((*y) == 0) {
						return 0;
					};
					return -1;
				}
				if ((*y) == 0) {
					return 1;
				}

				chrx = chrToLowerCaseAscii(*x);
				chry = chrToLowerCaseAscii(*y);

				if(chrx!=chry) {
					return chrx - chry;
				};

				++x;
				++y;
			};

		};

		int StringBaseWord::compareIgnoreCaseNAscii(const word *x, const word *y, size_t ln) {
			word chrx;
			word chry;

			for (;;) {
				if (ln == 0) {
					return 0;
				}
				if ((*x) == 0) {
					if ((*y) == 0) {
						return 0;
					};
					return -1;
				}
				if ((*y) == 0) {
					return 1;
				}

				chrx = chrToLowerCaseAscii(*x);
				chry = chrToLowerCaseAscii(*y);

				if(chrx!=chry) {
					return chrx - chry;
				};

				++x;
				++y;
				--ln;
			};

		};

		bool StringBaseWord::indexOfIgnoreCaseAscii(const word *the_string, size_t length, const word *patern, size_t patlength, size_t pos, size_t *result) {
			if (pos > length) {
				return false;
			}
			if (patlength > length) {
				return false;
			}
			for (*result = pos; *result <= length - patlength; *result = *result + 1) {
				if (compareIgnoreCaseNAscii(&the_string[*result], patern, patlength) == 0) {
					return true;
				}
			};
			return true;
		};

		bool StringBaseWord::indexOfFromEndIgnoreCaseAscii(const word *the_string, size_t length, const word *patern, size_t patlength, size_t pos, size_t *result) {
			if (pos > length) {
				return false;
			}
			if (pos < patlength) {
				pos = patlength;
			}
			for (*result = length - pos; *result >= 0; *result = *result - 1) {
				if (compareIgnoreCaseNAscii(&the_string[*result], patern, patlength) == 0) {
					return true;
				}
			};
			return false;
		};

	};
};


