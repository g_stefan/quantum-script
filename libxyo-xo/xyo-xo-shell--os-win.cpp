//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <windows.h>
#include <direct.h>
#include <io.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-shell.hpp"

namespace XYO {
	namespace XO {

		struct SShellFind_ {
			struct _finddata_t finddata;
			intptr_t hfind;
		};

		ShellFind::ShellFind() {
			isFile = false;
			isDirectory = false;
			isReadOnly = false;
			isValid_ = false;

			shellFind_ = new SShellFind_();
			shellFind_->hfind = -1;
			memset(&shellFind_->finddata, 0, sizeof (struct _finddata_t));
		};

		ShellFind::~ShellFind() {
			close();
			delete shellFind_;
		};

		bool ShellFind::findNext() {
			if(isValid_) {
				if (_findnext(shellFind_->hfind, &shellFind_->finddata) == 0) {
					name = shellFind_->finddata.name;
					isDirectory = (shellFind_->finddata.attrib & _A_SUBDIR) == _A_SUBDIR;
					isFile = !isDirectory;
					isReadOnly = (shellFind_->finddata.attrib & _A_RDONLY) == _A_RDONLY;
					return true;
				};
				close();
			};
			return false;
		};

		void ShellFind::close() {
			if (shellFind_->hfind != -1) {
				_findclose(shellFind_->hfind);
				shellFind_->hfind = -1L;
			};
			isValid_ = false;
		};

		bool ShellFind::find(const char *name_) {
			shellFind_->hfind = _findfirst(name_, &shellFind_->finddata);
			if (shellFind_->hfind != -1L) {
				isValid_ = true;
				name = shellFind_->finddata.name;
				isDirectory = (shellFind_->finddata.attrib & _A_SUBDIR) == _A_SUBDIR;
				isFile = !isDirectory;
				isReadOnly = (shellFind_->finddata.attrib & _A_RDONLY) == _A_RDONLY;
				return true;
			};
			return false;
		};

		bool Shell::chdir(const char *path) {
			return (_chdir(path) == 0);
		};

		bool Shell::rmdir(const char *path) {
			return (_rmdir(path) == 0);
		};

		bool Shell::mkdir(const char *path) {
			return (_mkdir(path) == 0);
		};

		bool Shell::getcwd(char *buffer, size_t bufferSize) {
			return (_getcwd(buffer, bufferSize) != NULL);
		};

		bool Shell::copyFile(const char *source, const char *destination) {
			return (CopyFile(source, destination, FALSE) != 0);
		};

		bool Shell::rename(const char *source, const char *destination) {
			return (::rename(source, destination) == 0);
		};

		bool Shell::remove(const char *file) {
			return (::remove(file) == 0);
		};

		int Shell::compareLastWriteTime(const char *fileA, const char *fileB) {
			HANDLE hFileA, hFileB;
			FILETIME lastWriteTimeA, lastWriteTimeB;

			hFileA = CreateFile(fileA, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			hFileB = CreateFile(fileB, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (hFileA == INVALID_HANDLE_VALUE) {
				if (hFileB == INVALID_HANDLE_VALUE) {
					return 0;
				} else {
					CloseHandle(hFileB);
					return -1;
				};
			} else {
				if (hFileB == INVALID_HANDLE_VALUE) {
					CloseHandle(hFileA);
					return 1;
				};
			};
			GetFileTime(hFileA, NULL, NULL, &lastWriteTimeA);
			GetFileTime(hFileB, NULL, NULL, &lastWriteTimeB);
			CloseHandle(hFileA);
			CloseHandle(hFileB);

			return (int) CompareFileTime(&lastWriteTimeA, &lastWriteTimeB);
		};

		int Shell::system(const char *cmd) {
			return ::system(cmd);
		};

		bool Shell::touch(const char *file) {
			FILE *in;
			char ch;

			in = fopen(file, "rb+");
			if (in != NULL) {
				if (fread(&ch, 1, 1, in) == 1) {
					fseek(in, 0, SEEK_SET);
					fwrite(&ch, 1, 1, in);
				};
				fclose(in);
				return true;
			} else {
				in = fopen(file, "wb+");
				if (in != NULL) {
					fclose(in);
					return true;
				};
			};
			return false;
		};

		bool Shell::fileExists(const char *file) {
			struct stat status;
			if (access(file, 0) == 0) {
				stat(file, &status);
				if (status.st_mode & S_IFREG) {
					return true;
				};
			};
			return false;
		};

		bool Shell::directoryExists(const char *directory) {
			struct stat status;
			if (access(directory, 0) == 0) {
				stat(directory, &status);
				if (status.st_mode & S_IFDIR) {
					return true;
				};
			};
			return false;
		};

		char *Shell::getenv(const char *name) {
			return ::getenv(name);
		};

		bool Shell::putenv(const char *env) {
			return (::putenv(env) == 0);
		};

		bool Shell::realpath(const char *fileNameIn,char *fileNameOut,long int filenameOutSize) {
			DWORD retV;
			retV=GetFullPathNameA(fileNameIn,filenameOutSize,fileNameOut,NULL);
			if(retV==0||retV>=filenameOutSize) {
				return false;
			};
			return true;
		};

		dword Shell::execute(const char *cmd) {
			DWORD ReturnValue;
			STARTUPINFO         SInfo;
			PROCESS_INFORMATION PInfo;
			BOOL    IsOk;

			ReturnValue=0;
			memset(&SInfo,0,sizeof(SInfo));
			memset(&PInfo,0,sizeof(PInfo));
			SInfo.cb=sizeof(SInfo);
			SInfo.dwFlags=STARTF_USESHOWWINDOW;
			SInfo.wShowWindow=SW_SHOW;

			IsOk=CreateProcessA(
				     NULL,
				     (LPSTR)cmd,
				     NULL,
				     NULL,
				     FALSE,
				     0,
				     NULL,
				     NULL,
				     &SInfo,
				     &PInfo
			     );
			if(IsOk) {
				WaitForSingleObject(PInfo.hProcess,INFINITE);
				GetExitCodeProcess(PInfo.hProcess,&ReturnValue);
				CloseHandle(PInfo.hThread);
				CloseHandle(PInfo.hProcess);
			};
			return ReturnValue;
		};

		dword Shell::executeWriteOutputToFile(const char *cmd,const char *out) {
			DWORD ReturnValue;
			SECURITY_ATTRIBUTES SecAttr;
			STARTUPINFO         SInfo;
			PROCESS_INFORMATION PInfo;
			BOOL    IsOk;
			HANDLE              hWrite2Std =NULL;
			HANDLE              hRead2Std  =NULL;
			HANDLE              hWriteProc =NULL;
			HANDLE              hReadProc  =NULL;

			HANDLE              h_out  =NULL;

			BYTE  buffer[2048];
			DWORD buffer_ln;
			DWORD total_buffer_ln;

			ReturnValue=0;

			h_out=CreateFile((LPSTR)out,CREATE_ALWAYS,FILE_SHARE_READ,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
			if(h_out==INVALID_HANDLE_VALUE) {
				return 0;
			};

			memset(&SInfo,0,sizeof(SInfo));
			memset(&PInfo,0,sizeof(PInfo));
			memset(&SecAttr,0,sizeof(SecAttr));

			SInfo.cb=sizeof(SInfo);
			SInfo.dwFlags=STARTF_USESHOWWINDOW|STARTF_USESTDHANDLES;
			SInfo.wShowWindow=SW_SHOW;

			SecAttr.nLength=sizeof(SecAttr);
			SecAttr.bInheritHandle=TRUE;
			SecAttr.lpSecurityDescriptor=NULL;

			CreatePipe(&hReadProc,&hWrite2Std,&SecAttr,2048);
			CreatePipe(&hRead2Std,&hWriteProc,&SecAttr,2048);

			SInfo.hStdInput  =hRead2Std;
			SInfo.hStdOutput =hWrite2Std;
			SInfo.hStdError  =hWrite2Std;

			IsOk=CreateProcessA(
				     NULL,
				     (LPSTR)cmd,
				     NULL,
				     NULL,
				     TRUE,
				     0,
				     NULL,
				     NULL,
				     &SInfo,
				     &PInfo
			     );

			if(IsOk) {

				for(;;) {
					if(PeekNamedPipe(hReadProc,NULL,0,NULL,&total_buffer_ln,NULL)) {
						if(total_buffer_ln>0) {
							while(total_buffer_ln>2048) {
								buffer_ln=2048;
								ReadFile(hReadProc,buffer,buffer_ln,&buffer_ln,NULL);
								WriteFile(h_out,buffer,buffer_ln,&buffer_ln,NULL);
								total_buffer_ln-=2048;
							};
							if(total_buffer_ln>0) {
								buffer_ln=total_buffer_ln;
								ReadFile(hReadProc,buffer,buffer_ln,&buffer_ln,NULL);
								WriteFile(h_out,buffer,buffer_ln,&buffer_ln,NULL);
							};
						} else {
							//test end of process
							if(WAIT_TIMEOUT!=WaitForSingleObject(PInfo.hProcess,1)) {

								if(PeekNamedPipe(hReadProc,NULL,0,NULL,&total_buffer_ln,NULL)) {


									while(total_buffer_ln>2048) {
										buffer_ln=2048;
										ReadFile(hReadProc,buffer,buffer_ln,&buffer_ln,NULL);
										WriteFile(h_out,buffer,buffer_ln,&buffer_ln,NULL);
										total_buffer_ln-=2048;
									};
									if(total_buffer_ln>0) {
										buffer_ln=total_buffer_ln;
										ReadFile(hReadProc,buffer,buffer_ln,&buffer_ln,NULL);
										WriteFile(h_out,buffer,buffer_ln,&buffer_ln,NULL);
									};

								};


								break;
							};
						};
					} else if(WAIT_OBJECT_0==WaitForSingleObject(PInfo.hThread,1)) {
						break;
					}
				};

				GetExitCodeProcess(PInfo.hProcess,&ReturnValue);
				CloseHandle(PInfo.hThread);
				CloseHandle(PInfo.hProcess);

				CloseHandle(hWriteProc);
				CloseHandle(hRead2Std);

				CloseHandle(hWrite2Std);
				CloseHandle(hReadProc);
				CloseHandle(h_out);

			};

			return ReturnValue;
		};

		dword Shell::executeHidden(const char *cmd) {
			DWORD ReturnValue;
			STARTUPINFO         SInfo;
			PROCESS_INFORMATION PInfo;
			BOOL    IsOk;

			ReturnValue=0;

			memset(&SInfo,0,sizeof(SInfo));
			memset(&PInfo,0,sizeof(PInfo));

			SInfo.cb=sizeof(SInfo);
			SInfo.dwFlags=STARTF_USESHOWWINDOW;
			SInfo.wShowWindow=SW_HIDE;

			IsOk=CreateProcessA(
				     NULL,
				     (LPSTR)cmd,
				     NULL,
				     NULL,
				     FALSE,
				     0,
				     NULL,
				     NULL,
				     &SInfo,
				     &PInfo
			     );
			if(IsOk) {
				WaitForSingleObject(PInfo.hProcess,INFINITE);
				GetExitCodeProcess(PInfo.hProcess,&ReturnValue);
				CloseHandle(PInfo.hThread);
				CloseHandle(PInfo.hProcess);
			};
			return ReturnValue;
		};

		Shell::ProcessId Shell::executeNoWait(const char *cmd) {
			STARTUPINFO         SInfo;
			PROCESS_INFORMATION PInfo;

			memset(&SInfo,0,sizeof(SInfo));
			memset(&PInfo,0,sizeof(PInfo));
			SInfo.cb=sizeof(SInfo);
			SInfo.dwFlags=STARTF_USESHOWWINDOW;
			SInfo.wShowWindow=SW_SHOW;

			if(CreateProcessA(
				   NULL,
				   (LPSTR)cmd,
				   NULL,
				   NULL,
				   FALSE,
				   0,
				   NULL,
				   NULL,
				   &SInfo,
				   &PInfo
			   )) {
				return PInfo.dwProcessId;
			};

			return 0;
		};

		Shell::ProcessId Shell::executeNoWait(const char *cmd,const char *cmdline) {
			STARTUPINFO         SInfo;
			PROCESS_INFORMATION PInfo;

			memset(&SInfo,0,sizeof(SInfo));
			memset(&PInfo,0,sizeof(PInfo));
			SInfo.cb=sizeof(SInfo);
			SInfo.dwFlags=STARTF_USESHOWWINDOW;
			SInfo.wShowWindow=SW_SHOW;

			if(CreateProcessA(
				   (LPSTR)cmd,
				   (LPSTR)cmdline,
				   NULL,
				   NULL,
				   FALSE,
				   0,
				   NULL,
				   NULL,
				   &SInfo,
				   &PInfo
			   )) {
				return PInfo.dwProcessId;
			};

			return 0;
		};

		Shell::ProcessId Shell::executeHiddenNoWait(const char *cmd) {
			STARTUPINFO         SInfo;
			PROCESS_INFORMATION PInfo;
			BOOL    IsOk;

			memset(&SInfo,0,sizeof(SInfo));
			memset(&PInfo,0,sizeof(PInfo));
			SInfo.cb=sizeof(SInfo);
			SInfo.dwFlags=STARTF_USESHOWWINDOW;
			SInfo.wShowWindow=SW_HIDE;

			if(CreateProcessA(
				   NULL,
				   (LPSTR)cmd,
				   NULL,
				   NULL,
				   FALSE,
				   0,
				   NULL,
				   NULL,
				   &SInfo,
				   &PInfo
			   )) {
				return PInfo.dwProcessId;
			};
			return 0;
		};

		bool Shell::isProcessTerminated(const Shell::ProcessId processId) {
			DWORD dwProcessId;
			HANDLE hProcess;
			DWORD dwExitCode;

			dwProcessId=(DWORD)processId;
			hProcess=OpenProcess(PROCESS_QUERY_INFORMATION|SYNCHRONIZE, FALSE, dwProcessId);
			if(hProcess!=NULL) {
				dwExitCode=0;
				if(::GetExitCodeProcess(hProcess, &dwExitCode)) {
					if(dwExitCode != STILL_ACTIVE) {}
					else {
						DWORD waitProcess = ::WaitForSingleObject(hProcess, 0);
						if(waitProcess == WAIT_OBJECT_0) {
						} else if(waitProcess == WAIT_TIMEOUT) {
							CloseHandle(hProcess);
							return false;

						};
					}
				};

				CloseHandle(hProcess);
			};
			return true;
		};

		static BOOL CALLBACK shellTerminateAppEnum__( HWND hwnd, LPARAM lParam ) {
			DWORD dwProcessId ;

			GetWindowThreadProcessId(hwnd, &dwProcessId) ;

			if(dwProcessId == (DWORD)lParam) {
				PostMessage(hwnd, WM_CLOSE, 0, 0) ;
			}

			return TRUE ;
		};

		bool Shell::terminateProcess(const Shell::ProcessId processId,const dword wait_) {

			DWORD dwProcessId;
			HANDLE hProcess;
			DWORD  dwExitCode;

			dwProcessId=(DWORD)processId;
			hProcess=OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE,
					     dwProcessId);
			if(hProcess) {
				EnumWindows((WNDENUMPROC)shellTerminateAppEnum__, (LPARAM) dwProcessId);

				if(WaitForSingleObject(hProcess, wait_)!=WAIT_OBJECT_0) {
					TerminateProcess(hProcess,0);
				};


				CloseHandle(hProcess);

			};

			return true;
		};

	};
};

#endif

