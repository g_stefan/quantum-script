//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_FILE_HPP
#define XYO_XO_FILE_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

#ifndef XYO_XO_IREAD_HPP
#include "xyo-xo-iread.hpp"
#endif

#ifndef XYO_XO_IWRITE_HPP
#include "xyo-xo-iwrite.hpp"
#endif

#ifndef XYO_XO_ISEEK_HPP
#include "xyo-xo-iseek.hpp"
#endif

namespace XYO {
	namespace XO {

		typedef struct SFile File_;

		class File :
			public virtual IRead,
			public virtual IWrite,
			public virtual ISeek {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(File);
			protected:
				File_ *value_;
				File *linkNext_;
				File *linkBack_;
				bool aquireOwner_;
			public:
				XYO_XO_EXPORT File();
				XYO_XO_EXPORT ~File();

				XYO_XO_EXPORT bool openRead(char *);
				XYO_XO_EXPORT bool openWrite(char *);
				XYO_XO_EXPORT bool openReadAndWrite(char *);
				XYO_XO_EXPORT bool openAppend(char *);

				XYO_XO_EXPORT bool openStdIn();
				XYO_XO_EXPORT bool openStdOut();
				XYO_XO_EXPORT bool openStdErr();

				XYO_XO_EXPORT bool isValid();

				XYO_XO_EXPORT void close();
				XYO_XO_EXPORT void flush();

				XYO_XO_EXPORT size_t read(void *output, size_t lungime);
				XYO_XO_EXPORT size_t write(void *input, size_t lungime);
				XYO_XO_EXPORT bool seekFromBegin(long int x);
				XYO_XO_EXPORT bool seek(long int x);
				XYO_XO_EXPORT bool seekFromEnd(long int x);
				XYO_XO_EXPORT long int seekTell();

				XYO_XO_EXPORT void aquireOwner();
				XYO_XO_EXPORT bool becomeOwner(File &file_);
				XYO_XO_EXPORT void releaseOwner();
		};

	};
};

#endif
