//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGBYTE_HPP
#define XYO_XO_STRINGBYTE_HPP

#ifndef XYO_XO_STRINGREFERENCEBYTE_HPP
#include "xyo-xo-stringreferencebyte.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringByte;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::StringByte>:
			public TMemoryObjectPoolActive<XO::StringByte> {};

	};
};

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class StringByte :
			public Object {
			protected:
				TPointer<StringReferenceByte> value_;
			public:

				inline StringByte() {
					value_ = StringReferenceByte::init();
				};

				inline ~StringByte() {
					value_.deleteObject();
				};

				inline StringByte(size_t initialSize, size_t chunk) {
					value_ = StringReferenceByte::init2(initialSize, chunk);
				};

				inline StringByte(const byte *value) {
					value_ = StringReferenceByte::from(value);
				};

				inline StringByte(const char *value) {
					value_ = StringReferenceByte::from((const byte *)value);
				};

				inline StringByte(const StringByte &value) {
					value_ = const_cast<StringByte &>(value).value_;
				};

				inline StringByte(StringByte &&value) {
					value_.setObjectWithOwnerTransfer(value.value_);
				};

				inline StringByte(TPointer<StringReferenceByte> &value) {
					value_ = value;
					if (!value_.isValid()) {
						value_=StringReferenceByte::init();
					};
				};

				inline StringByte &operator=(const byte *value) {
					value_ = StringReferenceByte::from(value);
					return *this;
				};

				inline StringByte &operator=(const char *value) {
					value_ = StringReferenceByte::from((const byte *)value);
					return *this;
				};


				inline StringByte &operator=(const StringByte &value) {
					value_ = value.value_;
					return *this;
				};

				inline StringByte &operator=(StringByte &&value) {
					value_ = value.value_;
					return *this;
				};


				inline StringByte &operator=(TPointer<StringReferenceByte> value) {
					value_ = value;
					if (!value_.isValid()) {
						value_ = StringReferenceByte::from2(value->value(),value->length());
					};
					return *this;

				};

				inline StringByte &operator+=(const byte *value) {
					value_ = StringReferenceByte::concatenate(value_, value);
					return *this;

				};

				inline StringByte &operator+=(const char *value) {
					value_ = StringReferenceByte::concatenate(value_, (const byte *)value);
					return *this;

				};


				inline StringByte &operator+=(StringByte &value) {
					value_ = StringReferenceByte::concatenate2(value_, value.value_);
					return *this;

				};


				inline StringByte &operator+=(const StringByte &value) {
					value_ = StringReferenceByte::concatenate2(value_, const_cast<StringByte &>(value).value_);
					return *this;

				};


				inline StringByte &operator<<(const StringByte &value) {
					value_ = StringReferenceByte::concatenate2(value_, const_cast<StringByte &>(value).value_);
					return *this;

				};

				inline StringByte &operator<<(const byte *value) {
					value_ = StringReferenceByte::concatenate(value_, value);
					return *this;

				};

				inline StringByte &operator<<(const char *value) {
					value_ = StringReferenceByte::concatenate(value_, (const byte *)value);
					return *this;

				};


				inline StringByte &operator<<(const byte value) {
					value_ = StringReferenceByte::concatenate_one(value_, value);
					return *this;

				};

				inline StringByte &operator<<(const char value) {
					value_ = StringReferenceByte::concatenate_one(value_, (const byte)value);
					return *this;

				};


				inline StringByte &operator+=(const byte value) {
					value_ = StringReferenceByte::concatenate_one(value_, value);
					return *this;

				};

				inline StringByte &operator+=(const char value) {
					value_ = StringReferenceByte::concatenate_one(value_, (const byte)value);
					return *this;

				};

				inline operator byte *() {
					return value_->value();
				};

				inline operator char *() {
					return (char *)(value_->value());
				};

				inline byte *value() {
					return value_->value();
				};

				inline byte *index(size_t x) {
					return &(value_->value())[x];
				};

				inline size_t length() {
					return value_->length();
				};

				inline size_t getSize() {
					return value_->getSize();
				};

				inline size_t getChunk() {
					return value_->getChunk();
				};

				inline void setChunk(size_t x) {
					value_->setChunk(x);
				};

				inline void setLength(size_t x) {
					value_->setLength(x);
				};

				inline void updateLength() {
					value_->updateLength();
				};

				inline TPointer<StringReferenceByte> reference() {
					return value_;
				};

				inline void set(const byte *value, size_t length) {
					value_ = StringReferenceByte::from2(value, length);
				};

				inline void set(const char *value, size_t length) {
					value_ = StringReferenceByte::from2((const byte *)value, length);
				};

				inline void set(StringByte &value, size_t length) {
					value_ = StringReferenceByte::from2((const byte *)value, length);
				};

				inline void set(const StringByte &value, size_t length) {
					value_ = StringReferenceByte::from2((const byte *)const_cast<StringByte &>(value), length);
				};

				inline void concatenate(const byte *value, size_t length) {
					value_ = StringReferenceByte::concatenate3(value_,value, length);
				};

				inline void concatenate(const char *value, size_t length) {
					value_ = StringReferenceByte::concatenate3(value_,(const byte *)value, length);
				};

				inline void concatenate(StringByte &value, size_t length) {
					value_ = StringReferenceByte::concatenate3(value_,(const byte *)value, length);
				};

				inline void concatenate(const StringByte &value, size_t length) {
					value_ = StringReferenceByte::concatenate3(value_,(const byte *)const_cast<StringByte &>(value), length);
				};

				inline byte &operator [](int x) {
					return (value_->value())[x];
				};

				inline byte getElement(size_t x) {
					return (value_->value())[x];
				};

				inline bool operator ==(const byte *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)==0);
				};

				inline bool operator ==(const char *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)==0);
				};


				inline bool operator ==(const StringByte &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringByte &>(x).value())==0);
				};

				inline bool operator !=(const byte *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)!=0);
				};

				inline bool operator !=(const char *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)!=0);
				};


				inline bool operator !=(const StringByte &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringByte &>(x).value())!=0);
				};

				inline bool operator <(const byte *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<0);
				};

				inline bool operator <(const char *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<0);
				};


				inline bool operator <(const StringByte &x) {
					return (strcmp((const char *)value_->value(),(const char *)const_cast<StringByte &>(x).value())<0);
				};

				inline bool operator <=(const byte *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<=0);
				};

				inline bool operator <=(const char *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<=0);
				};


				inline bool operator <=(const StringByte &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringByte &>(x).value())<=0);
				};

				inline bool operator >(const byte *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>0);
				};

				inline bool operator >(const char *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>0);
				};

				inline bool operator >(const StringByte &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringByte &>(x).value())>0);
				};

				inline bool operator >=(const byte *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>=0);
				};

				inline bool operator >=(const char *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>=0);
				};

				inline bool operator >=(const StringByte &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringByte &>(x).value())>=0);
				};

				inline int compare(const StringByte &value) const {
					return strcmp((const char *)(const_cast<TPointer<StringReferenceByte> &>(value_))->value(), (const char *)(const_cast<StringByte &>(value)).value());
				};

				inline void activeDestructor() {
					value_ = StringReferenceByte::init();
				};

				inline static void memoryInit() {
					TMemory<StringReferenceByte>::memoryInit();
				};
		};
	};
};

namespace XYO {
	namespace XY {
		template<>
		class TComparator<XO::StringByte> {
			public:
				typedef XO::StringByte T;

				inline static bool isEqualTo(const T &a, const T &b) {
					return (a.compare(b)==0);
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return (a.compare(b)!=0);
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return (a.compare(b)<0);
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return (a.compare(b)>0);
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return (a.compare(b)<=0);
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return (a.compare(b)>=0);
				};

				inline static int compare(const T &a, const T &b) {
					return a.compare(b);
				};
		};
	};
};

#endif

