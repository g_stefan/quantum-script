//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "xyo-xo-file.hpp"

namespace XYO {
	namespace XO {

		struct SFile {
			FILE *hFile;
			bool reclaim;
		};

		File::File() {
			value_ = new SFile;
			value_->hFile = NULL;
			value_->reclaim = false;
			linkNext_=NULL;
			linkBack_=NULL;
			aquireOwner_=false;
		};

		File::~File() {
			close();
			delete value_;
		};

		bool File::isValid() {
			if(linkNext_!=NULL) {
				return (linkNext_->value_->hFile != NULL);
			};
			return (value_->hFile != NULL);
		};

		bool File::openRead(char *nume) {
			close();
			value_->hFile = fopen(nume, "rb");
			if (value_->hFile != NULL) {
				value_->reclaim = true;
				return true;
			};
			return false;
		};

		bool File::openWrite(char *nume) {
			close();
			value_->hFile = fopen(nume, "wb");
			if (value_->hFile != NULL) {
				value_->reclaim = true;
				return true;
			};
			return false;
		};

		bool File::openReadAndWrite(char *nume) {
			close();
			value_->hFile = fopen(nume, "rwb");
			if (value_->hFile != NULL) {
				value_->reclaim = true;
				return true;
			};
			return false;
		};

		bool File::openAppend(char *nume) {
			close();
			value_->hFile = fopen(nume, "ab");
			if (value_->hFile != NULL) {
				value_->reclaim = true;
				return true;
			};
			return false;
		};

		bool File::openStdOut() {
			close();
			value_->reclaim = false;
			value_->hFile = stdout;
			return true;
		};

		bool File::openStdIn() {
			close();
			value_->reclaim = false;
			value_->hFile = stdin;
			return true;
		};

		bool File::openStdErr() {
			close();
			value_->reclaim = false;
			value_->hFile = stderr;
			return true;
		};

		size_t File::read(void *output, size_t lungime) {
			return fread(output, 1, lungime, value_->hFile);
		};

		size_t File::write(void *input, size_t lungime) {
			return fwrite(input, 1, lungime, value_->hFile);
		};

		bool File::seekFromBegin(long int cantitate) {
			return (fseek(value_->hFile, cantitate, SEEK_SET) == 0);
		};

		bool File::seek(long int cantitate) {
			return (fseek(value_->hFile, cantitate, SEEK_CUR) == 0);
		};

		bool File::seekFromEnd(long int cantitate) {
			return (fseek(value_->hFile, cantitate, SEEK_END) == 0);
		};

		long int File::seekTell() {
			return ftell(value_->hFile);
		};

		void File::close() {
			if (value_->reclaim) {
				if (value_->hFile != NULL) {
					fclose(value_->hFile);
				};
			};
			value_->reclaim = false;
			value_->hFile = NULL;
			if(linkNext_!=NULL) {
				if (linkNext_->value_->reclaim) {
					if (linkNext_->value_->hFile != NULL) {
						fclose(linkNext_->value_->hFile);
					};
				};
				linkNext_->value_->reclaim = false;
				linkNext_->value_->hFile = NULL;
				linkNext_=NULL;
			};
			if(linkBack_!=NULL) {
				linkBack_->linkNext_=NULL;
				linkBack_=NULL;
			};
			aquireOwner_=false;
		};

		void File::flush() {
			fflush(value_->hFile);
		};

		void File::aquireOwner() {
			aquireOwner_=true;
		};

		bool File::becomeOwner(File &file_) {
			close();
			if(file_.linkBack_) {
				return false;
			};
			value_=file_.value_;
			file_.value_ = new SFile;
			file_.value_->hFile = NULL;
			file_.value_->reclaim = false;
			if(file_.aquireOwner_) {
				file_.linkNext_=this;
				linkBack_=&file_;
				file_.aquireOwner_=false;
			};
			return true;
		};

		void File::releaseOwner() {
			if(linkNext_!=NULL) {
				linkNext_->linkBack_=NULL;
				linkNext_=NULL;
			};
		};

	};
};

