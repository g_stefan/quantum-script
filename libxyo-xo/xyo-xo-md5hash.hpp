//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_MD5HASH_HPP
#define XYO_XO_MD5HASH_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		class MD5Hash :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(MD5Hash);
			protected:

				XYO_XO_EXPORT static int r[];
				XYO_XO_EXPORT static dword k[];

				dword h0;
				dword h1;
				dword h2;
				dword h3;

				dword process[16];
				byte lastData[4];
				qword length;
				size_t stateM;
			public:
				XYO_XO_EXPORT MD5Hash();
				XYO_XO_EXPORT void processInit();
				XYO_XO_EXPORT void hashBlock(dword *w);
				XYO_XO_EXPORT void processBytes(const byte *toHash, size_t length);
				XYO_XO_EXPORT void processDone();
				XYO_XO_EXPORT String getHashAsHex();
				XYO_XO_EXPORT static String getHashString(String toHash);
				XYO_XO_EXPORT void toBytes(byte *buffer);
				XYO_XO_EXPORT static void hashStringToBytes(String toHash,byte *buffer);

				inline static void memoryInit() {
					TMemory<String>::memoryInit();
				};
		};

	};
};

#endif
