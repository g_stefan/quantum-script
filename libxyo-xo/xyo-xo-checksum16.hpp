//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_CHECKSUM16_HPP
#define XYO_XO_CHECKSUM16_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		class CheckSum16 {
			public:

				XYO_XO_EXPORT static word add(word sum1,word sum2);
				XYO_XO_EXPORT static word addX2(word sum1,byte byteHigh,byte byteLow);
				XYO_XO_EXPORT static word complement(word sum1);
				XYO_XO_EXPORT static word unAdd(word sum1,word sum2);
				XYO_XO_EXPORT static word unAddX2(word sum1,byte byteHigh,byte byteLow);
				XYO_XO_EXPORT static word unComplement(word sum1);
				XYO_XO_EXPORT static word reAdd(word sum1,byte byteHigh,byte byteLow);
				XYO_XO_EXPORT static word reUnAdd(word sum1,byte byteHigh,byte byteLow);

		};

	};
};

#endif

