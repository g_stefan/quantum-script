//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

.solution("libxyo-xo",function() {

	.option("licence","mit");

	.project("libxyo-xo","version",function() {
		.option("version","xyo-version", {
			type:"xyo-cpp",
			sourceBegin:
			"#ifndef XYO_XO__EXPORT_HPP\r\n"+
			"#include \"xyo-xo--export.hpp\"\r\n"+
			"#endif\r\n"+
			"\r\n"+
			"namespace Lib {\r\n"+
			"\tnamespace XYO{\r\n"+
			"\t\tnamespace XO{\r\n",
			sourceEnd:
			"\t\t};\r\n"+
			"\t};\r\n"+
			"};\r\n",
			codeExport:"XYO_XO_EXPORT",
			lineBegin:"\t\t\t"
		});
	});

	.project("libxyo-xo","lib",function() {
		.file("source",["*.hpp","*.cpp"]);

		.dependencyOption("install-include","xyo-xo");

		.dependency("libxyo-xy","libxyo-xy","lib");
	});

	.project("libxyo-xo","dll",function() {
		.file("source",["*.hpp","*.cpp","*.rc"]);

		.option("define","XYO_XO_INTERNAL");
		.option("sign","xyo-security");
		.dependencyOption("install-include","xyo-xo");
		.dependency("libxyo-xy","libxyo-xy","dll");
	});

	.project("libxyo-xo","install",function() {
		.install("include","*.hpp","xyo-xo");
	});

});

