//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "xyo-xo-datetime.hpp"

namespace XYO {
	namespace XO {

		DateTime::DateTime() {
			value_ = new SYSTEMTIME;
			GetLocalTime((SYSTEMTIME *)value_);
		};

		DateTime::~DateTime() {
			delete ((SYSTEMTIME *)value_);
		};

		word DateTime::getYear() {
			return ((SYSTEMTIME *)value_)->wYear;
		};

		word DateTime::getMonth() {
			return ((SYSTEMTIME *)value_)->wMonth;
		};

		word DateTime::getDay() {
			return ((SYSTEMTIME *)value_)->wDay;
		};

		word DateTime::getDayOfWeek() {
			return ((SYSTEMTIME *)value_)->wDayOfWeek;
		};

		word DateTime::getHour() {
			return ((SYSTEMTIME *)value_)->wHour;
		};

		word DateTime::getMinute() {
			return ((SYSTEMTIME *)value_)->wMinute;
		};

		word DateTime::getSecond() {
			return ((SYSTEMTIME *)value_)->wSecond;
		};

		word DateTime::getMilliseconds() {
			return ((SYSTEMTIME *)value_)->wMilliseconds;
		};

		void DateTime::setLocalTime() {
			SetLocalTime((SYSTEMTIME *)value_);
		};

		void DateTime::setYear(word value) {
			((SYSTEMTIME *)value_)->wYear=value;
		};

		void DateTime::setMonth(word value) {
			((SYSTEMTIME *)value_)->wMonth=value;
		};

		void DateTime::setDay(word value) {
			((SYSTEMTIME *)value_)->wDay=value;
		};

		void DateTime::setDayOfWeek(word value) {
			((SYSTEMTIME *)value_)->wDayOfWeek=value;
		};

		void DateTime::setHour(word value) {
			((SYSTEMTIME *)value_)->wHour=value;
		};

		void DateTime::setMinute(word value) {
			((SYSTEMTIME *)value_)->wMinute=value;
		};

		void DateTime::setSecond(word value) {
			((SYSTEMTIME *)value_)->wSecond=value;
		};

		void DateTime::setMilliseconds(word value) {
			((SYSTEMTIME *)value_)->wMilliseconds=value;
		};

		void DateTime::copy(DateTime &in) {
			memcpy(value_,in.value_,sizeof(SYSTEMTIME));
		};

		void DateTime::plus(DateTime &in) {
			FILETIME a,b;
			ULARGE_INTEGER ax,bx;

			SystemTimeToFileTime((SYSTEMTIME *)value_,&a);
			SystemTimeToFileTime((SYSTEMTIME *)in.value_,&b);

			ax.u.LowPart=a.dwLowDateTime;
			ax.u.HighPart=a.dwHighDateTime;

			bx.u.LowPart=b.dwLowDateTime;
			bx.u.HighPart=b.dwHighDateTime;

			ax.QuadPart+=bx.QuadPart;

			a.dwLowDateTime=ax.u.LowPart;
			a.dwHighDateTime=ax.u.HighPart;

			FileTimeToSystemTime(&a,(SYSTEMTIME *)value_);
		};

		void DateTime::minus(DateTime &in) {
			FILETIME a,b;
			ULARGE_INTEGER ax,bx;

			SystemTimeToFileTime((SYSTEMTIME *)value_,&a);
			SystemTimeToFileTime((SYSTEMTIME *)in.value_,&b);

			ax.u.LowPart=a.dwLowDateTime;
			ax.u.HighPart=a.dwHighDateTime;

			bx.u.LowPart=b.dwLowDateTime;
			bx.u.HighPart=b.dwHighDateTime;

			ax.QuadPart-=bx.QuadPart;

			a.dwLowDateTime=ax.u.LowPart;
			a.dwHighDateTime=ax.u.HighPart;

			FileTimeToSystemTime(&a,(SYSTEMTIME *)value_);
		};

		int DateTime::compare(DateTime &in) {
			FILETIME a,b;
			ULARGE_INTEGER ax,bx;

			SystemTimeToFileTime((SYSTEMTIME *)value_,&a);
			SystemTimeToFileTime((SYSTEMTIME *)in.value_,&b);

			ax.u.LowPart=a.dwLowDateTime;
			ax.u.HighPart=a.dwHighDateTime;

			bx.u.LowPart=b.dwLowDateTime;
			bx.u.HighPart=b.dwHighDateTime;

			if(ax.QuadPart<bx.QuadPart) {
				return -1;
			};
			if(ax.QuadPart==bx.QuadPart) {
				return 0;
			};
			return 1;
		};

		static void unixTimeToFileTime_(time_t t, FILETIME &ft) {
			LONGLONG ll;
			ll = Int32x32To64(t, 10000000) + 116444736000000000ui64;
			ft.dwLowDateTime = (DWORD)ll;
			ft.dwHighDateTime = (DWORD)(ll >> 32);
		};

		static void fileTimeToUnixTime_(FILETIME &ft, time_t &t) {
			LONGLONG ll;
			ll = (((LONGLONG)(ft.dwHighDateTime)) << 32) + ft.dwLowDateTime;
			t = (time_t)((ll - (116444736000000000ui64))/10000000ui64);
		};

		static void fileTimeToUnixTimeMilliseconds_(FILETIME &ft, time_t &t) {
			LONGLONG ll;
			ll = (((LONGLONG)(ft.dwHighDateTime)) << 32) + ft.dwLowDateTime;
			t = (time_t)((ll - (116444736000000000ui64))/10000000ui64);
		};

		qword DateTime::toUnixTime() {
			time_t t;
			FILETIME ft;
			SystemTimeToFileTime((SYSTEMTIME *)value_, &ft);
			fileTimeToUnixTime_(ft, t);
			return (qword)t;
		};

		void DateTime::fromUnixTime(qword t) {
			FILETIME ft;
			unixTimeToFileTime_(t, ft);
			FileTimeToSystemTime(&ft, (SYSTEMTIME *)value_);
		};

		qword DateTime::timestampInMilliseconds() {
			SYSTEMTIME valueTmp_;
			GetLocalTime(&valueTmp_);
			time_t t;
			FILETIME ft;
			SystemTimeToFileTime(&valueTmp_, &ft);
			fileTimeToUnixTime_(ft, t);
			return ((qword)t)*(1000ui64)+((qword)valueTmp_.wMilliseconds);
		};

	};
};


#endif
