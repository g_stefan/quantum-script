//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_ISEEK_HPP
#define XYO_XO_ISEEK_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class ISeek :
			public virtual Object {
				XYO_XY_INTERFACE(ISeek);

			public:
				virtual bool seekFromBegin(long int x) = 0;
				virtual bool seek(long int x) = 0;
				virtual bool seekFromEnd(long int x) = 0;
				virtual long int seekTell() = 0;
		};

	};
};

#endif
