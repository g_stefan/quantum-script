//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-shellx.hpp"
#include "xyo-xo-file.hpp"
#include "xyo-xo-string.hpp"

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		String ShellX::getFileName(String fileName) {
			size_t index;
			if(StringX::indexOfFromEnd(fileName, "/", 0,index)) {
				return StringX::substring(fileName,index+1);
			};
			if(StringX::indexOfFromEnd(fileName, "\\", 0,index)) {
				return StringX::substring(fileName,index+1);
			};
			return fileName;
		};

		String ShellX::getFileExtension(String fileName) {
			size_t index;
			if(StringX::indexOfFromEnd(fileName, ".", 0,index)) {
				return StringX::substring(fileName,index+1);
			};
			return "";
		};

		String ShellX::getFileBasename(String fileName) {
			size_t index;
			if(StringX::indexOfFromEnd(fileName, ".", 0,index)) {
				return StringX::substring(fileName,0,index);
			};
			return fileName;
		};

		String ShellX::getFilePath(String fileName) {
			size_t index;
			if(StringX::indexOfFromEnd(fileName, "/", 0,index)) {
				return StringX::substring(fileName,0,index);
			};
			if(StringX::indexOfFromEnd(fileName, "\\", 0,index)) {
				return StringX::substring(fileName,0,index);
			};
			return "";
		};

		String ShellX::getFilePathX(String fileName) {
			size_t index;
			if(StringX::indexOfFromEnd(fileName, "/", 0,index)) {
				return StringX::substring(fileName,0,index+1);
			};
			if(StringX::indexOfFromEnd(fileName, "\\", 0,index)) {
				return StringX::substring(fileName,0,index+1);
			};
			return "";
		};

		bool ShellX::fileGetContents(String fileName,String &output) {
			File file;
			if (file.openRead(fileName)) {
				size_t size;

				file.seekFromEnd(0);
				size = file.seekTell();
				file.seekFromBegin(0);

				String result(size + 1, 0);
				result.setLength(size);
				if (file.read(result.index(0), size) == size) {
					file.close();
					result[size] = 0;
					output=result;
					return true;
				};
				file.close();
			};
			return false;
		};

		bool ShellX::fileGetContents(String fileName,StringWord &output) {
			File file;
			if (file.openRead(fileName)) {
				size_t size;

				file.seekFromEnd(0);
				size = file.seekTell();
				file.seekFromBegin(0);

				StringWord result((size/2) + 2, 0);
				result.setLength((size/2));
				if (file.read((byte *)(result.index(0)), size) == size) {
					file.close();
					result[(size/2)] = 0;
					output=result;
					return true;
				};
				file.close();
			};
			return false;
		};

		bool ShellX::fileGetContents(String fileName,StringDWord &output) {
			File file;
			if (file.openRead(fileName)) {
				size_t size;

				file.seekFromEnd(0);
				size = file.seekTell();
				file.seekFromBegin(0);

				StringDWord result((size/4) + 4, 0);
				result.setLength((size/4));
				if (file.read((byte *)(result.index(0)), size) == size) {
					file.close();
					result[(size/4)] = 0;
					output=result;
					return true;
				};
				file.close();
			};
			return false;
		};

		bool ShellX::filePutContents(String fileName,String value) {
			File file;
			if (file.openWrite(fileName)) {
				if (file.write(value.index(0), value.length()) == value.length()) {
					file.close();
					return true;
				};
				file.close();
			};
			return false;
		};

		bool ShellX::filePutContents(String fileName,StringWord value) {
			File file;
			if (file.openWrite(fileName)) {
				if (file.write((byte *)(value.index(0)), value.length()*2) == value.length()*2) {
					file.close();
					return true;
				};
				file.close();
			};
			return false;
		};

		bool ShellX::filePutContents(String fileName,StringDWord value) {
			File file;
			if (file.openWrite(fileName)) {
				if (file.write((byte *)(value.index(0)), value.length()*4) == value.length()*4) {
					file.close();
					return true;
				};
				file.close();
			};
			return false;
		};

		bool ShellX::removeEmptyDir(String dirName) {
			bool found;
			String find_;
			ShellFind findX;

			found=false;
			find_=dirName;
			find_+="/*";
			for(findX.find(find_); findX.isValid(); findX.findNext()) {
				if(findX.isDirectory) {
					if(strcmp(findX.name,"..")) {
						continue;
					};
					if(strcmp(findX.name,".")) {
						continue;
					};
					found=true;
					break;
				};
				found=true;
				break;
			};
			if(!found) {
				return Shell::rmdir(dirName);
			};
			return false;
		};

		bool ShellX::removeFile(String file) {
			if(Shell::fileExists(file)) {
				return Shell::remove(file);
			};
			return false;
		};

		bool ShellX::removeFileAndDirectoryIfEmpty(String target) {
			if(target.length()>0) {
				if(removeFile(target)) {
					target=getFilePath(target);
					if(target.length()>0) {
						return removeEmptyDir(target);
					};
				};
			};
			return false;
		};

		bool ShellX::touchIfExists(String file) {
			if(Shell::fileExists(file)) {
				return Shell::touch(file);
			};
			return false;
		};

		bool ShellX::isEnv(String name, String value) {
			char *v=Shell::getenv(name);
			if(strcmp(v,value)==0) {
				return true;
			};
			return false;
		};

		String ShellX::getEnv(String name) {
			String retV;
			char *result_ = Shell::getenv(name);
			if(result_) {
				retV=result_;
			};
			return retV;
		};

		String ShellX::getCwd() {
			String retV(2048, 0);
			if (Shell::getcwd(retV, 2040)) {
				retV.setLength(StringBase::length(retV));
			};
			return retV;
		};

	};
};

