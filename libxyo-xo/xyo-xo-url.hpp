//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_URL_HPP
#define XYO_XO_URL_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class URL {
			public:
				XYO_XO_EXPORT static String decodeComponent(String value);
				XYO_XO_EXPORT static String encodeComponent(String value);
				XYO_XO_EXPORT static bool getSchemeName(String url,String &out);
				XYO_XO_EXPORT static bool getHostNameAndPort(String url,String &out);
				XYO_XO_EXPORT static bool getUsernameAndPassword(String url,String &out);
				XYO_XO_EXPORT static bool getPathAndFileName(String url,String &out);
				XYO_XO_EXPORT static bool getQuery(String url,String &out);
		};

	};
};

#endif

