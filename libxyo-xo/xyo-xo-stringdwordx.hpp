//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGDWORDX_HPP
#define XYO_XO_STRINGDWORDX_HPP

#ifndef XYO_XO_STRINGDWORD_HPP
#include "xyo-xo-stringdword.hpp"
#endif

namespace XYO {
	namespace XO {

		class StringDWordX {
			public:
				XYO_XO_EXPORT static StringDWord trimWithElement(StringDWord, StringDWord);
				XYO_XO_EXPORT static StringDWord replace(StringDWord, StringDWord, StringDWord);
				XYO_XO_EXPORT static StringDWord nilAtFirstFromEnd(StringDWord, StringDWord);
				XYO_XO_EXPORT static StringDWord substring(StringDWord, size_t start, size_t length);
				XYO_XO_EXPORT static StringDWord substring(StringDWord, size_t start);

				inline  static bool indexOf(StringDWord a, StringDWord b, size_t start, size_t &index) {
					return StringBaseDWord::indexOf(a, a.length(), b, b.length(), start, index);
				};

				inline static bool indexOfFromEnd(StringDWord a, StringDWord b, size_t start, size_t &index) {
					return StringBaseDWord::indexOfFromEnd(a, a.length(), b, b.length(), start, index);
				};

				XYO_XO_EXPORT static StringDWord toLowerCaseAscii(StringDWord value);
				XYO_XO_EXPORT static StringDWord toUpperCaseAscii(StringDWord value);
				XYO_XO_EXPORT static bool matchAscii(StringDWord text, StringDWord sig);
				XYO_XO_EXPORT static bool split2FromBegin(StringDWord text, StringDWord sig,StringDWord &firstPart,StringDWord &secondPart);

		};


	};
};

#endif

