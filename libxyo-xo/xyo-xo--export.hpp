//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO__EXPORT_HPP
#define XYO_XO__EXPORT_HPP

#ifndef XYO_XY__EXPORT_HPP
#include "xyo-xy--export.hpp"
#endif

#ifdef XYO_DYNAMIC_LINK
#   ifdef  XYO_XO_INTERNAL
#       define XYO_XO_EXPORT XYO_EXPORT
#   else
#       define XYO_XO_EXPORT XYO_IMPORT
#   endif
#else
#   define XYO_XO_EXPORT
#endif

#endif

