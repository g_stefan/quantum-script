//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-error.hpp"


namespace XYO {
	namespace XO {

		Error::Error() {
		};

		Error::Error(const String message_) {
			message=message_;
		};

		Error::Error(const Error &e) {
			message=e.message;
		};

		Error::Error(Error &&e) {
			message=e.message;
		};

	};
};

