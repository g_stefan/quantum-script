//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//
#include <stdio.h>
#include <string.h>

#include "xyo-xo-checksum16.hpp"


namespace XYO {
	namespace XO {

		word CheckSum16::add(word sum1,word sum2) {
			dword sumX;
			sumX=sum1;
			sumX+=sum2;
			if(sumX&0xFFFF0000) {
				++sumX;
			}
			return ((word)(sumX&0x0000FFFF));
		};

		word CheckSum16::addX2(word sum1,byte byteHigh,byte byteLow) {
			word xword;
			xword=byteHigh;
			xword<<=8;
			xword|=byteLow;
			return add(sum1,xword);
		};

		word CheckSum16::complement(word sum1) {
			return (sum1^0xFFFF);
		};

		word CheckSum16::unAdd(word sum1,word sum2) {
			dword sumX;
			sumX=sum1;
			sumX|=0x00010000;
			--sumX;
			sumX-=sum2;
			if(sumX&0xFFFF0000) {
				++sumX;
			}
			return ((word)(sumX&0x0000FFFF));
		};

		word CheckSum16::unAddX2(word sum1,byte byteHigh,byte byteLow) {
			word xword;
			xword=byteHigh;
			xword<<=8;
			xword|=byteLow;
			return unAdd(sum1,xword);
		};

		word CheckSum16::unComplement(word sum1) {
			return (sum1^0xFFFF);
		};

		word CheckSum16::reAdd(word sum1,byte byteHigh,byte byteLow) {
			word sum2;
			sum2=unComplement(sum1);
			sum2=addX2(sum2,byteHigh,byteLow);
			return complement(sum2);
		};

		word CheckSum16::reUnAdd(word sum1,byte byteHigh,byte byteLow) {
			word sum2;
			sum2=unComplement(sum1);
			sum2=unAddX2(sum2,byteHigh,byteLow);
			return complement(sum2);
		};

	};
};


