//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_MEMORYFILEREAD_HPP
#define XYO_XO_MEMORYFILEREAD_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

#ifndef XYO_XO_IREAD_HPP
#include "xyo-xo-iread.hpp"
#endif

#ifndef XYO_XO_ISEEK_HPP
#include "xyo-xo-iseek.hpp"
#endif


namespace XYO {
	namespace XO {

		class MemoryFileRead :
			public virtual IRead,
			public virtual ISeek {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(MemoryFileRead);
			protected:
				const byte *data_;
				size_t size_;
				size_t pos_;
			public:
				XYO_XO_EXPORT MemoryFileRead();
				XYO_XO_EXPORT ~MemoryFileRead();

				XYO_XO_EXPORT bool openRead(const byte *data, size_t size);

				inline bool openRead(const char *data, size_t size) {
					return openRead((const byte *)data,size);
				};

				XYO_XO_EXPORT bool isValid();

				XYO_XO_EXPORT void close();

				XYO_XO_EXPORT size_t read(void *output, size_t lungime);
				XYO_XO_EXPORT bool seekFromBegin(long int x);
				XYO_XO_EXPORT bool seek(long int x);
				XYO_XO_EXPORT bool seekFromEnd(long int x);
				XYO_XO_EXPORT long int seekTell();

		};

	};
};

#endif
