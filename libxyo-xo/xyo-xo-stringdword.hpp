//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGDWORD_HPP
#define XYO_XO_STRINGDWORD_HPP

#ifndef XYO_XO_STRINGREFERENCEDWORD_HPP
#include "xyo-xo-stringreferencedword.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringDWord;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::StringDWord>:
			public TMemoryObjectPoolActive<XO::StringDWord> {};

	};
};

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class StringDWord :
			public Object {
			protected:
				TPointer<StringReferenceDWord> value_;
			public:

				inline StringDWord() {
					value_ = StringReferenceDWord::init();
				};

				inline ~StringDWord() {
					value_.deleteObject();
				};

				inline StringDWord(size_t initialSize, size_t chunk) {
					value_ = StringReferenceDWord::init2(initialSize, chunk);
				};

				inline StringDWord(const dword *value) {
					value_ = StringReferenceDWord::from(value);
				};

				inline StringDWord(const StringDWord &value) {
					value_ = const_cast<StringDWord &>(value).value_;
				};

				inline StringDWord(StringDWord &&value) {
					value_.setObjectWithOwnerTransfer(value.value_);
				};

				inline StringDWord(TPointer<StringReferenceDWord> &value) {
					value_ = value;
					if (!value_.isValid()) {
						value_=StringReferenceDWord::init();
					};
				};

				inline StringDWord &operator=(const dword *value) {
					value_ = StringReferenceDWord::from(value);
					return *this;
				};

				inline StringDWord &operator=(const StringDWord &value) {
					value_ = value.value_;
					return *this;
				};

				inline StringDWord &operator=(StringDWord &&value) {
					value_ = value.value_;
					return *this;
				};


				inline StringDWord &operator=(TPointer<StringReferenceDWord> value) {
					value_ = value;
					if (!value_.isValid()) {
						value_ = StringReferenceDWord::from2(value->value(),value->length());
					};
					return *this;

				};

				inline StringDWord &operator+=(const dword *value) {
					value_ = StringReferenceDWord::concatenate(value_, value);
					return *this;

				};

				inline StringDWord &operator+=(StringDWord &value) {
					value_ = StringReferenceDWord::concatenate2(value_, value.value_);
					return *this;

				};

				inline StringDWord &operator<<(const StringDWord &value) {
					value_ = StringReferenceDWord::concatenate2(value_, const_cast<StringDWord &>(value).value_);
					return *this;

				};

				inline StringDWord &operator<<(const dword *value) {
					value_ = StringReferenceDWord::concatenate(value_, value);
					return *this;

				};

				inline StringDWord &operator<<(const dword value) {
					value_ = StringReferenceDWord::concatenate_one(value_, value);
					return *this;

				};

				inline StringDWord &operator+=(const dword value) {
					value_ = StringReferenceDWord::concatenate_one(value_, value);
					return *this;

				};

				inline operator dword *() {
					return value_->value();
				};

				inline dword *value() {
					return value_->value();
				};

				inline dword *index(size_t x) {
					return &(value_->value())[x];
				};

				inline size_t length() {
					return value_->length();
				};

				inline size_t getSize() {
					return value_->getSize();
				};

				inline size_t getChunk() {
					return value_->getChunk();
				};

				inline void setChunk(size_t x) {
					value_->setChunk(x);
				};

				inline void setLength(size_t x) {
					value_->setLength(x);
				};

				inline void updateLength() {
					value_->updateLength();
				};

				inline TPointer<StringReferenceDWord> reference() {
					return value_;
				};

				inline void set(const dword *value, size_t length) {
					value_ = StringReferenceDWord::from2(value, length);
				};

				inline void concatenate(const dword *value, size_t length) {
					value_ = StringReferenceDWord::concatenate3(value_,value, length);
				};

				inline dword &operator [](int x) {
					return (value_->value())[x];
				};

				inline dword getElement(size_t x) {
					return (value_->value())[x];
				};

				inline bool operator ==(const dword *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)==0);
				};

				inline bool operator ==(const StringDWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringDWord &>(x).value())==0);
				};

				inline bool operator !=(const dword *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)!=0);
				};

				inline bool operator !=(const StringDWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringDWord &>(x).value())!=0);
				};

				inline bool operator <(const dword *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<0);
				};

				inline bool operator <(const StringDWord &x) {
					return (strcmp((const char *)value_->value(),(const char *)const_cast<StringDWord &>(x).value())<0);
				};

				inline bool operator <=(const dword *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<=0);
				};

				inline bool operator <=(const StringDWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringDWord &>(x).value())<=0);
				};

				inline bool operator >(const dword *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>0);
				};

				inline bool operator >(const StringDWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringDWord &>(x).value())>0);
				};

				inline bool operator >=(const dword *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>=0);
				};

				inline bool operator >=(const StringDWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringDWord &>(x).value())>=0);
				};

				inline int compare(const StringDWord &value) const {
					return strcmp((const char *)(const_cast<TPointer<StringReferenceDWord> &>(value_))->value(), (const char *)(const_cast<StringDWord &>(value)).value());
				};

				inline void activeDestructor() {
					value_ = StringReferenceDWord::init();
				};

				inline static void memoryInit() {
					TMemory<StringReferenceDWord>::memoryInit();
				};
		};
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TComparator<XO::StringDWord> {
			public:
				typedef XO::StringDWord T;

				inline static bool isEqualTo(const T &a, const T &b) {
					return (a.compare(b)==0);
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return (a.compare(b)!=0);
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return (a.compare(b)<0);
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return (a.compare(b)>0);
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return (a.compare(b)<=0);
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return (a.compare(b)>=0);
				};

				inline static int compare(const T &a, const T &b) {
					return a.compare(b);
				};
		};
	};
};

#endif

