//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-ip4.hpp"

namespace XYO {
	namespace XO {

		using namespace XYO::XY;


		IP4::IP4() {
			memset(ip,0,4);
		};

		dword IP4::toDWord() {
			dword retV;
			retV=ip[3];
			retV<<=8;
			retV|=ip[2];
			retV<<=8;
			retV|=ip[1];
			retV<<=8;
			retV|=ip[0];
			return retV;

		};

		void IP4::fromDWord(dword ip_) {
			ip[0]=(byte)(ip_&0x000000FF);
			ip_>>=8;
			ip[1]=(byte)(ip_&0x000000FF);
			ip_>>=8;
			ip[2]=(byte)(ip_&0x000000FF);
			ip_>>=8;
			ip[3]=(byte)(ip_&0x000000FF);
		};

		void IP4::copy(IP4 &ip_) {
			memcpy(ip,ip_.ip,4);
		};

		bool IP4::isInTheSameNetwork(IP4 &ip_,IP4 &mask_) {
			if(
				((ip[0]&mask_.ip[0])==(ip_.ip[0]&mask_.ip[0]))&&
				((ip[1]&mask_.ip[1])==(ip_.ip[1]&mask_.ip[1]))&&
				((ip[2]&mask_.ip[2])==(ip_.ip[2]&mask_.ip[2]))&&
				((ip[3]&mask_.ip[3])==(ip_.ip[3]&mask_.ip[3]))
			) {
				return true;
			};
			return false;
		};

	};
};


