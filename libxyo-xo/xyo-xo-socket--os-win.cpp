//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <windows.h>
#include <winsock.h>

#include "xyo-xo-socket.hpp"

namespace XYO {
	namespace XO {

		Socket::Socket() {
			this_=(void *)INVALID_SOCKET;
			linkNext_=NULL;
			linkBack_=NULL;
			aquireOwner_=false;
		};

		Socket::~Socket() {
			close();
		};

		bool Socket::isValid() {
			if(linkNext_!=NULL) {
				return (((SOCKET)linkNext_->this_)!=INVALID_SOCKET);
			};
			return (((SOCKET)this_)!=INVALID_SOCKET);
		};

		bool Socket::openClient(IPAddress4 &adr_) {
			struct sockaddr_in addr;
			close();

			if(!Net::isValid()) {
				return false;
			};

			memset(&addr,0,sizeof(struct sockaddr));
			addr.sin_family=AF_INET;
			addr.sin_port=htons(adr_.port);
			addr.sin_addr.s_addr=adr_.ip.toDWord();

			this_=(void *)socket(AF_INET,SOCK_STREAM,0);
			if((SOCKET)this_!=INVALID_SOCKET) {
				if(connect((SOCKET)this_,(struct sockaddr *)&addr,sizeof(struct sockaddr))==0) {
					address.copy(adr_);
					return true;
				};
				closesocket((SOCKET)this_);
				this_=(void *)INVALID_SOCKET;
			};
			return false;
		};

		bool Socket::openServer(IPAddress4 &adr_) {
			struct sockaddr_in addr;
			close();

			if(!Net::isValid()) {
				return false;
			};

			memset(&addr,0,sizeof(struct sockaddr));
			addr.sin_family=AF_INET;
			addr.sin_port=htons(adr_.port);
			addr.sin_addr.s_addr=adr_.ip.toDWord();

			this_=(void *)socket(AF_INET,SOCK_STREAM,0);
			if((SOCKET)this_!=INVALID_SOCKET) {
				if(bind((SOCKET)this_,(struct sockaddr *)&addr,sizeof(struct sockaddr))==0) {
					address.copy(adr_);
					return true;
				};
				closesocket((SOCKET)this_);
				this_=(void *)INVALID_SOCKET;
			};
			return false;
		};

		bool Socket::listen(word queue_) {
			if((SOCKET)this_==INVALID_SOCKET) {
				return false;
			};
			return ((::listen((SOCKET)this_,queue_))!=INVALID_SOCKET);
		};

		bool Socket::accept(Socket &socket_) {
			socket_.close();
			struct sockaddr_in addr;
			int _addrlen;

			if((SOCKET)this_==INVALID_SOCKET) {
				return false;
			};

			memset(&addr,0,sizeof(struct sockaddr));
			_addrlen=sizeof(struct sockaddr);
			addr.sin_family=AF_INET;

			socket_.this_=(void *)::accept((SOCKET)this_,(struct sockaddr *)&addr,&_addrlen);
			if(((SOCKET)socket_.this_)!=INVALID_SOCKET) {
				socket_.address.port=addr.sin_port;
				socket_.address.ip.fromDWord(addr.sin_addr.s_addr);
				return true;
			};
			return false;
		};

		void Socket::close() {
			if(((SOCKET)this_)!=INVALID_SOCKET) {
				shutdown((SOCKET)this_,2);
				closesocket((SOCKET)this_);
				this_=(void *)INVALID_SOCKET;
			};
			if(linkNext_!=NULL) {
				if(((SOCKET)linkNext_->this_)!=INVALID_SOCKET) {
					shutdown((SOCKET)linkNext_->this_,2);
					closesocket((SOCKET)linkNext_->this_);
					linkNext_->this_=(void *)INVALID_SOCKET;
				};
				linkNext_=NULL;
			};
			if(linkBack_!=NULL) {
				linkBack_->linkNext_=NULL;
				linkBack_=NULL;
			};
			address.ip.ip[0]=0;
			address.ip.ip[1]=0;
			address.ip.ip[2]=0;
			address.ip.ip[3]=0;
			address.port=0;
			aquireOwner_=false;
		};

		size_t Socket::read(void *output, size_t lungime) {
			long int recvLn;
			if((SOCKET)this_==INVALID_SOCKET) {
				return 0;
			};

			recvLn=recv((SOCKET)this_,(char *)output,lungime,0);
			if(recvLn==SOCKET_ERROR) {
				return 0;
			};
			return (recvLn);
		};

		size_t Socket::write(void *input, size_t lungime) {
			size_t idx=0;
			long int  ln=lungime;
			long int  sndln;

			if((SOCKET)this_==INVALID_SOCKET) {
				return 0;
			};
			if(lungime==0) { // keep alive
				waitToWrite(0,0);
				sndln=send((SOCKET)this_,(char *)input,lungime,0);
				if(sndln==SOCKET_ERROR) {
					return 0;
				};
				return sndln;
			};
			do {
				waitToWrite(0,0);
				sndln=send((SOCKET)this_,&(((char *)input)[idx]),ln,0);
				if(sndln==SOCKET_ERROR) {
					return idx;
				};
				ln-=sndln;
				idx+=sndln;
			} while(ln>0);
			return idx;

		};

		int Socket::waitToWrite(word seconds,word micro) {
			int ret;
			fd_set sock_set;
			struct timeval timev;

			if((SOCKET)this_==INVALID_SOCKET) {
				return -1;
			}

			timev.tv_sec=seconds;
			timev.tv_usec=micro;
			FD_ZERO(&sock_set);
			FD_SET(((SOCKET)this_),&sock_set);
			ret=select(1,NULL,&sock_set,NULL,&timev);
			if(ret==SOCKET_ERROR) {
				return -1;
			}
			if(ret==0) {
				return 0;
			}
			return 1;

		};

		int Socket::waitToRead(word seconds,word micro) {
			int ret;
			fd_set sock_set;
			struct timeval timev;

			if((SOCKET)this_==INVALID_SOCKET) {
				return -1;
			}

			timev.tv_sec=seconds;
			timev.tv_usec=micro;
			FD_ZERO(&sock_set);
			FD_SET(((SOCKET)this_),&sock_set);
			ret=select(1,&sock_set,NULL,NULL,&timev);
			if(ret==SOCKET_ERROR) {
				return -1;
			}
			if(ret==0) {
				return 0;
			}
			return 1;
		};

		bool Socket::openClientX(String adr_) {
			if(!Net::isValid()) {
				return false;
			};

			int ip0,ip1,ip2,ip3,port;
			if(sscanf((char *)(adr_.value()),"%d.%d.%d.%d:%d",&ip0,&ip1,&ip2,&ip3,&port)==5) {
				IPAddress4 adr;
				adr.ip.ip[0]=ip0;
				adr.ip.ip[1]=ip1;
				adr.ip.ip[2]=ip2;
				adr.ip.ip[3]=ip3;
				adr.port=port;
				return openClient(adr);
			};
			// get address list from url ...
			String urlPart1;
			String urlQuery;
			String urlPart2;
			String urlBase;
			String urlProto;
			String urlService;
			String urlAddress;
			String urlPort;
			StringX::split2FromBegin(adr_,"?",urlPart1,urlQuery);
			if(!StringX::split2FromBegin(urlPart1,"@",urlPart2,urlBase)) {
				urlBase=urlPart2;
			};
			if(!StringX::split2FromBegin(urlBase,"://",urlProto,urlService)) {
				urlService=urlProto;
				urlProto="";
			};
			if(!StringX::split2FromBegin(urlService,":",urlAddress,urlPort)) {
				if(StringX::matchAscii(urlProto,"http")) {
					urlPort="80";
				} else if(StringX::matchAscii(urlProto,"ftp")) {
					urlPort="20";
				} else if(StringX::matchAscii(urlProto,"ssh")) {
					urlPort="22";
				} else if(StringX::matchAscii(urlProto,"telnet")) {
					urlPort="23";
				} else {
					return false;
				};
			};
			if(sscanf((char *)(urlPort.value()),"%d",&port)!=1) {
				return false;
			};
			struct hostent *hserver;
			hserver=gethostbyname((char *)urlAddress.value());
			if(hserver!=NULL) {
				if(hserver->h_addrtype == AF_INET) {
					if(hserver->h_length==4) {
						int k;
						for(k=0; hserver->h_addr_list[k]!=NULL; ++k) {
							if(sscanf((char *)(inet_ntoa(*(struct in_addr *)hserver->h_addr_list[k])),"%d.%d.%d.%d",&ip0,&ip1,&ip2,&ip3)==4) {
								//try connect
								IPAddress4 adr;
								adr.ip.ip[0]=ip0;
								adr.ip.ip[1]=ip1;
								adr.ip.ip[2]=ip2;
								adr.ip.ip[3]=ip3;
								adr.port=port;
								if(openClient(adr)) {
									return true;
								};
							};
						};
					};
				};
			};
			return false;
		};

		bool Socket::openServerX(String adr_) {

			if(!Net::isValid()) {
				return false;
			};

			int ip0,ip1,ip2,ip3,port;
			if(sscanf((char *)(adr_.value()),"%d.%d.%d.%d:%d",&ip0,&ip1,&ip2,&ip3,&port)==5) {
				IPAddress4 adr;
				adr.ip.ip[0]=ip0;
				adr.ip.ip[1]=ip1;
				adr.ip.ip[2]=ip2;
				adr.ip.ip[3]=ip3;
				adr.port=port;
				return openServer(adr);
			};

			// get address list from url ...
			String urlPart1;
			String urlQuery;
			String urlPart2;
			String urlBase;
			String urlProto;
			String urlService;
			String urlAddress;
			String urlPort;
			StringX::split2FromBegin(adr_,"?",urlPart1,urlQuery);
			if(!StringX::split2FromBegin(urlPart1,"@",urlPart2,urlBase)) {
				urlBase=urlPart2;
			};
			if(!StringX::split2FromBegin(urlBase,"://",urlProto,urlService)) {
				urlService=urlProto;
				urlProto="";
			};
			if(!StringX::split2FromBegin(urlService,":",urlAddress,urlPort)) {
				if(StringX::matchAscii(urlProto,"http")) {
					urlPort="80";
				} else if(StringX::matchAscii(urlProto,"ftp")) {
					urlPort="20";
				} else if(StringX::matchAscii(urlProto,"ssh")) {
					urlPort="22";
				} else if(StringX::matchAscii(urlProto,"telnet")) {
					urlPort="23";
				} else {
					return false;
				};
			};
			if(sscanf((char *)(urlPort.value()),"%d",&port)!=1) {
				return false;
			};
			struct hostent *hserver;
			hserver=gethostbyname((char *)urlAddress.value());
			if(hserver!=NULL) {
				if(hserver->h_addrtype == AF_INET) {
					if(hserver->h_length==4) {
						int k;
						for(k=0; hserver->h_addr_list[k]!=NULL; ++k) {
							if(sscanf((char *)(inet_ntoa(*(struct in_addr *)hserver->h_addr_list[k])),"%d.%d.%d.%d",&ip0,&ip1,&ip2,&ip3)==4) {
								//try connect
								IPAddress4 adr;
								adr.ip.ip[0]=ip0;
								adr.ip.ip[1]=ip1;
								adr.ip.ip[2]=ip2;
								adr.ip.ip[3]=ip3;
								adr.port=port;
								if(openServer(adr)) {
									return true;
								};
							};
						};
					};
				};
			};
			return false;
		};

		void Socket::getIPAddress4(IPAddress4 &address_) {
			address_.copy(address);
		};

		bool Socket::becomeOwner(Socket &socket_) {
			close();
			if(socket_.linkBack_) {
				return false;
			};
			this_=socket_.this_;
			socket_.this_=(void *)INVALID_SOCKET;
			address.copy(socket_.address);
			if(socket_.aquireOwner_) {
				socket_.linkNext_=this;
				linkBack_=&socket_;
				socket_.aquireOwner_=false;
			};
			return true;
		};

		void Socket::releaseOwner() {
			if(linkNext_!=NULL) {
				linkNext_->linkBack_=NULL;
				linkNext_=NULL;
			};
		};

		void Socket::aquireOwner() {
			aquireOwner_=true;
		};

	};
};

#endif
