#ifndef LIBXYO_XO_VERSION_HPP
#define LIBXYO_XO_VERSION_HPP

#define LIBXYO_XO_VERSION_ABCD      5,2,0,50
#define LIBXYO_XO_VERSION_A         5
#define LIBXYO_XO_VERSION_B         2
#define LIBXYO_XO_VERSION_C         0
#define LIBXYO_XO_VERSION_D         50
#define LIBXYO_XO_VERSION_STR_ABCD  "5.2.0.50"
#define LIBXYO_XO_VERSION_STR       "5.2.0"
#define LIBXYO_XO_VERSION_STR_BUILD "50"
#define LIBXYO_XO_VERSION_BUILD     50
#define LIBXYO_XO_VERSION_HOUR      18
#define LIBXYO_XO_VERSION_MINUTE    44
#define LIBXYO_XO_VERSION_SECOND    58
#define LIBXYO_XO_VERSION_DAY       14
#define LIBXYO_XO_VERSION_MONTH     6
#define LIBXYO_XO_VERSION_YEAR      2015
#define LIBXYO_XO_VERSION_STR_DATETIME "2015-06-14 18:44:58"

#ifndef XYO_RC

#ifndef XYO_XO__EXPORT_HPP
#include "xyo-xo--export.hpp"
#endif

namespace Lib {
	namespace XYO {
		namespace XO {

			class Version {
				public:
					XYO_XO_EXPORT static const char *getABCD();
					XYO_XO_EXPORT static const char *getA();
					XYO_XO_EXPORT static const char *getB();
					XYO_XO_EXPORT static const char *getC();
					XYO_XO_EXPORT static const char *getD();
					XYO_XO_EXPORT static const char *getVersion();
					XYO_XO_EXPORT static const char *getBuild();
					XYO_XO_EXPORT static const char *getHour();
					XYO_XO_EXPORT static const char *getMinute();
					XYO_XO_EXPORT static const char *getSecond();
					XYO_XO_EXPORT static const char *getDay();
					XYO_XO_EXPORT static const char *getMonth();
					XYO_XO_EXPORT static const char *getYear();
					XYO_XO_EXPORT static const char *getDatetime();
			};

		};
	};
};

#endif
#endif

