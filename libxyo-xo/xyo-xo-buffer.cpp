//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-buffer.hpp"

namespace XYO {
	namespace XO {
		using namespace XYO::XY;

		Buffer::Buffer() {
			buffer=NULL;
			length=0;
			size=0;
		};

		Buffer::~Buffer() {
			if(buffer!=NULL) {
				delete[] buffer;
			};
		};

		void Buffer::setSize(size_t sz) {
			XYO::byte *newBuffer;
			newBuffer=new XYO::byte[sz];
			memset(newBuffer,0,sz);
			if(buffer!=NULL) {
				if(length>sz) {
					length=sz;
				};
				memcpy(newBuffer,buffer,length);
				delete[] buffer;
			};
			size=sz;
			buffer=newBuffer;
		};

		void Buffer::activeDestructor() {
			if(buffer!=NULL) {
				delete[] buffer;
				buffer=NULL;
			};
			size=0;
			length=0;
		};

		void Buffer::memoryInit() {
			TMemory<String>::memoryInit();
		};

		String Buffer::toString() {
			String retV;
			if(buffer!=NULL) {
				if(length>0) {
					retV.concatenate((char *)buffer,length);
				};
			};
			return retV;
		};

		void Buffer::load(XYO::byte *buf_,size_t size_) {
			setSize(size_);
			memcpy(buffer,buf_,size_);
			length=size_;
		};

		void Buffer::opXOR(XYO::byte *buf_,size_t size_) {
			size_t k,m;
			if(size_==0) {
				return;
			};
			for(k=0,m=0; k<length; ++k) {
				buffer[k]^=buf_[m];
				++m;
				if(m>=size_) {
					m=0;
				};
			};
		};

		void Buffer::fromString(String &str_) {
			setSize(str_.length());
			size=str_.length();
			length=size;
			memcpy(buffer,str_.index(0),length);
		};

	};
};


