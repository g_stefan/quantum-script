//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_HPP
#define XYO_XO_HPP

#ifndef LIBXYO_XY_HPP
#include "libxyo-xy.hpp"
#endif

#ifndef XYO_XO__EXPORT_HPP
#include "xyo-xo--export.hpp"
#endif

#ifndef XYO_XO__CONFIG_HPP
#include "xyo-xo--config.hpp"
#endif

#endif
