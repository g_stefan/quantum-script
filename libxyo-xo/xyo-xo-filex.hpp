//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_FILEX_HPP
#define XYO_XO_FILEX_HPP

#ifndef XYO_XO_FILE_HPP
#include "xyo-xo-file.hpp"
#endif

#ifndef XYO_XO_BUFFER_HPP
#include "xyo-xo-buffer.hpp"
#endif

namespace XYO {
	namespace XO {

		class FileX {
			public:
				XYO_XO_EXPORT static bool read(File &file,String &out,size_t size);
				XYO_XO_EXPORT static bool readLn(File &file,String &out,size_t size);
				XYO_XO_EXPORT static size_t write(File &file,String data);
				XYO_XO_EXPORT static size_t writeLn(File &file,String data);
				XYO_XO_EXPORT static size_t readToBuffer(File &file,Buffer &buffer,size_t ln);
				XYO_XO_EXPORT static size_t writeFromBuffer(File &file,Buffer &buffer);
		};

	};
};

#endif
