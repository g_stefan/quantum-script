//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_UNIX

#include <stdio.h>

#include "xyo-xo-net.hpp"

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		bool Net::isValid() {
			return true;
		};

	};
};

#endif

