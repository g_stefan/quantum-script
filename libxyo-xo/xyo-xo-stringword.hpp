//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGWORD_HPP
#define XYO_XO_STRINGWORD_HPP

#ifndef XYO_XO_STRINGREFERENCEWORD_HPP
#include "xyo-xo-stringreferenceword.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringWord;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::StringWord>:
			public TMemoryObjectPoolActive<XO::StringWord> {};

	};
};

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class StringWord :
			public Object {
			protected:
				TPointer<StringReferenceWord> value_;
			public:

				inline StringWord() {
					value_ = StringReferenceWord::init();
				};

				inline ~StringWord() {
					value_.deleteObject();
				};

				inline StringWord(size_t initialSize, size_t chunk) {
					value_ = StringReferenceWord::init2(initialSize, chunk);
				};

				inline StringWord(const word *value) {
					value_ = StringReferenceWord::from(value);
				};

				inline StringWord(const StringWord &value) {
					value_ = const_cast<StringWord &>(value).value_;
				};

				inline StringWord(StringWord &&value) {
					value_.setObjectWithOwnerTransfer(value.value_);
				};

				inline StringWord(TPointer<StringReferenceWord> &value) {
					value_ = value;
					if (!value_.isValid()) {
						value_=StringReferenceWord::init();
					};
				};

				inline StringWord &operator=(const word *value) {
					value_ = StringReferenceWord::from(value);
					return *this;
				};

				inline StringWord &operator=(const StringWord &value) {
					value_ = value.value_;
					return *this;
				};

				inline StringWord &operator=(StringWord &&value) {
					value_ = value.value_;
					return *this;
				};


				inline StringWord &operator=(TPointer<StringReferenceWord> value) {
					value_ = value;
					if (!value_.isValid()) {
						value_ = StringReferenceWord::from2(value->value(),value->length());
					};
					return *this;

				};

				inline StringWord &operator+=(const word *value) {
					value_ = StringReferenceWord::concatenate(value_, value);
					return *this;

				};

				inline StringWord &operator+=(StringWord &value) {
					value_ = StringReferenceWord::concatenate2(value_, value.value_);
					return *this;

				};

				inline StringWord &operator<<(const StringWord &value) {
					value_ = StringReferenceWord::concatenate2(value_, const_cast<StringWord &>(value).value_);
					return *this;

				};

				inline StringWord &operator<<(const word *value) {
					value_ = StringReferenceWord::concatenate(value_, value);
					return *this;

				};

				inline StringWord &operator<<(const word value) {
					value_ = StringReferenceWord::concatenate_one(value_, value);
					return *this;

				};

				inline StringWord &operator+=(const word value) {
					value_ = StringReferenceWord::concatenate_one(value_, value);
					return *this;

				};

				inline operator word *() {
					return value_->value();
				};

				inline word *value() {
					return value_->value();
				};

				inline word *index(size_t x) {
					return &(value_->value())[x];
				};

				inline size_t length() {
					return value_->length();
				};

				inline size_t getSize() {
					return value_->getSize();
				};

				inline size_t getChunk() {
					return value_->getChunk();
				};

				inline void setChunk(size_t x) {
					value_->setChunk(x);
				};

				inline void setLength(size_t x) {
					value_->setLength(x);
				};

				inline void updateLength() {
					value_->updateLength();
				};

				inline TPointer<StringReferenceWord> reference() {
					return value_;
				};

				inline void set(const word *value, size_t length) {
					value_ = StringReferenceWord::from2(value, length);
				};

				inline void concatenate(const word *value, size_t length) {
					value_ = StringReferenceWord::concatenate3(value_,value, length);
				};

				inline word &operator [](int x) {
					return (value_->value())[x];
				};

				inline word getElement(size_t x) {
					return (value_->value())[x];
				};

				inline bool operator ==(const word *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)==0);
				};

				inline bool operator ==(const StringWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringWord &>(x).value())==0);
				};

				inline bool operator !=(const word *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)!=0);
				};

				inline bool operator !=(const StringWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringWord &>(x).value())!=0);
				};

				inline bool operator <(const word *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<0);
				};

				inline bool operator <(const StringWord &x) {
					return (strcmp((const char *)value_->value(),(const char *)const_cast<StringWord &>(x).value())<0);
				};

				inline bool operator <=(const word *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)<=0);
				};

				inline bool operator <=(const StringWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringWord &>(x).value())<=0);
				};

				inline bool operator >(const word *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>0);
				};

				inline bool operator >(const StringWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringWord &>(x).value())>0);
				};

				inline bool operator >=(const word *x) {
					return (strcmp((const char *)value_->value(), (const char *)x)>=0);
				};

				inline bool operator >=(const StringWord &x) {
					return (strcmp((const char *)value_->value(), (const char *)const_cast<StringWord &>(x).value())>=0);
				};

				inline int compare(const StringWord &value) const {
					return strcmp((const char *)(const_cast<TPointer<StringReferenceWord> &>(value_))->value(), (const char *)(const_cast<StringWord &>(value)).value());
				};

				inline void activeDestructor() {
					value_ = StringReferenceWord::init();
				};

				inline static void memoryInit() {
					TMemory<StringReferenceWord>::memoryInit();
				};
		};
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TComparator<XO::StringWord> {
			public:
				typedef XO::StringWord T;

				inline static bool isEqualTo(const T &a, const T &b) {
					return (a.compare(b)==0);
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return (a.compare(b)!=0);
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return (a.compare(b)<0);
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return (a.compare(b)>0);
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return (a.compare(b)<=0);
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return (a.compare(b)>=0);
				};

				inline static int compare(const T &a, const T &b) {
					return a.compare(b);
				};
		};
	};
};

#endif

