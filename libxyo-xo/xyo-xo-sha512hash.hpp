//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_SHA512HASH_HPP
#define XYO_XO_SHA512HASH_HPP

#ifndef XYO_XO_STRING_HPP
#include "xyo-xo-string.hpp"
#endif

namespace XYO {
	namespace XO {

		class SHA512Hash :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(SHA512Hash);
			protected:
				XYO_XO_EXPORT static qword k_[];

				qword h0,h1,h2,h3,h4,h5,h6,h7;

				qword process[16];
				byte lastData[8];
				qword length0;
				qword length1;
				size_t stateM;
			public:
				XYO_XO_EXPORT SHA512Hash();
				XYO_XO_EXPORT void processInit();
				XYO_XO_EXPORT void hashBlock(qword *w_);
				XYO_XO_EXPORT void processBytes(const byte *toHash, size_t length);
				XYO_XO_EXPORT void processDone();
				XYO_XO_EXPORT String getHashAsHex();
				XYO_XO_EXPORT static String getHashString(String toHash);
				XYO_XO_EXPORT void toBytes(byte *buffer);
				XYO_XO_EXPORT static void hashStringToBytes(String toHash,byte *buffer);

				inline static void memoryInit() {
					TMemory<String>::memoryInit();
				};
		};

	};
};

#endif
