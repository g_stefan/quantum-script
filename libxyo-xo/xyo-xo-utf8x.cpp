//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <string.h>

#include "xyo-xo-utf8x.hpp"

namespace XYO {
	namespace XO {

		size_t Utf8X::memChrSize(utf8 *x) {
			if ((*x & 0x80) == 0x00) {
				return (1);
			}
			if ((*x & 0xE0) == 0xC0) {
				return (2);
			}
			if ((*x & 0xF0) == 0xE0) {
				return (3);
			}
			if ((*x & 0xF8) == 0xF0) {
				return (4);
			}
			if ((*x & 0xFC) == 0xF8) {
				return (5);
			}
			if ((*x & 0xFE) == 0xFC) {
				return (6);
			}
			return 0; //invalid character
		};

		bool Utf8X::chrIsValid(utf8 *x) {
			size_t sz;
			sz = memChrSize(x);
			if (sz == 0) {
				return false;
			}
			++x;
			--sz;
			while (sz) {
				if ((*x & 0xC0) != 0x80) {
					return false;
				}
				++x;
				--sz;
			};
			return true;
		};

		bool Utf8X::check(utf8 x) {
			return ((x & 0xC0) == 0x80);
		};

		bool Utf8X::chrIsEqual(utf8 *x, utf8 *y) {
			size_t sz;
			sz = memChrSize(x);
			if (sz != memChrSize(y)) {
				return false;
			}
			return (memcmp(x, y, sz) == 0);
		};

	};
};

