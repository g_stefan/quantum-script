//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringdwordx.hpp"

namespace XYO {
	namespace XO {

		StringDWord StringDWordX::trimWithElement(StringDWord o, StringDWord x) {
			const dword *x1;
			const dword *x2;
			StringDWord retV;
			x1 = StringBaseDWord::toNotInElement(o.value(), x);
			x2 = StringBaseDWord::toNotInFromEndElement(o.value(), x);

			if (*x1 == 0 || *x2 == 0) {
				return o;
			};
			retV.set(x1, x2 - x1 + 1);
			return retV;
		};

		StringDWord StringDWordX::replace(StringDWord o, StringDWord x, StringDWord y) {
			StringDWord retV;
			size_t k, ln;
			size_t x_ln;
			size_t y_ln;

			x_ln = StringBaseDWord::length(x);
			y_ln = StringBaseDWord::length(y);
			if (o.length() < x_ln) {
				return o;
			};
			ln = o.length() - x_ln;

			k = 0;
			while (k < ln) {
				if (StringBaseDWord::compareN(&o[(int) k], x, x_ln) == 0) {
					retV.concatenate(y, y_ln);
					k += x_ln;
					continue;
				} else {
					retV.concatenate(&o[(int) k], 1);
					k++;
				};
			};


			if (StringBaseDWord::compareN(&o[(int) k], x, x_ln) == 0) {
				retV.concatenate(y, y_ln);
			} else {
				retV << &o[(int) k];
			};

			return retV;
		};

		StringDWord StringDWordX::nilAtFirstFromEnd(StringDWord o, StringDWord x) {
			size_t idx;
			if (StringBaseDWord::indexOfFromEnd(o, x, idx)) {
				StringDWord retV;
				retV.set(o, idx);
				return retV;
			};
			return o;
		};

		StringDWord StringDWordX::substring(StringDWord o, size_t start, size_t length) {
			StringDWord retV;
			if (length == 0) {
				return retV;
			};
			if (start > o.length()) {
				return retV;
			};
			if (start + length > o.length()) {
				length=o.length()-start;
			};
			retV.set(&o[(int) start], length);
			return retV;
		};

		StringDWord StringDWordX::substring(StringDWord o, size_t start) {
			StringDWord retV;
			if (start > o.length()) {
				return retV;
			};
			retV.set(&o[(int) start], o.length() - start);
			return retV;
		};

		StringDWord StringDWordX::toLowerCaseAscii(StringDWord value) {
			StringDWord out_(value.getSize(), value.getChunk());

			size_t totalLn;
			dword *inx;
			dword *outx;

			totalLn = 0;
			inx = value.value();
			outx = out_.value();
			while (*inx) {
				*outx = StringBaseDWord::chrToLowerCaseAscii(*inx);
				++outx;
				++inx;
				++totalLn;
			};
			*outx = 0;
			out_.setLength(totalLn);
			return out_;
		};

		StringDWord StringDWordX::toUpperCaseAscii(StringDWord value) {
			StringDWord out_(value.getSize(), value.getChunk());

			size_t totalLn;
			dword *inx;
			dword *outx;

			totalLn = 0;
			inx = value.value();
			outx = out_.value();
			while (*inx) {
				*outx = StringBaseDWord::chrToUpperCaseAscii(*inx);
				++outx;
				++inx;
				++totalLn;
			};
			*outx = 0;

			out_.setLength(totalLn);
			return out_;
		};

		bool StringDWordX::matchAscii(StringDWord text, StringDWord sig) {
			size_t i;
			size_t j;

			i = 0;
			j = 0;
			for (; (i<sig.length()) && (j<text.length()); ++i) {
				if (sig[i] == '*') {
					while (text[j] && (StringBaseDWord::chrToUpperCaseAscii(text[j]) != StringBaseDWord::chrToUpperCaseAscii(sig[i + 1]))) {
						j++;
					};
				} else if (StringBaseDWord::chrToUpperCaseAscii(text[j]) == StringBaseDWord::chrToUpperCaseAscii(sig[i]) || sig[i] == '?') {
					j++;
				} else {
					return false;
				}
			};
			if (sig[i] == 0) {
				if (text[j] == 0) {
					return true;
				};
			};
			return false;
		};

		bool StringDWordX::split2FromBegin(StringDWord text, StringDWord sig,StringDWord &firstPart,StringDWord &secondPart) {
			size_t index;
			if(StringBaseDWord::indexOf(text, text.length(), sig, sig.length(), 0, index)) {
				firstPart=substring(text,0,index);
				secondPart=substring(text,index+sig.length());
				return true;
			};
			firstPart=text;
			secondPart=(const dword *)StringBaseDWord::empty;
			return false;
		};

	};
};


