//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_UTFX_HPP
#define XYO_XO_UTFX_HPP

#ifndef XYO_XO_UTF8X_HPP
#include "xyo-xo-utf8x.hpp"
#endif

#ifndef XYO_XO_UTF16X_HPP
#include "xyo-xo-utf16x.hpp"
#endif

#ifndef XYO_XO_UTF32X_HPP
#include "xyo-xo-utf32x.hpp"
#endif

namespace XYO {
	namespace XO {

		class UtfX {
			public:

				XYO_XO_EXPORT static size_t chrUtf32FromUtf8(utf32 *out, utf8 *in);
				XYO_XO_EXPORT static size_t memChrUtf8FromUtf32Size(utf32 in);
				XYO_XO_EXPORT static size_t chrUtf8FromUtf32(utf8 *out, utf32 in);
				XYO_XO_EXPORT static size_t memChrUtf16FromUtf32Size(utf32 in);
				XYO_XO_EXPORT static size_t chrUtf16FromUtf32(utf16 *out, utf32 in);
				XYO_XO_EXPORT static bool chrUtf32FromUtf16(utf32 *out, utf16 *in);
				XYO_XO_EXPORT static size_t memChrUtf8FromUtf16Size(utf16 *in);

				XYO_XO_EXPORT static StringUtf8 utf8FromUtf16X(StringUtf16 in, StringUtf8 err);
				XYO_XO_EXPORT static StringUtf8 utf8FromUtf32X(StringUtf32 in, StringUtf8 err);
				XYO_XO_EXPORT static StringUtf16 utf16FromUtf8X(StringUtf8 in, StringUtf16 err);
				XYO_XO_EXPORT static StringUtf16 utf16FromUtf32X(StringUtf32 in, StringUtf16 err);
				XYO_XO_EXPORT static StringUtf32 utf32FromUtf8X(StringUtf8 in, StringUtf32 err);
				XYO_XO_EXPORT static StringUtf32 utf32FromUtf16X(StringUtf16 in, StringUtf32 err);

				XYO_XO_EXPORT static StringUtf8 utf8FromUtf16(StringUtf16 in);
				XYO_XO_EXPORT static StringUtf8 utf8FromUtf32(StringUtf32 in);
				XYO_XO_EXPORT static StringUtf16 utf16FromUtf8(StringUtf8 in);
				XYO_XO_EXPORT static StringUtf16 utf16FromUtf32(StringUtf32 in);
				XYO_XO_EXPORT static StringUtf32 utf32FromUtf8(StringUtf8 in);
				XYO_XO_EXPORT static StringUtf32 utf32FromUtf16(StringUtf16 in);

				inline static void memoryInit() {
					TMemory<StringUtf8>::memoryInit();
					TMemory<StringUtf16>::memoryInit();
					TMemory<StringUtf32>::memoryInit();
				};
		};

	};
};

#endif

