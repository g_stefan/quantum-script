//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_UTF8X_HPP
#define XYO_XO_UTF8X_HPP

#ifndef XYO_XO_STRINGBYTE_HPP
#include "xyo-xo-stringbyte.hpp"
#endif

namespace XYO {

	namespace XO {

		typedef byte utf8;
		typedef StringByte StringUtf8;

		class Utf8X {
			public:

				XYO_XO_EXPORT static size_t memChrSize(utf8 *x);
				XYO_XO_EXPORT static bool chrIsValid(utf8 *x);
				XYO_XO_EXPORT static bool check(utf8 x);
				XYO_XO_EXPORT static bool chrIsEqual(utf8 *x, utf8 *y);

		};

	};
};

#endif

