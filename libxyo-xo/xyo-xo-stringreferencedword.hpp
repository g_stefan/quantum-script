//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGREFERENCEDWORD_HPP
#define XYO_XO_STRINGREFERENCEDWORD_HPP

#ifndef XYO_XO_STRINGBASEDWORD_HPP
#include "xyo-xo-stringbasedword.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringReferenceDWord;
	};
};

namespace XYO {
	namespace XY {

		template<>
		class TMemoryObject<XO::StringReferenceDWord>:
			public TMemoryObjectPoolActive<XO::StringReferenceDWord> {};

	};
};

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class StringReferenceDWord:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(StringReferenceDWord);
			protected:
				dword *value_;
				size_t length_;
				size_t size_;
				size_t chunk_;
				bool mode_;

				XYO_XO_EXPORT void memoryDelete_();
				XYO_XO_EXPORT void memoryNew_(size_t size);
				XYO_XO_EXPORT void memoryResize_(size_t size);
			public:

				static const size_t defaultChunk_ = 32;

				XYO_XO_EXPORT StringReferenceDWord();
				XYO_XO_EXPORT ~StringReferenceDWord();
				XYO_XO_EXPORT dword *value();
				XYO_XO_EXPORT size_t length();
				XYO_XO_EXPORT size_t getSize();
				XYO_XO_EXPORT size_t getChunk();
				XYO_XO_EXPORT void setChunk(size_t x);
				XYO_XO_EXPORT void setLength(size_t x);
				XYO_XO_EXPORT void updateLength();
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> init();
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> init2(size_t initialSize, size_t chunk);
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> from(const dword *o);
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> concatenate(StringReferenceDWord *o, const dword *x);
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> concatenate2(StringReferenceDWord *o, StringReferenceDWord *x);
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> concatenate3(StringReferenceDWord *o, const dword *x, size_t x_ln);
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> from2(const dword *o, size_t o_ln);
				XYO_XO_EXPORT static TPointer<StringReferenceDWord> concatenate_one(StringReferenceDWord *o, const dword x);
				XYO_XO_EXPORT void activeDestructor();
				XYO_XO_EXPORT static void memoryInit();
		};

	};
};

#endif

