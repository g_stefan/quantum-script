//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <stdio.h>
#include <windows.h>

#include "xyo-xo-net.hpp"

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class XNet {
			public:
				bool isValid;

				XNet();
				~XNet();
		};

		XNet::XNet() {
			isValid=false;
			WSADATA wsaData;
			WORD    wVersionRequested=MAKEWORD(1,1);
			if (WSAStartup(wVersionRequested,&wsaData)==0) {
				isValid=true;
			};
		};

		XNet::~XNet() {
			if(isValid) {
				WSACleanup();
			};
		};

		bool Net::isValid() {
			return (TXSingletonProcess<XNet>::getValue())->isValid;
		};

	};
};

#endif

