//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringreferencebyte.hpp"

//
// Accelerated memory allocation for fixed size of
// 32,64,96,128,160,192,224,256
//

namespace XYO {
	namespace XO {

		template<size_t sizeOfMemory>
		class TXStringReferenceByteMemory {
			public:
				byte memory[sizeOfMemory];
		};

		StringReferenceByte::StringReferenceByte() {
			mode_ = false;
			value_ = NULL;
		};

		StringReferenceByte::~StringReferenceByte() {
			memoryDelete_();
		};

		void StringReferenceByte::activeDestructor() {
			memoryDelete_();
			value_=NULL;
			mode_=false;
		};

		void StringReferenceByte::memoryInit() {
			TMemory<TXStringReferenceByteMemory<32> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<64> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<96> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<128> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<160> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<192> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<224> >::memoryInit();
			TMemory<TXStringReferenceByteMemory<256> >::memoryInit();
		};

		void StringReferenceByte::memoryDelete_() {
			if (value_ != NULL) {
				if(mode_) {
					switch(size_) {
						case 32:
							TMemory<TXStringReferenceByteMemory<32> >::memoryDelete((TXStringReferenceByteMemory<32> *)((void *)value_));
							break;
						case 64:
							TMemory<TXStringReferenceByteMemory<64> >::memoryDelete((TXStringReferenceByteMemory<64> *)((void *)value_));
							break;
						case 96:
							TMemory<TXStringReferenceByteMemory<96> >::memoryDelete((TXStringReferenceByteMemory<96> *)((void *)value_));
							break;
						case 128:
							TMemory<TXStringReferenceByteMemory<128> >::memoryDelete((TXStringReferenceByteMemory<128> *)((void *)value_));
							break;
						case 160:
							TMemory<TXStringReferenceByteMemory<160> >::memoryDelete((TXStringReferenceByteMemory<160> *)((void *)value_));
							break;
						case 192:
							TMemory<TXStringReferenceByteMemory<192> >::memoryDelete((TXStringReferenceByteMemory<192> *)((void *)value_));
							break;
						case 224:
							TMemory<TXStringReferenceByteMemory<224> >::memoryDelete((TXStringReferenceByteMemory<224> *)((void *)value_));
							break;
						case 256:
							TMemory<TXStringReferenceByteMemory<256> >::memoryDelete((TXStringReferenceByteMemory<256> *)((void *)value_));
							break;
					};
				} else {
					delete[] value_;
				};
			};
			mode_=false;
		};


		void StringReferenceByte::memoryNew_(size_t size) {
			size_=size;
			switch(size) {
				case 32:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<32> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 64:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<64> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 96:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<96> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 128:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<128> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 160:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<160> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 192:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<192> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 224:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<224> >::memoryNew()));
					mode_=true;
					return;
					break;
				case 256:
					value_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<256> >::memoryNew()));
					mode_=true;
					return;
					break;
				default:
					break;
			};
			value_ = new byte[size];
			mode_=false;
		};

		void StringReferenceByte::memoryResize_(size_t size) {
			byte *newValue_;
			bool  newMode_;
			newMode_=false;
			switch(size) {
				case 32:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<32> >::memoryNew()));
					newMode_=true;
					break;
				case 64:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<64> >::memoryNew()));
					newMode_=true;
					break;
				case 96:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<96> >::memoryNew()));
					newMode_=true;
					break;
				case 128:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<128> >::memoryNew()));
					newMode_=true;
					break;
				case 160:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<160> >::memoryNew()));
					newMode_=true;
					break;
				case 192:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<192> >::memoryNew()));
					newMode_=true;
					break;
				case 224:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<224> >::memoryNew()));
					newMode_=true;
					break;
				case 256:
					newValue_=(byte *)((void *)(TMemory<TXStringReferenceByteMemory<256> >::memoryNew()));
					newMode_=true;
					break;
				default:
					newValue_ = new byte[size];
					break;
			};
			StringBaseByte::copyNMemory(newValue_,value_,length_);
			memoryDelete_();
			value_=newValue_;
			mode_=newMode_;
			size_=size;
		};


		byte *StringReferenceByte::value() {
			return value_;
		};

		size_t StringReferenceByte::length() {
			return length_;
		};

		size_t StringReferenceByte::getSize() {
			return size_;
		};

		size_t StringReferenceByte::getChunk() {
			return chunk_;
		};

		void StringReferenceByte::setChunk(size_t x) {
			chunk_ = x;
		};

		void StringReferenceByte::setLength(size_t x) {
			if(x<size_) {
				length_ = x;
				return;
			};
			length_=size_-1;
		};

		void StringReferenceByte::updateLength() {
			length_ = StringBaseByte::length(value_);
		};

		TPointer<StringReferenceByte > StringReferenceByte::init() {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->memoryNew_(defaultChunk_);
			retV->value_[0] = StringBaseByte::empty[0];
			retV->length_ = 0;
			return retV;
		};

		TPointer<StringReferenceByte > StringReferenceByte::init2(size_t initialSize, size_t chunk) {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = (chunk) ? chunk : defaultChunk_;
			retV->length_ = 0;
			retV->memoryNew_(initialSize);
			retV->value_[0] = StringBaseByte::empty[0];
			return retV;
		};

		TPointer<StringReferenceByte> StringReferenceByte::from(const byte *o) {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = StringBaseByte::length(o);
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseByte::copy(retV->value_, o);
			return retV;
		};

		TPointer<StringReferenceByte> StringReferenceByte::concatenate(StringReferenceByte *o, const byte *x) {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + StringBaseByte::length(x);
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseByte::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseByte::copy(&retV->value_[o->length_], x);
			return retV;
		};

		TPointer<StringReferenceByte> StringReferenceByte::concatenate2(StringReferenceByte *o, StringReferenceByte *x) {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + x->length_;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseByte::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseByte::copyNMemory(&retV->value_[o->length_], x->value_,x->length_);
			return retV;
		};

		TPointer<StringReferenceByte> StringReferenceByte::concatenate3(StringReferenceByte *o, const byte *x, size_t x_ln) {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + x_ln;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};

			StringBaseByte::copyNMemory(retV->value_, o->value_,o->length_);
			StringBaseByte::copyNMemory(&retV->value_[o->length_], x, x_ln);
			return retV;
		};

		TPointer<StringReferenceByte> StringReferenceByte::from2(const byte *o, size_t o_ln) {
			TPointer<StringReferenceByte> retV;
			retV.setObject(TMemory<StringReferenceByte>::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o_ln;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};

			StringBaseByte::copyNMemory(retV->value_, o, o_ln);
			return retV;
		};

		TPointer<StringReferenceByte> StringReferenceByte::concatenate_one(StringReferenceByte *o, const byte x) {
			TPointer<StringReferenceByte > retV;
			retV.setObject(TMemory<StringReferenceByte >::newObject());
			retV->chunk_ = defaultChunk_;
			retV->length_ = o->length_ + 1;
			if (retV->length_ + 1 >= defaultChunk_) {
				retV->memoryNew_(((retV->length_ / defaultChunk_) + 1) * defaultChunk_);
			} else {
				retV->memoryNew_(defaultChunk_);
			};
			StringBaseByte::copyNMemory(retV->value_, o->value_,o->length_);
			retV->value_[o->length_]= x;
			retV->value_[o->length_+1]= 0;
			return retV;
		};


	};
};


