//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-ipaddress4.hpp"

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		IPAddress4::IPAddress4() {
			port=0;
		};

		void IPAddress4::copy(IPAddress4 &addr) {
			ip.copy(addr.ip);
			port=addr.port;
		};
	};
};


