//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringwordx.hpp"

namespace XYO {
	namespace XO {

		StringWord StringWordX::trimWithElement(StringWord o, StringWord x) {
			const word *x1;
			const word *x2;
			StringWord retV;
			x1 = StringBaseWord::toNotInElement(o.value(), x);
			x2 = StringBaseWord::toNotInFromEndElement(o.value(), x);

			if (*x1 == 0 || *x2 == 0) {
				return o;
			};
			retV.set(x1, x2 - x1 + 1);
			return retV;
		};

		StringWord StringWordX::replace(StringWord o, StringWord x, StringWord y) {
			StringWord retV;
			size_t k, ln;
			size_t x_ln;
			size_t y_ln;

			x_ln = StringBaseWord::length(x);
			y_ln = StringBaseWord::length(y);
			if (o.length() < x_ln) {
				return o;
			};
			ln = o.length() - x_ln;

			k = 0;
			while (k < ln) {
				if (StringBaseWord::compareN(&o[(int) k], x, x_ln) == 0) {
					retV.concatenate(y, y_ln);
					k += x_ln;
					continue;
				} else {
					retV.concatenate(&o[(int) k], 1);
					k++;
				};
			};


			if (StringBaseWord::compareN(&o[(int) k], x, x_ln) == 0) {
				retV.concatenate(y, y_ln);
			} else {
				retV << &o[(int) k];
			};

			return retV;
		};

		StringWord StringWordX::nilAtFirstFromEnd(StringWord o, StringWord x) {
			size_t idx;
			if (StringBaseWord::indexOfFromEnd(o, x, idx)) {
				StringWord retV;
				retV.set(o, idx);
				return retV;
			};
			return o;
		};

		StringWord StringWordX::substring(StringWord o, size_t start, size_t length) {
			StringWord retV;
			if (length == 0) {
				return retV;
			};
			if (start > o.length()) {
				return retV;
			};
			if (start + length > o.length()) {
				length=o.length()-start;
			};
			retV.set(&o[(int) start], length);
			return retV;
		};

		StringWord StringWordX::substring(StringWord o, size_t start) {
			StringWord retV;
			if (start > o.length()) {
				return retV;
			};
			retV.set(&o[(int) start], o.length() - start);
			return retV;
		};

		StringWord StringWordX::toLowerCaseAscii(StringWord value) {
			StringWord out_(value.getSize(), value.getChunk());

			size_t totalLn;
			word *inx;
			word *outx;

			totalLn = 0;
			inx = value.value();
			outx = out_.value();
			while (*inx) {
				*outx = StringBaseWord::chrToLowerCaseAscii(*inx);
				++outx;
				++inx;
				++totalLn;
			};
			*outx = 0;
			out_.setLength(totalLn);
			return out_;
		};

		StringWord StringWordX::toUpperCaseAscii(StringWord value) {
			StringWord out_(value.getSize(), value.getChunk());

			size_t totalLn;
			word *inx;
			word *outx;

			totalLn = 0;
			inx = value.value();
			outx = out_.value();
			while (*inx) {
				*outx = StringBaseWord::chrToUpperCaseAscii(*inx);
				++outx;
				++inx;
				++totalLn;
			};
			*outx = 0;

			out_.setLength(totalLn);
			return out_;
		};

		bool StringWordX::matchAscii(StringWord text, StringWord sig) {
			size_t i;
			size_t j;

			i = 0;
			j = 0;
			for (; (i<sig.length()) && (j<text.length()); ++i) {
				if (sig[i] == '*') {
					while (text[j] && (StringBaseWord::chrToUpperCaseAscii(text[j]) != StringBaseWord::chrToUpperCaseAscii(sig[i + 1]))) {
						j++;
					};
				} else if (StringBaseWord::chrToUpperCaseAscii(text[j]) == StringBaseWord::chrToUpperCaseAscii(sig[i]) || sig[i] == '?') {
					j++;
				} else {
					return false;
				}
			};
			if (sig[i] == 0) {
				if (text[j] == 0) {
					return true;
				};
			};
			return false;
		};

		bool StringWordX::split2FromBegin(StringWord text, StringWord sig,StringWord &firstPart,StringWord &secondPart) {
			size_t index;
			if(StringBaseWord::indexOf(text, text.length(), sig, sig.length(), 0, index)) {
				firstPart=substring(text,0,index);
				secondPart=substring(text,index+sig.length());
				return true;
			};
			firstPart=text;
			secondPart=(const word *)StringBaseWord::empty;
			return false;
		};

	};
};


