//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_STRINGBASEDWORD_HPP
#define XYO_XO_STRINGBASEDWORD_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {
		class StringBaseDWord {
			public:

				XYO_XO_EXPORT static dword empty[];
				XYO_XO_EXPORT static size_t length(const dword *o);
				XYO_XO_EXPORT static void copy(dword *o, const dword *v);
				XYO_XO_EXPORT static void copyN(dword *o, const dword *v, size_t k);
				XYO_XO_EXPORT static void copyNMemory(dword *o, const dword *v, size_t k);
				XYO_XO_EXPORT static int compare(const dword *o, const dword *v);
				XYO_XO_EXPORT static int compareN(const dword *o, const dword *v, size_t k);

				inline static bool isEqual(const dword *o, const dword *v) {
					return (compare(o,v)==0);
				};

				inline static bool isEqualN(const dword *o, const dword *v, size_t k) {
					return (compareN(o,v,k)==0);
				};

				inline static bool beginWith(const dword *o, const dword *v) {
					return (compareN(o, v, length(v)) == 0);
				};

				XYO_XO_EXPORT static void concatenate(dword *o, const dword *v);
				XYO_XO_EXPORT static void concatenateN(dword *o, const dword *v, size_t k);
				XYO_XO_EXPORT static bool indexOf(const dword *o, size_t o_ln, const dword *v, size_t v_ln, size_t pos, size_t &retV);
				XYO_XO_EXPORT static bool indexOfFromEnd(const dword *o, size_t o_ln, const dword *v, size_t v_ln, size_t pos, size_t &retV);

				inline static bool indexOf(const dword *o, const dword *v, size_t &retV) {
					return indexOf(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOf(const dword *o, const dword *v, size_t pos, size_t &retV) {
					return indexOf(o, length(o), v, length(v), pos, retV);
				};

				inline static bool indexOfFromEnd(const dword *o, const dword *v, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), 0, retV);
				};

				inline static bool indexOfFromEnd(const dword *o, const dword *v, size_t pos, size_t &retV) {
					return indexOfFromEnd(o, length(o), v, length(v), pos, retV);
				};

				XYO_XO_EXPORT static bool nilAt(dword *o, const dword *v);
				XYO_XO_EXPORT static bool nilAtFromEnd(dword *o, const dword *v);
				XYO_XO_EXPORT static bool skipTo(dword *o, const dword *v);
				XYO_XO_EXPORT static bool skipToFromEnd(dword *o, const dword *v);
				XYO_XO_EXPORT static bool hasOnlyElement(const dword *o, const dword *v);
				XYO_XO_EXPORT static const dword *toNotInElement(const dword *o, const dword *v);
				XYO_XO_EXPORT static const dword *toNotInFromEndElement(const dword *o, const dword *v);

				inline static dword chrToLowerCaseAscii(dword in) {
					if(in<0x41||in>0x5A) {
						return in;
					};
					return in+0x20;
				};

				inline static dword chrToUpperCaseAscii(dword in) {
					if(in<0x61||in>0x7A) {
						return in;
					};
					return in-0x20;
				};

				XYO_XO_EXPORT static int  compareIgnoreCaseAscii(const dword *x, const dword *y);
				XYO_XO_EXPORT static int  compareIgnoreCaseNAscii(const dword *x, const dword *y, size_t ln);
				XYO_XO_EXPORT static bool indexOfIgnoreCaseAscii(const dword *the_string, size_t length, const dword *patern, size_t patlength, size_t pos, size_t *result);
				XYO_XO_EXPORT static bool indexOfFromEndIgnoreCaseAscii(const dword *the_string, size_t length, const dword *patern, size_t patlength, size_t pos, size_t *result);

		};
	};
};

#endif


