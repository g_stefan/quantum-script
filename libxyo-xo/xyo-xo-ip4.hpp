//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_IP4_HPP
#define XYO_XO_IP4_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		using namespace XYO::XY;

		class IP4:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(IP4);
			public:
				byte ip[4];

				XYO_XO_EXPORT IP4();
				XYO_XO_EXPORT dword toDWord();
				XYO_XO_EXPORT void fromDWord(dword ip_);
				XYO_XO_EXPORT void copy(IP4 &ip_);
				XYO_XO_EXPORT bool isInTheSameNetwork(IP4 &ip_,IP4 &mask_);
		};

	};
};

#endif

