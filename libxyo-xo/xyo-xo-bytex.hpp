//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_BYTEX_HPP
#define XYO_XO_BYTEX_HPP

#ifndef XYO_XO_HPP
#include "xyo-xo.hpp"
#endif

namespace XYO {
	namespace XO {

		class ByteX {
			public:

				static inline dword dwordLeftRotate(dword x, word c) {
					return (x << c) | (x >> (32 - c));
				};

				static inline dword dwordRightRotate(dword x, word c) {
					return (x >> c) | (x << (32 - c));
				};

				static inline qword qwordLeftRotate(qword x, word c) {
					return (x << c) | (x >> (64 - c));
				};

				static inline qword qwordRightRotate(qword x, word c) {
					return (x >> c) | (x << (64 - c));
				};

				static inline dword dwordFromByte(const byte *in) {
					dword retV;
					retV = *(in + 3);
					retV <<= 8;
					retV |= *(in + 2);
					retV <<= 8;
					retV |= *(in + 1);
					retV <<= 8;
					retV |= *(in);
					return retV;
				};

				static inline dword dwordFromByteReversed(const byte *in) {
					dword retV;
					retV = *(in);
					retV <<= 8;
					retV |= *(in + 1);
					retV <<= 8;
					retV |= *(in + 2);
					retV <<= 8;
					retV |= *(in + 3);
					return retV;
				};

				static inline qword qwordFromByte(const byte *in) {
					qword retV;
					retV = *(in + 7);
					retV <<= 8;
					retV |= *(in + 6);
					retV <<= 8;
					retV |= *(in + 5);
					retV <<= 8;
					retV |= *(in + 4);
					retV <<= 8;
					retV |= *(in + 3);
					retV <<= 8;
					retV |= *(in + 2);
					retV <<= 8;
					retV |= *(in + 1);
					retV <<= 8;
					retV |= *(in);
					return retV;
				};

				static inline qword qwordFromByteReversed(const byte *in) {
					qword retV;
					retV = *(in);
					retV <<= 8;
					retV |= *(in + 1);
					retV <<= 8;
					retV |= *(in + 2);
					retV <<= 8;
					retV |= *(in + 3);
					retV <<= 8;
					retV |= *(in + 4);
					retV <<= 8;
					retV |= *(in + 5);
					retV <<= 8;
					retV |= *(in + 6);
					retV <<= 8;
					retV |= *(in + 7);
					return retV;
				};

				static inline void dwordToByte(dword in, byte *out) {
					out[0] = (byte) ((in)&0x00FF);
					in>>=8;
					out[1] = (byte) ((in)&0x00FF);
					in>>=8;
					out[2] = (byte) ((in)&0x00FF);
					in>>=8;
					out[3] = (byte) ((in)&0x00FF);
				};

				static inline void dwordToByteReversed(dword in, byte *out) {
					out[3] = (byte) ((in)&0x00FF);
					in>>=8;
					out[2] = (byte) ((in)&0x00FF);
					in>>=8;
					out[1] = (byte) ((in)&0x00FF);
					in>>=8;
					out[0] = (byte) ((in)&0x00FF);
				};

				static inline void qwordToByte(qword in, byte *out) {
					out[0] = (byte) ((in)&0x00FF);
					in>>=8;
					out[1] = (byte) ((in)&0x00FF);
					in>>=8;
					out[2] = (byte) ((in)&0x00FF);
					in>>=8;
					out[3] = (byte) ((in)&0x00FF);
					in>>=8;
					out[4] = (byte) ((in)&0x00FF);
					in>>=8;
					out[5] = (byte) ((in)&0x00FF);
					in>>=8;
					out[6] = (byte) ((in)&0x00FF);
					in>>=8;
					out[7] = (byte) ((in)&0x00FF);
				};

				static inline void qwordToByteReversed(qword in, byte *out) {
					out[7] = (byte) ((in)&0x00FF);
					in>>=8;
					out[6] = (byte) ((in)&0x00FF);
					in>>=8;
					out[5] = (byte) ((in)&0x00FF);
					in>>=8;
					out[4] = (byte) ((in)&0x00FF);
					in>>=8;
					out[3] = (byte) ((in)&0x00FF);
					in>>=8;
					out[2] = (byte) ((in)&0x00FF);
					in>>=8;
					out[1] = (byte) ((in)&0x00FF);
					in>>=8;
					out[0] = (byte) ((in)&0x00FF);
				};

		};

	};
};

#endif
