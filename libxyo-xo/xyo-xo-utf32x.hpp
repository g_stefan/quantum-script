//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XO_UTF32X_HPP
#define XYO_XO_UTF32X_HPP

#ifndef XYO_XO_STRINGDWORD_HPP
#include "xyo-xo-stringdword.hpp"
#endif

namespace XYO {
	namespace XO {

		typedef dword utf32;
		typedef StringDWord StringUtf32;

		class Utf32X {
			public:

				XYO_XO_EXPORT static size_t memChrSize(utf32 *x);
				XYO_XO_EXPORT static bool chrIsValid(utf32 *x);

		};

	};
};

#endif

