//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xo-stringbytex.hpp"

namespace XYO {
	namespace XO {

		StringByte StringByteX::trimWithElement(StringByte o, StringByte x) {
			const byte *x1;
			const byte *x2;
			StringByte retV;
			x1 = StringBaseByte::toNotInElement(o.value(), x);
			x2 = StringBaseByte::toNotInFromEndElement(o.value(), x);

			if (*x1 == 0 || *x2 == 0) {
				return o;
			};
			retV.set(x1, x2 - x1 + 1);
			return retV;
		};

		StringByte StringByteX::replace(StringByte o, StringByte x, StringByte y) {
			StringByte retV;
			size_t k, ln;
			size_t x_ln;
			size_t y_ln;

			x_ln = StringBaseByte::length(x);
			y_ln = StringBaseByte::length(y);
			if (o.length() < x_ln) {
				return o;
			};
			ln = o.length() - x_ln;

			k = 0;
			while (k < ln) {
				if (StringBaseByte::compareN(&o[(int) k], x, x_ln) == 0) {
					retV.concatenate(y, y_ln);
					k += x_ln;
					continue;
				} else {
					retV.concatenate(&o[(int) k], 1);
					k++;
				};
			};


			if (StringBaseByte::compareN(&o[(int) k], x, x_ln) == 0) {
				retV.concatenate(y, y_ln);
			} else {
				retV << &o[(int) k];
			};

			return retV;
		};

		StringByte StringByteX::nilAtFirstFromEnd(StringByte o, StringByte x) {
			size_t idx;
			if (StringBaseByte::indexOfFromEnd(o, x, idx)) {
				StringByte retV;
				retV.set(o, idx);
				return retV;
			};
			return o;
		};

		StringByte StringByteX::substring(StringByte o, size_t start, size_t length) {
			StringByte retV;
			if (length == 0) {
				return retV;
			};
			if (start > o.length()) {
				return retV;
			};
			if (start + length > o.length()) {
				length=o.length()-start;
			};
			retV.set(&o[(int) start], length);
			return retV;
		};

		StringByte StringByteX::substring(StringByte o, size_t start) {
			StringByte retV;
			if (start > o.length()) {
				return retV;
			};
			retV.set(&o[(int) start], o.length() - start);
			return retV;
		};

		StringByte StringByteX::toLowerCaseAscii(StringByte value) {
			StringByte out_(value.getSize(), value.getChunk());

			size_t totalLn;
			byte *inx;
			byte *outx;

			totalLn = 0;
			inx = value.value();
			outx = out_.value();
			while (*inx) {
				*outx = StringBaseByte::chrToLowerCaseAscii(*inx);
				++outx;
				++inx;
				++totalLn;
			};
			*outx = 0;
			out_.setLength(totalLn);
			return out_;
		};

		StringByte StringByteX::toUpperCaseAscii(StringByte value) {
			StringByte out_(value.getSize(), value.getChunk());

			size_t totalLn;
			byte *inx;
			byte *outx;

			totalLn = 0;
			inx = value.value();
			outx = out_.value();
			while (*inx) {
				*outx = StringBaseByte::chrToUpperCaseAscii(*inx);
				++outx;
				++inx;
				++totalLn;
			};
			*outx = 0;

			out_.setLength(totalLn);
			return out_;
		};

		bool StringByteX::matchAscii(StringByte text, StringByte sig) {
			size_t i;
			size_t j;

			i = 0;
			j = 0;
			for (; (i<sig.length()) && (j<text.length()); ++i) {
				if (sig[i] == '*') {
					while (text[j] && (StringBaseByte::chrToUpperCaseAscii(text[j]) != StringBaseByte::chrToUpperCaseAscii(sig[i + 1]))) {
						j++;
					};
				} else if (StringBaseByte::chrToUpperCaseAscii(text[j]) == StringBaseByte::chrToUpperCaseAscii(sig[i]) || sig[i] == '?') {
					j++;
				} else {
					return false;
				}
			};
			if (sig[i] == 0) {
				if (text[j] == 0) {
					return true;
				};
			};
			return false;
		};

		bool StringByteX::split2FromBegin(StringByte text, StringByte sig,StringByte &firstPart,StringByte &secondPart) {
			size_t index;
			if(StringBaseByte::indexOf(text, text.length(), sig, sig.length(), 0, index)) {
				firstPart=substring(text,0,index);
				secondPart=substring(text,index+sig.length());
				return true;
			};
			firstPart=text;
			secondPart=(const byte *)StringBaseByte::empty;
			return false;
		};

		StringByte StringByteX::encodeC(StringByte in) {
			StringByte retV=(const byte *)"\"";
			byte buffer[8];
			size_t k;
			byte *scan;
			scan=in.value();
			for(k=0; k<in.length(); ++k,++scan) {
				if(*scan=='\\') {
					retV<<(const byte *)"\\\\";
					continue;
				};
				if(*scan=='"') {
					retV<<(const byte *)"\\\"";
					continue;
				};
				if(*scan=='\'') {
					retV<<(const byte *)"'";
					continue;
				};
				if(*scan=='\n') {
					retV<<(const byte *)"\\n";
					continue;
				};
				if(*scan=='\r') {
					retV<<(const byte *)"\\r";
					continue;
				};
				if(*scan=='\t') {
					retV<<(const byte *)"\\t";
					continue;
				};
				if(*scan>=0x20&&*scan<=0x7E) {
					retV.concatenate(scan,1);
					continue;
				};
				sprintf((char *)buffer,"\\x%02X",(int)*scan);
				retV<<buffer;
			};
			retV<<(const byte *)"\"";
			return retV;
		};

		StringByte StringByteX::fromHex(StringByte in) {
			size_t ln=in.length()/2;
			if(ln==0) {
				return "";
			};
			StringByte retV(ln+1,0);
			retV.setLength(ln);
			byte *buffer=retV.index(0);

			size_t m;
			size_t k;
			unsigned int h1,h2;

			for(k=0,m=0; m<ln*2; m+=2,++k) {
				h1=in[m];
				if(h1>='0'&&h1<='9') {
					h1-='0';
				} else if(h1>='A'&&h1<='F') {
					h1-='A';
					h1+=10;
				} else if(h1>='a'&&h1<='f') {
					h1-='a';
					h1+=10;
				} else {
					h1=0;
				};
				h2=in[m+1];
				if(h2>='0'&&h2<='9') {
					h2-='0';
				} else if(h2>='A'&&h2<='F') {
					h2-='A';
					h2+=10;
				} else if(h2>='a'&&h2<='f') {
					h2-='a';
					h2+=10;
				} else {
					h2=0;
				};

				buffer[k]=(h1<<4|h2);
			};

			buffer[ln]=0;

			return retV;
		};

		StringByte StringByteX::toHex(StringByte in) {
			StringByte retV(in.length()*2+1,0);
			size_t k;
			size_t m;
			size_t ln;
			ln=in.length();
			for(k=0,m=0; k<ln; ++k,++m) {
				retV[m]=(in[k]>>4)&0x0F;
				if(retV[m]>9) {
					retV[m]-=10;
					retV[m]+='a';
				} else {
					retV[m]+='0';
				};
				++m;
				retV[m]=in[k]&0x0F;
				if(retV[m]>9) {
					retV[m]-=10;
					retV[m]+='a';
				} else {
					retV[m]+='0';
				};
			};
			retV[m]=0;
			retV.setLength(m);
			return retV;
		};

	};
};


