//
// XYO XO Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xyo-xo-md5hash.hpp"
#include "xyo-xo-bytex.hpp"

//
// http://en.wikipedia.org/wiki/MD5
//

namespace XYO {
	namespace XO {

		int MD5Hash::r[] = {
			7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
			5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
			4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
			6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
		};

		dword MD5Hash::k[] = {
			0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
			0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
			0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
			0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
			0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
			0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
			0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
			0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
			0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
			0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
			0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
			0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
			0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
			0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
			0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
			0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
		};

		MD5Hash::MD5Hash() {
			processInit();
		};

		void MD5Hash::processInit() {
			h0 = 0x67452301;
			h1 = 0xefcdab89;
			h2 = 0x98badcfe;
			h3 = 0x10325476;
			length=0;
			stateM=0;
			lastData[0]=0;
			lastData[1]=0;
			lastData[2]=0;
			lastData[3]=0;
		};

		void MD5Hash::hashBlock(dword *w) {
			dword a;
			dword b;
			dword c;
			dword d;

			dword f;
			dword temp;

			int i;
			int g;

			a = h0;
			b = h1;
			c = h2;
			d = h3;

			f = 0;
			g = 0;

			for (i = 0; i < 64; ++i) {
				if (i < 16) {
					f = (b & c) | ((~b) & d);
					g = i;
				};
				if (i >= 16 && i < 32) {
					f = (d & b) | ((~d) & c);
					g = (5 * i + 1) % 16;
				};
				if (i >= 32 && i < 48) {
					f = b ^ c ^ d;
					g = (3 * i + 5) % 16;
				};
				if (i >= 48 && i < 64) {
					f = c ^ (b | (~d));
					g = (7 * i) % 16;
				};
				temp = d;
				d = c;
				c = b;

				b = b + ByteX::dwordLeftRotate(a + f + k[i] + w[g], r[i]);
				a = temp;

			};

			h0 += a;
			h1 += b;
			h2 += c;
			h3 += d;

		};

		void MD5Hash::processBytes(const byte *toHash,size_t length_) {
			size_t k,z;
			size_t m=length%4;
			size_t length__=length_&(~((size_t)(0x03)));

			length+=length_;

			if(m) {

				for(k=m,z=0; z<length_;) {
					lastData[k]=*toHash;
					++toHash;
					++z;
					++k;
					if(k==4) {
						break;
					};
				};
				if(k!=4) {
					return;
				};
				length_-=z;
				process[stateM]=ByteX::dwordFromByte(lastData);

				++stateM;
				if(stateM==16) {
					hashBlock(process);
					stateM=0;
				};
			};


			for(k=0; k<length__; k+=4) {
				process[stateM]=ByteX::dwordFromByte(toHash);
				toHash+=4;
				++stateM;
				if(stateM==16) {
					hashBlock(process);
					stateM=0;
				};
			};

			m=length_%4;
			if(m) {
				for(k=0; k<m; ++k) {
					lastData[k]=*toHash;
					++toHash;
				};
			};
		};

		void MD5Hash::processDone() {
			size_t m=length%64;
			qword finalLength=length*8;
			byte data[8];
			if(m<56) {
				data[0]=0x80;
				processBytes(data,1);
				++m;
				data[0]=0;
				for(; m<56; ++m) {
					processBytes(data,1);
				};
				ByteX::qwordToByte(finalLength,data);
				processBytes(data,8);
				return;
			};
			if(m<63) {
				data[0]=0x80;
				processBytes(data,1);
				++m;
				data[0]=0;
				for(; m<64+56; ++m) {
					processBytes(data,1);
				};
				ByteX::qwordToByte(finalLength,data);
				processBytes(data,8);
				return;
			};
			data[0]=0x80;
			processBytes(data,1);
			++m;
			data[0]=0;
			for(; m<56; ++m) {
				processBytes(data,1);
			};
			ByteX::qwordToByte(finalLength,data);
			processBytes(data,8);
		};

		String MD5Hash::getHashAsHex() {
			String retV(64, 0);
			byte result[16];

			toBytes(result);

			sprintf((char *) retV.value(), "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				result[0], result[1], result[2], result[3],
				result[4], result[5], result[6], result[7],
				result[8], result[9], result[10], result[11],
				result[12], result[13], result[14], result[15]);

			retV.setLength(32);
			return retV;
		};

		String MD5Hash::getHashString(String toHash) {
			MD5Hash hash;
			hash.processBytes((byte *)toHash.value(), toHash.length());
			hash.processDone();
			return hash.getHashAsHex();
		};

		void MD5Hash::hashStringToBytes(String toHash,byte *buffer) {
			MD5Hash hash;
			hash.processBytes((byte *)toHash.value(), toHash.length());
			hash.processDone();
			hash.toBytes(buffer);
		};

		void MD5Hash::toBytes(byte *buffer) {
			ByteX::dwordToByte(h0, buffer);
			ByteX::dwordToByte(h1, buffer + 4);
			ByteX::dwordToByte(h2, buffer + 8);
			ByteX::dwordToByte(h3, buffer + 12);
		};

	};
};

