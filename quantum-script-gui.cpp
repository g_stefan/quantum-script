//
// Quantum Script
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif

#include "quantum-script-licence.hpp"
#include "quantum-script-copyright.hpp"
#ifndef QUANTUM_SCRIPT_NO_VERSION
#include "quantum-script-version.hpp"
#endif

#include "libquantum-script.hpp"


using namespace XYO;
using namespace XYO::XY;
using namespace Quantum::Script;

class Application :
	public virtual IMain {
		XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Application);
	protected:

		static void initExecutive(Executive *);

		void showUsage();
		void showLicence();

		char buffer[4096];
		static char *msgBoxTitle;
	public:

		inline Application() {};

		int main(int cmdN, char *cmdS[]);

};

char *Application::msgBoxTitle="Quantum Script";

void Application::initExecutive(Executive *executive) {
};

void Application::showUsage() {
	String message;
#ifdef QUANTUM_SCRIPT_INTERNAL
	message<<"Quantum Script\n";
#else
	sprintf(buffer,"Quantum Script - version %s build %s [%s]\n", Quantum::Script::Version::getVersion(), Quantum::Script::Version::getBuild(), Quantum::Script::Version::getDatetime());
	message<<buffer;
#endif
	message<<(char *)Quantum::Script::Copyright::fullCopyright()<<"\n\n";

	message<<
	       "options:\n"
	       "    --licence           show licence\n"
	       "    script.js           execute script\n";
	message<<"\n";
	MessageBox(NULL,(char *)message.value(),msgBoxTitle,MB_OK|MB_ICONINFORMATION);
};

void Application::showLicence() {
	MessageBox(NULL,Quantum::Script::Licence::content(),msgBoxTitle,MB_OK|MB_ICONINFORMATION);
};

int Application::main(int cmdN, char *cmdS[]) {
	int i;
	char *opt;
	char *fileIn;

	fileIn = NULL;
	for (i = 1; i < cmdN; ++i) {
		if (strncmp(cmdS[i], "--", 2) == 0) {
			opt = &cmdS[i][2];
			if (strcmp(opt, "licence") == 0) {
				showLicence();
				if (cmdN == 2) {
					return 0;
				};
			};
			continue;
		};
		if (!fileIn) {
			fileIn = cmdS[i];
		};
	};

	if(fileIn==NULL) {
#ifndef QUANTUM_SCRIPT_APPLICATION
		showUsage();
		return 0;
#else
		fileIn="application.js";
#endif
	};

	if(ExecutiveX::initExecutive(cmdN,cmdS,initExecutive)) {
		if(ExecutiveX::executeFile(fileIn)) {
			ExecutiveX::executeEnd();
			return 0;
		};
	};

	MessageBox(NULL,(ExecutiveX::getError()),msgBoxTitle,MB_OK|MB_ICONERROR);

	ExecutiveX::executeEnd();
	return -1;
};

XYO_XY_WINMAIN_STD(Application);

#endif

#ifdef QUANTUM_SCRIPT_AMALGAM
#include "quantum-script-amalgam.cpp"
#endif
