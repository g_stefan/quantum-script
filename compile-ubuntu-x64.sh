#!/bin/sh

./compile-ubuntu-x64-file-to-cs.sh

./file-to-cs libStdErrorSource  	  libquantum-script/quantum-script-libstderror.js 		libquantum-script/quantum-script-libstderror.src
./file-to-cs libStdScriptSource 	  libquantum-script/quantum-script-libstdscript.js 		libquantum-script/quantum-script-libstdscript.src
./file-to-cs libStdArraySource  	  libquantum-script/quantum-script-libstdarray.js 		libquantum-script/quantum-script-libstdarray.src
./file-to-cs libStdFiberSource  	  libquantum-script/quantum-script-libstdfiber.js 		libquantum-script/quantum-script-libstdfiber.src
./file-to-cs libStdThreadSource 	  libquantum-script/quantum-script-libstdthread.js 		libquantum-script/quantum-script-libstdthread.src
./file-to-cs libStdApplicationSource  	  libquantum-script/quantum-script-libstdapplication.js 	libquantum-script/quantum-script-libstdapplication.src
./file-to-cs libStdMathSource 	  	  libquantum-script/quantum-script-libstdmath.js 		libquantum-script/quantum-script-libstdmath.src
./file-to-cs libStdShellSource 	  	  libquantum-script/quantum-script-libstdshell.js 		libquantum-script/quantum-script-libstdshell.src
./file-to-cs libStdJSONSource 	  	  libquantum-script/quantum-script-libstdjson.js 		libquantum-script/quantum-script-libstdjson.src
./file-to-cs libStdJobSource 	  	  libquantum-script/quantum-script-libstdjob.js 		libquantum-script/quantum-script-libstdjob.src
./file-to-cs libStdHTTPSource 	  	  libquantum-script/quantum-script-libstdhttp.js 		libquantum-script/quantum-script-libstdhttp.src
./file-to-cs libStdFunctionSource 	  libquantum-script/quantum-script-libstdfunction.js 		libquantum-script/quantum-script-libstdfunction.src
./file-to-cs libStdObjectSource 	  libquantum-script/quantum-script-libstdobject.js 		libquantum-script/quantum-script-libstdobject.src


SRC=""

SRC="$SRC quantum-script.cpp"
SRC="$SRC quantum-script-copyright.cpp"
SRC="$SRC quantum-script-licence.cpp"

INC=""
INC="$INC -Ilibquantum-script"
INC="$INC -Ilibxyo-xy"
INC="$INC -Ilibxyo-xo"

DEF=""
DEF="$DEF -DXYO_OS_TYPE_UNIX"
DEF="$DEF -DXYO_MACHINE_64BIT"
DEF="$DEF -DXYO_COMPILER_GNU"
DEF="$DEF -DXYO_XY_INTERNAL"
DEF="$DEF -DXYO_XO_INTERNAL"
DEF="$DEF -DQUANTUM_SCRIPT_INTERNAL"
DEF="$DEF -DLIBXYO_XY_NO_VERSION"
DEF="$DEF -DLIBXYO_XO_NO_VERSION"
DEF="$DEF -DLIBQUANTUM_SCRIPT_NO_VERSION"
DEF="$DEF -DQUANTUM_SCRIPT_NO_VERSION"
DEF="$DEF -DQUANTUM_SCRIPT_AMALGAM"

#DEF="$DEF -DXYO_MEMORY_LEAK_DETECTOR"
#DEF="$DEF -DXYO_SINGLE_THREAD"
#DEF="$DEF -DQUANTUM_SCRIPT_DEBUG_ASM"
#DEF="$DEF -DQUANTUM_SCRIPT_DEBUG_RUNTIME"
#DEF="$DEF -DQUANTUM_SCRIPT_DISABLE_ASM_OPTIMIZER"
#DEF="$DEF -DQUANTUM_SCRIPT_SINGLE_FIBER"
#DEF="$DEF -DQUANTUM_SCRIPT_SINGLE_THREAD"
#DEF="$DEF -DQUANTUM_SCRIPT_DISABLE_CLOSURE"

gcc -o quantum-script -O3 -std=c++11 -std=gnu++11 $DEF $INC $SRC -lstdc++ -lpthread -ldl -lm

rm -f *.o
