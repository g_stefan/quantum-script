
Quantum Script
--------------

This is an C++ embeddable prototype-based script engine.
It uses an Recursive Descent Parser that emits an abstract high-level assembly language whitch is made from native functions.
It has an intermediate step to produce some optimization on the assembly instructions.	 
	
The virtual machine supports fibers (single thread co-operative multitasking) and threads.
Can be configured to use only a single thread multiple fibers (no thread support),
or single thread single fiber (the fastest, but no fiber/thread support)

The memory manager is built around multi-threading suport in "isolation", every thread has its own
memory pool. This allows for nonblocking garbage collector.

Full garbage-collector based on executive-dynamic reference counting with mark-and-sweep algorithm.	

Include
-------

- libxyo-xy
- libxyo-xo

