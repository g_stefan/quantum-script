#!/bin/sh
SRC=""

XLIB="file-to-cs.src"

SRC="$SRC $XLIB/file-to-cs.cpp"
SRC="$SRC $XLIB/file-to-cs-copyright.cpp"
SRC="$SRC $XLIB/file-to-cs-licence.cpp"

INC=""
INC="$INC -Ifile-to-cs.src"

DEF=""
DEF="$DEF -DXYO_OS_TYPE_UNIX"
DEF="$DEF -DXYO_MACHINE_32BIT"
DEF="$DEF -DXYO_COMPILER_GNU"
DEF="$DEF -DFILE_TO_CS_NO_VERSION"

gcc -o file-to-cs -O3 -std=c++11 -std=gnu++11 $DEF $INC $SRC -lstdc++ -lpthread -ldl -lm

rm -f *.o
