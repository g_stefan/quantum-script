#ifndef FILE_TO_CS_VERSION_HPP
#define FILE_TO_CS_VERSION_HPP

#define FILE_TO_CS_VERSION_ABCD      2,0,0,43
#define FILE_TO_CS_VERSION_A         2
#define FILE_TO_CS_VERSION_B         0
#define FILE_TO_CS_VERSION_C         0
#define FILE_TO_CS_VERSION_D         43
#define FILE_TO_CS_VERSION_STR_ABCD  "2.0.0.43"
#define FILE_TO_CS_VERSION_STR       "2.0.0"
#define FILE_TO_CS_VERSION_STR_BUILD "43"
#define FILE_TO_CS_VERSION_BUILD     43
#define FILE_TO_CS_VERSION_HOUR      18
#define FILE_TO_CS_VERSION_MINUTE    45
#define FILE_TO_CS_VERSION_SECOND    2
#define FILE_TO_CS_VERSION_DAY       14
#define FILE_TO_CS_VERSION_MONTH     6
#define FILE_TO_CS_VERSION_YEAR      2015
#define FILE_TO_CS_VERSION_STR_DATETIME "2015-06-14 18:45:02"

#ifndef XYO_RC

namespace FileToCs {

	class Version {
		public:
			static const char *getABCD();
			static const char *getA();
			static const char *getB();
			static const char *getC();
			static const char *getD();
			static const char *getVersion();
			static const char *getBuild();
			static const char *getHour();
			static const char *getMinute();
			static const char *getSecond();
			static const char *getDay();
			static const char *getMonth();
			static const char *getYear();
			static const char *getDatetime();
	};


};

#endif
#endif

