//
// File To C Source
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#ifdef FILE_TO_CS_NO_VERSION
#else
#       include "file-to-cs-version.hpp"
#endif

#include "file-to-cs-copyright.hpp"
#include "file-to-cs-licence.hpp"

void showLicence() {
	printf("%s", FileToCs::Licence::content());
};

void showUsage() {
#ifdef FILE_TO_CS_NO_VERSION
	printf("file-to-cs\n");
#else
	printf("file-to-cs - version %s build %s [%s]\n", FileToCs::Version::getVersion(), FileToCs::Version::getBuild(), FileToCs::Version::getDatetime());
#endif
	printf("%s\n\n", FileToCs::Copyright::fullCopyright());
	printf("\n");
	printf("Usage:\n\n");
	printf("\tfile-to-cs --licence\n");
	printf("\tfile-to-cs [--overwrite] [name] [input] [output]\n");
	printf("\t\t- Write file [input] into [output] with name [name]\n");
	printf("\tfile-to-cs --append [name] [input] [output]\n");
	printf("\t\t- Append file [input] into [output] with name [name]\n");
};

int main(int cmdN, char *cmdS[]) {
	FILE *input, *output;
	unsigned int ch;
	int retV;
	int k;
	int index;
	int overWrite;
	int x1;
	int x2;
	int x3;
	const char *mode="wb";

	retV = 1;

	if (cmdN == 2) {
		if (strcmp(cmdS[1], "--licence") == 0) {
			showLicence();
			return 0;
		};
		return 1;
	};

	if (cmdN < 4) {
		showUsage();
		return 2;
	};

	x1=1;
	x2=2;
	x3=3;

	if (strcmp(cmdS[1], "--overwrite") == 0) {
		overWrite=1;
		x1=2;
		x2=3;
		x3=4;
		mode="wb";
		if (cmdN < 5) {
			showUsage();
			return 2;
		};
	};

	if (strcmp(cmdS[1], "--append") == 0) {
		overWrite=1;
		x1=2;
		x2=3;
		x3=4;
		mode="ab";
		if (cmdN < 5) {
			showUsage();
			return 2;
		};
	};


	input = fopen(cmdS[x2], "rb");
	if (input != NULL) {
		output = fopen(cmdS[x3], mode);
		if (output != NULL) {
			fprintf(output, "static unsigned char %s[]={", cmdS[x1]);

			ch = 0x00;
			index=0;
			while(fread(&ch, 1, 1, input)==1) {
				if(index==0) {
					fprintf(output, "\n\t");
				};

				++index;
				index%=32;

				fprintf(output, "0x%02X,", ch);
				ch=0;
			};

			if(index==0) {
				fprintf(output, "\n\t");
			};

			++index;
			index%=32;

			fprintf(output, "0x00");

			fprintf(output, "\n};\n");
			retV = 0;
			fclose(output);

		} else {
			printf("Fatal: Unable to open %s\n", cmdS[3]);
		};
		fclose(input);
	} else {
		printf("Fatal: Unable to open %s\n", cmdS[2]);
	};
	return retV;
};

