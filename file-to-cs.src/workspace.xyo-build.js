//
// File To C Source
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

.solution("file-to-cs",function() {

	.project("file-to-cs","exe",function() {
		.file("source",["*.cpp","*.hpp","*.rc"]);

		.option("version","xyo-version", {
			type:"xyo-cpp",
			sourceBegin:"namespace FileToCs{\r\n",
			sourceEnd:"\r\n};\r\n",
			lineBegin:"\t"
		});

		.option("sign","xyo-security");
		.option("licence","mit");
		.option("crt","type","static");
		.option("cpp","exceptions",false);
		.option("cpp","rtti",false);

	});
});

