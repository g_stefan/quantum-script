//
// File To C Source
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef FILE_TO_CS_COPYRIGHT_HPP
#define FILE_TO_CS_COPYRIGHT_HPP

#define FILE_TO_CS_COPYRIGHT            "Copyright (C) Grigore Stefan."
#define FILE_TO_CS_PUBLISHER            "Grigore Stefan"
#define FILE_TO_CS_COMPANY              FILE_TO_CS_PUBLISHER
#define FILE_TO_CS_CONTACT              "g_stefan@yahoo.com"
#define FILE_TO_CS_FULL_COPYRIGHT       FILE_TO_CS_COPYRIGHT " <" FILE_TO_CS_CONTACT ">"

#ifndef XYO_RC

namespace FileToCs {

	class Copyright {
		public:
			static const char *copyright();
			static const char *publisher();
			static const char *company();
			static const char *contact();
			static const char *fullCopyright();

	};

};

#endif
#endif
