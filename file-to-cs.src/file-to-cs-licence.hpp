//
// File To C Source
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef FILE_TO_CS_LICENCE_HPP
#define FILE_TO_CS_LICENCE_HPP

namespace FileToCs {

	class Licence {
		public:
			static const char *content();
			static const char *shortContent();
	};

};

#endif
