//
// Quantum Script
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

.solution("quantum-script",function() {

	.option("licence","mit");

	.project("quantum-script","exe",function() {

		.file("source",[
			      "quantum-script.cpp",
			      "quantum-script-copyright.cpp",
			      "quantum-script-licence.cpp",
			      "quantum-script-version.cpp",
			      "*.hpp",
			      "quantum-script.rc"
		      ]);

		.option("version","xyo-version", {
			type:"xyo-cpp",
			sourceBegin:
			"namespace Quantum{\r\n"+
			"\tnamespace Script{\r\n",
			sourceEnd:
			"\t};\r\n"+
			"};",
			lineBegin:"\t\t"
		});

		.option("sign","xyo-security");

		.dependency("libquantum-script","libquantum-script","dll");
	});

	if(Platform.is("win")) {
		.project("quantum-script-gui","exe",function() {
			.option("include","library");
			.file("source",[
				      "quantum-script-gui.cpp",
				      "quantum-script-copyright.cpp",
				      "quantum-script-licence.cpp",
				      "quantum-script-version.cpp",
				      "*.hpp",
				      "quantum-script-gui.rc"
			      ]);

			.option("version","xyo-version", {
				type:"xyo-cpp",
				sourceBegin:
				"namespace Quantum{\r\n"+
				"\tnamespace Script{\r\n",
				sourceEnd:
				"\t};\r\n"+
				"};",
				lineBegin:"\t\t"
			});
			.option("sign","xyo-security");

			.dependency("libquantum-script","libquantum-script","dll");
		});
	};

});

