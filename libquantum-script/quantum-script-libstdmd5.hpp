//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_LIBSTDMD5_HPP
#define QUANTUM_SCRIPT_LIBSTDMD5_HPP

#ifndef QUANTUM_SCRIPT_EXECUTIVE_HPP
#include "quantum-script-executive.hpp"
#endif

namespace Quantum {
	namespace Script {

		namespace LibStdMD5 {

			QUANTUM_SCRIPT_EXPORT void initExecutive(Executive *executive,void *extensionId);

		};
	};
};

#endif
