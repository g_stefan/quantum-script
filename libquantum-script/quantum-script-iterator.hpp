//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#define QUANTUM_SCRIPT_ITERATOR_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class Iterator;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::Iterator>:
			public TMemoryObjectPoolActive<Quantum::Script::Iterator> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class Iterator :
			public virtual Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Iterator);
			public:

				inline Iterator() {
				};

				QUANTUM_SCRIPT_EXPORT virtual bool next(TPointerX<Variable> &out);
		};
	};
};



#endif
