﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-context.hpp"
#include "quantum-script-variablefunction.hpp"
#include "quantum-script-variablearray.hpp"
#include "quantum-script-variableassociativearray.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variabledatetime.hpp"
#include "quantum-script-variablerandom.hpp"
#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableatomic.hpp"
#include "quantum-script-variablebuffer.hpp"
#include "quantum-script-variablequitmessage.hpp"
#include "quantum-script-variableutf16.hpp"
#include "quantum-script-variableutf32.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		Symbol Context::getSymbol(String name) {
			return (TXSingletonThread<SymbolList>::getValue())->getSymbol(name);
		};

		String Context::getSymbolMirror(Symbol id) {
			return (TXSingletonThread<SymbolList>::getValue())->getSymbolMirror(id);
		};

		SymbolList &Context::getSymbolList() {
			return *(TXSingletonThread<SymbolList>::getValue());
		};

		class XValueUndefined:
			public Object {
			public:
				TPointerX<Variable> valueUndefined;
				XValueUndefined();
				static void memoryInit();
		};

		XValueUndefined::XValueUndefined() {
			valueUndefined.memoryLink(this);
		};

		void XValueUndefined::memoryInit() {
			TMemory<Variable>::memoryInit();
		};

		TPointerX<Variable> &Context::getValueUndefined() {
			return  (TXSingletonThread<XValueUndefined>::getValue())->valueUndefined;
		};

		class XGlobalObject:
			public Object {
			public:
				TPointerX<Variable> globalObject;
				XGlobalObject();
				static void memoryInit();
		};

		XGlobalObject::XGlobalObject() {
			globalObject.memoryLink(this);
		};

		void XGlobalObject::memoryInit() {
			TMemory<Variable>::memoryInit();
			TMemory<VariableObject>::memoryInit();
		};

		TPointerX<Variable> &Context::getGlobalObject() {
			return  (TXSingletonThread<XGlobalObject>::getValue())->globalObject;
		};

		class XContext:
			public Object {
			public:

				TPointerX<Prototype> prototypeBoolean;
				TPointerX<Prototype> prototypeNumber;
				TPointerX<Prototype> prototypeString;
				TPointerX<Prototype> prototypeArray;
				TPointerX<Prototype> prototypeFunction;
				TPointerX<Prototype> prototypeObject;
				TPointerX<Prototype> prototypeAssociativeArray;
				TPointerX<Prototype> prototypeResource;
				TPointerX<Prototype> prototypeDateTime;
				TPointerX<Prototype> prototypeRandom;
				TPointerX<Prototype> prototypeAtomic;
				TPointerX<Prototype> prototypeBuffer;
				TPointerX<Prototype> prototypeUtf16;
				TPointerX<Prototype> prototypeUtf32;

				Symbol symbolLength;
				Symbol symbolPrototype;
				Symbol symbolFunctionBoolean;
				Symbol symbolFunctionNumber;
				Symbol symbolFunctionString;
				Symbol symbolFunctionArray;
				Symbol symbolFunctionFunction;
				Symbol symbolFunctionObject;
				Symbol symbolFunctionAssociativeArray;
				Symbol symbolFunctionResource;
				Symbol symbolFunctionDateTime;
				Symbol symbolFunctionRandom;
				Symbol symbolFunctionAtomic;
				Symbol symbolSize;
				Symbol symbolFunctionBuffer;
				Symbol symbolFunctionUtf16;
				Symbol symbolFunctionUtf32;
				Symbol symbolToString;
				Symbol symbolToNumber;

				XContext();

				void newContext();
				void deleteContext();

				static void memoryInit();
		};

		XContext::XContext() {
			prototypeBoolean.memoryLink(this);
			prototypeNumber.memoryLink(this);
			prototypeString.memoryLink(this);
			prototypeArray.memoryLink(this);
			prototypeFunction.memoryLink(this);
			prototypeObject.memoryLink(this);
			prototypeAssociativeArray.memoryLink(this);
			prototypeResource.memoryLink(this);
			prototypeDateTime.memoryLink(this);
			prototypeRandom.memoryLink(this);
			prototypeAtomic.memoryLink(this);
			prototypeBuffer.memoryLink(this);
			prototypeUtf16.memoryLink(this);
			prototypeUtf32.memoryLink(this);
		};

		static TPointerOwner<Variable> functionBoolean(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableBoolean::newVariable((arguments->index(0))->toBoolean());
		};

		static TPointerOwner<Variable> functionNumber(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableNumber::newVariable((arguments->index(0))->toNumber());
		};

		static TPointerOwner<Variable> functionString(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableString::newVariable((arguments->index(0))->toString());
		};

		static TPointerOwner<Variable> functionFunction(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return Context::getValueUndefined();
		};

		static TPointerOwner<Variable> functionObject(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return this_;
		};

		static TPointerOwner<Variable> functionArray(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableArray::newVariable();
		};

		static TPointerOwner<Variable> functionAssociativeArray(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableAssociativeArray::newVariable();
		};

		static TPointerOwner<Variable> functionDateTime(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableDateTime::newVariable();
		};

		static TPointerOwner<Variable> functionRandom(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableRandom::newVariable();
		};

		static TPointerOwner<Variable> functionAtomic(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableAtomic::newVariable();
		};

		static TPointerOwner<Variable> functionBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			Number value=(arguments->index(0))->toNumber();
			if(isnan(value)||isinf(value)||signbit(value)) {
				value=1024;
			};
			return VariableBuffer::newVariable((size_t)value);
		};

		static TPointerOwner<Variable> functionUtf16(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableUtf16::newVariable(VariableUtf16::getUtf16(arguments->index(0)));
		};

		static TPointerOwner<Variable> functionUtf32(VariableFunction *function,Variable *this_,VariableArray *arguments) {
			return VariableUtf32::newVariable(VariableUtf32::getUtf32(arguments->index(0)));
		};

		void XContext::newContext() {
			VariableFunction *defaultPrototypeFunction;
			symbolLength=Context::getSymbol("length");
			symbolPrototype=Context::getSymbol("prototype");
			symbolFunctionBoolean=Context::getSymbol("Boolean");
			symbolFunctionNumber=Context::getSymbol("Number");
			symbolFunctionString=Context::getSymbol("String");
			symbolFunctionArray=Context::getSymbol("Array");
			symbolFunctionFunction=Context::getSymbol("Function");
			symbolFunctionObject=Context::getSymbol("Object");
			symbolFunctionAssociativeArray=Context::getSymbol("AssociativeArray");
			symbolFunctionResource=Context::getSymbol("Resource");
			symbolFunctionDateTime=Context::getSymbol("DateTime");
			symbolFunctionRandom=Context::getSymbol("Random");
			symbolFunctionAtomic=Context::getSymbol("Atomic");
			symbolSize=Context::getSymbol("size");
			symbolFunctionBuffer=Context::getSymbol("Buffer");
			symbolFunctionUtf16=Context::getSymbol("Utf16");
			symbolFunctionUtf32=Context::getSymbol("Utf32");
			symbolToString=Context::getSymbol("toString");
			symbolToNumber=Context::getSymbol("toNumber");

			prototypeBoolean.newObject();
			prototypeNumber.newObject();
			prototypeString.newObject();
			prototypeArray.newObject();
			prototypeFunction.newObject();
			prototypeObject.newObject();
			prototypeAssociativeArray.newObject();
			prototypeResource.newObject();
			prototypeDateTime.newObject();
			prototypeRandom.newObject();
			prototypeAtomic.newObject();
			prototypeBuffer.newObject();
			prototypeUtf16.newObject();
			prototypeUtf32.newObject();

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionBoolean,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionBoolean)).setObjectX(defaultPrototypeFunction);
			prototypeBoolean=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionNumber,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionNumber)).setObjectX(defaultPrototypeFunction);
			prototypeNumber=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionString,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionString)).setObjectX(defaultPrototypeFunction);
			prototypeString=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionArray,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionArray)).setObjectX(defaultPrototypeFunction);
			prototypeArray=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionFunction,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionFunction)).setObjectX(defaultPrototypeFunction);
			prototypeFunction=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionObject,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionObject)).setObjectX(defaultPrototypeFunction);
			prototypeObject=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionAssociativeArray,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionAssociativeArray)).setObjectX(defaultPrototypeFunction);
			prototypeAssociativeArray=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionFunction,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionResource)).setObjectX(defaultPrototypeFunction);
			prototypeResource=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionDateTime,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionDateTime)).setObjectX(defaultPrototypeFunction);
			prototypeDateTime=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionRandom,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionRandom)).setObjectX(defaultPrototypeFunction);
			prototypeRandom=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionAtomic,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionAtomic)).setObjectX(defaultPrototypeFunction);
			prototypeAtomic=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionBuffer,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionBuffer)).setObjectX(defaultPrototypeFunction);
			prototypeBuffer=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionUtf16,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionUtf16)).setObjectX(defaultPrototypeFunction);
			prototypeUtf16=defaultPrototypeFunction->prototype;

			defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionUtf32,NULL,NULL);
			((Context::getGlobalObject())->operatorReferenceOwnProperty(symbolFunctionUtf32)).setObjectX(defaultPrototypeFunction);
			prototypeUtf32=defaultPrototypeFunction->prototype;

		};

		void XContext::deleteContext() {
			prototypeBoolean.deleteObject();
			prototypeNumber.deleteObject();
			prototypeString.deleteObject();
			prototypeArray.deleteObject();
			prototypeFunction.deleteObject();
			prototypeObject.deleteObject();
			prototypeAssociativeArray.deleteObject();
			prototypeResource.deleteObject();
			prototypeDateTime.deleteObject();
			prototypeRandom.deleteObject();
			prototypeAtomic.deleteObject();
			prototypeBuffer.deleteObject();
			prototypeUtf16.deleteObject();
			prototypeUtf32.deleteObject();

			symbolLength=0;
			symbolPrototype=0;
			symbolFunctionBoolean=0;
			symbolFunctionNumber=0;
			symbolFunctionString=0;
			symbolFunctionArray=0;
			symbolFunctionFunction=0;
			symbolFunctionObject=0;
			symbolFunctionAssociativeArray=0;
			symbolFunctionResource=0;
			symbolFunctionDateTime=0;
			symbolFunctionRandom=0;
			symbolFunctionAtomic=0;
			symbolSize=0;
			symbolFunctionBuffer=0;
			symbolFunctionUtf16=0;
			symbolFunctionUtf32=0;
			symbolToString=0;
			symbolToNumber=0;
		};


		void XContext::memoryInit() {
			TMemory<SymbolList>::memoryInit();
			TMemory<Variable>::memoryInit();
			TMemory<VariableObject>::memoryInit();
			TMemory<VariableFunction>::memoryInit();
		};

		TPointerX<Prototype> &Context::getPrototypeBoolean() {
			return (TXSingletonThread<XContext>::getValue())->prototypeBoolean;
		};

		TPointerX<Prototype> &Context::getPrototypeNumber() {
			return (TXSingletonThread<XContext>::getValue())->prototypeNumber;
		};

		TPointerX<Prototype> &Context::getPrototypeString() {
			return (TXSingletonThread<XContext>::getValue())->prototypeString;
		};

		TPointerX<Prototype> &Context::getPrototypeArray() {
			return (TXSingletonThread<XContext>::getValue())->prototypeArray;
		};

		TPointerX<Prototype> &Context::getPrototypeFunction() {
			return (TXSingletonThread<XContext>::getValue())->prototypeFunction;
		};

		TPointerX<Prototype> &Context::getPrototypeObject() {
			return (TXSingletonThread<XContext>::getValue())->prototypeObject;
		};

		TPointerX<Prototype> &Context::getPrototypeAssociativeArray() {
			return (TXSingletonThread<XContext>::getValue())->prototypeAssociativeArray;
		};

		TPointerX<Prototype> &Context::getPrototypeResource() {
			return (TXSingletonThread<XContext>::getValue())->prototypeResource;
		};

		TPointerX<Prototype> &Context::getPrototypeDateTime() {
			return (TXSingletonThread<XContext>::getValue())->prototypeDateTime;
		};

		TPointerX<Prototype> &Context::getPrototypeRandom() {
			return (TXSingletonThread<XContext>::getValue())->prototypeRandom;
		};

		TPointerX<Prototype> &Context::getPrototypeAtomic() {
			return (TXSingletonThread<XContext>::getValue())->prototypeAtomic;
		};

		TPointerX<Prototype> &Context::getPrototypeBuffer() {
			return (TXSingletonThread<XContext>::getValue())->prototypeBuffer;
		};

		TPointerX<Prototype> &Context::getPrototypeUtf16() {
			return (TXSingletonThread<XContext>::getValue())->prototypeUtf16;
		};

		TPointerX<Prototype> &Context::getPrototypeUtf32() {
			return (TXSingletonThread<XContext>::getValue())->prototypeUtf32;
		};

		Symbol Context::getSymbolLength() {
			return (TXSingletonThread<XContext>::getValue())->symbolLength;
		};

		Symbol Context::getSymbolPrototype() {
			return (TXSingletonThread<XContext>::getValue())->symbolPrototype;
		};

		Symbol Context::getSymbolFunctionBoolean() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionBoolean;
		};

		Symbol Context::getSymbolFunctionNumber() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionNumber;
		};

		Symbol Context::getSymbolFunctionString() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionString;
		};

		Symbol Context::getSymbolFunctionArray() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionArray;
		};

		Symbol Context::getSymbolFunctionFunction() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionFunction;
		};

		Symbol Context::getSymbolFunctionObject() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionObject;
		};

		Symbol Context::getSymbolFunctionAssociativeArray() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionAssociativeArray;
		};

		Symbol Context::getSymbolFunctionResource() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionResource;
		};

		Symbol Context::getSymbolFunctionDateTime() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionDateTime;
		};

		Symbol Context::getSymbolFunctionRandom() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionRandom;
		};

		Symbol Context::getSymbolFunctionAtomic() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionAtomic;
		};

		Symbol Context::getSymbolSize() {
			return (TXSingletonThread<XContext>::getValue())->symbolSize;
		};

		Symbol Context::getSymbolFunctionBuffer() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionBuffer;
		};

		Symbol Context::getSymbolFunctionUtf16() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionUtf16;
		};

		Symbol Context::getSymbolFunctionUtf32() {
			return (TXSingletonThread<XContext>::getValue())->symbolFunctionUtf32;
		};

		Symbol Context::getSymbolToNumber() {
			return (TXSingletonThread<XContext>::getValue())->symbolToNumber;
		};

		Symbol Context::getSymbolToString() {
			return (TXSingletonThread<XContext>::getValue())->symbolToString;
		};

		void Context::memoryInit() {
			TXSingletonThread<SymbolList>::memoryInit();
			TXSingletonThread<XValueUndefined>::memoryInit();
			TXSingletonThread<XGlobalObject>::memoryInit();
			TXSingletonThread<XContext>::memoryInit();
		};

		void Context::newContext() {
			(TXSingletonThread<SymbolList>::getValue())->symbolList.empty();
			(TXSingletonThread<SymbolList>::getValue())->symbolListMirror.empty();
			(TXSingletonThread<XValueUndefined>::getValue())->valueUndefined.newObject();
			(TXSingletonThread<XGlobalObject>::getValue())->globalObject.setObject(VariableObject::newVariable());
			(TXSingletonThread<XContext>::getValue())->newContext();
		};

		void Context::deleteContext() {
			(TXSingletonThread<XContext>::getValue())->deleteContext();
			(TXSingletonThread<XGlobalObject>::getValue())->globalObject.deleteObject();
			(TXSingletonThread<XValueUndefined>::getValue())->valueUndefined.deleteObject();
			(TXSingletonThread<SymbolList>::getValue())->symbolList.empty();
			(TXSingletonThread<SymbolList>::getValue())->symbolListMirror.empty();
		};


	};
};


