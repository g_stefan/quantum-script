//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-input.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		Input::Input() {
			inputChar = 0;
			stack.length = 0;
			filePos = 0;
			lineNumber = 1;
			lineColumn = 1;
			error = InputError::None;
		};

		Input::~Input() {
		};

		bool Input::init(IRead *input_) {
			input = input_;
			inputChar = 0;
			stack.length = 0;
			filePos = 0;
			lineNumber = 1;
			lineColumn = 1;
			error = InputError::None;
			return true;
		};

		bool Input::push() {
			if (stack.length == QUANTUM_SCRIPT_INPUT_STACK_SIZE) {
				return false;
			};
			stack.stack[stack.length]=inputChar;
			++stack.length;
			return true;
		};

		bool Input::pop() {
			--stack.length;
			inputChar=stack.stack[stack.length];
			return 1;
		};

		bool Input::read() {
			if (stack.length) {
				return pop();
			};
			if (error == InputError::Eof) {
				inputChar= 0;
				return false;
			};
			if (input->read(&inputChar, sizeof (byte))) {
				++filePos;
				++lineColumn;
				if (inputChar== '\n') {
					++lineNumber;
					lineColumn = 0;
				};
				return true;
			};
			inputChar = 0;
			error = InputError::Eof;
			return false;
		};

		bool Input::isN(const char *name) {
			int k;
			for (k = 0; name[k] != 0; ++k) {
				if (inputChar==name[k]) {
					if (read()) {
						continue;
					};
				};
				break;
			};
			if (name[k] == 0) {
				return true;
			};
			for (; k > 0; --k) {
				push();
				inputChar=name[k];
			};
			return false;
		};


	};
};


