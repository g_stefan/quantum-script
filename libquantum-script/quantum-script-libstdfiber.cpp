﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/timeb.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-instruction.hpp"
#include "quantum-script-libstdfiber.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablequitmessage.hpp"

#include "quantum-script-libstdfiber.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME


#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
#else

namespace Quantum {
	namespace Script {

		namespace LibStdFiber {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> fiberImplementation(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-implementation\n");
#endif
				//function,this,arguments
				TPointer<Variable> fiberFunction=arguments->index(0);
				TPointer<Variable> fiberThis=arguments->index(1);
				TPointer<Variable> fiberArguments=arguments->index(2);
				if(fiberFunction->variableType==VariableVmFunction::typeVmFunction) {
					if(fiberArguments->variableType==VariableArray::typeArray) {
					} else {
						fiberArguments.setObject(VariableArray::newVariable());
					};

					TPointer<InstructionContext> newFiber;
					newFiber.newObject();
					Executive *executive = (Executive *)function->valueSuper;

					newFiber->configPrintStackTraceLimit=executive->configPrintStackTraceLimit;
					newFiber->init();
					newFiber->executiveSuper=executive;
					newFiber->contextStack.newObject();
					newFiber->contextStack->enterMaster(newFiber->functionContext);
					newFiber->functionContext->this_=Context::getValueUndefined();
#ifndef QUANTUM_SCRIPT_DISABLE_CLOSURE
#endif
					newFiber->fiberInfo->prototype=function->prototype;
					newFiber->fiberInfo->isSuspended=true;

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
					newFiber->threadInfo=executive->threadInfo;
#endif

					newFiber->error = InstructionError::None;
					newFiber->currentProgramCounter=NULL;
					newFiber->instructionListExecutive=executive->assembler->instructionList.value();

					TPointer<Variable> functionArguments;
					functionArguments.setObject(VariableArray::newVariable());
					(*(((VariableArray *)(functionArguments.value()))->value))[0]=fiberArguments;

					// apply
					newFiber->pushX(fiberFunction);
					newFiber->pushX(fiberThis);
					newFiber->pushX(functionArguments);
					InstructionVmXCallThisModeApply(newFiber,NULL);
					newFiber->currentProgramCounter=newFiber->nextProgramCounter;

					executive->fiberCount++;
					executive->fiberContext.pushX(newFiber);

					return newFiber->fiberInfo.getObject();
				};
				throw(Error("require vm function"));
			};

			static TPointerOwner<Variable> suspend(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-implementation-suspend\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				((VariableFiberInfo *)( this_ ))->isSuspended=true;
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> resume(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-implementation-resume\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				((VariableFiberInfo *)( this_ ))->isSuspended=false;
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> terminate(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-implementation-terminate\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				VariableFiberInfo *fiberInfo=(VariableFiberInfo *)( this_ );
				fiberInfo->returnValue=arguments->index(0);
				fiberInfo->isSuspended=true;
				fiberInfo->isTerminated=true;
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> getReturnedValue(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-implementation-get-returned-value\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				return ((VariableFiberInfo *)( this_ ))->returnValue;
			};

			static TPointerOwner<Variable> isTerminated(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-implementation-is-terminated\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};
				return VariableBoolean::newVariable(((VariableFiberInfo *)( this_ ))->isTerminated);
			};

			static TPointerOwner<Variable> postMessage(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-post-message\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableFiberInfo *)( this_ ))->isTerminated) {
					return Context::getValueUndefined();
				};
				((VariableFiberInfo *)( this_ ))->messageQueue->pushX(arguments->index(0));
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> sendMessageX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-send-message-x\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableFiberInfo *)( this_ ))->isTerminated) {
					return VariableBoolean::newVariable(true);
				};
				if(((VariableFiberInfo *)( this_ ))->isSendMessage) {
					return VariableBoolean::newVariable(false);
				};
				((VariableFiberInfo *)( this_ ))->receivedMessage=arguments->index(0);
				((VariableFiberInfo *)( this_ ))->messageProcessed=false;
				((VariableFiberInfo *)( this_ ))->isSendMessage=true;
				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> isMessageProcessedX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-is-message-processed-x\n");
#endif
				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableFiberInfo *)( this_ ))->isTerminated) {
					return VariableBoolean::newVariable(true);
				};
				return VariableBoolean::newVariable(((VariableFiberInfo *)( this_ ))->messageProcessed);
			};

			static TPointerOwner<Variable> getMessageReturnValueX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-get-message-return-value-x\n");
#endif

				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableFiberInfo *)( this_ ))->isTerminated) {
					return Context::getValueUndefined();
				};
				((VariableFiberInfo *)( this_ ))->messageProcessed=false;
				((VariableFiberInfo *)( this_ ))->isSendMessage=false;
				return ((VariableFiberInfo *)( this_ ))->returnValue;
			};

			static TPointerOwner<Variable> isBusy(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-is-busy\n");
#endif

				if(this_->variableType!=VariableFiberInfo::typeFiberInfo) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFiberInfo *)( this_ ))->isBusy);
			};


			static TPointerOwner<Variable> quitMessage(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- fiber-quit-message\n");
#endif

				return VariableQuitMessage::newVariable();
			};


			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_exit) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-exit\n", context->currentProgramCounter);
#endif

				context->fiberInfo->returnValue=context->getArgument(0);
				if(context->fiberInfo->isSendMessage) {
					context->fiberInfo->messageProcessed=true;
				};
				context->nextProgramCounter = NULL;
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_criticalSectionEnter) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-critical-section-enter\n", context->currentProgramCounter);
#endif

				context->fiberCriticalSection++;
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_criticalSectionLeave) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-critical-section-leave\n", context->currentProgramCounter);
#endif

				context->fiberCriticalSection--;
				if(context->fiberCriticalSection<0) {
					context->fiberCriticalSection=0;
				};
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_getMilliseconds) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-get-milliseconds\n", context->currentProgramCounter);
#endif

				timeb tb;
				ftime(&tb);
				long int currentMilliseconds = tb.millitm + (tb.time & 0xFFFF) * 1000;
				context->pushOwner(VariableNumber::newVariable(currentMilliseconds));
				InstructionVmReturn(context,NULL);
				return;
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_sleep) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-sleep\n", context->currentProgramCounter);
#endif

				timeb tb;
				ftime(&tb);
				long int currentMilliseconds = tb.millitm + (tb.time & 0xFFFF) * 1000;
				context->fiberInfo->sleepMilliseconds=(context->getArgument(0))->toIndex();
				context->fiberInfo->startMilliseconds=currentMilliseconds;
				context->fiberInfo->isSuspended=true;
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_hasMessage) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-has-message\n", context->currentProgramCounter);
#endif
				context->pushOwner(VariableBoolean::newVariable(context->fiberInfo->hasMessage()));
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_getMessageX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-get-message-x\n", context->currentProgramCounter);
#endif
				context->pushX(context->fiberInfo->getMessageX());
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_messageProcessedX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-message-processed-x\n", context->currentProgramCounter);
#endif
				context->fiberInfo->returnValue=context->getArgument(0);
				if(context->fiberInfo->isSendMessage) {
					context->fiberInfo->messageProcessed=true;
				};
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_isMessageProcessedX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-is-message-processed-x\n", context->currentProgramCounter);
#endif
				if(context->fiberInfo->isSendMessage) {
					context->pushOwner(VariableBoolean::newVariable(context->fiberInfo->messageProcessed));
				} else {
					context->pushOwner(VariableBoolean::newVariable(false));
				};
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentFiber_setIsBusyX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-fiber-set-is-busy-x\n", context->currentProgramCounter);
#endif

				context->fiberInfo->isBusy=(context->getArgument(0))->toBoolean();
				context->pushX(Context::getValueUndefined());
				InstructionVmReturn(context, NULL);
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction4("FiberImplementation", fiberImplementation, executive);
				executive->setFunction2("FiberImplementation.prototype.suspend()", suspend);
				executive->setFunction2("FiberImplementation.prototype.resume()", resume);
				executive->setFunction2("FiberImplementation.prototype.terminate(returnValue)",terminate);
				executive->setFunction2("FiberImplementation.prototype.getReturnedValue()", getReturnedValue);
				executive->setFunction2("FiberImplementation.prototype.isTerminated()", isTerminated);
				executive->setFunction2("FiberImplementation.prototype.postMessage(message)", postMessage);
				executive->setFunction2("FiberImplementation.prototype.sendMessageX(message)", sendMessageX);
				executive->setFunction2("FiberImplementation.prototype.isMessageProcessedX()", isMessageProcessedX);
				executive->setFunction2("FiberImplementation.prototype.getMessageReturnValueX()", getMessageReturnValueX);
				executive->setFunction2("FiberImplementation.prototype.isBusy()", isBusy);
				executive->setFunction2("FiberImplementation.prototype.quitMessage()", quitMessage);

				executive->compileStringX("var CurrentFiber={};");

				executive->setVmFunction("CurrentFiber.exit(returnValue)", InstructionLibStdCurrentFiber_exit, NULL);
				executive->setVmFunction("CurrentFiber.criticalSectionEnter()", InstructionLibStdCurrentFiber_criticalSectionEnter, NULL);
				executive->setVmFunction("CurrentFiber.criticalSectionLeave()", InstructionLibStdCurrentFiber_criticalSectionLeave, NULL);
				executive->setVmFunction("CurrentFiber.getMilliseconds()", InstructionLibStdCurrentFiber_getMilliseconds, NULL);
				executive->setVmFunction("CurrentFiber.sleep(milliseconds)", InstructionLibStdCurrentFiber_sleep, NULL);
				executive->setVmFunction("CurrentFiber.hasMessage()", InstructionLibStdCurrentFiber_hasMessage, NULL);
				executive->setVmFunction("CurrentFiber.getMessageX()", InstructionLibStdCurrentFiber_getMessageX, NULL);
				executive->setVmFunction("CurrentFiber.messageProcessedX(returnValue)", InstructionLibStdCurrentFiber_messageProcessedX, NULL);
				executive->setVmFunction("CurrentFiber.isMessageProcessedX()", InstructionLibStdCurrentFiber_isMessageProcessedX, NULL);
				executive->setVmFunction("CurrentFiber.setIsBusyX()", InstructionLibStdCurrentFiber_setIsBusyX, NULL);

				executive->compileString((char *)libStdFiberSource);
			};


		};
	};
};


#endif


