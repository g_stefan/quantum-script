//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_PROTOTYPE_HPP
#define QUANTUM_SCRIPT_PROTOTYPE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class Prototype;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::Prototype>:
			public TMemoryObjectPoolActive<Quantum::Script::Prototype> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class Prototype :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Prototype);
			public:

				TPointerX<Prototype> parent;
				TPointerX<Variable> prototype;

				QUANTUM_SCRIPT_EXPORT Prototype();

				inline void activeDestructor() {
					parent.deleteObject();
					prototype.deleteObject();
				};

				inline static void memoryInit() {
					TMemory<Variable>::memoryInit();
				};
		};


	};
};


#endif
