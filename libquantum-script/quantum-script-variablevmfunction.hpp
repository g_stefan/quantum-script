﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEVMFUNCTION_HPP
#define QUANTUM_SCRIPT_VARIABLEVMFUNCTION_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEFUNCTION_HPP
#include "quantum-script-variablefunction.hpp"
#endif

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXT_HPP
#include "quantum-script-executivecontext.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PARSERFUNCTIONHINT_HPP
#include "quantum-script-parserfunctionhint.hpp"
#endif

#ifndef QUANTUM_SCRIPT_INSTRUCTIONX_HPP
#include "quantum-script-instructionx.hpp"
#endif


namespace Quantum {
	namespace Script {

		class VariableVmFunction;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableVmFunction>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableVmFunction> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableVmFunction :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableVmFunction);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeVmFunction;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeVmFunction;

				TPointerX<Prototype> prototype;

				TPointer<InstructionList> instructionList;

				ProgramCounter *value;
				ProgramCounter *valueEnd;

				// coroutine
				ProgramCounter *originalValue;
				TPointerX<TStack1<TPointerX<ExecutiveContextPc> > > coroutineContext;

				TPointerX<FunctionParent> functionParent;

				int functionHint;

				inline VariableVmFunction() {
					prototype.memoryLink(this);
					coroutineContext.memoryLink(this);
					functionParent.memoryLink(this);
					variableType = typeVmFunction;
					functionHint=ParserFunctionHint::All;
					coroutineContext.newObject();
				};

				inline void activeDestructor() {
					prototype.deleteObject();
					coroutineContext->empty();
					functionParent.deleteObject();
					instructionList.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(ProgramCounter *value);


				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> functionApply(Variable *this_,VariableArray *arguments);

				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT bool instanceOfPrototype(Prototype *&out);

				inline static void memoryInit() {
					TMemory<Prototype>::memoryInit();
					TMemory<InstructionList>::memoryInit();
					TMemory<ExecutiveContext>::memoryInit();
					TMemory<FunctionParent>::memoryInit();
					TPointerX<TStack1<TPointerX<ExecutiveContextPc> > >::memoryInit();
				};

				QUANTUM_SCRIPT_EXPORT String getSource();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
