﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEOPERATOR21_HPP
#define QUANTUM_SCRIPT_VARIABLEOPERATOR21_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableOperator21;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableOperator21>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableOperator21> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableOperator21 :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableOperator21);
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeOperator21;

				Symbol symbol;
				Number value;

				inline VariableOperator21() {
					variableType = typeOperator21;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif

