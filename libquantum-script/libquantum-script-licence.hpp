//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBQUANTUM_SCRIPT_LICENCE_HPP
#define LIBQUANTUM_SCRIPT_LICENCE_HPP

#ifndef QUANTUM_SCRIPT__EXPORT_HPP
#include "quantum-script--export.hpp"
#endif

namespace Lib {
	namespace Quantum {
		namespace Script {

			class Licence {
				public:
					QUANTUM_SCRIPT_EXPORT static const char *content();
					QUANTUM_SCRIPT_EXPORT static const char *shortContent();
			};

		};
	};
};

#endif
