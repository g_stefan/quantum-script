﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdapplication.hpp"

#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-libstdapplication.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdApplication {

			using namespace XYO::XY;
			using namespace XYO::XO;


			static TPointerOwner<Variable> getCmdN(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- application-get-cmd-n\n");
#endif

				return VariableNumber::newVariable(
					       ((Executive *)(function->valueSuper))->mainCmdN
				       );
			};

			static TPointerOwner<Variable> getCmdS(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- application-get-cmd-s\n");
#endif

				return VariableString::newVariable(
					       (((Executive *)(function->valueSuper))->mainCmdS[(arguments->index(0))->toIndex()])
				       );
			};

			static TPointerOwner<Variable> getExecutable(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- application-get-executable\n");
#endif

				return VariableString::newVariable(
					       ((Executive *)(function->valueSuper))->mainCmdS[0]
				       );
			};

			static TPointerOwner<Variable> getPathExecutable(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- application-get-path-executable\n");
#endif

				return VariableString::newVariable(
					       ((Executive *)(function->valueSuper))->pathExecutable
				       );
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX(
					"var Application={};"
				);

				executive->setFunction4("Application.getCmdN", getCmdN,executive);
				executive->setFunction4("Application.getCmdS(x)",getCmdS,executive);
				executive->setFunction4("Application.getExecutable", getExecutable,executive);
				executive->setFunction4("Application.getPathExecutable", getPathExecutable,executive);

				executive->compileStringX((char *)libStdApplicationSource);
			};

		};
	};
};


