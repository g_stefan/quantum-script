﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/timeb.h>

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>
#endif

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-executivex.hpp"
#include "quantum-script-parserasm.hpp"
#include "quantum-script-libstd.hpp"
#include "quantum-script-variable.hpp"

#include "quantum-script-variablestring.hpp"

#include "quantum-script-executivecontext.hpp"
#include "quantum-script-instructionx.hpp"
#include "quantum-script-variablefiberinfo.hpp"
#include "quantum-script-variablethreadinfo.hpp"
#include "quantum-script-variablevmfunction.hpp"
#include "quantum-script-instructioncontext.hpp"
#include "quantum-script-variablestacktrace.hpp"
#include "quantum-script-variableargumentlevel.hpp"
#include "quantum-script-variablereferenceobject.hpp"
#include "quantum-script-variableoperator21.hpp"
#include "quantum-script-variableoperator22.hpp"
#include "quantum-script-variableoperator23.hpp"
#include "quantum-script-variableoperator31.hpp"
#include "quantum-script-variablenativevmfunction.hpp"
#include "quantum-script-variablevmprogramcounter.hpp"

#include "quantum-script-arrayiteratorkey.hpp"
#include "quantum-script-arrayiteratorvalue.hpp"
#include "quantum-script-objectiteratorkey.hpp"
#include "quantum-script-objectiteratorvalue.hpp"

namespace Quantum {
	namespace Script {


		class ExecutiveX_ {
			public:
				Executive executive;
				String error;
				String stackTrace;
				TPointer<Variable> returnValue;

				ExecutiveX_();
				~ExecutiveX_();

				static void memoryInit();
		};

		ExecutiveX_::ExecutiveX_() {
			returnValue=Context::getValueUndefined();
		};

		ExecutiveX_::~ExecutiveX_() {
		};

		void ExecutiveX_::memoryInit() {
			String::memoryInit();
			Error::memoryInit();
			Executive::memoryInit();
		};

		bool ExecutiveX::initExecutive(int cmdN,char **cmdS,QuantumScriptInitExecutiveProc applicationInitExecutive) {
			Executive &executive=(TXSingletonThread<ExecutiveX_>::getValue())->executive;
			String &error=(TXSingletonThread<ExecutiveX_>::getValue())->error;
			String pathMain;
			size_t idx;

#ifdef XYO_OS_TYPE_WIN
			char strExe[MAX_PATH];
#endif
#ifdef XYO_OS_TYPE_UNIX
			char *strExe;
#endif

#ifdef XYO_OS_TYPE_WIN
			GetModuleFileName(NULL, strExe, MAX_PATH);
#endif
#ifdef XYO_OS_TYPE_UNIX
			strExe = cmdS[0];
#endif

			pathMain = strExe;
			if (StringX::indexOfFromEnd(pathMain, "\\", 0, idx)) {
				pathMain = StringX::substring(pathMain, 0, idx);
			} else {
				if (StringX::indexOfFromEnd(pathMain, "/", 0, idx)) {
					pathMain = StringX::substring(pathMain, 0, idx);
				} else {
					pathMain = ".";
				};
			};

			executive.includePath->push(pathMain);

			executive.mainCmdN=cmdN;
			executive.mainCmdS=cmdS;
			executive.pathExecutable=pathMain;
			executive.applicationInitExecutive=applicationInitExecutive;

			try {

				executive.initExecutive();
				executive.compileEnd();
				if (executive.execute() == 0) {
					return true;
				} else {
					error="Error: Internal virtual machine execution error.";
					return false;
				};

			} catch(const Error &e) {
				error="Error: ";
				error<<(const_cast<Error &>(e)).getMessage();
			} catch (const std::exception &e) {
				error="Error: ";
				error<<e.what();
			} catch (...) {
				error="Error: Unknown\n";
			};

			return false;
		};

		String ExecutiveX::getError() {
			return (TXSingletonThread<ExecutiveX_>::getValue())->error;
		};

		void ExecutiveX::setStackTrace(String stackTrace) {
			(TXSingletonThread<ExecutiveX_>::getValue())->stackTrace=stackTrace;
		};

		String ExecutiveX::getStackTrace() {
			return (TXSingletonThread<ExecutiveX_>::getValue())->stackTrace;
		};

		Executive &ExecutiveX::getExecutive() {
			return (TXSingletonThread<ExecutiveX_>::getValue())->executive;
		};

		bool ExecutiveX::executeFile(const char *fileName) {
			String &error=(TXSingletonThread<ExecutiveX_>::getValue())->error;

			try {
				TPointer<VariableArray> arguments;
				arguments.setObject(VariableArray::newArray());
				(arguments->index(0)).setObject(VariableString::newVariable(fileName));
				Variable &script_((Context::getGlobalObject())->operatorReference(Context::getSymbol("Script")));
				(TXSingletonThread<ExecutiveX_>::getValue())->returnValue=(script_.operatorReference(Context::getSymbol("include"))).functionApply(&script_,arguments);
				return true;
			} catch(const Error &e) {
				error="Error: ";
				error<<(const_cast<Error &>(e)).getMessage();
			} catch (const std::exception &e) {
				error="Error: ";
				error<<e.what();
			} catch (...) {
				error="Error: Unknown\n";
			};

			return false;
		};

		bool ExecutiveX::executeString(const char *strSource) {
			String &error=(TXSingletonThread<ExecutiveX_>::getValue())->error;

			try {
				TPointer<VariableArray> arguments;
				arguments.setObject(VariableArray::newArray());
				(arguments->index(0)).setObject(VariableString::newVariable(strSource));
				Variable &script_((Context::getGlobalObject())->operatorReference(Context::getSymbol("Script")));
				(TXSingletonThread<ExecutiveX_>::getValue())->returnValue=(script_.operatorReference(Context::getSymbol("execute"))).functionApply(&script_,arguments);
				return true;

			} catch(const Error &e) {
				error="Error: ";
				error<<(const_cast<Error &>(e)).getMessage();
			} catch (const std::exception &e) {
				error="Error: ";
				error<<e.what();
			} catch (...) {
				error="Error: Unknown\n";
			};

			return false;

		};

		bool ExecutiveX::executeEnd() {
			Executive &executive=(TXSingletonThread<ExecutiveX_>::getValue())->executive;
			String &error=(TXSingletonThread<ExecutiveX_>::getValue())->error;

			try {
				executive.executeEnd();
				return true;
			} catch(const Error &e) {
				error="Error: ";
				error<<(const_cast<Error &>(e)).getMessage();
			} catch (const std::exception &e) {
				error="Error: ";
				error<<e.what();
			} catch (...) {
				error="Error: Unknown\n";
			};

			return false;
		};

		TPointer<Variable> ExecutiveX::returnValue() {
			return (TXSingletonThread<ExecutiveX_>::getValue())->returnValue;
		};

	};
};



