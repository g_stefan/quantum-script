﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableoperator31.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableOperator31::typeOperator31= "{7539857E-DB8F-4744-A1DF-8E25EC271281}";

		Variable *VariableOperator31::newVariable() {
			return (Variable *) TMemory<VariableOperator31>::newObject();
		};

		bool VariableOperator31::toBoolean() {
			return true;
		};

		String VariableOperator31::toString() {
			return strTypeUndefined;
		};

	};

};


