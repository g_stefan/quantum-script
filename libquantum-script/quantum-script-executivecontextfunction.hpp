//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXTFUNCTION_HPP
#define QUANTUM_SCRIPT_EXECUTIVECONTEXTFUNCTION_HPP

#ifndef QUANTUM_SCRIPT_HPP
#include "quantum-script.hpp"
#endif

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXTPC_HPP
#include "quantum-script-executivecontextpc.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEFUNCTION_HPP
#include "quantum-script-variablefunction.hpp"
#endif

namespace Quantum {
	namespace Script {

		class ExecutiveContextFunction;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ExecutiveContextFunction>:
			public TMemoryObjectPoolActive<Quantum::Script::ExecutiveContextFunction> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class ExecutiveContextFunction :
			public ExecutiveContextPc {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ExecutiveContextFunction);
			public:

				TPointerX<Variable> thisFunction_;
				TPointerX<Variable> this_;

				TPointerX<VariableArray> functionArguments;
				TPointerX<VariableArray> functionLocalVariables;

#ifndef QUANTUM_SCRIPT_DISABLE_CLOSURE
				TPointerX<FunctionParent> functionParent;
#endif

				inline ExecutiveContextFunction() {

					functionContext = this;

					thisFunction_.memoryLink(this);
					this_.memoryLink(this);
					functionArguments.memoryLink(this);
					functionLocalVariables.memoryLink(this);

#ifndef QUANTUM_SCRIPT_DISABLE_CLOSURE
					functionParent.memoryLink(this);
#endif
				};

				inline void activeConstructor() {
					ExecutiveContextPc::activeConstructor();
				};

				inline void activeDestructor() {
					ExecutiveContextPc::activeDestructor();
					//
					thisFunction_.deleteObject();
					this_.deleteObject();
					functionArguments.deleteObject();
					functionLocalVariables.deleteObject();

#ifndef QUANTUM_SCRIPT_DISABLE_CLOSURE
					functionParent.deleteObject();
#endif

				};


				inline static void memoryInit() {
					TMemory<ExecutiveContextPc>::memoryInit();
				};

		};

	};
};


#endif
