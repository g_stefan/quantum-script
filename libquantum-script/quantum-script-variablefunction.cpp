﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variablefunction.hpp"
#include "quantum-script-variablearray.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableFunction::typeFunction="{84EAB78C-BA85-4E5E-AC2F-06E7DF2E9E8B}";
		const char *VariableFunction::strTypeFunction="Function";
		const char *VariableFunction::strNativeFunction="NativeFunction";

		String VariableFunction::getType() {
			return strTypeFunction;
		};

		Variable *VariableFunction::newVariable(FunctionParent *functionParent,VariableArray *parentVariables,VariableArray *parentArguments,FunctionProcedure functionProcedure,Object *super,void *valueSuper) {
			VariableFunction *retV;
			retV=TMemory<VariableFunction>::newObject();
			if(functionParent||parentVariables||parentArguments) {
				retV->functionParent.newObject();
				retV->functionParent->functionParent=functionParent;
				retV->functionParent->variables=parentVariables;
				retV->functionParent->arguments=parentArguments;
			};
			retV->functionProcedure=functionProcedure;
			retV->super=super;
			retV->valueSuper=valueSuper;
			return (Variable *) retV;
		};

		Variable *VariableFunction::newVariableX(FunctionParent *functionParent,VariableArray *parentVariables,VariableArray *parentArguments,FunctionProcedure functionProcedure,Object *super,void *valueSuper) {
			VariableFunction *retV;
			retV=TMemory<VariableFunction>::newObjectX();
			if(functionParent||parentVariables||parentArguments) {
				retV->functionParent.newObject();
				retV->functionParent->functionParent=functionParent;
				retV->functionParent->variables=parentVariables;
				retV->functionParent->arguments=parentArguments;
			};
			retV->functionProcedure=functionProcedure;
			retV->super=super;
			retV->valueSuper=valueSuper;
			return (Variable *) retV;
		};

		TPointerOwner<Variable> VariableFunction::functionApply(Variable *this_,VariableArray *arguments) {
			return (*functionProcedure)(this,this_,arguments);
		};

		TPointerX<Variable> &VariableFunction::operatorReferenceOwnProperty(Symbol symbolId) {
			if(symbolId==Context::getSymbolPrototype()) {
				return prototype->prototype;
			};
			throw Error("operatorReferenceOwnProperty");
		};


		Variable &VariableFunction::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolPrototype()) {
				return *prototype->prototype;
			};
			return operatorReferenceX(symbolId,(Context::getPrototypeFunction())->prototype);
		};

		Variable *VariableFunction::instancePrototype() {
			return (Context::getPrototypeFunction())->prototype;
		};

		bool VariableFunction::instanceOfPrototype(Prototype *&out) {
			out=prototype;
			return true;
		};

		void VariableFunction::memoryInit() {
			TMemory<Prototype>::memoryInit();
			TMemory<FunctionParent>::memoryInit();
		};

		bool VariableFunction::toBoolean() {
			return true;
		};

		String VariableFunction::toString() {
			return strTypeFunction;
		};

	};
};


