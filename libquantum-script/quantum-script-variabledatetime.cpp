﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-variabledatetime.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablenull.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableDateTime::typeDateTime="{18EC322A-3BE8-4107-8D89-65C24C7E1561}";
		const char *VariableDateTime::strTypeDateTime="DateTime";

		String VariableDateTime::getType() {
			return strTypeDateTime;
		};

		Variable *VariableDateTime::newVariable() {
			return (Variable *) TMemory<VariableDateTime>::newObject();
		};

		Variable &VariableDateTime::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(Context::getPrototypeDateTime())->prototype);
		};

		Variable *VariableDateTime::instancePrototype() {
			return (Context::getPrototypeDateTime())->prototype;
		};

		Variable *VariableDateTime::clone(SymbolList &inSymbolList) {
			VariableDateTime *out=(VariableDateTime *)newVariable();
			out->value.copy(value);
			return out;
		};

		bool VariableDateTime::toBoolean() {
			return true;
		};

		String VariableDateTime::toString() {
			return strTypeDateTime;
		};

	};
};


