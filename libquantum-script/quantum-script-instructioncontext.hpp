﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_INSTRUCTIONCONTEXT_HPP
#define QUANTUM_SCRIPT_INSTRUCTIONCONTEXT_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLETHREADINFO_HPP
#include "quantum-script-variablethreadinfo.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEFIBERINFO_HPP
#include "quantum-script-variablefiberinfo.hpp"
#endif


#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXT_HPP
#include "quantum-script-executivecontext.hpp"
#endif

#ifndef QUANTUM_SCRIPT_INSTRUCTIONX_HPP
#include "quantum-script-instructionx.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEVMFUNCTION_HPP
#include "quantum-script-variablevmfunction.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#include "quantum-script-variablearray.hpp"
#endif

namespace Quantum {
	namespace Script {


		using namespace XYO::XY;
		using namespace XYO::XO;


		class InstructionError {
			public:

				enum {
					None,
					Error,
					Throw
				};

		};

		class InstructionTrace {
			public:
				dword sourceSymbol;
				dword sourcePos;
				dword sourceLineNumber;
				dword sourceLineColumn;
		};

		class InstructionContext :
			public Stack {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(InstructionContext);
			public:

				TStack1<String> includedFile;

				TPointer<ExecutiveContext> contextStack;

				TPointer<TStack2<InstructionTrace> > stackTrace;
				TRedBlackTree<String,bool> listIncludeOnce;

				InstructionList *instructionListExecutive;
				ProgramCounter *currentProgramCounter;
				ProgramCounter *nextProgramCounter;

				ExecutiveContextPc    *pcContext;
				ExecutiveContextFunction  *functionContext;

				TPointer<VariableFiberInfo> fiberInfo;
				TPointer<VariableThreadInfo> threadInfo;
				int fiberCriticalSection;

				int error;
				int configPrintStackTraceLimit;

				void *executiveSuper;

				String strLength__;
				String strPrototype__;
				String strArguments__;
				String strString__;
				String strArray__;
				String strMessage__;
				String strError__;
				String strStackTrace__;
				String strValue__;

				Symbol symbolLength__;
				Symbol symbolPrototype__;
				Symbol symbolArguments__;
				Symbol symbolString__;
				Symbol symbolArray__;
				Symbol symbolMessage__;
				Symbol symbolError__;
				Symbol symbolStackTrace__;
				Symbol symbolValue__;

				QUANTUM_SCRIPT_EXPORT InstructionContext();
				QUANTUM_SCRIPT_EXPORT bool init();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> newError(String str);

				inline TPointerX<Variable> &getReference(Symbol &name) {
					return (Context::getGlobalObject())->operatorReferenceOwnProperty(name);
				};

				inline bool getFunctionX(dword &name, TPointer<Variable> &out) {
					Variable *out_=&(Context::getGlobalObject())->operatorReference(name);
					if (out_->variableType == VariableVmFunction::typeVmFunction) {
						out = out_;
						return true;
					};
					return false;
				};

#ifndef QUANTUM_SCRIPT_DISABLE_CLOSURE
				inline TPointer<Variable> getFunctionLocalVariablesLevel(int level) {
					FunctionParent *scan;
					--level;
					for (scan = functionContext->functionParent.value(); scan; scan = scan->functionParent.value()) {
						if (level == 0) {
							return scan->variables.value();
						};
						--level;
					};
					return NULL;
				};
#endif

				inline TPointer<Variable> getLocalVariable(int index) {
					if (index <((int) ((VariableArray *) (functionContext->functionLocalVariables.value()))->value->length())) {
						return (*(((VariableArray *) (functionContext->functionLocalVariables.value()))->value.value()))[index];
					};
					return VariableUndefined::newVariable();
				};

				inline TPointer<Variable> getArgument(int index) {
					if (index <((int) ((VariableArray *) (functionContext->functionArguments.value()))->value->length())) {
						return (*(((VariableArray *) (functionContext->functionArguments.value()))->value.value()))[index];
					};
					return TPointerOwner<Variable>(VariableUndefined::newVariable());
				};

#ifndef QUANTUM_SCRIPT_DISABLE_CLOSURE
				inline TPointer<Variable> getFunctionArgumentsLevel(int level) {
					FunctionParent *scan;
					--level;
					for (scan = functionContext->functionParent.value(); scan; scan = scan->functionParent.value()) {
						if (level == 0) {
							return scan->arguments.value();
						};
						--level;
					};
					return NULL;
				};
#endif

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

		};


	};
};



#endif
