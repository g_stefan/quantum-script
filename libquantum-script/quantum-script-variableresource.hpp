﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLERESOURCE_HPP
#define QUANTUM_SCRIPT_VARIABLERESOURCE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableResource;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableResource>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableResource> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableResource :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableResource);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeResource;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeResource;

				typedef void (*ResourceDelete)(void *);

				const void *resourceType;
				void *resource;
				ResourceDelete resourceDelete;
				bool threadCopy;

				inline VariableResource() {
					variableType = typeResource;
					resourceType = typeResource;
					resource=NULL;
					resourceDelete=NULL;
					threadCopy=false;
				};

				inline void drop() {
					resourceType = typeResource;
					resource=NULL;
					resourceDelete=NULL;
					threadCopy=false;
				};

				inline void close() {
					if(resource!=NULL) {
						if(resourceDelete!=NULL) {
							(*resourceDelete)(resource);
							resourceDelete=NULL;
						};
						resource=NULL;
					};
					variableType = typeResource;
					threadCopy=false;
				};

				inline void activeDestructor() {
					close();
				};


				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(void *resource,ResourceDelete resourceDelete);
				QUANTUM_SCRIPT_EXPORT static Variable *newObjectV2(void *resource,ResourceDelete resourceDelete,const void *resourceType);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
