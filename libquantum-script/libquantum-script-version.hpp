#ifndef LIBQUANTUM_SCRIPT_VERSION_HPP
#define LIBQUANTUM_SCRIPT_VERSION_HPP

#define LIBQUANTUM_SCRIPT_VERSION_ABCD      5,2,0,98
#define LIBQUANTUM_SCRIPT_VERSION_A         5
#define LIBQUANTUM_SCRIPT_VERSION_B         2
#define LIBQUANTUM_SCRIPT_VERSION_C         0
#define LIBQUANTUM_SCRIPT_VERSION_D         98
#define LIBQUANTUM_SCRIPT_VERSION_STR_ABCD  "5.2.0.98"
#define LIBQUANTUM_SCRIPT_VERSION_STR       "5.2.0"
#define LIBQUANTUM_SCRIPT_VERSION_STR_BUILD "98"
#define LIBQUANTUM_SCRIPT_VERSION_BUILD     98
#define LIBQUANTUM_SCRIPT_VERSION_HOUR      18
#define LIBQUANTUM_SCRIPT_VERSION_MINUTE    45
#define LIBQUANTUM_SCRIPT_VERSION_SECOND    0
#define LIBQUANTUM_SCRIPT_VERSION_DAY       14
#define LIBQUANTUM_SCRIPT_VERSION_MONTH     6
#define LIBQUANTUM_SCRIPT_VERSION_YEAR      2015
#define LIBQUANTUM_SCRIPT_VERSION_STR_DATETIME "2015-06-14 18:45:00"

#ifndef XYO_RC

#ifndef QUANTUM_SCRIPT__EXPORT_HPP
#include "quantum-script--export.hpp"
#endif

namespace Lib {
	namespace Quantum {
		namespace Script {

			class Version {
				public:
					QUANTUM_SCRIPT_EXPORT static const char *getABCD();
					QUANTUM_SCRIPT_EXPORT static const char *getA();
					QUANTUM_SCRIPT_EXPORT static const char *getB();
					QUANTUM_SCRIPT_EXPORT static const char *getC();
					QUANTUM_SCRIPT_EXPORT static const char *getD();
					QUANTUM_SCRIPT_EXPORT static const char *getVersion();
					QUANTUM_SCRIPT_EXPORT static const char *getBuild();
					QUANTUM_SCRIPT_EXPORT static const char *getHour();
					QUANTUM_SCRIPT_EXPORT static const char *getMinute();
					QUANTUM_SCRIPT_EXPORT static const char *getSecond();
					QUANTUM_SCRIPT_EXPORT static const char *getDay();
					QUANTUM_SCRIPT_EXPORT static const char *getMonth();
					QUANTUM_SCRIPT_EXPORT static const char *getYear();
					QUANTUM_SCRIPT_EXPORT static const char *getDatetime();
			};

		};
	};
};

#endif
#endif

