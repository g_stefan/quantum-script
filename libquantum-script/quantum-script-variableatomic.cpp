﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-context.hpp"
#include "quantum-script-variableatomic.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableAtomic::typeAtomic="{89225310-A868-445B-B620-171D417FC882}";
		const char *VariableAtomic::strTypeAtomic="Atomic";

		VariableAtomic::VariableAtomic() {
			variableType = typeAtomic;
			value=NULL;
		};

		VariableAtomic::~VariableAtomic() {
			if(value!=NULL) {
				value->decReferenceCount();
			};
		};

		String VariableAtomic::getType() {
			return strTypeAtomic;
		};

		Variable *VariableAtomic::newVariable() {
			VariableAtomic *retV=TMemory<VariableAtomic>::newObject();
			retV->value=new Atomic();
			retV->value->referenceCount++;
			return (Variable *) retV;
		};

		Variable &VariableAtomic::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(Context::getPrototypeAtomic())->prototype);
		};

		Variable *VariableAtomic::instancePrototype() {
			return (Context::getPrototypeAtomic())->prototype;
		};

		void VariableAtomic::activeDestructor() {
			if(value!=NULL) {
				value->decReferenceCount();
				value=NULL;
			};
		};

		Variable *VariableAtomic::clone(SymbolList &inSymbolList) {
			VariableAtomic *out=TMemory<VariableAtomic>::newObject();
			out->value=value;
			if(out->value) {
				out->value->incReferenceCount();
			};
			return out;
		};

		bool VariableAtomic::toBoolean() {
			return true;
		};

		String VariableAtomic::toString() {
			return strTypeAtomic;
		};


	};
};


