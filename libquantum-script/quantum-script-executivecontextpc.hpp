//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXTPC_HPP
#define QUANTUM_SCRIPT_EXECUTIVECONTEXTPC_HPP

#ifndef QUANTUM_SCRIPT_HPP
#include "quantum-script.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROGRAMCOUNTER_HPP
#include "quantum-script-programcounter.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {

		class ExecutiveContextPc;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ExecutiveContextPc>:
			public TMemoryObjectPoolActive<Quantum::Script::ExecutiveContextPc> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class ExecutiveContextFunction;

		class ExecutiveContextPc :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ExecutiveContextPc);
			public:
				ProgramCounter *catch_;
				ProgramCounter *finally_;
				ProgramCounter *break_;
				ProgramCounter *continue_;
				ProgramCounter *tryBreak_;
				ProgramCounter *tryContinue_;
				ProgramCounter *tryReturn_;
				ProgramCounter *tryThrow_;
				ProgramCounter *pc_;

				Stack::Node *stackLink_;

				ExecutiveContextFunction *functionContext;

				TPointerX<Variable>  registerValue;
				TPointerX<Iterator>  iteratorValue;
				TPointerX<Variable> *referenceValue;

				inline ExecutiveContextPc() {
					registerValue.memoryLink(this);
					iteratorValue.memoryLink(this);

					functionContext=NULL;

					catch_ = NULL;
					finally_ = NULL;
					break_ = NULL;
					continue_ = NULL;
					tryBreak_ = NULL;
					tryContinue_ = NULL;
					tryReturn_ = NULL;
					tryThrow_ = NULL;
					pc_ = NULL;
					stackLink_=NULL;
					//
					referenceValue=NULL;
				};

				inline void activeConstructor() {
					catch_ = NULL;
					finally_ = NULL;
					break_ = NULL;
					continue_ = NULL;
					tryBreak_ = NULL;
					tryContinue_ = NULL;
					tryReturn_ = NULL;
					tryThrow_ = NULL;
					pc_ = NULL;
					stackLink_ = NULL;
					//
					referenceValue=NULL;
				};

				inline void activeDestructor() {
					registerValue.deleteObject();
					iteratorValue.deleteObject();
				};

				inline static void memoryInit() {
					TMemory<Variable>::memoryInit();
					TMemory<Iterator>::memoryInit();
				};
		};

	};
};


#endif
