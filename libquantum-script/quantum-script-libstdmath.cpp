﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdmath.hpp"

#include "quantum-script-variablenumber.hpp"

#include "quantum-script-libstdmath.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdMath {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> mathAbs(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-abs\n");
#endif
				return VariableNumber::newVariable(fabs((arguments->index(0))->toNumber()));
			};


			static TPointerOwner<Variable> mathAcos(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-acos\n");
#endif
				return VariableNumber::newVariable(acos((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathAcosh(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-acosh\n");
#endif
				return VariableNumber::newVariable(acosh((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathAsin(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-asin\n");
#endif
				return VariableNumber::newVariable(asin((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathAsinh(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-asinh\n");
#endif
				return VariableNumber::newVariable(asinh((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathAtan(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-atan\n");
#endif
				return VariableNumber::newVariable(atan((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathAtanh(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-atanh\n");
#endif
				return VariableNumber::newVariable(atanh((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathAtan2(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-atan2\n");
#endif
				return VariableNumber::newVariable(atan2((arguments->index(0))->toNumber(),(arguments->index(1))->toNumber()));
			};

			static TPointerOwner<Variable> mathCbrt(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-cbrt\n");
#endif
				return VariableNumber::newVariable(cbrt((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathCeil(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-ceil\n");
#endif
				return VariableNumber::newVariable(ceil((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathCos(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-cos\n");
#endif
				return VariableNumber::newVariable(cos((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathCosh(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-cosh\n");
#endif
				return VariableNumber::newVariable(cosh((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathExp(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-exp\n");
#endif
				return VariableNumber::newVariable(exp((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathExpm1(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-expm1\n");
#endif
				return VariableNumber::newVariable(expm1((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathFloor(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-floor\n");
#endif
				return VariableNumber::newVariable(floor((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathImul(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-imul\n");
#endif
				return VariableNumber::newVariable(((Integer)((arguments->index(0))->toNumber()))*(((Integer)((arguments->index(1))->toNumber()))));
			};

			static TPointerOwner<Variable> mathLog(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-log\n");
#endif
				return VariableNumber::newVariable(log((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathLog1p(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-log1p\n");
#endif
				return VariableNumber::newVariable(log1p((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathLog10(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-log10\n");
#endif
				return VariableNumber::newVariable(log10((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathLog2(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-log2\n");
#endif
				return VariableNumber::newVariable(log2((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathPow(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-pow\n");
#endif
				return VariableNumber::newVariable(pow((arguments->index(0))->toNumber(),(arguments->index(1))->toNumber()));
			};

			static TPointerOwner<Variable> mathRandom(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-random\n");
#endif
				return VariableNumber::newVariable(((Number)rand())/(RAND_MAX+1.0));
			};

			static TPointerOwner<Variable> mathRound(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-round\n");
#endif
				return VariableNumber::newVariable(round((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathSign(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-sign\n");
#endif
				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)) {
					return VariableNumber::newVariable(NAN);
				};
				if(value<0.0) {
					return VariableNumber::newVariable(-1);
				};
				if(value>0.0) {
					return VariableNumber::newVariable(1);
				};
				return VariableNumber::newVariable(0);
			};

			static TPointerOwner<Variable> mathSin(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-sin\n");
#endif
				return VariableNumber::newVariable(sin((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathSinh(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-sinh\n");
#endif
				return VariableNumber::newVariable(sinh((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathSqrt(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-sqrt\n");
#endif
				return VariableNumber::newVariable(sqrt((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathTan(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-tan\n");
#endif
				return VariableNumber::newVariable(tan((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathTanh(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-tanh\n");
#endif
				return VariableNumber::newVariable(tanh((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> mathTrunc(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- math-trunc\n");
#endif
				return VariableNumber::newVariable(trunc((arguments->index(0))->toNumber()));
			};

			void initExecutive(Executive *executive,void *extensionId) {
				srand (static_cast <unsigned> (time(NULL)));

				executive->compileStringX("Math={};");
				executive->setFunction2("Math.abs(x)",mathAbs);
				executive->setFunction2("Math.acos(x)",mathAcos);
				executive->setFunction2("Math.acosh(x)",mathAcosh);
				executive->setFunction2("Math.asin(x)",mathAsin);
				executive->setFunction2("Math.asinh(x)",mathAsinh);
				executive->setFunction2("Math.atan(x)",mathAtan);
				executive->setFunction2("Math.atanh(x)",mathAtanh);
				executive->setFunction2("Math.atan2(y,x)",mathAtan2);
				executive->setFunction2("Math.cbrt(x)",mathCbrt);
				executive->setFunction2("Math.ceil(x)",mathCeil);
				executive->setFunction2("Math.cos(x)",mathCos);
				executive->setFunction2("Math.cosh(x)",mathCosh);
				executive->setFunction2("Math.exp(x)",mathExp);
				executive->setFunction2("Math.expm1(x)",mathExpm1);
				executive->setFunction2("Math.floor(x)",mathFloor);
				executive->setFunction2("Math.imul(x,y)",mathImul);
				executive->setFunction2("Math.log(x)",mathLog);
				executive->setFunction2("Math.log1p(x)",mathLog1p);
				executive->setFunction2("Math.log10(x)",mathLog10);
				executive->setFunction2("Math.log2(x)",mathLog2);
				executive->setFunction2("Math.pow(x,y)",mathPow);
				executive->setFunction2("Math.random()",mathRandom);
				executive->setFunction2("Math.round(x)",mathRound);
				executive->setFunction2("Math.sign(x)",mathSign);
				executive->setFunction2("Math.sin(x)",mathSin);
				executive->setFunction2("Math.sinh(x)",mathSinh);
				executive->setFunction2("Math.sqrt(x)",mathSqrt);
				executive->setFunction2("Math.tan(x)",mathTan);
				executive->setFunction2("Math.tanh(x)",mathTanh);
				executive->setFunction2("Math.trunc(x)",mathTrunc);
				executive->compileStringX((char *)libStdMathSource);
			};

		};
	};
};


