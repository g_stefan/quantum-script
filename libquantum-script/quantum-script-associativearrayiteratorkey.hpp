//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ASSOCIATIVEARRAYITERATORKEY_HPP
#define QUANTUM_SCRIPT_ASSOCIATIVEARRAYITERATORKEY_HPP

#ifndef QUANTUM_SCRIPT_VARIABLEASSOCIATIVEARRAY_HPP
#include "quantum-script-variableassociativearray.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {

		class AssociativeArrayIteratorKey;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::AssociativeArrayIteratorKey>:
			public TMemoryObjectPoolActive<Quantum::Script::AssociativeArrayIteratorKey> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class AssociativeArrayIteratorKey :
			public Iterator {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(AssociativeArrayIteratorKey);
			public:

				Integer index;
				TPointer<VariableAssociativeArray> sourceArray;

				inline AssociativeArrayIteratorKey() {
				};

				QUANTUM_SCRIPT_EXPORT bool next(TPointerX<Variable> &out);

				inline void activeDestructor() {
					sourceArray.deleteObject();
				};

		};

	};
};


#endif

