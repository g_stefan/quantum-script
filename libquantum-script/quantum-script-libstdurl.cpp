﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdurl.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variablestring.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdURL {

			using namespace XYO::XY;
			using namespace XYO::XO;


			static TPointerOwner<Variable> decodeComponent(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-decode-component\n");
#endif
				String in=(arguments->index(0))->toString();
				String out;
				size_t k;
				int value;
				char buf[3];
				buf[2]=0;
				for(k=0; k<in.length(); ++k) {
					if(in[k]=='%') {
						if(k+1<in.length()) {
							++k;
							buf[0]=in[k];
							if(k+1<in.length()) {
								++k;
								buf[1]=in[k];
								if(sscanf(buf,"%02X",&value)==1) {
									out<<(char)value;
								};
							};
						};
						continue;
					};
					out<<in[k];
				};
				return VariableString::newVariable(out);
			};

			static TPointerOwner<Variable> encodeComponent(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-encode-component\n");
#endif

				String in=(arguments->index(0))->toString();
				String out;
				size_t k;
				int value;
				char buf[4];
				for(k=0; k<in.length(); ++k) {
					if(
						(in[k]>='A'&&in[k]<='Z')
						||(in[k]>='a'&&in[k]<='z')
						||(in[k]=='.')
						||(in[k]=='!')
						||(in[k]=='~')
						||(in[k]=='*')
						||(in[k]=='\'')
						||(in[k]=='(')
						||(in[k]==')')
					) {
						out<<in[k];
						continue;
					};
					out<<'%';
					sprintf(buf,"%02X",in[k]);
					out<<buf;
				};
				return VariableString::newVariable(out);
			};

			static TPointerOwner<Variable> getSchemeName(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-get-scheme-name\n");
#endif

				String url=(arguments->index(0))->toString();
				size_t index;
				if(StringX::indexOf(url,"://",0,index)) {
					return VariableString::newVariable(StringX::substring(url,0,index));
				};
				return VariableNull::newVariable();
			};

			static TPointerOwner<Variable> getHostNameAndPort(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-get-host-name-and-port\n");
#endif

				String url=(arguments->index(0))->toString();
				size_t index;
				size_t part;
				String firstPart;
				String secondPart;
				if(StringX::indexOf(url,"://",0,index)) {
					if(StringX::indexOf(url,"/",index+3,part)) {
						if(StringX::split2FromBegin(StringX::substring(url,index+3,part-(index+3)), "@",firstPart,secondPart)) {
							return VariableString::newVariable(secondPart);
						};
						return VariableString::newVariable(StringX::substring(url,index+3,part-(index+3)));
					};
					return VariableString::newVariable(StringX::substring(url,index+3));
				};
				return VariableNull::newVariable();
			};

			static TPointerOwner<Variable> getUsernameAndPassword(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-get-username-and-password\n");
#endif

				String url=(arguments->index(0))->toString();
				size_t index;
				size_t part;
				String firstPart;
				String secondPart;
				if(StringX::indexOf(url,"://",0,index)) {
					if(StringX::indexOf(url,"/",index+3,part)) {
						if(StringX::split2FromBegin(StringX::substring(url,index+3,part-(index+3)), "@",firstPart,secondPart)) {
							return VariableString::newVariable(firstPart);
						};
					};
				};
				return VariableNull::newVariable();
			};


			static TPointerOwner<Variable> getPathAndFileName(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-get-path-and-file-name\n");
#endif

				String url=(arguments->index(0))->toString();
				size_t index;
				size_t part;
				String firstPart;
				String secondPart;
				if(StringX::indexOf(url,"://",0,index)) {
					if(StringX::indexOf(url,"/",index+3,part)) {
						if(StringX::split2FromBegin(StringX::substring(url,part), "?",firstPart,secondPart)) {
							return VariableString::newVariable(firstPart);
						};
						return VariableString::newVariable(StringX::substring(url,part));
					};
					return VariableString::newVariable("/");
				};
				return VariableNull::newVariable();
			};

			static TPointerOwner<Variable> getQuery(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-get-query\n");
#endif

				String url=(arguments->index(0))->toString();
				size_t index;
				size_t part;
				String firstPart;
				String secondPart;
				String thirdPart;
				if(StringX::indexOf(url,"://",0,index)) {
					if(StringX::indexOf(url,"/",index+3,part)) {
						if(StringX::split2FromBegin(StringX::substring(url,part+1), "?",firstPart,secondPart)) {
							if(StringX::split2FromBegin(secondPart, "#",firstPart,thirdPart)) {
								return VariableString::newVariable(firstPart);
							};
							return VariableString::newVariable(secondPart);
						};
					};
				};
				return VariableNull::newVariable();
			};

			static TPointerOwner<Variable> getPathAndFileNameWithQuery(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- url-get-path-and-file-name-with-query\n");
#endif

				String url=(arguments->index(0))->toString();
				size_t index;
				size_t part;
				String firstPart;
				String secondPart;
				if(StringX::indexOf(url,"://",0,index)) {
					if(StringX::indexOf(url,"/",index+3,part)) {
						if(StringX::split2FromBegin(StringX::substring(url,part), "#",firstPart,secondPart)) {
							return VariableString::newVariable(firstPart);
						};
						return VariableString::newVariable(StringX::substring(url,part));
					};
				};
				return VariableNull::newVariable();
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var URL={};");
				executive->setFunction2("URL.decodeComponent(str)", decodeComponent);
				executive->setFunction2("URL.encodeComponent(value)", encodeComponent);
				executive->setFunction2("URL.getSchemeName(url)", getSchemeName);
				executive->setFunction2("URL.getHostNameAndPort(url)", getHostNameAndPort);
				executive->setFunction2("URL.getUsernameAndPassword(url)", getUsernameAndPassword);
				executive->setFunction2("URL.getPathAndFileName(url)", getPathAndFileName);
				executive->setFunction2("URL.getPathAndFileNameWithQuery(url)", getPathAndFileNameWithQuery);
				executive->setFunction2("URL.getQuery(url)", getQuery);
			};

		};
	};
};


