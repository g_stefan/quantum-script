//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_IASSEMBLER_HPP
#define QUANTUM_SCRIPT_IASSEMBLER_HPP

#ifndef QUANTUM_SCRIPT_HPP
#include "quantum-script.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROGRAMCOUNTER_HPP
#include "quantum-script-programcounter.hpp"
#endif

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class IAssembler :
			public virtual Object {
				XYO_XY_INTERFACE(IAssembler);
			public:

				virtual ProgramCounter *assemble(int type_, const char *name_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) = 0;
				virtual ProgramCounter *assembleX(int type_, const char *name_, const char *nameX_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) = 0;
				virtual ProgramCounter *assembleProgramCounter(int type_, ProgramCounter *pc_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) = 0;
				virtual void linkProgramCounter(ProgramCounter *old_, ProgramCounter *new_) = 0;
				virtual void linkProgramCounterEnd(ProgramCounter *old_, ProgramCounter *new_) = 0;
				virtual void linkProgramCounterSource(ProgramCounter *pc_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) = 0;
				virtual void removeProgramCounter(ProgramCounter *pc_)=0;

		};

	};
};


#endif
