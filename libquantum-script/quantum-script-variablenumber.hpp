﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLENUMBER_HPP
#define QUANTUM_SCRIPT_VARIABLENUMBER_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableNumber;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableNumber>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableNumber> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableNumber :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableNumber);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeNumber;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeNumber;

				Number value;

				inline VariableNumber() {
					variableType = typeNumber;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(Number value);
				QUANTUM_SCRIPT_EXPORT static Variable *newVariableX(Number value);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);


				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT Number toNumber();
				QUANTUM_SCRIPT_EXPORT String toString();
		};
	};
};


#endif

