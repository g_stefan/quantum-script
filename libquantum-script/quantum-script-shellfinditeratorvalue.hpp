//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_SHELLFINDITERATORVALUE_HPP
#define QUANTUM_SCRIPT_SHELLFINDITERATORVALUE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLESHELLFIND_HPP
#include "quantum-script-variableshellfind.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {

		class ShellFindIteratorValue;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ShellFindIteratorValue>:
			public TMemoryObjectPoolActive<Quantum::Script::ShellFindIteratorValue> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class ShellFindIteratorValue :
			public Iterator {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ShellFindIteratorValue);
			public:

				TPointer<VariableShellFind> value_;
				ShellFind *sourceShellFind;

				inline ShellFindIteratorValue() {
				};

				QUANTUM_SCRIPT_EXPORT bool next(TPointerX<Variable> &out);

				inline void activeDestructor() {
					value_.deleteObject();
				};

		};

	};
};



#endif
