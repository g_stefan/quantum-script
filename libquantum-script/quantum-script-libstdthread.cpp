﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/timeb.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-instruction.hpp"
#include "quantum-script-libstdthread.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablequitmessage.hpp"

#include "quantum-script-libstdthread.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
#else

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD

namespace Quantum {
	namespace Script {

		namespace LibStdThread {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> threadImplementation(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-create\n");
#endif

				ThreadTransfer *threadTransfer;
				VariableThreadInfo *threadInfo;

				TPointer<Variable> threadMode;
				TPointer<Variable> threadFile;
				TPointer<Variable> threadThis;
				TPointer<Variable> threadArguments;
				bool sourceOk;

				threadMode = arguments->index(0);
				threadFile = arguments->index(1);
				threadThis = arguments->index(2);
				threadArguments = arguments->index(3);

				sourceOk=false;
				if((Integer)(threadMode->toNumber())==0) {
					if(threadFile->variableType==VariableVmFunction::typeVmFunction) {
						threadFile.setObject(VariableString::newVariable(threadFile->toString()));
						sourceOk=true;
					};
				} else {
					sourceOk=true;
				};

				if(sourceOk) {
					if(threadFile->variableType==VariableString::typeString) {

						threadInfo=TMemory<VariableThreadInfo>::newObject();
						threadInfo->returnValueSuper.deleteObject();
						threadInfo->returnValueLocal=NULL;
						threadInfo->vmLocal=NULL;
						threadInfo->finalizeLocal=false;
						threadInfo->terminatedLocal=false;
						threadInfo->isTerminated=false;
						threadInfo->isSuspended=false;
						threadInfo->messageProcessed=true;
						threadInfo->typeOfMessage=false;
						threadInfo->waitChilds=false;
						threadInfo->prototype=function->prototype;

						threadTransfer=new ThreadTransfer();
						threadTransfer->threadFile=threadFile.getObject();
						threadTransfer->threadThis=threadThis.getObject();
						threadTransfer->threadArguments=threadArguments.getObject();
						threadTransfer->super=function->valueSuper;
						threadTransfer->threadInfo=(Variable *)threadInfo;
						threadTransfer->threadStarted=false;
						threadTransfer->isFile=(((Integer)(threadMode->toNumber()))==1);

						threadInfo->incReferenceCount();
						((Executive *)(function->valueSuper))->threadCount++;
						((Executive *)(function->valueSuper))->childThread.push(threadInfo);


						if(XY::XThread::startWithNotify(
							   (XY::XThread::Procedure)Executive::threadExecutor,threadTransfer,
							   (XY::XThread::Procedure)Executive::threadExecutorNew,threadInfo,
							   (XY::XThread::Procedure)Executive::threadExecutorDelete,threadInfo
						   )) {

							while(!threadTransfer->threadStarted) {
								XThread::sleepOneMillisecond();
							};

							threadTransfer->threadFile->decReferenceCount();
							threadTransfer->threadThis->decReferenceCount();
							threadTransfer->threadArguments->decReferenceCount();
							delete threadTransfer;

							return (Variable *)threadInfo;
						};

						threadTransfer->threadFile->decReferenceCount();
						threadTransfer->threadThis->decReferenceCount();
						threadTransfer->threadArguments->decReferenceCount();
						threadInfo->decReferenceCount();

						delete threadTransfer;

						throw(Error("unable to start thread"));

					};
				};
				throw(Error("invalid parameters"));
			};

			static TPointerOwner<Variable> suspend(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-suspend\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				((VariableThreadInfo *)( this_ ))->isSuspended=true;

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> resume(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-resume\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				((VariableThreadInfo *)( this_ ))->isSuspended=false;

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> terminate(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-terminate\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				VariableThreadInfo *threadInfo=(VariableThreadInfo *)( this_ );
				threadInfo->returnValueSuper=arguments->index(0);
				threadInfo->isSuspended=true;
				threadInfo->isTerminated=true;

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> getReturnedValue(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-get-returned-value\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				return ((VariableThreadInfo *)( this_ ))->returnValueSuper;
			};


			static TPointerOwner<Variable> isTerminated(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-is-terminated\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableThreadInfo *)( this_ ))->terminatedLocal);
			};


			static TPointerOwner<Variable> postMessage(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-post-message\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableThreadInfo *)( this_ ))->terminatedLocal) {
					return Context::getValueUndefined();
				};

				if(((VariableThreadInfo *)( this_ ))->isTerminated) {
					return Context::getValueUndefined();
				};

				if(((VariableThreadInfo *)( this_ ))->waitChilds) {
					return Context::getValueUndefined();
				};

				((VariableThreadInfo *)( this_ ))->isPostMessage=true;
				((VariableThreadInfo *)( this_ ))->isSendMessage=false;
				((VariableThreadInfo *)( this_ ))->messageToReceive=(arguments->index(0)).value();
				while(((VariableThreadInfo *)( this_ ))->messageToReceive) {
					if(((VariableThreadInfo *)( this_ ))->terminatedLocal) {
						return Context::getValueUndefined();
					};
					if(((VariableThreadInfo *)( this_ ))->isTerminated) {
						return Context::getValueUndefined();
					};
					XThread::sleepOneMillisecond();
				};
				((VariableThreadInfo *)( this_ ))->isPostMessage=false;
				return Context::getValueUndefined();
			};


			static TPointerOwner<Variable> sendMessageX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-send-message-x\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableThreadInfo *)( this_ ))->terminatedLocal) {
					return VariableBoolean::newVariable(true);
				};

				if(((VariableThreadInfo *)( this_ ))->isTerminated) {
					return VariableBoolean::newVariable(true);
				};

				if(((VariableThreadInfo *)( this_ ))->waitChilds) {
					return VariableBoolean::newVariable(true);
				};

				if(((VariableThreadInfo *)( this_ ))->isSendMessage) {
					return VariableBoolean::newVariable(false);
				};

				if(!((VariableThreadInfo *)( this_ ))->messageProcessed) {
					return VariableBoolean::newVariable(false);
				};

				((VariableThreadInfo *)( this_ ))->isPostMessage=false;
				((VariableThreadInfo *)( this_ ))->messageProcessed=false;
				((VariableThreadInfo *)( this_ ))->messageToReceive=(arguments->index(0)).value();
				while(((VariableThreadInfo *)( this_ ))->messageToReceive) {
					if(((VariableThreadInfo *)( this_ ))->terminatedLocal) {
						return VariableBoolean::newVariable(true);
					};
					if(((VariableThreadInfo *)( this_ ))->isTerminated) {
						return VariableBoolean::newVariable(true);
					};
					XThread::sleepOneMillisecond();
				};
				((VariableThreadInfo *)( this_ ))->isSendMessage=true;
				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> isMessageProcessedX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-is-message-processed-x\n");
#endif

				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableThreadInfo *)( this_ ))->terminatedLocal) {
					return VariableBoolean::newVariable(true);
				};
				if(((VariableThreadInfo *)( this_ ))->isTerminated) {
					return VariableBoolean::newVariable(true);
				};
				if(((VariableThreadInfo *)( this_ ))->waitChilds) {
					return VariableBoolean::newVariable(true);
				};

				return VariableBoolean::newVariable(((VariableThreadInfo *)( this_ ))->messageProcessed);
			};

			static TPointerOwner<Variable> getMessageReturnValueX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-get-message-return-value-x\n");
#endif
				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				if(((VariableThreadInfo *)( this_ ))->terminatedLocal) {
					return Context::getValueUndefined();
				};
				if(((VariableThreadInfo *)( this_ ))->isTerminated) {
					return Context::getValueUndefined();
				};
				if(((VariableThreadInfo *)( this_ ))->waitChilds) {
					return Context::getValueUndefined();
				};

				return ((VariableThreadInfo *)( this_ ))->returnValueSuper;
			};

			static TPointerOwner<Variable> isBusy(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-is-busy\n");
#endif
				if(this_->variableType!=VariableThreadInfo::typeThreadInfo) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableThreadInfo *)( this_ ))->isBusy);
			};

			static TPointerOwner<Variable> quitMessage(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- thread-implementation-quit-message\n");
#endif

				return VariableQuitMessage::newVariable();
			};


			QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(VmLibStdCurrentThread_exit) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-exit\n", context->currentProgramCounter);
#endif

				context->threadInfo->returnValueLocal->decReferenceCount();
				context->threadInfo->returnValueLocal=(context->getArgument(0)).getObject();

				context->threadInfo->isSuspended=true;
				context->threadInfo->isTerminated=true;
				context->nextProgramCounter = NULL;
			};


			QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(VmLibStdCurrentThread_setReturnValue) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-set-return-value\n", context->currentProgramCounter);
#endif
				context->threadInfo->returnValueLocal->decReferenceCount();
				context->threadInfo->returnValueLocal=(context->getArgument(0)).getObject();
			};


			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentThread_hasMessage) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-has-message\n", context->currentProgramCounter);
#endif

				context->pushOwner(VariableBoolean::newVariable(context->threadInfo->hasMessage()));
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentThread_getMessageX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-get-message-x\n", context->currentProgramCounter);
#endif
				context->pushX(context->threadInfo->getMessageX());
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentThread_messageProcessed) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-message-processed\n", context->currentProgramCounter);
#endif

				if(context->threadInfo->typeOfMessage) {
					context->threadInfo->messageToSend=(context->getArgument(0)).value();
					while(context->threadInfo->messageToSend) {
						XThread::sleepOneMillisecond();
					};
					context->threadInfo->messageProcessed=true;
					context->threadInfo->isSendMessage=false;
				};
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentThread_isMessageProcessedX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-is-message-processed-x\n", context->currentProgramCounter);
#endif
				if(context->threadInfo->typeOfMessage) {
					context->pushOwner(VariableBoolean::newVariable(context->threadInfo->messageProcessed));
				} else {
					context->pushOwner(VariableBoolean::newVariable(false));
				};
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdCurrentThread_setIsBusyX) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    current-thread-set-is-busy-x\n", context->currentProgramCounter);
#endif

				context->threadInfo->isBusy=(context->getArgument(0))->toBoolean();
				context->pushX(Context::getValueUndefined());
				InstructionVmReturn(context, NULL);
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction4("ThreadImplementation(mode,this_,parameters)", threadImplementation, executive);
				executive->setFunction2("ThreadImplementation.prototype.suspend()", suspend);
				executive->setFunction2("ThreadImplementation.prototype.resume()", resume);
				executive->setFunction2("ThreadImplementation.prototype.terminate(returnValue)", terminate);
				executive->setFunction2("ThreadImplementation.prototype.getReturnedValue()", getReturnedValue);
				executive->setFunction2("ThreadImplementation.prototype.isTerminated()", isTerminated);
				executive->setFunction2("ThreadImplementation.prototype.postMessage(message)", postMessage);
				executive->setFunction2("ThreadImplementation.prototype.sendMessageX(message)", sendMessageX);
				executive->setFunction2("ThreadImplementation.prototype.isMessageProcessedX()", isMessageProcessedX);
				executive->setFunction2("ThreadImplementation.prototype.getMessageReturnValueX()", getMessageReturnValueX);
				executive->setFunction2("ThreadImplementation.prototype.isBusy()", isBusy);
				executive->setFunction2("ThreadImplementation.prototype.quitMessage()", quitMessage);

				executive->compileStringX("var CurrentThread={};");

				executive->setVmFunction("CurrentThread.exit(returnValue)", InstructionVmLibStdCurrentThread_exit, NULL);
				executive->setVmFunction("CurrentThread.setReturnValue(returnValue)", InstructionVmLibStdCurrentThread_setReturnValue, NULL);
				executive->setVmFunction("CurrentThread.hasMessage()", InstructionLibStdCurrentThread_hasMessage, NULL);
				executive->setVmFunction("CurrentThread.getMessageX()", InstructionLibStdCurrentThread_getMessageX, NULL);
				executive->setVmFunction("CurrentThread.messageProcessed(returnValue)", InstructionLibStdCurrentThread_messageProcessed, NULL);
				executive->setVmFunction("CurrentThread.isMessageProcessedX()", InstructionLibStdCurrentThread_isMessageProcessedX, NULL);
				executive->setVmFunction("CurrentThread.setIsBusyX(value)", InstructionLibStdCurrentThread_setIsBusyX, NULL);

				executive->compileStringX((char *)libStdThreadSource);
			};
		};

	};
};


#endif

#endif
