﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEREFERENCEOBJECT_HPP
#define QUANTUM_SCRIPT_VARIABLEREFERENCEOBJECT_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableReferenceObject;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableReferenceObject>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableReferenceObject> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableReferenceObject :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableReferenceObject);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeReferenceObject;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeReferenceObject;

				TPointerX<Variable> *value;

				inline VariableReferenceObject() {
					variableType = typeReferenceObject;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(TPointerX<Variable> *value);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
