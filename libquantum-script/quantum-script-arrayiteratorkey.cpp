﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-arrayiteratorkey.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variablenumber.hpp"

namespace Quantum {
	namespace Script {

		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		bool ArrayIteratorKey::next(TPointerX<Variable> &out) {
			if(index>=sourceArray->value->length()) {
				out=Context::getValueUndefined();
				return false;
			};
			out.setObject(VariableNumber::newVariable(index));
			++index;
			return true;
		};

	};
};


