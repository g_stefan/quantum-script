//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_FUNCTIONPARENT_HPP
#define QUANTUM_SCRIPT_FUNCTIONPARENT_HPP

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#include "quantum-script-variablearray.hpp"
#endif

namespace Quantum {
	namespace Script {

		class FunctionParent;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::FunctionParent>:
			public TMemoryObjectPoolActive<Quantum::Script::FunctionParent> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class FunctionParent:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(FunctionParent);
			public:

				TPointerX<FunctionParent> functionParent;
				TPointerX<VariableArray> variables;
				TPointerX<VariableArray> arguments;

				inline FunctionParent() {
					functionParent.memoryLink(this);
					variables.memoryLink(this);
					arguments.memoryLink(this);
				};

				inline void activeDestructor() {
					functionParent.deleteObject();
					variables.deleteObject();
					arguments.deleteObject();
				};

		};

	};
};


#endif
