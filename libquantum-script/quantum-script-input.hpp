//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_INPUT_HPP
#define QUANTUM_SCRIPT_INPUT_HPP

#ifndef QUANTUM_SCRIPT_HPP
#include "quantum-script.hpp"
#endif

#define QUANTUM_SCRIPT_INPUT_STACK_SIZE 32

namespace Quantum {
	namespace Script {


		class InputError {
			public:
				enum {
					None,
					Eof
				};
		};

		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		typedef struct s_InputStack {
			char stack[QUANTUM_SCRIPT_INPUT_STACK_SIZE];
			int length;
		} InputStack;

		class Input :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Input);
			protected:
				TPointer<IRead> input;
				InputStack stack;

				bool pop();
			public:
				int error;
				dword filePos;
				dword lineNumber;
				dword lineColumn;
				char inputChar;

				QUANTUM_SCRIPT_EXPORT Input();
				QUANTUM_SCRIPT_EXPORT ~Input();

				QUANTUM_SCRIPT_EXPORT bool init(IRead *input_);
				QUANTUM_SCRIPT_EXPORT bool push();
				QUANTUM_SCRIPT_EXPORT bool read();

				inline bool isEof() {
					return (error == InputError::Eof);
				};

				inline bool is(const char *inputChar_) {
					return (inputChar==inputChar_[0]);
				};

				inline void set(const char *inputChar_) {
					inputChar=inputChar_[0];
				};

				inline bool is1(const char *inputChar_) {
					if (inputChar==inputChar_[0]) {
						read();
						return true;
					};
					return false;
				};

				QUANTUM_SCRIPT_EXPORT bool isN(const char *name);
		};

	};
};


#endif
