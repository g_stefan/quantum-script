//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_LIBSTDTHREAD_HPP
#define QUANTUM_SCRIPT_LIBSTDTHREAD_HPP

#ifndef QUANTUM_SCRIPT_EXECUTIVE_HPP
#include "quantum-script-executive.hpp"
#endif

#ifndef QUANTUM_SCRIPT_SINGLE_FIBER

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD

namespace Quantum {
	namespace Script {

		namespace LibStdThread {

			QUANTUM_SCRIPT_EXPORT void initExecutive(Executive *executive,void *extensionId);

		};
	};
};


#endif

#endif

#endif
