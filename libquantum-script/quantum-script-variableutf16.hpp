﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEUTF16_HPP
#define QUANTUM_SCRIPT_VARIABLEUTF16_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableUtf16;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableUtf16>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableUtf16> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableUtf16 :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableUtf16);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeUtf16;

				TPointer<Variable> vLength;
			public:
				StringWord value;
				QUANTUM_SCRIPT_EXPORT static const void *typeUtf16;

				inline VariableUtf16() {
					variableType = typeUtf16;
				};

				inline void activeDestructor() {
					value.activeDestructor();
					vLength.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT  static Variable *newVariable(StringWord value);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT static StringWord getUtf16(Variable *this_);

				QUANTUM_SCRIPT_EXPORT bool hasProperty(Variable *variable);


				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT Number toNumber();
				QUANTUM_SCRIPT_EXPORT String toString();
				QUANTUM_SCRIPT_EXPORT bool isString();
		};

	};
};


#endif
