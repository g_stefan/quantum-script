﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEATOMIC_HPP
#define QUANTUM_SCRIPT_VARIABLEATOMIC_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ATOMIC_HPP
#include "quantum-script-atomic.hpp"
#endif

namespace Quantum {
	namespace Script {


		class VariableAtomic;

	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableAtomic>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableAtomic> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableAtomic :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableAtomic);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeAtomic;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeAtomic;

				Atomic *value;

				QUANTUM_SCRIPT_EXPORT VariableAtomic();
				QUANTUM_SCRIPT_EXPORT ~VariableAtomic();

				QUANTUM_SCRIPT_EXPORT void activeDestructor();

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};


	};
};


#endif

