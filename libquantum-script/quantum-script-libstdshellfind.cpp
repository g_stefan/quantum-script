﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdshellfind.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableshellfind.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdShellFind {

			using namespace XYO::XY;
			using namespace XYO::XO;


			ShellFindContext::ShellFindContext() {
				symbolFunctionShellFind=0;
				prototypeShellFind.memoryLink(this);
			};

			ShellFindContext *getContext() {
				return TXSingleton<ShellFindContext>::getValue();
			};

			static TPointerOwner<Variable> functionShellFind(VariableFunction *function,Variable *this_,VariableArray *arguments) {
				TPointerX<Variable> &toFind(arguments->index(0));
				if((toFind->variableType==VariableUndefined::typeUndefined)||
				   (toFind->variableType==VariableNull::typeNull)) {
					return VariableShellFind::newVariable();
				};
				TPointerOwner<Variable> find_(VariableShellFind::newVariable());
				((VariableShellFind *)find_.value())->value.find(toFind->toString());
				return find_;
			};

			static void deleteContext() {
				ShellFindContext *shellFindContext=getContext();
				shellFindContext->prototypeShellFind.deleteObject();
				shellFindContext->symbolFunctionShellFind=0;
			};

			static void newContext(Executive *executive,void *extensionId) {
				VariableFunction *defaultPrototypeFunction;

				ShellFindContext *shellFindContext=getContext();
				executive->setExtensionDeleteContext(extensionId,deleteContext);

				shellFindContext->symbolFunctionShellFind=Context::getSymbol("ShellFind");
				shellFindContext->prototypeShellFind.newObject();

				defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionShellFind,NULL,NULL);
				((Context::getGlobalObject())->operatorReferenceOwnProperty(shellFindContext->symbolFunctionShellFind)).setObjectX(defaultPrototypeFunction);
				shellFindContext->prototypeShellFind=defaultPrototypeFunction->prototype;
			};

			static TPointerOwner<Variable> isShellFind(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-shell-find\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableShellFind::typeShellFind);
			};

			static TPointerOwner<Variable> findFile(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-find\n");
#endif
				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				if(((VariableShellFind *)this_)->value.find((arguments->index(0))->toString())) {
					this_->incReferenceCount();
					return this_;
				};

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> findNext(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-find-next\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableShellFind *)this_)->value.findNext());
			};

			static TPointerOwner<Variable> findClose(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-close\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				((VariableShellFind *) this_)->value.close();

				return Context::getValueUndefined();
			};


			static TPointerOwner<Variable> isReadOnly(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-is-read-only\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableShellFind *)this_)->value.isReadOnly);
			};

			static TPointerOwner<Variable> isDirectory(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-is-directory\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableShellFind *)this_)->value.isDirectory);
			};

			static TPointerOwner<Variable> isFile(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-is-file\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableShellFind *)this_)->value.isFile);
			};

			static TPointerOwner<Variable> name(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-name\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				return VariableString::newVariable(((VariableShellFind *)this_)->value.name);
			};

			static TPointerOwner<Variable> isValid(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shellfind-is-valid\n");
#endif

				if(this_->variableType!=VariableShellFind::typeShellFind) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableShellFind *)this_)->value.isValid());
			};

			void initExecutive(Executive *executive,void *extensionId) {

				newContext(executive,extensionId);

				executive->setFunction2("Script.isShellFind(x)", isShellFind);
				executive->setFunction2("ShellFind.prototype.find(file)",  findFile);
				executive->setFunction2("ShellFind.prototype.next()",  findNext);
				executive->setFunction2("ShellFind.prototype.close()",  findClose);
				executive->setFunction2("ShellFind.prototype.isReadOnly()",  isReadOnly);
				executive->setFunction2("ShellFind.prototype.isDirectory()",  isDirectory);
				executive->setFunction2("ShellFind.prototype.isFile()",  isFile);
				executive->setFunction2("ShellFind.prototype.name()",  name);
				executive->setFunction2("ShellFind.prototype.isValid()",  isValid);

			};

		};
	};
};


