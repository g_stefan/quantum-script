﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-instructioncontext.hpp"
#include "quantum-script-variablestacktrace.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableStackTrace::typeStackTrace= "{D2C5840D-48B9-4424-8BCC-39147444BC1A}";
		const char *VariableStackTrace::strTypeStackTrace="StackTrace";

		Variable *VariableStackTrace::newVariable(TPointer<TStack2<InstructionTrace> > stackTrace, InstructionContext *context) {
			VariableStackTrace *retV;
			retV=TMemory<VariableStackTrace>::newObject();
			retV->stackTrace = stackTrace;
			retV->context = context;
			return (Variable *) retV;
		};

		String VariableStackTrace::getType() {
			return strTypeStackTrace;
		};

		String VariableStackTrace::toString() {
			return toString(configPrintStackTraceLimit);
		};

		String VariableStackTrace::toString(int level_) {
			String out = "";
			char buffer[1024];
			if (stackTrace) {
				dword level = 0;
				TYList2<InstructionTrace> *scan;
				for (scan = stackTrace->tail(); scan && (level < level_); scan = scan->back, ++level) {

					File inFile;
					MemoryFileRead inMemory;
					IRead *sourceFile = NULL;
					ISeek *sourceSeek = NULL;

					if(scan->value.sourceSymbol) {

						String symbol=Context::getSymbolMirror(scan->value.sourceSymbol);

						if (symbol[0] == '#') {
							out<<"- file "<<symbol.index(1)<<" ";
							sprintf(buffer,"line %d column %d pos %d\n", scan->value.sourceLineNumber, scan->value.sourceLineColumn, scan->value.sourcePos);
							out<<buffer;
							if (inFile.openRead((char *)symbol.index(1))) {
								sourceFile = &inFile;
								sourceSeek = &inFile;
							};
						};
						if (symbol[0] == '@') {
							out<<"- memory "<<symbol.index(1)<<" ";
							sprintf(buffer,"line %d column %d pos %d\n", scan->value.sourceLineNumber, scan->value.sourceLineColumn, scan->value.sourcePos);
							out<<buffer;
							if (inMemory.openRead((char *)symbol.index(1), symbol.length() - 1)) {
								sourceFile = &inMemory;
								sourceSeek = &inMemory;
							};
						};
						if (sourceFile) {
							bool isOk;
							long int currentPos;
							long int scanStep;
							long int inBack;
							long int inFront;
							long int inIndex;
							int indexColumn;
							byte code;


							if (scan->value.sourcePos > 1) {
								sourceSeek->seekFromBegin(scan->value.sourcePos - 2);
							};
							isOk = false;
							scanStep = 56;
							inBack = scan->value.sourceLineColumn;
							do {
								currentPos = sourceSeek->seekTell();
								if (sourceFile->read(&code, sizeof (byte))) {
									--scanStep;
									--inBack;
									if (inBack == 0 || scanStep == 0) {
										isOk = true;
										break;
									};
									sourceSeek->seek(-2);
									continue;
								};
								break;
							} while (currentPos > 0);
							if (isOk) {
								out<<"...  ";
								if (inBack == 0) {
									inFront = scan->value.sourceLineColumn + 20;
									inIndex = scan->value.sourceLineColumn - 1;
								};
								if (scanStep == 0) {
									inFront = 56;
									inIndex = scan->value.sourceLineColumn - inFront - 1;
								};
								while (sourceFile->read(&code, sizeof (byte))) {
									--inFront;
									if (code == '\n' || code == '\r' || inFront == 0) {
										out<<"  ...\n";

										out<<"     ";
										for (indexColumn = 0; indexColumn < inIndex; ++indexColumn) {
											out<<" ";
										};
										out<<"^\n";
										break;
									};
									if (code == '\t') {
										out<<" ";
										continue;
									};
									sprintf(buffer,"%c", code);
									out<<buffer;
								};
							};
						};
					};
				};
			};
			return out;
		};


		bool VariableStackTrace::toBoolean() {
			return true;
		};


	};
};


