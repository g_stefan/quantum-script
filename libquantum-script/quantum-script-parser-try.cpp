//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-parser.hpp"
#include "quantum-script-parserasm.hpp"

namespace Quantum {
	namespace Script {


		bool Parser::statementTry() {
			if (token.isSymbolX("try")) {

				if (token.checkIs1("{")) {
					ProgramCounter *linkCatch;
					ProgramCounter *linkFinally;
					ProgramCounter *linkTryBreak;
					ProgramCounter *linkTryContinue;
					ProgramCounter *linkTryReturn;
					ProgramCounter *linkGoFinally;
					ProgramCounter *linkTryThrow;
					assemble(ParserAsm::EnterContext);

					assemble(ParserAsm::ContextSetStack);
					linkCatch = assembleProgramCounter(ParserAsm::ContextSetCatch, NULL);
					linkFinally = assembleProgramCounter(ParserAsm::ContextSetFinally, NULL);
					linkTryBreak = assembleProgramCounter(ParserAsm::ContextSetTryBreak, NULL);
					linkTryContinue = assembleProgramCounter(ParserAsm::ContextSetTryContinue, NULL);
					linkTryReturn = assembleProgramCounter(ParserAsm::ContextSetTryReturn, NULL);
					linkTryThrow = assembleProgramCounter(ParserAsm::ContextSetTryThrow, NULL);


					if (isBlockStatement()) {

						assemble(ParserAsm::LeaveContext);
						linkGoFinally = assembleProgramCounter(ParserAsm::Goto, NULL);

						linkProgramCounter(linkTryBreak,
								   assemble(ParserAsm::Break)
								  );
						linkProgramCounter(linkTryContinue,
								   assemble(ParserAsm::Continue)
								  );
						linkProgramCounter(linkTryReturn,
								   assemble(ParserAsm::Return)
								  );
						linkProgramCounter(linkTryThrow,
								   assemble(ParserAsm::Throw)
								  );


						if (token.isSymbolX("catch")) {
							if (token.is1("(")) {
								token.reset();
								if (token.isSymbol()) {
									if (token.is1(")")) {
										linkProgramCounter(linkCatch,
												   assemble(ParserAsm::Catch)
												  );
										assemble1(ParserAsm::PushObjectReference, token.value);
										assemble(ParserAsm::AssignReverse);

										if (token.checkIs1("{")) {
											assemble(ParserAsm::EnterContext);

											if (isBlockStatement()) {

												assemble(ParserAsm::LeaveContext);

												if (token.isSymbolX("finally")) {
													if (token.checkIs1("{")) {

														linkProgramCounter(linkGoFinally,assemble(ParserAsm::EnterContext));

														if (isBlockStatement()) {
															assemble(ParserAsm::LeaveContext);
															return true;
														};
													};
												} else {
													linkProgramCounter(linkGoFinally,assemble(ParserAsm::Mark));
													return true;
												};



											};
										};
									};
								};
							};
							error = ParserError::Compile;
							return false;
						};
						if (token.isSymbolX("finally")) {

							if (token.checkIs1("{")) {

								linkProgramCounter(linkGoFinally,assemble(ParserAsm::EnterContext));

								if (isBlockStatement()) {

									assemble(ParserAsm::LeaveContext);

									return true;
								};
							};

						};


					};
				};

				error = ParserError::Compile;
				return false;
			};
			return false;
		};


	};
};


