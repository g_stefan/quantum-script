﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-context.hpp"
#include "quantum-script-variablesocket.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-libstdsocket.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableSocket::typeSocket="{DBB0BD0F-547B-445D-B070-0A744845F8F4}";
		const char *VariableSocket::strTypeSocket="Socket";

		String VariableSocket::getType() {
			return strTypeSocket;
		};

		Variable *VariableSocket::newVariable() {
			return (Variable *) TMemory<VariableSocket>::newObject();
		};

		Variable &VariableSocket::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(LibStdSocket::getContext())->prototypeSocket->prototype);
		};

		Variable *VariableSocket::instancePrototype() {
			return (LibStdSocket::getContext())->prototypeSocket->prototype;
		};

		void VariableSocket::activeDestructor() {
			value.close();
		};

		Variable *VariableSocket::clone(SymbolList &inSymbolList) {
			VariableSocket *out=(VariableSocket *)newVariable();
			if(out->value.becomeOwner(value)) {
				return  out;
			};
			out->decReferenceCount();
			return VariableUndefined::newVariable();
		};

		bool VariableSocket::toBoolean() {
			return true;
		};

		String VariableSocket::toString() {
			return strTypeSocket;
		};


	};
};


