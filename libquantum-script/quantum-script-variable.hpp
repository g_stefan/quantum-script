﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#define QUANTUM_SCRIPT_VARIABLE_HPP

#ifndef QUANTUM_SCRIPT_HPP
#include "quantum-script.hpp"
#endif

namespace Quantum {
	namespace Script {

		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class Variable;

		typedef TDynamicArray<TPointerX<Variable>, 4, TXMemoryPoolActive> Array;

		typedef dword Symbol;
		typedef TRedBlackTree<Symbol, TPointerX<Variable>, TXMemoryPoolActive> Property;

		typedef TStack1PointerUnsafe<Variable,TXMemoryPoolActive> Stack;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::Variable>:
			public TMemoryObjectPoolActive<Quantum::Script::Variable> {};

		template<>
		class TMemoryObject<Quantum::Script::Array>:
			public TMemoryObjectPoolActive<Quantum::Script::Array> {};

		template<>
		class TMemoryObject<Quantum::Script::Property>:
			public TMemoryObjectPoolActive<Quantum::Script::Property> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class Variable;

		typedef bool Boolean;
		typedef int64_t Integer;
		typedef uint64_t UInteger;
		typedef double Number;

		typedef Property::Node PropertyNode;

		class SymbolList;
		class Context;
		class Prototype;
		typedef Variable VariableUndefined;
		class Iterator;

		class VariableArray;

#ifdef XYO_OS_TYPE_WIN
#define QUANTUM_SCRIPT_FORMAT_INTEGER "%I64d"
#define QUANTUM_SCRIPT_FORMAT_NUMBER "%g"
#define QUANTUM_SCRIPT_FORMAT_NUMBER_INTEGER "%.0lf"
#define QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT "%lf"
#define QUANTUM_SCRIPT_FORMAT_DWORD XYO_FORMAT_DWORD
#endif

#ifdef XYO_OS_TYPE_UNIX
#ifdef XYO_MACHINE_64BIT
#define QUANTUM_SCRIPT_FORMAT_INTEGER "%ld"
#define QUANTUM_SCRIPT_FORMAT_NUMBER "%g"
#define QUANTUM_SCRIPT_FORMAT_NUMBER_INTEGER "%.0lf"
#define QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT "%lf"
#define QUANTUM_SCRIPT_FORMAT_DWORD XYO_FORMAT_DWORD
#else
#define QUANTUM_SCRIPT_FORMAT_INTEGER "%lld"
#define QUANTUM_SCRIPT_FORMAT_NUMBER "%g"
#define QUANTUM_SCRIPT_FORMAT_NUMBER_INTEGER "%.0lf"
#define QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT "%lf"
#define QUANTUM_SCRIPT_FORMAT_DWORD "%u"
#endif
#endif

		class Variable :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Variable);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeUndefined;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeUndefined;

				const void *variableType;

				//
				// Constructor/Destructor
				//

				inline Variable() {
					variableType = typeUndefined;
				};

				inline static Variable *newVariable() {
					return TMemory<Variable>::newObject();
				};

				inline static Variable *newVariableX() {
					return TMemory<Variable>::newObjectX();
				};

				//
				// Basic conversion
				//
				QUANTUM_SCRIPT_EXPORT virtual bool toBoolean();
				QUANTUM_SCRIPT_EXPORT virtual Number toNumber();
				QUANTUM_SCRIPT_EXPORT virtual String toString();
				QUANTUM_SCRIPT_EXPORT virtual bool isString();
				QUANTUM_SCRIPT_EXPORT size_t toIndex();

				//
				// Member access operators
				//
				QUANTUM_SCRIPT_EXPORT virtual TPointerX<Variable> &operatorIndex(dword index);
				QUANTUM_SCRIPT_EXPORT virtual TPointerX<Variable> &operatorReferenceOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT virtual Variable &operatorReference(Symbol symbolId);

				//
				// Function call
				//
				QUANTUM_SCRIPT_EXPORT virtual TPointerOwner<Variable> functionApply(Variable *this_,VariableArray *arguments);

				//
				// Object
				//
				QUANTUM_SCRIPT_EXPORT virtual Variable *instancePrototype();
				QUANTUM_SCRIPT_EXPORT bool instanceOf(Variable *value);
				QUANTUM_SCRIPT_EXPORT virtual bool instanceOfPrototype(Prototype *&out);
				QUANTUM_SCRIPT_EXPORT virtual bool findOwnProperty(Symbol symbolId,Variable *&out);

				QUANTUM_SCRIPT_EXPORT static Variable &operatorReferenceX(Symbol symbolId,Variable *prototype);
				QUANTUM_SCRIPT_EXPORT virtual bool operatorDeleteIndex(Variable *variable);
				QUANTUM_SCRIPT_EXPORT virtual bool operatorDeleteOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT virtual Variable &operatorIndex2(Variable *variable);
				QUANTUM_SCRIPT_EXPORT virtual TPointerX<Variable> &operatorReferenceIndex(Variable *variable);

				QUANTUM_SCRIPT_EXPORT virtual TPointerOwner<Iterator> getIteratorKey();
				QUANTUM_SCRIPT_EXPORT virtual TPointerOwner<Iterator> getIteratorValue();

				QUANTUM_SCRIPT_EXPORT virtual bool hasProperty(Variable *variable);

				//
				//
				//
				QUANTUM_SCRIPT_EXPORT virtual String getType();

				inline Variable *getObject() {
					incReferenceCount();
					return this;
				};

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				//
				// Thread copy
				//
				QUANTUM_SCRIPT_EXPORT virtual Variable *clone(SymbolList &inSymbolList);

				//
				//
				//
				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> newObjectFunctionApply(VariableArray *arguments);
				QUANTUM_SCRIPT_EXPORT bool isLessThan(Variable *b);
				QUANTUM_SCRIPT_EXPORT bool isLessThanOrEqual(Variable *b);
				QUANTUM_SCRIPT_EXPORT bool isGreaterThan(Variable *b);
				QUANTUM_SCRIPT_EXPORT bool isGreaterThanOrEqual(Variable *b);
				QUANTUM_SCRIPT_EXPORT bool isEqual(Variable *b);
				QUANTUM_SCRIPT_EXPORT bool isEqualStrict(Variable *b);

				QUANTUM_SCRIPT_EXPORT int compare(Variable *b);

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> operatorPlus(Variable *b);

		};


	};
};



#endif

