﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableoperator23.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableOperator23::typeOperator23= "{4BFBFC10-9165-45FE-BCB2-73461E55C03F}";

		Variable *VariableOperator23::newVariable() {
			return (Variable *) TMemory<VariableOperator23>::newObject();
		};

		bool VariableOperator23::toBoolean() {
			return true;
		};

		String VariableOperator23::toString() {
			return strTypeUndefined;
		};

	};
};


