//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-parser.hpp"
#include "quantum-script-parserasm.hpp"

namespace Quantum {
	namespace Script {


		bool Parser::statementDo() {
			if (token.isSymbolX("do")) {
				ProgramCounter *linkDo;
				ProgramCounter *linkDoEnd;
				ProgramCounter *linkDoBreak;

				linkDo = assemble(ParserAsm::Mark);

				if (token.checkIs1("{")) {

					assemble(ParserAsm::EnterContext);
					linkDoBreak = assembleProgramCounter(ParserAsm::ContextSetBreak, NULL);
					assembleProgramCounter(ParserAsm::ContextSetContinue, linkDo);

					if (isBlockStatement()) {

						assemble(ParserAsm::LeaveContext);

						if (token.isSymbolX("while")) {

							if (expressionParentheses()) {

								assembleProgramCounter(ParserAsm::IfTrueGoto, linkDo);
								linkProgramCounter(
									linkDoBreak,
									assemble(ParserAsm::Mark)
								);

								return true;
							};
						};

					};
				};

				error = ParserError::Compile;
				return false;
			};
			return false;
		};


	};
};


