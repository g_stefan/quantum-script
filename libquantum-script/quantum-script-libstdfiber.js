//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

FiberImplementation.prototype.onFinish=function(fn) {
	CurrentFiber.setInterval(function(fnX) {
		if(this.isTerminated()) {
			fnX(this.getReturnedValue());
			CurrentFiber.exit();
		};
	},1,this,[fn]);
};

FiberImplementation.prototype.join=function() {
	while(!this.isTerminated()) {
		CurrentFiber.sleep(1);
	};
};

FiberImplementation.prototype.sendMessage=function(message) {
	while(!this.sendMessageX(message)) {
		CurrentFiber.sleep(1);
	};
	while(!this.isMessageProcessedX()) {
		CurrentFiber.sleep(1);
	};
	return this.getMessageReturnValueX();
};

FiberImplementation.prototype.sendQuitMessage=function() {
	return this.sendMessage(this.quitMessage());
};

FiberImplementation.prototype.postQuitMessage=function() {
	return this.postMessage(this.quitMessage());
};

function newFiber(procedure,this_,parameters) {
	var fiber=new FiberImplementation(function() {
		try {
			CurrentFiber.exit(procedure.apply(this,arguments));
		} catch(e) {
			Console.writeLn(e.toString());
			Console.write(e.stackTrace);
			CurrentFiber.exit();
		};
	},this_,parameters);
	fiber.resume();
	return fiber;
};

function newFiberWorker(fn,this_) {
	return newFiber(function(fn) {
		while(CurrentFiber.getMessage()) {
			CurrentFiber.messageProcessed(fn.call(this,CurrentFiber.message));
		};
	},this_,[fn]);
};

CurrentFiber.setTimeout=function(procedure,milliseconds,this_,parameters) {
	return newFiber(function() {
		CurrentFiber.sleep(milliseconds);
		procedure.apply(this,arguments);
	},this_,parameters);
};

CurrentFiber.setInterval=function(procedure,milliseconds,this_,parameters) {
	return newFiber(function() {
		while(true) {
			CurrentFiber.sleep(milliseconds);
			procedure.apply(this,arguments);
		};
	},this_,parameters);
};

CurrentFiber.getMessage=function() {
	CurrentFiber.setIsBusyX(false);
	while(!CurrentFiber.hasMessage()) {
		CurrentFiber.sleep(1);
	};
	CurrentFiber.setIsBusyX(true);
	CurrentFiber.message=CurrentFiber.getMessageX();
	if(Script.isQuitMessage(CurrentFiber.message)) {
		CurrentFiber.messageProcessed(undefined);
		return false;
	};
	return true;
};

CurrentFiber.messageProcessed=function(returnValue) {
	CurrentFiber.messageProcessedX(returnValue);
	while(CurrentFiber.isMessageProcessedX()) {
		CurrentFiber.sleep(1);
	};
};

