﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdsocket.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablebuffer.hpp"
#include "quantum-script-variablesocket.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdSocket {

			using namespace XYO::XY;
			using namespace XYO::XO;

			SocketContext::SocketContext() {
				symbolFunctionSocket=0;
				prototypeSocket.memoryLink(this);
			};

			SocketContext *getContext() {
				return TXSingleton<SocketContext>::getValue();
			};

			static TPointerOwner<Variable> functionSocket(VariableFunction *function,Variable *this_,VariableArray *arguments) {
				return VariableSocket::newVariable();
			};

			static void deleteContext() {
				SocketContext *socketContext=getContext();
				socketContext->prototypeSocket.deleteObject();
				socketContext->symbolFunctionSocket=0;
			};

			static void newContext(Executive *executive,void *extensionId) {
				VariableFunction *defaultPrototypeFunction;

				SocketContext *socketContext=getContext();
				executive->setExtensionDeleteContext(extensionId,deleteContext);

				socketContext->symbolFunctionSocket=Context::getSymbol("Socket");
				socketContext->prototypeSocket.newObject();

				defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionSocket,NULL,NULL);
				((Context::getGlobalObject())->operatorReferenceOwnProperty(socketContext->symbolFunctionSocket)).setObjectX(defaultPrototypeFunction);
				socketContext->prototypeSocket=defaultPrototypeFunction->prototype;

			};

			static TPointerOwner<Variable> isSocket(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-socket\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableSocket::typeSocket);
			};

			static TPointerOwner<Variable> openServer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-open-server\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableSocket *)( this_ ))->value.openServerX((arguments->index(0))->toString()));
			};


			static TPointerOwner<Variable> openClient(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-open-client\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableSocket *)( this_ ))->value.openClientX((arguments->index(0))->toString()));
			};


			static TPointerOwner<Variable> socketRead(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-read\n");
#endif
				String  retV(1024,1024);  // first 1024, next + 1024 bytes
				Number ln;
				size_t readLn;
				size_t readToLn;
				size_t readX;
				size_t readTotal;
				size_t k;
				char buffer[1024];

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				readToLn=(size_t)(ln);
				readTotal=0;
				readX=1024;
				if(readToLn<readX) {
					readX=readToLn;
				};
				for(;;) {
					readLn=((VariableSocket *) this_)->value.read(buffer, readX);

					if(readLn>0) {
						retV.concatenate(buffer,readLn);
					};
					//end of file
					if(readLn<readX) {
						break;
					};
					//end of read
					readTotal+=readLn;
					if(readTotal>=readToLn) {
						break;
					};
					readX=readToLn-readTotal;
					if(readX>1024) {
						readX=1024;
					};
				};

				return VariableString::newVariable(retV);
			};


			static TPointerOwner<Variable> socketReadLn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-read-ln\n");
#endif
				String  retV(1024,1024);  // first 1024, next + 1024 bytes
				Number ln;
				size_t readLn;
				size_t readToLn;
				size_t readTotal;
				size_t k;
				char buffer[2];

				buffer[1]=0;
				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				readToLn=(size_t)(ln);
				readTotal=0;
				if(readToLn<1) {
					return VariableString::newVariable("");
				};
				for(;;) {
					readLn=((VariableSocket *) this_)->value.read(buffer, 1);
					if(readLn>0) {

						if(buffer[0]=='\r') {
							if(readTotal+1>=readToLn) {
								retV.concatenate("\r",1);
								return VariableString::newVariable(retV);
							};

							readLn=((VariableSocket *) this_)->value.read(buffer, 1);
							if(readLn>0) {
								if(buffer[0]=='\n') {
									return VariableString::newVariable(retV);
									break;
								};
								retV.concatenate(buffer,1);
								readTotal+=2;
								if(readTotal>=readToLn) {
									return VariableString::newVariable(retV);
								};
								continue;
							};

							retV.concatenate("\r",1);
							//end of file
							return VariableString::newVariable(retV);
						};

						if(buffer[0]=='\n') {
							return VariableString::newVariable(retV);
						};

						retV.concatenate(buffer,1);
						readTotal++;
						if(readTotal>=readToLn) {
							return VariableString::newVariable(retV);
						};
						continue;
					};
					// connection interrupted - 0 to read ...
					if(readTotal==0) {
						break;
					};
					//end of file
					return VariableString::newVariable(retV);
				};

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> socketWrite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-write\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				String toWrite=(arguments->index(0))->toString();
				return VariableNumber::newVariable((Number)(((VariableSocket *) this_)->value.write(
						toWrite.value(),toWrite.length()
								   )));
			};


			static TPointerOwner<Variable> socketWriteLn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-write-ln\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				String toWrite=(arguments->index(0))->toString();
				toWrite<<"\r\n";
				return VariableNumber::newVariable((Number)(((VariableSocket *) this_)->value.write(
						toWrite.value(),toWrite.length()
								   )));
			};

			static TPointerOwner<Variable> socketListen(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-listen\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				Number count_=(arguments->index(0))->toNumber();
				if(isnan(count_)||isinf(count_)||signbit(count_)) {
					return VariableBoolean::newVariable(false);
				};

				return VariableBoolean::newVariable(((VariableSocket *) this_)->value.listen((word)count_));
			};

			static TPointerOwner<Variable> socketClose(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-close\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				((VariableSocket *) this_)->value.close();

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> socketAccept(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-accept\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				VariableSocket *socket_=(VariableSocket *)VariableSocket::newVariable();

				if(((VariableSocket *) this_)->value.accept(socket_->value)) {
					return socket_;
				};
				socket_->decReferenceCount();
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> waitToWrite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-wait-to-write\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				Number seconds=(arguments->index(0))->toNumber();
				if(isnan(seconds)||isinf(seconds)||signbit(seconds)) {
					return VariableNumber::newVariable(NAN);
				};
				Number micro=(arguments->index(1))->toNumber();
				if(isnan(micro)||isinf(micro)||signbit(micro)) {
					return VariableNumber::newVariable(NAN);
				};
				return VariableNumber::newVariable(((VariableSocket *) this_)->value.waitToWrite((word)seconds,(word)micro));
			};


			static TPointerOwner<Variable> waitToRead(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-wait-to-read\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				Number seconds=(arguments->index(0))->toNumber();
				if(isnan(seconds)||isinf(seconds)||signbit(seconds)) {
					return VariableNumber::newVariable(NAN);
				};
				Number micro=(arguments->index(1))->toNumber();
				if(isnan(micro)||isinf(micro)||signbit(micro)) {
					return VariableNumber::newVariable(NAN);
				};
				return VariableNumber::newVariable(((VariableSocket *) this_)->value.waitToRead((word)seconds,(word)micro));
			};

			static TPointerOwner<Variable> releaseOwner(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-release-owner\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				((VariableSocket *) this_)->value.releaseOwner();

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> aquireOwner(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-aquire-owner\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				((VariableSocket *) this_)->value.aquireOwner();

				return Context::getValueUndefined();
			};


			static TPointerOwner<Variable> socketReadToBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-read-to-buffer\n");
#endif
				size_t readLn;
				size_t readToLn;
				size_t readX;
				size_t readTotal;
				size_t k;
				Number ln;

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				TPointerX<Variable> &buffer(arguments->index(0));

				if(buffer->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				ln=(arguments->index(1))->toNumber();
				if(isnan(ln)||signbit(ln)||ln==0.0) {
					((VariableBuffer *)buffer.value())->length=0;
					return VariableNumber::newVariable(0);
				};
				if(isinf(ln)) {
					ln=((VariableBuffer *)buffer.value())->size;
				};

				if(ln>((VariableBuffer *)buffer.value())->size) {
					ln=((VariableBuffer *)buffer.value())->size;
				};

				readToLn=(size_t)ln;
				readTotal=0;
				readX=readToLn;
				for(;;) {
					readLn=((VariableSocket *) this_)->value.read(&(((VariableBuffer *)buffer.value())->buffer)[readTotal], readX);
					//end of transmision
					if(readLn==0) {
						break;
					};
					readTotal+=readLn;
					if(readTotal>=readToLn) {
						break;
					};
					readX=readToLn-readTotal;
				};
				((VariableBuffer *)buffer.value())->length=readTotal;
				return VariableNumber::newVariable(readTotal);
			};


			static TPointerOwner<Variable> socketWriteFromBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- socket-write-from-buffer\n");
#endif

				if(this_->variableType!=VariableSocket::typeSocket) {
					throw(Error("invalid parameter"));
				};

				TPointerX<Variable> &buffer(arguments->index(0));

				if(buffer->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable((Number)(((VariableSocket *) this_)->value.write(
						((VariableBuffer *)buffer.value())->buffer,((VariableBuffer *)buffer.value())->length
								   )));
			};

			void initExecutive(Executive *executive,void *extensionId) {
				newContext(executive,extensionId);
				executive->setFunction2("Script.isSocket(x)", isSocket);
				executive->setFunction2("Socket.prototype.openServer(addr)",  openServer);
				executive->setFunction2("Socket.prototype.openClient(addr)",  openClient);
				executive->setFunction2("Socket.prototype.read(size)",  socketRead);
				executive->setFunction2("Socket.prototype.readLn(size)",  socketReadLn);
				executive->setFunction2("Socket.prototype.write(str)",  socketWrite);
				executive->setFunction2("Socket.prototype.writeLn(str)",  socketWriteLn);
				executive->setFunction2("Socket.prototype.listen(count)",  socketListen);
				executive->setFunction2("Socket.prototype.close()",  socketClose);
				executive->setFunction2("Socket.prototype.accept()",  socketAccept);
				executive->setFunction2("Socket.prototype.waitToWrite(sec,micro)",  waitToWrite);
				executive->setFunction2("Socket.prototype.waitToRead(sec,micro)",  waitToRead);
				executive->setFunction2("Socket.prototype.releaseOwner()",  releaseOwner);
				executive->setFunction2("Socket.prototype.aquireOwner()",  aquireOwner);
				executive->setFunction2("Socket.prototype.readToBuffer(buffer)",  socketReadToBuffer);
				executive->setFunction2("Socket.prototype.writeFromBuffer(buffer)",  socketWriteFromBuffer);
			};

		};
	};
};




