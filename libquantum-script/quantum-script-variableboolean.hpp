﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEBOOLEAN_HPP
#define QUANTUM_SCRIPT_VARIABLEBOOLEAN_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableBoolean;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableBoolean>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableBoolean> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableBoolean :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableBoolean);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTrue;
				QUANTUM_SCRIPT_EXPORT static const char *strFalse;
				QUANTUM_SCRIPT_EXPORT static const char *strTypeBoolean;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeBoolean;

				Boolean value;

				inline VariableBoolean() {
					variableType = typeBoolean;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(bool value);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT Number toNumber();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
