﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdshell.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variableutf16.hpp"
#include "quantum-script-variableutf32.hpp"

#include "quantum-script-libstdshell.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdShell {

			using namespace XYO::XY;
			using namespace XYO::XO;


			static TPointerOwner<Variable> fileGetContents(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-get-contents\n");
#endif
				String output;
				if(ShellX::fileGetContents((arguments->index(0))->toString(),output)) {
					return VariableString::newVariable(output);
				};
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> fileGetContentsUtf16(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-get-contents-utf16\n");
#endif
				StringWord output;
				if(ShellX::fileGetContents((arguments->index(0))->toString(),output)) {
					return VariableUtf16::newVariable(output);
				};
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> fileGetContentsUtf32(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-get-contents-utf32\n");
#endif
				StringDWord output;
				if(ShellX::fileGetContents((arguments->index(0))->toString(),output)) {
					return VariableUtf32::newVariable(output);
				};
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> filePutContents(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-put-contents\n");
#endif
				return VariableBoolean::newVariable(ShellX::filePutContents((arguments->index(0))->toString(),(arguments->index(1))->toString()));
			};

			static TPointerOwner<Variable> filePutContentsUtf16(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-put-contents-utf16\n");
#endif
				StringWord fileText=VariableUtf16::getUtf16(arguments->index(1));
				return VariableBoolean::newVariable(ShellX::filePutContents((arguments->index(0))->toString(),fileText));

			};

			static TPointerOwner<Variable> filePutContentsUtf32(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-put-contents-utf32\n");
#endif
				StringDWord fileText=VariableUtf32::getUtf32(arguments->index(1));
				return VariableBoolean::newVariable(ShellX::filePutContents((arguments->index(0))->toString(),fileText));
			};

			static TPointerOwner<Variable> system(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-system\n");
#endif
				return VariableNumber::newVariable((Number)XO::Shell::system( (arguments->index(0))->toString() ));
			};

			static TPointerOwner<Variable> getenv(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-getenv\n");
#endif
				return VariableString::newVariable(ShellX::getEnv((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> putenv(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-putenv\n");
#endif
				return VariableBoolean::newVariable(XO::Shell::putenv( (arguments->index(0))->toString() ));
			};


			static TPointerOwner<Variable> fileExists(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-file-exists\n");
#endif

				return VariableBoolean::newVariable(XO::Shell::fileExists( (arguments->index(0))->toString() ));
			};


			static TPointerOwner<Variable> directoryExists(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-directory-exists\n");
#endif

				return VariableBoolean::newVariable(XO::Shell::directoryExists( (arguments->index(0))->toString() ));
			};

			static TPointerOwner<Variable> chdir(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-chdir\n");
#endif
				return VariableBoolean::newVariable(XO::Shell::chdir( (arguments->index(0))->toString() ));
			};

			static TPointerOwner<Variable> rmdir(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-rmdir\n");
#endif
				return VariableBoolean::newVariable(XO::Shell::rmdir( (arguments->index(0))->toString() ));
			};

			static TPointerOwner<Variable> mkdir(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-mkdir\n");
#endif
				return VariableBoolean::newVariable(XO::Shell::mkdir( (arguments->index(0))->toString() ));
			};


			static TPointerOwner<Variable> getcwd(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-getcwd\n");
#endif
				return VariableString::newVariable(ShellX::getCwd());
			};


			static TPointerOwner<Variable> copy(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-copy\n");
#endif

				return VariableBoolean::newVariable(XO::Shell::copyFile(
						(arguments->index(0))->toString(),
						(arguments->index(1))->toString()
								    ));
			};


			static TPointerOwner<Variable> rename(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-rename\n");
#endif

				return VariableBoolean::newVariable(XO::Shell::rename(
						(arguments->index(0))->toString(),
						(arguments->index(1))->toString()
								    ));
			};

			static TPointerOwner<Variable> remove(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-remove\n");
#endif

				return VariableBoolean::newVariable(XO::Shell::remove( (arguments->index(0))->toString() ));
			};

			static TPointerOwner<Variable> compareLastWriteTime(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-compare-last-write-time\n");
#endif

				return VariableNumber::newVariable(
					       XO::Shell::compareLastWriteTime(
						       (arguments->index(0))->toString(),
						       (arguments->index(1))->toString()
					       )
				       );
			};

			static TPointerOwner<Variable> touch(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-touch\n");
#endif

				return VariableBoolean::newVariable(
					       XO::Shell::touch(
						       (arguments->index(0))->toString()
					       )
				       );
			};

			static TPointerOwner<Variable> is(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-is\n");
#endif
				String name=(arguments->index(0))->toString();

#ifdef XYO_OS_TYPE_WIN
				if (name == "win") {
					return VariableBoolean::newVariable(true);
				};
#endif

#ifdef XYO_OS_TYPE_UNIX
				if (name == "unix") {
					return VariableBoolean::newVariable(true);
				};
#endif

				return VariableBoolean::newVariable(false);
			};

			static TPointerOwner<Variable> getFileName(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-get-file-name\n");
#endif
				return VariableString::newVariable(ShellX::getFileName((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> getFileExtension(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-get-file-extension\n");
#endif
				return VariableString::newVariable(ShellX::getFileExtension((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> getFileBasename(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-get-file-basename\n");
#endif
				return VariableString::newVariable(ShellX::getFileBasename((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> getFilePath(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-get-file-path\n");
#endif
				return VariableString::newVariable(ShellX::getFilePath((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> getFilePathX(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-get-file-pathx\n");
#endif
				return VariableString::newVariable(ShellX::getFilePathX((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> execute(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-execute\n");
#endif
				return VariableNumber::newVariable(Shell::execute((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> executeHidden(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-execute-hidden\n");
#endif
				return VariableNumber::newVariable(Shell::executeHidden((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> executeNoWait(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-execute-no-wait\n");
#endif
				TPointerX<Variable> &cmd_=arguments->index(1);
				if(cmd_->variableType == VariableUndefined::typeUndefined) {
					return VariableNumber::newVariable(Shell::executeNoWait((arguments->index(0))->toString()));
				};
				return VariableNumber::newVariable(Shell::executeNoWait((arguments->index(0))->toString(),cmd_->toString()));
			};

			static TPointerOwner<Variable> executeHiddenNoWait(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-execute-hidden-no-wait\n");
#endif
				return VariableNumber::newVariable(Shell::executeHiddenNoWait((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> isProcessTerminated(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-is-process-terminated\n");
#endif
				return VariableBoolean::newVariable(Shell::isProcessTerminated((Shell::ProcessId)((arguments->index(0))->toNumber())));
			};

			static TPointerOwner<Variable> terminateProcess(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-terminate-process\n");
#endif
				return VariableBoolean::newVariable(Shell::terminateProcess((Shell::ProcessId)((arguments->index(0)->toNumber())),(arguments->index(1))->toNumber()));
			};

			static TPointerOwner<Variable> removeFile(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-remove-file\n");
#endif
				return VariableBoolean::newVariable(ShellX::removeFile((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> removeEmptyDir(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-remove-empty-dir\n");
#endif
				return VariableBoolean::newVariable(ShellX::removeEmptyDir((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> removeFileAndDirectoryIfEmpty(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-remove-file-and-directory-if-empty\n");
#endif
				return VariableBoolean::newVariable(ShellX::removeFileAndDirectoryIfEmpty((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> touchIfExists(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-touch-if-exists\n");
#endif
				return VariableBoolean::newVariable(ShellX::touchIfExists((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> isEnv(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- shell-is-env\n");
#endif
				return VariableBoolean::newVariable(ShellX::isEnv((arguments->index(0))->toString(),(arguments->index(1))->toString()));
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var Shell={};");
				executive->setFunction2("Shell.fileGetContents(file)", fileGetContents);
				executive->setFunction2("Shell.fileGetContentsUtf8(file)", fileGetContents);
				executive->setFunction2("Shell.fileGetContentsUtf16(file)", fileGetContentsUtf16);
				executive->setFunction2("Shell.fileGetContentsUtf32(file)", fileGetContentsUtf32);
				executive->setFunction2("Shell.filePutContents(file,text)", filePutContents);
				executive->setFunction2("Shell.filePutContentsUtf8(file,text)", filePutContents);
				executive->setFunction2("Shell.filePutContentsUtf16(file,text)", filePutContentsUtf16);
				executive->setFunction2("Shell.filePutContentsUtf32(file,text)", filePutContentsUtf32);
				executive->setFunction2("Shell.system(cmd)", system);
				executive->setFunction2("Shell.getenv(name)", getenv);
				executive->setFunction2("Shell.putenv(name)", putenv);
				executive->setFunction2("Shell.fileExists(name)", fileExists);
				executive->setFunction2("Shell.directoryExists(name)", directoryExists);
				executive->setFunction2("Shell.chdir(path)", chdir);
				executive->setFunction2("Shell.rmdir(path)", rmdir);
				executive->setFunction2("Shell.mkdir(path)", mkdir);
				executive->setFunction2("Shell.getcwd()", getcwd);
				executive->setFunction2("Shell.copy(src,dst)", copy);
				executive->setFunction2("Shell.rename(src,dst)", rename);
				executive->setFunction2("Shell.remove(file)", remove);
				executive->setFunction2("Shell.compareLastWriteTime(fileA,fileB)", compareLastWriteTime);
				executive->setFunction2("Shell.touch(file)", touch);
				executive->setFunction2("Shell.is(what)", is);
				executive->setFunction2("Shell.getFileName(fileName)", getFileName);
				executive->setFunction2("Shell.getFileExtension(fileName)", getFileExtension);
				executive->setFunction2("Shell.getFileBasename(fileName)", getFileBasename);
				executive->setFunction2("Shell.getFilePath(fileName)", getFilePath);
				executive->setFunction2("Shell.getFilePathX(fileName)", getFilePathX);
				executive->setFunction2("Shell.execute(cmd)", execute);
				executive->setFunction2("Shell.executeHidden(cmd)", executeHidden);
				executive->setFunction2("Shell.executeNoWait(cmd,parameters)", executeNoWait);
				executive->setFunction2("Shell.executeHiddenNoWait(cmd)", executeHiddenNoWait);
				executive->setFunction2("Shell.isProcessTerminated(id)", isProcessTerminated);
				executive->setFunction2("Shell.terminateProcess(id,timeout)", terminateProcess);
				executive->setFunction2("Shell.removeFile(file)", removeFile);
				executive->setFunction2("Shell.removeEmptyDir(dir)", removeEmptyDir);
				executive->setFunction2("Shell.removeFileAndDirectoryIfEmpty(file)", removeFileAndDirectoryIfEmpty);
				executive->setFunction2("Shell.touchIfExists(file)", touchIfExists);
				executive->setFunction2("Shell.isEnv(name,value)", isEnv);
				executive->compileStringX((char *)libStdShellSource);
			};

		};
	};
};




