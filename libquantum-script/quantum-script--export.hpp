//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT__EXPORT_HPP
#define QUANTUM_SCRIPT__EXPORT_HPP

#ifndef XYO_XY__EXPORT_HPP
#include "xyo-xy--export.hpp"
#endif

#ifdef XYO_DYNAMIC_LINK
#   ifdef  QUANTUM_SCRIPT_INTERNAL
#       define QUANTUM_SCRIPT_EXPORT XYO_EXPORT
#   else
#       define QUANTUM_SCRIPT_EXPORT XYO_IMPORT
#   endif
#else
#   define QUANTUM_SCRIPT_EXPORT
#endif

#endif
