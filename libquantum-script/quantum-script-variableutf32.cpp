﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-variablestring.hpp"
#include "quantum-script-variableutf32.hpp"
#include "quantum-script-variableutf16.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablenull.hpp"
#include "quantum-script-variablesymbol.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableUtf32::typeUtf32="{75FBE2BD-2D2E-445E-957C-504D34A48B04}";
		const char *VariableUtf32::strTypeUtf32="Utf32";

		String VariableUtf32::getType() {
			return strTypeUtf32;
		};

		Variable *VariableUtf32::newVariable(StringDWord value) {
			VariableUtf32 *retV;
			retV = TMemory<VariableUtf32>::newObject();
			retV->value=value;
			return (Variable *) retV;
		};

		Variable &VariableUtf32::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolLength()) {
				if(vLength) {
					((VariableNumber *)vLength.value())->value=(Number)value.length();
				} else {
					vLength.setObject(VariableNumber::newVariable((Number)value.length()));
				};
				return *vLength;
			};
			return operatorReferenceX(symbolId,(Context::getPrototypeString())->prototype);
		};

		Variable *VariableUtf32::instancePrototype() {
			return (Context::getPrototypeUtf32())->prototype;
		};

		void VariableUtf32::memoryInit() {
			TMemory<StringDWord>::memoryInit();
		};

		Variable *VariableUtf32::clone(SymbolList &inSymbolList) {
			return newVariable(value.value());
		};


		StringDWord VariableUtf32::getUtf32(Variable *this_) {
			if(this_->variableType==VariableString::typeString) {
				return UtfX::utf32FromUtf8(((VariableString *)this_)->value);
			};
			if(this_->variableType==VariableUtf16::typeUtf16) {
				return UtfX::utf32FromUtf16(((VariableUtf16 *)this_)->value);
			};
			if(this_->variableType==VariableUtf32::typeUtf32) {
				return ((VariableUtf32 *)this_)->value;
			};
			return UtfX::utf32FromUtf8(this_->toString());
		};

		bool VariableUtf32::hasProperty(Variable *variable) {
			if(variable->variableType==VariableSymbol::typeSymbol) {
				if((static_cast<VariableSymbol *>(variable))->value==Context::getSymbolLength()) {
					return true;
				};
			};
			return (Context::getPrototypeUtf32())->prototype->hasProperty(variable);
		};

		bool VariableUtf32::toBoolean() {
			return (value.length()>0);
		};

		Number VariableUtf32::toNumber() {
			Number retV;
			if(sscanf(toString(), QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &retV) == 1) {
				return retV;
			};
			return NAN;
		};

		String VariableUtf32::toString() {
			return UtfX::utf8FromUtf32(value);
		};

		bool VariableUtf32::isString() {
			return true;
		};


	};
};


