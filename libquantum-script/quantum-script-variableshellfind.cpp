﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-context.hpp"
#include "quantum-script-variableshellfind.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-shellfinditeratorvalue.hpp"
#include "quantum-script-libstdshellfind.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableShellFind::typeShellFind="{6F5DF4E6-0BBB-469D-B990-4AC7D36B8887}";
		const char *VariableShellFind::strTypeShellFind="ShellFind";

		String VariableShellFind::getType() {
			return strTypeShellFind;
		};

		Variable *VariableShellFind::newVariable() {
			return (Variable *) TMemory<VariableShellFind>::newObject();
		};

		Variable &VariableShellFind::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(LibStdShellFind::getContext())->prototypeShellFind->prototype);
		};

		Variable *VariableShellFind::instancePrototype() {
			return (LibStdShellFind::getContext())->prototypeShellFind->prototype;
		};

		void VariableShellFind::activeDestructor() {
			value.close();
		};

		TPointerOwner<Iterator> VariableShellFind::getIteratorValue() {
			ShellFindIteratorValue *iterator_=(ShellFindIteratorValue *)TMemory<ShellFindIteratorValue>::newObject();
			iterator_->value_=this;
			iterator_->sourceShellFind=&this->value;
			return iterator_;
		};

		bool VariableShellFind::toBoolean() {
			return true;
		};

		String VariableShellFind::toString() {
			return strTypeShellFind;
		};


	};
};


