﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEOPERATOR22_HPP
#define QUANTUM_SCRIPT_VARIABLEOPERATOR22_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableOperator22;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableOperator22>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableOperator22> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableOperator22 :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableOperator22);
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeOperator22;

				Symbol symbol1;
				Symbol symbol2;

				inline VariableOperator22() {
					variableType = typeOperator22;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif

