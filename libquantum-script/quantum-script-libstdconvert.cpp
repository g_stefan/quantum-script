﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#else
#include <dlfcn.h>
#endif

#include "quantum-script-libstdconvert.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variableutf16.hpp"
#include "quantum-script-variableutf32.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdConvert {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> toBoolean(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- convert-to-boolean\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->toBoolean());
			};

			static TPointerOwner<Variable> toNumber(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- convert-to-number\n");
#endif
				return VariableNumber::newVariable((arguments->index(0))->toNumber());
			};

			static TPointerOwner<Variable> toString(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- convert-to-string\n");
#endif
				return VariableString::newVariable((arguments->index(0))->toString());
			};

			static TPointerOwner<Variable> toStringSource(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- convert-to-string-source\n");
#endif
				return VariableString::newVariable(StringX::encodeC((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> toUtf16(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- convert-to-utf16\n");
#endif
				return VariableUtf16::newVariable(VariableUtf16::getUtf16(arguments->index(0)));
			};

			static TPointerOwner<Variable> toUtf32(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- convert-to-utf32\n");
#endif
				return VariableUtf32::newVariable(VariableUtf32::getUtf32(arguments->index(0)));
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var Convert={};");

				executive->setFunction2("Convert.toBoolean(x)", toBoolean);
				executive->setFunction2("Convert.toNumber(x)", toNumber);
				executive->setFunction2("Convert.toString(x)", toString);
				executive->setFunction2("Convert.toStringSource(x)", toStringSource);
				executive->setFunction2("Convert.toUtf8(x)", toString);
				executive->setFunction2("Convert.toUtf16(x)", toUtf16);
				executive->setFunction2("Convert.toUtf32(x)", toUtf32);
			};

		};
	};
};


