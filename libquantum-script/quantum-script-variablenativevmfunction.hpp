﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLENATIVEFUNCTION_HPP
#define QUANTUM_SCRIPT_VARIABLENATIVEFUNCTION_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_INSTRUCTIONX_HPP
#include "quantum-script-instructionx.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableNativeVmFunction;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableNativeVmFunction>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableNativeVmFunction> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableNativeVmFunction :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableNativeVmFunction);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeFunction;
				QUANTUM_SCRIPT_EXPORT static const char *strTypeNativeFunction;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeNativeVmFunction;

				InstructionProcedure procedure;
				TPointer<Variable> operand;

				inline VariableNativeVmFunction() {
					variableType = typeNativeVmFunction;
				};

				inline void activeDestructor() {
					operand.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(InstructionProcedure procedure, TPointer<Variable> &operand);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
