//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT__CONFIG_HPP
#define QUANTUM_SCRIPT__CONFIG_HPP

//#define QUANTUM_SCRIPT_DEBUG_ASM
//#define QUANTUM_SCRIPT_DEBUG_RUNTIME
//#define QUANTUM_SCRIPT_DISABLE_ASM_OPTIMIZER
//#define QUANTUM_SCRIPT_SINGLE_FIBER
//#define QUANTUM_SCRIPT_SINGLE_THREAD
//#define QUANTUM_SCRIPT_DISABLE_CLOSURE

#endif

