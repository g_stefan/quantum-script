﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdstring.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdString {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> indexOf(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-index-of\n");
#endif
				size_t index_;
				if (StringX::indexOf(
					    this_->toString(),
					    (arguments->index(0))->toString(),
					    (arguments->index(1))->toIndex(),
					    index_)) {
					return VariableNumber::newVariable((Number) index_);
				};
				return VariableNumber::newVariable((Number)-1);
			};

			static TPointerOwner<Variable> lastIndexOf(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-last-index-of\n");
#endif

				size_t index_;
				if (StringX::indexOfFromEnd(
					    this_->toString(),
					    (arguments->index(0))->toString(),
					    (arguments->index(1))->toIndex(),
					    index_)) {
					return VariableNumber::newVariable((Number) index_);
				};
				return VariableNumber::newVariable((Number)-1);
			};

			static TPointerOwner<Variable> substring(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-substring\n");
#endif
				String value=this_->toString();
				Number ln=(arguments->index(1))->toNumber();
				return VariableString::newVariable(
					       StringX::substring(
						       value,
						       (arguments->index(0))->toIndex(),
						       (isnan(ln))?value.length():((isinf(ln))?value.length():((Integer)ln))
					       ));
			};

			static TPointerOwner<Variable> toLowerCaseAscii(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-to-lower-case-ascii\n");
#endif

				return VariableString::newVariable(StringX::toLowerCaseAscii(this_->toString()));
			};

			static TPointerOwner<Variable> toUpperCaseAscii(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-to-upper-case-ascii\n");
#endif

				return VariableString::newVariable(StringX::toUpperCaseAscii(this_->toString()));
			};

			static TPointerOwner<Variable> replace(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-replace\n");
#endif

				return VariableString::newVariable(
					       StringX::replace(
						       this_->toString(),
						       (arguments->index(0))->toString(),
						       (arguments->index(1))->toString()
					       )
				       );
			};

			static TPointerOwner<Variable> matchAscii(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-match-ascii\n");
#endif

				return VariableBoolean::newVariable(
					       StringX::matchAscii((arguments->index(0))->toString(),(arguments->index(1))->toString())
				       );
			};

			static TPointerOwner<Variable> getByte(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-get-byte\n");
#endif

				Number x=(arguments->index(0))->toNumber();
				String this__=this_->toString();
				if(isnan(x)||isinf(x)||signbit(x)) {
					return VariableNumber::newVariable(0);
				};
				if(x>this__.length()) {
					return VariableNumber::newVariable(0);
				};

				return VariableNumber::newVariable(this__[(int)x]);
			};


			static TPointerOwner<Variable> trim(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-trim\n");
#endif

				return VariableString::newVariable(StringX::trimWithElement(this_->toString()," \t"));
			};

			static TPointerOwner<Variable> splitBy(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-split-by\n");
#endif
				size_t index;
				size_t pos;
				int ln;
				String src=this_->toString();
				String sig=(arguments->index(0))->toString();
				if(StringX::indexOf(src,sig,0,index)) {
					TPointerOwner<Variable> retV(VariableArray::newVariable());
					ln=1;
					(retV->operatorIndex(0)).setObject(VariableString::newVariable(StringX::substring(src,0,index)));
					pos=index+1;
					while(StringX::indexOf(src,sig,pos,index)) {
						(retV->operatorIndex(ln)).setObject(VariableString::newVariable(StringX::substring(src,pos,index-pos)));
						pos=index+1;
						++ln;
					};
					(retV->operatorIndex(ln)).setObject(VariableString::newVariable(StringX::substring(src,pos)));
					return retV;
				};
				return VariableNull::newVariable();
			};


			static TPointerOwner<Variable> encodeC(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-encodeC\n");
#endif

				return VariableString::newVariable(StringX::encodeC(this_->toString()));
			};

			static TPointerOwner<Variable> toHex(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- string-toHex\n");
#endif

				return VariableString::newVariable(StringX::toHex(this_->toString()));
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var Utf8=String;");
				executive->setFunction2("String.prototype.indexOf(text,st)", indexOf);
				executive->setFunction2("String.prototype.lastIndexOf(text,st)", lastIndexOf);
				executive->setFunction2("String.prototype.substring(st,ln)", substring);
				executive->setFunction2("String.prototype.toLowerCaseAscii()", toLowerCaseAscii);
				executive->setFunction2("String.prototype.toUpperCaseAscii()", toUpperCaseAscii);
				executive->setFunction2("String.prototype.replace(what_,with_)", replace);
				executive->setFunction2("String.prototype.matchAscii(text,signature)", matchAscii);
				executive->setFunction2("String.prototype.getByte(pos)", getByte);
				executive->setFunction2("String.prototype.trim()", trim);
				executive->setFunction2("String.prototype.splitBy(str)", splitBy);
				executive->setFunction2("String.prototype.encodeC()", encodeC);
				executive->setFunction2("String.prototype.toHex()", toHex);
			};

		};
	};
};


