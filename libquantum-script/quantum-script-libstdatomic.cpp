﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdatomic.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-variableatomic.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdAtomic {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> getValue(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- atomic-get\n");
#endif

				if(this_->variableType!=VariableAtomic::typeAtomic) {
					throw(Error("invalid parameter"));
				};

				if(((VariableAtomic *)( this_ ))->value->atomicType==Atomic::typeBoolean) {
					return VariableBoolean::newVariable(((VariableAtomic *)( this_ ))->value->valueBooloean);
				};

				if(((VariableAtomic *)( this_ ))->value->atomicType==Atomic::typeNumber) {
					return VariableNumber::newVariable(((VariableAtomic *)( this_ ))->value->valueNumber);
				};

				if(((VariableAtomic *)( this_ ))->value->atomicType==Atomic::typeString) {
					return VariableString::newVariable(((VariableAtomic *)( this_ ))->value->valueString);
				};

				return VariableNull::newVariable();
			};


			static TPointerOwner<Variable> setValue(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- atomic-set\n");
#endif

				if(this_->variableType!=VariableAtomic::typeAtomic) {
					throw(Error("invalid parameter"));
				};

				TPointerX<Variable> &value=arguments->index(0);

				if(value->variableType==VariableBoolean::typeBoolean) {
					((VariableAtomic *)( this_ ))->value->setBoolean(value->toBoolean());
					return Context::getValueUndefined();
				};

				if(value->variableType==VariableNumber::typeNumber) {
					((VariableAtomic *)( this_ ))->value->setNumber(value->toNumber());
					return Context::getValueUndefined();
				};

				if(value->variableType==VariableString::typeString) {
					((VariableAtomic *)( this_ ))->value->setString(value->toString());
					return Context::getValueUndefined();
				};

				if(value->variableType==VariableNull::typeNull) {
					((VariableAtomic *)( this_ ))->value->clear();
					return Context::getValueUndefined();
				};

				if(value->variableType==VariableUndefined::typeUndefined) {
					((VariableAtomic *)( this_ ))->value->clear();
					return Context::getValueUndefined();
				};


				((VariableAtomic *)( this_ ))->value->setString(value->toString());

				return Context::getValueUndefined();
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction2("Atomic.prototype.get()",  getValue);
				executive->setFunction2("Atomic.prototype.set(value)",  setValue);
			};

		};
	};
};




