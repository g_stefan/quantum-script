﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLESTACKTRACE_HPP
#define QUANTUM_SCRIPT_VARIABLESTACKTRACE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_INSTRUCTIONCONTEXT_HPP
#include "quantum-script-instructioncontext.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableStackTrace;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableStackTrace>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableStackTrace> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class InstructionContext;

		class VariableStackTrace :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableStackTrace);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeStackTrace;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeStackTrace;

				TPointer<TStack2<InstructionTrace> > stackTrace;
				InstructionContext *context;
				int configPrintStackTraceLimit;

				inline VariableStackTrace() {
					variableType = typeStackTrace;
					configPrintStackTraceLimit=QUANTUM_SCRIPT_DEFAULT_STACK_TRACE_LEVEL;
				};

				inline void activeDestructor() {
					stackTrace.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(TPointer<TStack2<InstructionTrace> > stackTrace, InstructionContext *context);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT String toString();
				QUANTUM_SCRIPT_EXPORT String toString(int level_);


				QUANTUM_SCRIPT_EXPORT bool toBoolean();
		};

	};
};


#endif

