﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-shellfinditeratorvalue.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		bool ShellFindIteratorValue::next(TPointerX<Variable> &out) {
			if(!sourceShellFind->isValid()) {
				out=Context::getValueUndefined();
				return false;
			};
			out=TPointerOwner<Variable>(VariableString::newVariable(sourceShellFind->name));
			sourceShellFind->findNext();
			return true;
		};

	};
};



