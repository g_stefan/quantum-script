//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_LIBSTDSHELLFIND_HPP
#define QUANTUM_SCRIPT_LIBSTDSHELLFIND_HPP

#ifndef QUANTUM_SCRIPT_EXECUTIVE_HPP
#include "quantum-script-executive.hpp"
#endif

namespace Quantum {
	namespace Script {

		namespace LibStdShellFind {

			class ShellFindContext:
				public Object {
					XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ShellFindContext);
				public:

					Symbol symbolFunctionShellFind;
					TPointerX<Prototype> prototypeShellFind;

					QUANTUM_SCRIPT_EXPORT ShellFindContext();
			};

			QUANTUM_SCRIPT_EXPORT ShellFindContext *getContext();
			QUANTUM_SCRIPT_EXPORT void initExecutive(Executive *executive,void *extensionId);

		};
	};
};


#endif
