//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_LIBSTDSOCKET_HPP
#define QUANTUM_SCRIPT_LIBSTDSOCKET_HPP

#ifndef QUANTUM_SCRIPT_EXECUTIVE_HPP
#include "quantum-script-executive.hpp"
#endif

namespace Quantum {
	namespace Script {

		namespace LibStdSocket {

			class SocketContext:
				public Object {
					XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(SocketContext);
				public:

					Symbol symbolFunctionSocket;
					TPointerX<Prototype> prototypeSocket;

					QUANTUM_SCRIPT_EXPORT SocketContext();
			};

			QUANTUM_SCRIPT_EXPORT SocketContext *getContext();
			QUANTUM_SCRIPT_EXPORT void initExecutive(Executive *executive,void *extensionId);

		};
	};
};


#endif
