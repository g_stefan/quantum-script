//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ASM_HPP
#define QUANTUM_SCRIPT_ASM_HPP

#ifndef QUANTUM_SCRIPT_IAssembler_HPP
#include "quantum-script-iassembler.hpp"
#endif

#ifndef QUANTUM_SCRIPT_INSTRUCTION_HPP
#include "quantum-script-instruction.hpp"
#endif

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		typedef struct SAsmLink {
			ProgramCounter *old_;
			ProgramCounter *new_;
		} AsmLink;

		class Asm :
			public virtual IAssembler {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Asm);
			protected:
				bool lastIsMark_;
				TRedBlackTree<ProgramCounter *,ProgramCounter *> asmLink_;
			public:
				TPointer<InstructionList> instructionList;

				QUANTUM_SCRIPT_EXPORT Asm();

				QUANTUM_SCRIPT_EXPORT void resetLinks();

				QUANTUM_SCRIPT_EXPORT ProgramCounter *assemble(int type_, const char *name_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn);
				QUANTUM_SCRIPT_EXPORT ProgramCounter *assembleX(int type_, const char *name_, const char *nameX_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn);
				QUANTUM_SCRIPT_EXPORT ProgramCounter *assembleProgramCounter(int type_, ProgramCounter *pc_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn);
				QUANTUM_SCRIPT_EXPORT void linkProgramCounter(ProgramCounter *old_, ProgramCounter *new_);
				QUANTUM_SCRIPT_EXPORT void linkProgramCounterEnd(ProgramCounter *old_, ProgramCounter *new_);
				QUANTUM_SCRIPT_EXPORT void linkProgramCounterSource(ProgramCounter *pc_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn);
				QUANTUM_SCRIPT_EXPORT void removeProgramCounter(ProgramCounter *pc_);

				QUANTUM_SCRIPT_EXPORT ProgramCounter *assembleDirect(InstructionProcedure procedure, Variable *operand);


#ifdef QUANTUM_SCRIPT_DISABLE_ASM_OPTIMIZER
				inline ProgramCounter *optimizeCode(ProgramCounter *pc) {
					return pc;
				};
#else
				ProgramCounter *optimizeCode(ProgramCounter *);
#endif
		};

	};
};


#endif

