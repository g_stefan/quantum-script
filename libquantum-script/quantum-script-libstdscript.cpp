﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#else
#include <dlfcn.h>
#endif

#include "quantum-script-libstdscript.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variableassociativearray.hpp"

#include "quantum-script-variablenativevmfunction.hpp"

#include "quantum-script-variableresource.hpp"
#include "quantum-script-variabledatetime.hpp"
#include "quantum-script-variablefile.hpp"
#include "quantum-script-variablesocket.hpp"
#include "quantum-script-variableshellfind.hpp"
#include "quantum-script-variablestacktrace.hpp"
#include "quantum-script-variablerandom.hpp"
#include "quantum-script-variablequitmessage.hpp"

#include "quantum-script-libstdscript.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdScript {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> isUndefined(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-undefined\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableUndefined::typeUndefined);
			};

			static TPointerOwner<Variable> isNull(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-null\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableNull::typeNull);
			};

			static TPointerOwner<Variable> isNil(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-nil\n");
#endif
				TPointerX<Variable> &value=arguments->index(0);
				return VariableBoolean::newVariable(
					       (value->variableType == VariableUndefined::typeUndefined)||
					       (value->variableType == VariableNull::typeNull)
				       );
			};

			static TPointerOwner<Variable> isNaN(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-nan\n");
#endif
				return VariableBoolean::newVariable(isnan((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> isFinite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-finite\n");
#endif
				return VariableBoolean::newVariable(isfinite((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> isInfinite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-infinite\n");
#endif
				return VariableBoolean::newVariable(isinf((arguments->index(0))->toNumber()));
			};

			static TPointerOwner<Variable> signBit(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-sign-bit\n");
#endif
				return VariableBoolean::newVariable(signbit((arguments->index(0))->toNumber()));
			};


			static TPointerOwner<Variable> isBoolean(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-boolean\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableBoolean::typeBoolean);
			};

			static TPointerOwner<Variable> isNumber(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-number\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableNumber::typeNumber);
			};

			static TPointerOwner<Variable> isString(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-string\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableString::typeString);
			};

			static TPointerOwner<Variable> isArray(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-array\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableArray::typeArray);
			};

			static TPointerOwner<Variable> isResource(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-resource\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableResource::typeResource);
			};

			static TPointerOwner<Variable> isDateTime(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-datetime\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableDateTime::typeDateTime);
			};

			static TPointerOwner<Variable> isRandom(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-random\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableRandom::typeRandom);
			};

			static TPointerOwner<Variable> isAssociativeArray(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-associative-array\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableAssociativeArray::typeAssociativeArray);
			};

			static TPointerOwner<Variable> isFunction(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-function\n");
#endif

				TPointerX<Variable> &variable=arguments->index(0);
				return VariableBoolean::newVariable(
					       (variable->variableType == VariableFunction::typeFunction)||
					       (variable->variableType == VariableFunctionWithYield::typeFunctionWithYield)||
					       (variable->variableType == VariableVmFunction::typeVmFunction)||
					       (variable->variableType == VariableNativeVmFunction::typeNativeVmFunction)
				       );

			};

			static TPointerOwner<Variable> isNativeFunction(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-function\n");
#endif
				TPointerX<Variable> &variable=arguments->index(0);
				return VariableBoolean::newVariable(
					       (variable->variableType == VariableFunction::typeFunction)||
					       (variable->variableType == VariableFunctionWithYield::typeFunctionWithYield)||
					       (variable->variableType == VariableNativeVmFunction::typeNativeVmFunction)
				       );
			};

			static TPointerOwner<Variable> isObject(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-object\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableObject::typeObject);
			};

			static TPointerOwner<Variable> isDefined(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-defined\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType != VariableUndefined::typeUndefined);
			};

			static TPointerOwner<Variable> isStackTrace(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-stack-trace\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableStackTrace::typeStackTrace);
			};

			static TPointerOwner<Variable> isQuitMessage(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-quit-message\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableQuitMessage::typeQuitMessage);
			};


			static TPointerOwner<Variable> stackTraceWithLevel(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-stack-trace-with-level\n");
#endif
				return VariableString::newVariable((((VariableStackTrace *) (arguments->index(0)).value() ))->toString((Integer)((arguments->index(1))->toNumber())));
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_include) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-include\n", context->currentProgramCounter);
#endif

				TPointer<Variable> operand1;
				TPointer<Variable> operand2;
				char buf[2048];

				operand1 = context->getArgument(0);
				operand2 = context->getArgument(1);

				if(operand2->variableType==VariableArray::typeArray) {
				} else {
					operand2.setObject(VariableArray::newVariable());
				};
				context->functionContext->functionArguments.setObjectX(static_cast<VariableArray *>(operand2.value()));

				if(!context->functionContext->this_) {
					context->functionContext->this_.setObjectX(VariableUndefined::newVariableX());
				};

				if (operand1) {
					if (operand1->variableType == VariableString::typeString) {

						Executive *executive = (Executive *) (((VariableResource *) operand)->resource);
						bool found = false;
						String fileName;
						fileName = ((VariableString *) operand1.value())->value.value();
						if (Shell::fileExists(fileName)) {
							found = true;
						} else {
							TYList2<String > *scan;
							for (scan = executive->includePath->head(); scan; scan = scan->next) {
								fileName = scan->value;
								fileName << "/" << ((VariableString *) operand1.value())->value.value();
								if (Shell::fileExists(fileName)) {
									found = true;
									break;
								};
							};
						};

						if (found) {
							context->includedFile.push(fileName);
							int retV = executive->includeAndExecuteFile(context,fileName);
							if (retV == VmParserError::None) {
								return;
							} else if (retV == VmParserError::Compile) {
								context->includedFile.popEmpty();
								sprintf( buf, "Compile error in %s line %d column %d\n",
									 executive->errorInfo.compileFileName,
									 executive->errorInfo.compileLineNumber,
									 executive->errorInfo.compileLineColumn
								       );
								context->pushX(context->newError(buf));
								InstructionVmThrow(context, NULL);
								return;
							} else if (retV == VmParserError::FileNotFound) {
								context->includedFile.popEmpty();
								sprintf( buf, "File not found \"%s\"\n",
									 ((VariableString *) operand1.value())->value.value()
								       );
								context->pushX(context->newError(buf));
								InstructionVmThrow(context, NULL);
								return;
							};
							context->includedFile.popEmpty();
						};

						sprintf( buf, "Unable to open \"%s\"\n",
							 ((VariableString *) operand1.value())->value.value()
						       );

						context->pushX(context->newError(buf));
						InstructionVmThrow(context, NULL);
						return;
					};
				};


				context->pushX(context->newError("invalid arguments"));
				InstructionVmThrow(context, NULL);

			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_includeOnce) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-include-once\n", context->currentProgramCounter);
#endif

				TPointer<Variable> operand1;
				TPointer<Variable> operand2;
				char buf[2048];

				operand1 = context->getArgument(0);
				operand2 = context->getArgument(1);

				if(operand2->variableType==VariableArray::typeArray) {
				} else {
					operand2.setObject(VariableArray::newVariable());
				};

				context->functionContext->functionArguments.setObjectX(static_cast<VariableArray *>(operand2.value()));

				if(!context->functionContext->this_) {
					context->functionContext->this_.setObjectX(VariableUndefined::newVariableX());
				};

				if (operand1) {
					if (operand1->variableType == VariableString::typeString) {
						Executive *executive = (Executive *) (((VariableResource *) operand)->resource);
						bool found = false;
						String fileName;
						fileName = ((VariableString *) operand1.value())->value.value();
						if (Shell::fileExists(fileName)) {
							found = true;
						} else {
							TYList2<String > *scan;
							for (scan = executive->includePath->head(); scan; scan = scan->next) {
								fileName = scan->value;
								fileName << "/" << ((VariableString *) operand1.value())->value.value();
								if (Shell::fileExists(fileName)) {
									found = true;
									break;
								};
							};
						};

						if (found) {
							char fileNameOut[1024];
							if(Shell::realpath(fileName,fileNameOut,1024)) {
								TYRedBlackTreeNode<String,bool> *node=context->listIncludeOnce.find(fileNameOut);
								if(node) {
									return;
								};
								context->listIncludeOnce.set(fileNameOut,true);

								context->includedFile.push(fileName);

								int retV = executive->includeAndExecuteFile(context,fileName);
								if (retV == VmParserError::None) {
									return;
								} else if (retV == VmParserError::Compile) {
									context->includedFile.popEmpty();
									sprintf( buf, "Compile error in %s line %d column %d\n",
										 executive->errorInfo.compileFileName,
										 executive->errorInfo.compileLineNumber,
										 executive->errorInfo.compileLineColumn
									       );
									context->pushX(context->newError(buf));
									InstructionVmThrow(context, NULL);
									return;
								} else if (retV == VmParserError::FileNotFound) {
									context->includedFile.popEmpty();
									sprintf( buf, "File not found \"%s\"\n",
										 ((VariableString *) operand1.value())->value.value()
									       );
									context->pushX(context->newError(buf));
									InstructionVmThrow(context, NULL);
									return;
								};

								context->includedFile.popEmpty();

							};
						};

						sprintf( buf, "Unable to open \"%s\"\n",
							 ((VariableString *) operand1.value())->value.value()
						       );

						context->pushX(context->newError(buf));
						InstructionVmThrow(context, NULL);
						return;
					};
				};


				context->pushX(context->newError("invalid arguments"));
				InstructionVmThrow(context, NULL);

			};


			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_execute) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-execute\n", context->currentProgramCounter);
#endif

				TPointer<Variable> operand1;
				TPointer<Variable> operand2;
				char buf[2048];

				operand1 = context->getArgument(0);
				operand2 = context->getArgument(1);

				if(operand2->variableType==VariableArray::typeArray) {
				} else {
					operand2.setObject(VariableArray::newVariable());
				};
				context->functionContext->functionArguments.setObjectX(static_cast<VariableArray *>(operand2.value()));

				if(!context->functionContext->this_) {
					context->functionContext->this_.setObjectX(VariableUndefined::newVariableX());
				};

				if (operand1) {
					if (operand1->variableType == VariableString::typeString) {
						Executive *executive = (Executive *) (((VariableResource *) operand)->resource);
						bool found = false;
						String stringToCompile;
						stringToCompile = ((VariableString *) operand1.value())->value.value();
						int retV = executive->includeAndExecuteString(context,stringToCompile);
						if (retV == VmParserError::None) {
							return;
						} else if (retV == VmParserError::Compile) {
							sprintf( buf, "Compile error in %s line %d column %d\n",
								 executive->errorInfo.compileFileName,
								 executive->errorInfo.compileLineNumber,
								 executive->errorInfo.compileLineColumn
							       );
							context->pushX(context->newError(buf));
							InstructionVmThrow(context, NULL);
							return;
						};
					};
				};

				context->pushX(context->newError("invalid arguments"));
				InstructionVmThrow(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_getIncludedFile) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-get-included-file\n", context->currentProgramCounter);
#endif
				context->pushOwner(VariableString::newVariable((context->includedFile.head())->value));
				InstructionVmReturn(context, NULL);
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_requireExtension) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-require-extension\n", context->currentProgramCounter);
#endif

				TPointer<Variable> operand1;
				TPointer<Variable> operand2;
				char buf[2048];

				operand1 = context->getArgument(0);
				operand2 = context->getArgument(1);

				if(operand2->variableType==VariableArray::typeArray) {
				} else {
					operand2.setObject(VariableArray::newVariable());
				};
				context->functionContext->functionArguments.setObjectX(static_cast<VariableArray *>(operand2.value()));

				if(!context->functionContext->this_) {
					context->functionContext->this_.setObjectX(VariableUndefined::newVariableX());
				};

				if (operand1) {
					if (operand1->variableType == VariableString::typeString) {
						Executive *executive = (Executive *) (((VariableResource *) operand)->resource);

						if(executive->isExtensionLoaded(((VariableString *) operand1.value())->value)) {
							return;
						};

						bool found = false;
						String fileNameFull;
						String filePath;
						String fileName;
						String fileNameFinal;
						String fileNameX;
						size_t index;
						fileNameFull = ((VariableString *) operand1.value())->value;
						fileName="qse-";
						if(StringX::indexOfFromEnd(fileNameFull, "\\", 0,index)) {
							filePath=StringX::substring(fileNameFull,0,index+1);
							fileName<<StringX::toLowerCaseAscii(StringX::substring(fileNameFull,index+1));
						} else if(StringX::indexOfFromEnd(fileNameFull, "/", 0,index)) {
							filePath=StringX::substring(fileNameFull,0,index+1);
							fileName<<StringX::toLowerCaseAscii(StringX::substring(fileNameFull,index+1));
						} else {
							fileName<<StringX::toLowerCaseAscii(fileNameFull);
						};

						fileNameFinal=filePath;
						fileNameFinal<<fileName;

#ifdef XYO_OS_TYPE_WIN
						fileNameFinal<<".dll";
#else
						fileNameFinal<<".so";
#endif

						if (Shell::fileExists( fileNameFinal)) {
							fileNameX=fileNameFinal;
							found = true;
						} else {
							TYList2<String> *scan;
							for (scan = executive->includePath->head(); scan; scan = scan->next) {
								fileNameX = scan->value;
								fileNameX << "/" << fileNameFinal;
								if (Shell::fileExists( fileNameX)) {
									found = true;
									break;
								};
							};
						};

						if (found) {
							TYList2<Extension_> *scan;
							for (scan = executive->extensionList->head(); scan; scan = scan->next) {
								if(scan->value.fileName==fileNameX) {
									return;
								};
							};
#ifdef XYO_OS_TYPE_WIN
							HMODULE hModule=LoadLibrary(fileNameX);
							if(hModule) {
								FARPROC farProc=GetProcAddress(hModule,"quantumScriptExtension");
								if(farProc) {
									executive->extensionList->pushEmpty();
									(executive->extensionList->head())->value.fileName=fileNameX;
									(executive->extensionList->head())->value.name=((VariableString *) operand1.value())->value;
									(executive->extensionList->head())->value.isPublic=true;
									if(executive->executeExtension(context,(QuantumScriptExtensionProc)farProc,executive->extensionList->head())==0) {
										return;
									};
								};
							};
#else
							void *hModule=dlopen(fileNameX, RTLD_LAZY);
							if(hModule) {
								QuantumScriptExtensionProc farProc=(QuantumScriptExtensionProc)dlsym(hModule, "quantumScriptExtension");
								if(dlerror()==NULL) {
									if(farProc) {
										executive->extensionList->pushEmpty();
										(executive->extensionList->head())->value.fileName=fileNameX;
										(executive->extensionList->head())->value.name=((VariableString *) operand1.value())->value;
										(executive->extensionList->head())->value.isPublic=true;
										if(executive->executeExtension(context,(QuantumScriptExtensionProc)farProc,executive->extensionList->head())==0) {
											return;
										};
									};
								};
							};
#endif
						} else {

							TYList2<InternalExtension_ > *scan;
							String extensionName_=StringX::toLowerCaseAscii(((VariableString *) operand1.value())->value);
							for (scan = executive->internalExtensionList->head(); scan; scan = scan->next) {
								if(extensionName_==StringX::toLowerCaseAscii(scan->value.name)) {
									executive->extensionList->pushEmpty();
									(executive->extensionList->head())->value.fileName="";
									(executive->extensionList->head())->value.name=((VariableString *) operand1.value())->value;
									(executive->extensionList->head())->value.isPublic=true;
									if(executive->executeExtension(context,(QuantumScriptExtensionProc)scan->value.extensionProc,executive->extensionList->head())==0) {
										return;
									};
									break;
								};
							};

						};
						sprintf( buf, "Unable to open \"%s\"\n",
							 ((VariableString *) operand1.value())->value.value()
						       );

						context->pushX(context->newError(buf));
						InstructionVmThrow(context, NULL);
						return;
					};
				};

				context->pushX(context->newError("invalid arguments"));
				InstructionVmThrow(context, NULL);

			};


			static TPointerOwner<Variable> setIncludePath(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-set-include-path\n");
#endif

				((Executive *)function->valueSuper)->includePath->push((arguments->index(0))->toString());
				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> getPrototypeOf(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-get-prototype-of\n");
#endif

				TPointerX<Variable> &value=arguments->index(0);
				Variable *prototype=value->instancePrototype();
				if(prototype) {
					prototype->incReferenceCount();
					return prototype;
				};
				return Context::getValueUndefined();
			};


			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_getExtensionList) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-get-extension-list\n");
#endif

				Executive *executive = (Executive *) (((VariableResource *) operand)->resource);

				Variable *out=VariableArray::newVariable();
				TYList2<Extension_> *scan;
				Symbol symFileName=Context::getSymbol("fileName");
				Symbol symName=Context::getSymbol("name");
				Symbol symInfo=Context::getSymbol("info");
				int k;
				for (scan = executive->extensionList->head(),k=0; scan; scan = scan->next,++k) {
					if(scan->value.isPublic) {
						(out->operatorIndex(k)).setObject(VariableObject::newVariable());
						((out->operatorIndex(k))->operatorReferenceOwnProperty(symFileName)).setObject(VariableString::newVariable(scan->value.fileName));
						((out->operatorIndex(k))->operatorReferenceOwnProperty(symName)).setObject(VariableString::newVariable(scan->value.name));
						((out->operatorIndex(k))->operatorReferenceOwnProperty(symInfo)).setObject(VariableString::newVariable(scan->value.info));
					};
				};
				context->pushOwner(out);
				InstructionVmReturn(context, NULL);
			};

			static TPointerOwner<Variable> protectSource(VariableFunction *function,Variable *this_,VariableArray *arguments) {
				TPointerX<Variable> &value=arguments->index(0);
				if(value->variableType==VariableVmFunction::typeVmFunction) {
					if(((VariableVmFunction *)value.value())->valueEnd) {
						TYList2<InstructionX> *index=(TYList2<InstructionX> *)(((VariableVmFunction *)value.value())->value);
						TYList2<InstructionX> *indexEnd=(TYList2<InstructionX> *)(((VariableVmFunction *)value.value())->valueEnd);

						for(; index!=indexEnd; index=index->next) {
							index->value.sourceSymbol=0;
							index->value.sourcePos=0;
							index->value.sourceLineNumber=0;
							index->value.sourceLineColumn=0;

						};

						// indexEnd also
						index->value.sourceSymbol=0;
						index->value.sourcePos=0;
						index->value.sourceLineNumber=0;
						index->value.sourceLineColumn=0;

					};
				};
				return Context::getValueUndefined();
			};

			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_setFunctionFromFile) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-set-function-from-file\n", context->currentProgramCounter);
#endif

				TPointer<Variable> operand1;
				TPointer<Variable> operand2;
				char buf[2048];

				operand2 = context->getArgument(0);
				operand1 = context->getArgument(1);

				if(!context->functionContext->this_) {
					context->functionContext->this_.setObjectX(VariableUndefined::newVariableX());
				};

				if (operand1) {
					if (operand1->variableType == VariableString::typeString) {
						if (operand2) {
							if (operand2->variableType == VariableString::typeString) {
								Executive *executive = (Executive *) (((VariableResource *) operand)->resource);
								bool found = false;
								String fileName;
								fileName = ((VariableString *) operand1.value())->value.value();
								if (Shell::fileExists(fileName)) {
									found = true;
								} else {
									TYList2<String > *scan;
									for (scan = executive->includePath->head(); scan; scan = scan->next) {
										fileName = scan->value;
										fileName << "/" << ((VariableString *) operand1.value())->value.value();
										if (Shell::fileExists(fileName)) {
											found = true;
											break;
										};
									};
								};

								if (found) {
									context->includedFile.push(fileName);
									int retV = executive->setVmFunctionFromFileX(context,((VariableString *) operand2.value())->value,fileName);
									if (retV == VmParserError::None) {
										return;
									} else if (retV == VmParserError::Compile) {
										context->includedFile.popEmpty();
										sprintf( buf, "Compile error in %s line %d column %d\n",
											 executive->errorInfo.compileFileName,
											 executive->errorInfo.compileLineNumber,
											 executive->errorInfo.compileLineColumn
										       );
										context->pushX(context->newError(buf));
										InstructionVmThrow(context, NULL);
										return;
									} else if (retV == VmParserError::FileNotFound) {
										context->includedFile.popEmpty();
										sprintf( buf, "File not found \"%s\"\n",
											 ((VariableString *) operand1.value())->value.value()
										       );
										context->pushX(context->newError(buf));
										InstructionVmThrow(context, NULL);
										return;
									};
									context->includedFile.popEmpty();

								};


								sprintf( buf, "Unable to open \"%s\"\n",
									 ((VariableString *) operand1.value())->value.value()
								       );

								context->pushX(context->newError(buf));
								InstructionVmThrow(context, NULL);
								return;
							};
						};
					};
				};


				context->pushX(context->newError("invalid arguments"));
				InstructionVmThrow(context, NULL);
			};


			static QUANTUM_SCRIPT_INSTRUCTION_IMPLEMENT(LibStdScript_setFunctionFromString) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("#%p    script-set-function-from-string\n", context->currentProgramCounter);
#endif

				TPointer<Variable> operand1;
				TPointer<Variable> operand2;
				char buf[2048];

				operand2 = context->getArgument(0);
				operand1 = context->getArgument(1);

				if(!context->functionContext->this_) {
					context->functionContext->this_.setObjectX(VariableUndefined::newVariableX());
				};

				if (operand1) {
					if (operand1->variableType == VariableString::typeString) {
						if (operand2) {
							if (operand2->variableType == VariableString::typeString) {
								Executive *executive = (Executive *) (((VariableResource *) operand)->resource);

								int retV = executive->setVmFunctionFromStringX(context,((VariableString *) operand2.value())->value,((VariableString *) operand1.value())->value);
								if (retV == VmParserError::None) {
									return;
								} else if (retV == VmParserError::Compile) {
									sprintf( buf, "Compile error in %s line %d column %d\n",
										 executive->errorInfo.compileFileName,
										 executive->errorInfo.compileLineNumber,
										 executive->errorInfo.compileLineColumn
									       );
									context->pushX(context->newError(buf));
									InstructionVmThrow(context, NULL);
									return;
								};

								context->pushX(context->newError("script-set-function-from-string"));
								InstructionVmThrow(context, NULL);
								return;
							};
						};
					};
				};


				context->pushX(context->newError("invalid arguments"));
				InstructionVmThrow(context, NULL);
			};


			static TPointerOwner<Variable> compare(VariableFunction *function,Variable *this_,VariableArray *arguments) {
				return VariableNumber::newVariable((arguments->index(0))->compare(arguments->index(1)));
			};

			static TPointerOwner<Variable> reverseCompare(VariableFunction *function,Variable *this_,VariableArray *arguments) {
				return VariableNumber::newVariable(-((arguments->index(0))->compare(arguments->index(1))));
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var Script={};");
				executive->setFunction2("Script.isUndefined(x)", isUndefined);
				executive->setFunction2("Script.isNull(x)", isNull);
				executive->setFunction2("Script.isNil(x)", isNil);
				executive->setFunction2("Script.isNaN(x)", isNaN);
				executive->setFunction2("Script.isFinite(x)", isFinite);
				executive->setFunction2("Script.isInfinite(x)", isInfinite);
				executive->setFunction2("Script.signBit(x)", signBit);
				executive->setFunction2("Script.isBoolean(x)", isBoolean);
				executive->setFunction2("Script.isNumber(x)", isNumber);
				executive->setFunction2("Script.isString(x)", isString);
				executive->setFunction2("Script.isArray(x)", isArray);
				executive->setFunction2("Script.isResource(x)", isResource);
				executive->setFunction2("Script.isRandom(x)", isRandom);
				executive->setFunction2("Script.isAssociativeArray(x)", isAssociativeArray);
				executive->setFunction2("Script.isDateTime(x)", isDateTime);
				executive->setFunction2("Script.isFunction(x)", isFunction);
				executive->setFunction2("Script.isNativeFunction(x)", isNativeFunction);
				executive->setFunction2("Script.isObject(x)", isObject);
				executive->setFunction2("Script.isDefined(x)", isDefined);
				executive->setFunction2("Script.isStackTrace(x)", isStackTrace);
				executive->setFunction2("Script.isQuitMessage(x)", isQuitMessage);
				executive->setFunction2("Script.stackTraceWithLevel(x,level)", stackTraceWithLevel);
				executive->setVmFunction("Script.include", InstructionLibStdScript_include,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setVmFunction("Script.includeOnce", InstructionLibStdScript_includeOnce,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setVmFunction("Script.execute", InstructionLibStdScript_execute,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setVmFunction("Script.requireExtension", InstructionLibStdScript_requireExtension,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setVmFunction("Script.getExtensionList", InstructionLibStdScript_getExtensionList,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setFunction4("Script.setIncludePath(path)", setIncludePath,executive);
				executive->setFunction2("Script.getPrototypeOf(x)", getPrototypeOf);
				executive->setFunction2("Script.protectSource(fn)", protectSource);
				executive->setFunction2("Script.compare(a,b)", compare);
				executive->setFunction2("Script.reverseCompare(a,b)", reverseCompare);
				executive->setVmFunction("Script.setFunctionFromFile", InstructionLibStdScript_setFunctionFromFile,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setVmFunction("Script.setFunctionFromString", InstructionLibStdScript_setFunctionFromString,TPointerOwner<Variable>(VariableResource::newVariable(executive,NULL)));
				executive->setVmFunction("Script.getIncludedFile", InstructionLibStdScript_getIncludedFile,NULL);
				executive->compileStringX((char *)libStdScriptSource);
			};

		};
	};
};


