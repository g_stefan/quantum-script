﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdutf16.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-variableutf16.hpp"
#include "quantum-script-variableutf32.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdUtf16 {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> indexOf(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-index-of\n");
#endif
				size_t index_;
				Number pos=(arguments->index(1))->toNumber();
				if (StringWordX::indexOf(
					    VariableUtf16::getUtf16(this_),
					    VariableUtf16::getUtf16(arguments->index(0)),
					    (isnan(pos))?0:((isinf(pos))?0:((Integer)pos)),
					    index_)) {
					return VariableNumber::newVariable((Number) index_);
				};
				return VariableNumber::newVariable((Number)-1);
			};

			static TPointerOwner<Variable> lastIndexOf(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-last-index-of\n");
#endif

				size_t index_;
				Number pos=(arguments->index(1))->toNumber();
				if (StringWordX::indexOfFromEnd(
					    VariableUtf16::getUtf16(this_),
					    VariableUtf16::getUtf16(arguments->index(0)),
					    (isnan(pos))?0:((isinf(pos))?0:((Integer)pos)),
					    index_)) {
					return VariableNumber::newVariable((Number) index_);
				};
				return VariableNumber::newVariable((Number)-1);
			};

			static TPointerOwner<Variable> substring(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-substring\n");
#endif
				StringWord value=VariableUtf16::getUtf16(this_);
				Number index=(arguments->index(0))->toNumber();
				Number ln=(arguments->index(1))->toNumber();
				return VariableUtf16::newVariable(
					       StringWordX::substring(
						       value,
						       (isnan(index))?0:((isinf(index))?0:((Integer)index)),
						       (isnan(ln))?value.length():((isinf(ln))?value.length():((Integer)ln))
					       ));
			};

			static TPointerOwner<Variable> toLowerCaseAscii(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-to-lower-case-ascii\n");
#endif

				return VariableUtf16::newVariable(StringWordX::toLowerCaseAscii(VariableUtf16::getUtf16(this_)));
			};

			static TPointerOwner<Variable> toUpperCaseAscii(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-to-upper-case-ascii\n");
#endif

				return VariableUtf16::newVariable(StringWordX::toUpperCaseAscii(VariableUtf16::getUtf16(this_)));
			};

			static TPointerOwner<Variable> replace(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-replace\n");
#endif

				return VariableUtf16::newVariable(
					       StringWordX::replace(
						       VariableUtf16::getUtf16(this_),
						       VariableUtf16::getUtf16(arguments->index(0)),
						       VariableUtf16::getUtf16(arguments->index(1))
					       )
				       );
			};

			static TPointerOwner<Variable> matchAscii(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-match-ascii\n");
#endif

				return VariableBoolean::newVariable(
					       StringWordX::matchAscii(VariableUtf16::getUtf16(arguments->index(0)),VariableUtf16::getUtf16(arguments->index(1)))
				       );
			};

			static TPointerOwner<Variable> getWord(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-get-word\n");
#endif

				Number x=(arguments->index(0))->toNumber();
				StringWord this__=VariableUtf16::getUtf16(this_);
				if(isnan(x)||isinf(x)||signbit(x)) {
					return VariableNumber::newVariable(0);
				};
				if(x>this__.length()) {
					return VariableNumber::newVariable(0);
				};

				return VariableNumber::newVariable(this__[(int)x]);
			};


			static TPointerOwner<Variable> trim(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf16-trim\n");
#endif

				return VariableUtf16::newVariable(StringWordX::trimWithElement(VariableUtf16::getUtf16(this_),UtfX::utf16FromUtf8(" \t")));
			};

			static TPointerOwner<Variable> splitBy(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- utf8-split-by\n");
#endif
				size_t index;
				size_t pos;
				int ln;
				StringWord src=VariableUtf16::getUtf16(this_);
				StringWord sig=VariableUtf16::getUtf16(arguments->index(0));
				if(StringWordX::indexOf(src,sig,0,index)) {
					TPointerOwner<Variable> retV(VariableArray::newVariable());
					ln=1;
					(retV->operatorIndex(0)).setObject(VariableUtf16::newVariable(StringWordX::substring(src,0,index)));
					pos=index+1;
					while(StringWordX::indexOf(src,sig,pos,index)) {
						(retV->operatorIndex(ln)).setObject(VariableUtf16::newVariable(StringWordX::substring(src,pos,index-pos)));
						pos=index+1;
						++ln;
					};
					(retV->operatorIndex(ln)).setObject(VariableUtf16::newVariable(StringWordX::substring(src,pos)));
					return retV;
				};
				return VariableNull::newVariable();
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction2("Utf16.prototype.indexOf(text,st)", indexOf);
				executive->setFunction2("Utf16.prototype.lastIndexOf(text,st)", lastIndexOf);
				executive->setFunction2("Utf16.prototype.substring(st,ln)", substring);
				executive->setFunction2("Utf16.prototype.toLowerCaseAscii()", toLowerCaseAscii);
				executive->setFunction2("Utf16.prototype.toUpperCaseAscii()", toUpperCaseAscii);
				executive->setFunction2("Utf16.prototype.replace(what_,with_)", replace);
				executive->setFunction2("Utf16.prototype.matchAscii(text,signature)", matchAscii);
				executive->setFunction2("Utf16.prototype.getWord(pos)", getWord);
				executive->setFunction2("Utf16.prototype.trim()", trim);
				executive->setFunction2("Utf16.prototype.splitBy(str)", splitBy);
			};

		};
	};
};


