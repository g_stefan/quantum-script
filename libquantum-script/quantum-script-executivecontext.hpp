//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXT_HPP
#define QUANTUM_SCRIPT_EXECUTIVECONTEXT_HPP

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXTPC_HPP
#include "quantum-script-executivecontextpc.hpp"
#endif

#ifndef QUANTUM_SCRIPT_EXECUTIVECONTEXTFUNCTION_HPP
#include "quantum-script-executivecontextfunction.hpp"
#endif

namespace Quantum {
	namespace Script {

		class ExecutiveContext;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ExecutiveContext>:
			public TMemoryObjectPoolActive<Quantum::Script::ExecutiveContext> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class ExecutiveContext :
			public TStack1PointerUnsafe<ExecutiveContextPc,TXMemoryPoolActive> {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ExecutiveContext);
			public:

				inline ExecutiveContext() {};

				inline void enter(ExecutiveContextPc *&out) {
					out=TMemory<ExecutiveContextPc>::newObject();
					out->functionContext=head_->value->functionContext;
					pushOwner(out);
				};

				inline void enterMaster(ExecutiveContextFunction *&out) {
					out=TMemory<ExecutiveContextFunction>::newObject();
					pushOwner(out);
				};

				inline void leave(ExecutiveContextPc *&out,ExecutiveContextFunction *&outFunction) {
					popEmpty();
					if (head_) {
						out=head_->value;
						outFunction=out->functionContext;
					} else {
						out=NULL;
						outFunction=NULL;
					};
				};

				inline static void memoryInit() {
					TMemory<TStack1PointerUnsafe<ExecutiveContextPc,TXMemoryPoolActive> >::memoryInit();
					TMemory<ExecutiveContextFunction>::memoryInit();
				};

				inline void activeDestructor() {
					TStack1PointerUnsafe<ExecutiveContextPc,TXMemoryPoolActive>::activeDestructor();
				};
		};

	};
};


#endif
