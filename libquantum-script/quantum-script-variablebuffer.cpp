﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-variablebuffer.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablenull.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;
		using XYO::byte;

		const void *VariableBuffer::typeBuffer="{D28585E9-AA47-4220-820A-AC777702F002}";
		const char *VariableBuffer::strTypeBuffer="Buffer";

		String VariableBuffer::getType() {
			return strTypeBuffer;
		};

		Variable *VariableBuffer::newVariable(size_t size) {
			VariableBuffer *retV;
			retV = TMemory<VariableBuffer>::newObject();
			retV->buffer=new byte[size];
			retV->length=0;
			retV->size=size;
			memset(retV->buffer,0,size);
			return (Variable *) retV;
		};

		Variable *VariableBuffer::newObjectVFromString(String str_) {
			VariableBuffer *retV;
			retV = TMemory<VariableBuffer>::newObject();
			retV->buffer=new byte[str_.length()];
			retV->length=str_.length();
			retV->size=str_.length();
			memcpy(retV->buffer,str_.value(),str_.length());
			return (Variable *) retV;
		};

		Variable &VariableBuffer::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolLength()) {
				if(vLength) {
					((VariableNumber *)vLength.value())->value=(Number)length;
				} else {
					vLength.setObject(VariableNumber::newVariable((Number)length));
				};
				return *vLength;
			};
			if(symbolId==Context::getSymbolSize()) {
				if(vSize) {
					((VariableNumber *)vSize.value())->value=(Number)size;
				} else {
					vSize.setObject(VariableNumber::newVariable((Number)size));
				};
				return *vSize;
			};
			return operatorReferenceX(symbolId,(Context::getPrototypeBuffer())->prototype);
		};

		Variable *VariableBuffer::instancePrototype() {
			return (Context::getPrototypeBuffer())->prototype;
		};

		void VariableBuffer::memoryInit() {
			TMemory<String>::memoryInit();
		};

		Variable *VariableBuffer::clone(SymbolList &inSymbolList) {
			VariableBuffer *out=(VariableBuffer *)newVariable(size);
			memcpy(out->buffer,buffer,size);
			return out;
		};

		bool VariableBuffer::toBoolean() {
			return (length>0);
		};

		Number VariableBuffer::toNumber() {
			Number retV;
			if(sscanf((char *)buffer, QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &retV) == 1) {
				return retV;
			};
			return NAN;
		};

		String VariableBuffer::toString() {
			String retVX;
			retVX.set(buffer,length);
			return retVX;
		};

		bool VariableBuffer::isString() {
			return true;
		};


	};
};


