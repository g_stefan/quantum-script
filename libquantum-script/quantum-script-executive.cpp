﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/timeb.h>

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>
#endif

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-executive.hpp"
#include "quantum-script-executivex.hpp"
#include "quantum-script-parserasm.hpp"
#include "quantum-script-libstd.hpp"
#include "quantum-script-variable.hpp"
#include "quantum-script-executivecontext.hpp"
#include "quantum-script-instructionx.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-variablesymbol.hpp"
#include "quantum-script-variableassociativearray.hpp"
#include "quantum-script-variableresource.hpp"
#include "quantum-script-variabledatetime.hpp"

#include "quantum-script-variablefiberinfo.hpp"
#include "quantum-script-variablethreadinfo.hpp"
#include "quantum-script-variablevmfunction.hpp"
#include "quantum-script-instructioncontext.hpp"
#include "quantum-script-variablestacktrace.hpp"
#include "quantum-script-variableargumentlevel.hpp"
#include "quantum-script-variablereferenceobject.hpp"
#include "quantum-script-variableoperator21.hpp"
#include "quantum-script-variableoperator22.hpp"
#include "quantum-script-variableoperator23.hpp"
#include "quantum-script-variableoperator31.hpp"
#include "quantum-script-variablenativevmfunction.hpp"
#include "quantum-script-variablevmprogramcounter.hpp"

#include "quantum-script-arrayiteratorkey.hpp"
#include "quantum-script-arrayiteratorvalue.hpp"
#include "quantum-script-objectiteratorkey.hpp"
#include "quantum-script-objectiteratorvalue.hpp"

#include "quantum-script-executivex.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;
		using XYO::byte;

		Executive::Executive() {
			//
			// Init context singleton
			//
			applicationInitExecutive=NULL;

			Context::newContext();

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
			threadInfo=NULL;
			symbolList=&Context::getSymbolList();
#endif

			configPrintStackTraceLimit=QUANTUM_SCRIPT_DEFAULT_STACK_TRACE_LEVEL;
			currentFiberExit=NULL;
			includePath.newObject();
			extensionList.newObject();
			internalExtensionList.newObject();
			errorInfoInit();

			// for sys extension
			mainCmdN=0;
			mainCmdS=NULL;
		};

		Executive::~Executive() {
#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
#else
			// allow safe shutdown if reentrant, wait for child threads/fibers to finish
			execute_(NULL);
#endif

#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
			instructionContext.deleteObject();
#else
			fiberContext.empty();
#endif

			//
			// Release extension context
			//
			TYList2<Extension_ > *scan;
			for (scan = extensionList->head(); scan; scan = scan->next) {
				if(scan->value.deleteContext) {
					(*scan->value.deleteContext)();
				};
			};

			//
			// Release context singleton
			//
			Context::deleteContext();
		};

		void Executive::errorInfoInit() {
			errorInfo.compileError = 0;
			errorInfo.compileFilePos = 0;
			errorInfo.compileLineNumber = 0;
			errorInfo.compileLineColumn = 0;
			errorInfo.compileFileName = "";
		};

		void Executive::initExecutive() {
			assembler.newObject();
#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
			instructionContext.newObject();
			instructionContext->configPrintStackTraceLimit=configPrintStackTraceLimit;
			instructionContext->init();
			instructionContext->executiveSuper=this;
			instructionContext->contextStack.newObject();
			instructionContext->contextStack->enterMaster(instructionContext->functionContext);
			instructionContext->pcContext=instructionContext->functionContext;
			instructionContext->functionContext->this_.newObject(); // undefined
			instructionContext->fiberInfo->isSuspended=false;
			instructionContext->fiberInfo->sleepMilliseconds=0;
#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
			instructionContext->threadInfo=threadInfo;
#endif

#else
			fiberCount=0;
			fiberContext.pushEmpty();
			(fiberContext.head())->value.newObject();
			(fiberContext.head())->value->configPrintStackTraceLimit=configPrintStackTraceLimit;
			(fiberContext.head())->value->init();
			(fiberContext.head())->value->executiveSuper=this;
			(fiberContext.head())->value->contextStack.newObject();
			(fiberContext.head())->value->contextStack->enterMaster((fiberContext.head())->value->functionContext);
			(fiberContext.head())->value->pcContext=(fiberContext.head())->value->functionContext;
			(fiberContext.head())->value->functionContext->this_.newObject(); // undefined
			(fiberContext.head())->value->fiberInfo->isSuspended=false;
			(fiberContext.head())->value->fiberInfo->sleepMilliseconds=0;
#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
			(fiberContext.head())->value->threadInfo=threadInfo;
			threadCount=0;
#endif


#endif
			assembler->instructionList.newObject();
			ProgramCounter *skipFiberExit;

			skipFiberExit = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);
			currentFiberExit=assembler->assemble(ParserAsm::CurrentFiberExit, "", 0, 0, 0, 0);
			assembler->linkProgramCounter(skipFiberExit,
						      assembler->assemble(ParserAsm::Mark, "", 0, 0, 0, 0)
						     );

			initExtension(LibStd::initExecutive);
			if(applicationInitExecutive) {
				(*applicationInitExecutive)(this);
			};

#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
			instructionContext->configPrintStackTraceLimit=configPrintStackTraceLimit;
#else
			(fiberContext.head())->value->configPrintStackTraceLimit=configPrintStackTraceLimit;
#endif
		};

		bool Executive::isExtensionLoaded(String extensionName) {
			TYList2<Extension_ > *scan;
			extensionName=StringX::toLowerCaseAscii(extensionName);
			for (scan = extensionList->head(); scan; scan = scan->next) {
				if(extensionName==StringX::toLowerCaseAscii(scan->value.name)) {
					return true;
				};
			};
			return false;
		};

		void Executive::setExtensionInfo(void *extensionId,String info) {
			((TYList2<Extension_ > *)extensionId)->value.info=info;
		};

		void Executive::setExtensionName(void *extensionId,String name) {
			((TYList2<Extension_ > *)extensionId)->value.name=name;
		};

		void Executive::setExtensionPublic(void *extensionId,bool isPublic) {
			((TYList2<Extension_ > *)extensionId)->value.isPublic=isPublic;
		};

		void Executive::setExtensionDeleteContext(void *extensionId,QuantumScriptExtensionDeleteContextProc deleteContext) {
			((TYList2<Extension_ > *)extensionId)->value.deleteContext=deleteContext;
		};

		void Executive::initExtension(QuantumScriptExtensionInitProc extensionProc) {
			extensionList->pushEmpty();
			(*extensionProc)(this,extensionList->head());
		};

		void Executive::registerInternalExtension(String extensionName,QuantumScriptExtensionInitProc extensionProc) {
			TYList2<InternalExtension_ > *scan;
			String extensionName_=StringX::toLowerCaseAscii(extensionName);
			for (scan = internalExtensionList->head(); scan; scan = scan->next) {
				if(extensionName_==StringX::toLowerCaseAscii(scan->value.name)) {
					scan->value.extensionProc=extensionProc;
					return;
				};
			};
			internalExtensionList->pushEmpty();
			scan=internalExtensionList->head();
			scan->value.name=extensionName;
			scan->value.extensionProc=extensionProc;
		};

		Variable *Executive::cloneVariable(SymbolList &inSymbolList,Variable *in) {
			if(in==NULL) {
				return VariableUndefined::newVariable();
			};
			return in->clone(inSymbolList);
		};

		int Executive::execute() {
#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
			instructionContext->error = InstructionError::None;
			instructionContext->contextStack->enter(instructionContext->pcContext);
			instructionContext->instructionListExecutive=assembler->instructionList.value();
			instructionContext->currentProgramCounter=reinterpret_cast<ProgramCounter *> (assembler->instructionList->head());
#else
			(fiberContext.head())->value->error = InstructionError::None;
			(fiberContext.head())->value->contextStack->enter((fiberContext.head())->value->pcContext);
			(fiberContext.head())->value->instructionListExecutive=assembler->instructionList.value();
			(fiberContext.head())->value->currentProgramCounter=reinterpret_cast<ProgramCounter *> (assembler->instructionList->head());
#endif
#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
			return execute_(instructionContext.value());
#else
			return execute_(NULL);
#endif
		};


		int Executive::executeEnd() {
#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
			return 0;
#else
			return execute_(NULL);
#endif
		};

		int Executive::executeReturn_(InstructionContext *context,int returnError) {
#ifndef QUANTUM_SCRIPT_SINGLE_THREAD

			TYList2<VariableThreadInfo *> *threadLink, *nextLink_;

			if(context) {
				return returnError;
			};

			if(threadInfo) {
				threadInfo->waitChilds=true;
			};

			// wait for child threads
			threadLink=childThread.head();

			while(threadLink) {
				if(threadInfo) {
					if(threadInfo->messageToReceive) {
						threadInfo->messageToReceive=NULL;
					};
				};
				if(threadLink->value->messageToSend) {
					threadLink->value->messageToSend=NULL;
				};

				if(threadLink->value->terminatedLocal) {
					childThread.extractNode(threadLink);
					threadLink->value->decReferenceCount();
					nextLink_=threadLink->next;
					childThread.nodeMemoryDelete(threadLink);
					--threadCount;
					threadLink=nextLink_;

					if(threadLink) {
					} else {
						threadLink=childThread.head();
					};
					continue;
				};

				if(threadLink->value->finalizeLocal) {
					Variable *returnValue_;
					// nothing to transfer
					threadLink->value->returnValueSuper.setObject(VariableUndefined::newVariable());
					threadLink->value->finalizeLocal=false;
				};

				XThread::sleepOneMillisecond();
				threadLink=threadLink->next;
				if(threadLink) {
				} else {
					threadLink=childThread.head();
				};
			};

#endif
			return returnError;
		};


		int Executive::execute_(InstructionContext *context) {

#ifdef QUANTUM_SCRIPT_SINGLE_FIBER

			while(context->currentProgramCounter) {

				try {
					while(context->currentProgramCounter) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
						fflush(stdout);
#endif

						context->nextProgramCounter = reinterpret_cast<ProgramCounter *> (reinterpret_cast<TYList2<InstructionX> *> (context->currentProgramCounter)->next);
						(*reinterpret_cast<TYList2<InstructionX> *> (context->currentProgramCounter)->value.procedure)(
							context,
							reinterpret_cast<TYList2<InstructionX> *> (context->currentProgramCounter)->value.operand
						);

						context->currentProgramCounter = context->nextProgramCounter;
					};
					break;

				} catch(const Error &e) {
					context->pushX(context->newError((const_cast<Error &>(e)).getMessage()));
					InstructionVmThrow(context, NULL);
				};
				context->currentProgramCounter = context->nextProgramCounter;
			};

#else
			TYList2<TPointer<InstructionContext> > *fiber,*endFiber, *fiberInSleep;
			int errorFiber;

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
			TYList2<VariableThreadInfo *> *threadLink;

			threadLink=childThread.head();
#endif


			fiberInSleep=NULL;

			fiber=fiberContext.head();
			while(fiber) {
#ifndef QUANTUM_SCRIPT_SINGLE_THREAD

				if(threadInfo) {
					if(threadInfo->messageToReceive) {
						if(!threadInfo->isPostMessage) {
							threadInfo->receivedMessage.setObject(cloneVariable(Context::getSymbolList(),threadInfo->messageToReceive));
						} else {
							threadInfo->messageQueueLocal->pushX(TPointerOwner<Variable>(cloneVariable(Context::getSymbolList(),threadInfo->messageToReceive)));
						};
						threadInfo->messageToReceive=NULL;
					};
					if(threadInfo->isTerminated) {
						break;
					};
					if(threadInfo->isSuspended) {
						XThread::sleepOneMillisecond();
						continue;
					};
				};

				if(threadCount) {



					if(threadLink) {

						if(threadLink->value->messageToSend) {
							threadLink->value->returnValueSuper.setObject(cloneVariable(*(((Executive *)(threadLink->value->vmLocal))->symbolList),threadLink->value->messageToSend));
							threadLink->value->messageToSend=NULL;
						};

						if(threadLink->value->terminatedLocal) {
							TYList2<VariableThreadInfo *> *nextLink_;
							childThread.extractNode(threadLink);
							threadLink->value->decReferenceCount();
							nextLink_=threadLink->next;
							childThread.nodeMemoryDelete(threadLink);
							threadLink=nextLink_;
							--threadCount;
						};

					};

					if(threadLink) {


						if(threadLink->value->finalizeLocal) {
							threadLink->value->returnValueSuper.setObject(cloneVariable(*(((Executive *)(threadLink->value->vmLocal))->symbolList),threadLink->value->returnValueLocal));
							threadLink->value->finalizeLocal=false;
						};

						threadLink=threadLink->next;
						if(threadLink) {
						} else {
							threadLink=childThread.head();
						};
					} else {
						threadLink=childThread.head();
					};

				};

#endif


				if(fiber->value->fiberInfo->isSuspended) {
					if(fiber->value->fiberInfo->isTerminated) {
						if(fiber->value->error) {

							if(context) {
								if(context==fiber->value.value()) {
									errorFiber=fiber->value->error;
									endFiber=fiber;
									fiberContext.extractNode(endFiber);
									endFiber->value.deleteObject();
									fiberContext.nodeMemoryDelete(endFiber);
									--fiberCount;
									return executeReturn_(context,errorFiber);
								};
								// not current fiber, continue
								fiber=fiber->next;
								if(fiber) {
									continue;
								};
								fiber=fiberContext.head();
								if(fiber) {
									continue;
								};
								break;
							} else {
								errorFiber=fiber->value->error;
								endFiber=fiber;
								fiberContext.extractNode(endFiber);
								endFiber->value.deleteObject();
								fiberContext.nodeMemoryDelete(endFiber);
								--fiberCount;
								return executeReturn_(context,errorFiber);
							};
						};
						endFiber=fiber;
						if(context==endFiber->value.value()) {
							fiberContext.extractNode(endFiber);
							endFiber->value.deleteObject();
							fiberContext.nodeMemoryDelete(endFiber);
							--fiberCount;
							return executeReturn_(context,InstructionError::None);
						};
						fiber=fiber->next;
						fiberContext.extractNode(endFiber);
						endFiber->value.deleteObject();
						fiberContext.nodeMemoryDelete(endFiber);
						--fiberCount;
						if(fiber) {
							continue;
						};
						fiber=fiberContext.head();
						if(fiber) {
							continue;
						};
						break;
					};

					if(fiber->value->fiberInfo->sleepMilliseconds>0) {
						timeb tb;
						ftime(&tb);
						long int currentMilliseconds = tb.millitm + (tb.time & 0xFFFF) * 1000;

						if((currentMilliseconds-(fiber->value->fiberInfo->startMilliseconds))<fiber->value->fiberInfo->sleepMilliseconds) {
							if(fiberInSleep==fiber) {
								//
								//  Safe to suspend whole thread, all fibers are in a sleep state
								//
								XThread::sleepOneMillisecond();


							} else {
								if(!fiberInSleep) {
									fiberInSleep=fiber;
								};
							};
						} else {
							fiber->value->fiberInfo->sleepMilliseconds=0;
							fiber->value->fiberInfo->isSuspended=false;
						};
					};

					if(fiber->value->fiberCriticalSection>0) {
						continue;
					};

					fiber=fiber->next;
					if(fiber) {
						continue;
					};
					fiber=fiberContext.head();
					if(fiber) {
						continue;
					};
					break;
				};
				fiberInSleep=NULL;

				if(fiber->value->currentProgramCounter) {

#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
					fflush(stdout);
#endif

					try {
						fiber->value->nextProgramCounter = reinterpret_cast<ProgramCounter *> (reinterpret_cast<TYList2<InstructionX> *> (fiber->value->currentProgramCounter)->next);
						(*reinterpret_cast<TYList2<InstructionX> *> (fiber->value->currentProgramCounter)->value.procedure)(
							fiber->value,
							reinterpret_cast<TYList2<InstructionX> *> (fiber->value->currentProgramCounter)->value.operand
						);

					} catch(const Error &e) {
						fiber->value->pushX(fiber->value->newError((const_cast<Error &>(e)).getMessage()));
						InstructionVmThrow(fiber->value, NULL);
					};


					fiber->value->currentProgramCounter = fiber->value->nextProgramCounter;

					// fiberCount==0 => we have only one fiber
					if(!fiberCount) {
						continue;
					};

					if(fiber->value->fiberCriticalSection>0) {
						continue;
					};

					fiber=fiber->next;
					if(fiber) {
						continue;
					};
					fiber=fiberContext.head();
					if(fiber) {
						continue;
					};
					break;
				};
				fiber->value->fiberInfo->isTerminated=true;
				if(fiber->value->error) {

					if(context) {
						if(context==fiber->value.value()) {
							errorFiber=fiber->value->error;
							endFiber=fiber;
							fiberContext.extractNode(endFiber);
							endFiber->value.deleteObject();
							fiberContext.nodeMemoryDelete(endFiber);
							--fiberCount;
							return executeReturn_(context,errorFiber);
						};
						// not current fiber, continue
						fiber=fiber->next;
						if(fiber) {
							continue;
						};
						fiber=fiberContext.head();
						if(fiber) {
							continue;
						};
						break;
					} else {
						errorFiber=fiber->value->error;
						endFiber=fiber;
						fiberContext.extractNode(endFiber);
						endFiber->value.deleteObject();
						fiberContext.nodeMemoryDelete(endFiber);
						--fiberCount;
						return executeReturn_(context,errorFiber);
					};


				};
				endFiber=fiber;

				if(context==endFiber->value.value()) {
					fiber=fiber->next;
					fiberContext.extractNode(endFiber);
					endFiber->value.deleteObject();
					fiberContext.nodeMemoryDelete(endFiber);
					--fiberCount;
					return executeReturn_(context,InstructionError::None);
				};


				fiber=fiber->next;
				fiberContext.extractNode(endFiber);
				endFiber->value.deleteObject();
				fiberContext.nodeMemoryDelete(endFiber);
				--fiberCount;

				if(fiber) {
					continue;
				};
				fiber=fiberContext.head();
				if(fiber) {
					continue;
				};
				break;
			};

			return executeReturn_(context,InstructionError::None);

#endif
			return InstructionError::None;
		};

#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
#else

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD

		void  Executive::threadExecutorNew(VariableThreadInfo *threadInfo_) {
			TMemory<Executive>::memoryInit();
		};

		void  Executive::threadExecutorDelete(VariableThreadInfo *threadInfo_) {
			threadInfo_->terminatedLocal=true;
		};

		void  Executive::threadExecutor(ThreadTransfer *threadTransfer) {
			Executive &executive=ExecutiveX::getExecutive();

			Executive *parent;
			TPointer<Variable> threadThis_;
			TPointer<Variable> threadArguments_;
			TYList2<TPointer<Executive> > *parentThreadNode;
			bool flag;

			parent=(Executive *)threadTransfer->super;
			executive.mainCmdN=parent->mainCmdN;
			executive.mainCmdS=parent->mainCmdS;
			executive.pathExecutable=parent->pathExecutable.value(); // clone
			executive.threadInfo=((VariableThreadInfo *)(threadTransfer->threadInfo));
			executive.threadInfo->returnValueLocal=VariableUndefined::newVariable();
			executive.threadInfo->messageQueueLocal=TMemory<TStack2<TPointerX<Variable> > >::newObject();
			executive.applicationInitExecutive=parent->applicationInitExecutive;

			TYList2<String > *scanIncludePath;
			for(scanIncludePath=parent->includePath->head(); scanIncludePath; scanIncludePath=scanIncludePath->next) {
				executive.includePath->push(scanIncludePath->value.value());
			};

			try {
				executive.initExecutive();

				bool isFile=threadTransfer->isFile;
				String sourceCode=((VariableString *)(threadTransfer->threadFile))->value.value();
				threadThis_.setObject(executive.cloneVariable(*(parent->symbolList),threadTransfer->threadThis));
				threadArguments_.setObject(executive.cloneVariable(*(parent->symbolList),threadTransfer->threadArguments));

				executive.threadInfo->vmLocal=&executive;
				threadTransfer->threadStarted=true;
				// threadTransfer - unsafe to access from here

				flag=true;
				if(threadArguments_) {
					if(threadArguments_->variableType==VariableArray::typeArray) {
						(executive.fiberContext.head())->value->functionContext->functionArguments.setObjectX(static_cast<VariableArray *>(threadArguments_.value()));
						flag=false;
					};
				};
				if(flag) {
					(executive.fiberContext.head())->value->functionContext->functionArguments.setObject(VariableArray::newArray());
				};

				(executive.fiberContext.head())->value->functionContext->this_=threadThis_;

				executive.compileEnd();

				executive.threadInfo->isSuspended=false;
				executive.threadInfo->isTerminated=false;

				try {


					if (executive.execute() == 0) {

						//
						// reset waitChilds for threadWorker message passing
						//
						executive.threadInfo->waitChilds=false;

						TPointerOwner<Variable> retVX(VariableUndefined::newVariable());

						if(isFile) {

							TPointer<VariableArray> arguments;
							arguments.setObject(VariableArray::newArray());
							(arguments->index(0)).setObject(VariableString::newVariable(sourceCode));
							(arguments->index(1))=threadArguments_;
							Variable &script_((Context::getGlobalObject())->operatorReference(Context::getSymbol("Script")));
							retVX=(script_.operatorReference(Context::getSymbol("include"))).functionApply(threadThis_,arguments);

						} else {

							String finalSourceCode="return (";
							finalSourceCode<<sourceCode;
							finalSourceCode<<").apply(this,arguments);";
							TPointer<VariableArray> arguments;
							arguments.setObject(VariableArray::newArray());
							(arguments->index(0)).setObject(VariableString::newVariable(finalSourceCode));
							(arguments->index(1))=threadArguments_;
							Variable &script_((Context::getGlobalObject())->operatorReference(Context::getSymbol("Script")));

							retVX=(script_.operatorReference(Context::getSymbol("execute"))).functionApply(threadThis_,arguments);
						};

						executive.threadInfo->returnValueLocal->decReferenceCount();
						executive.threadInfo->returnValueLocal=retVX.getObject();


						executive.execute_(NULL);

					};

				} catch(const Error &e) {
				} catch (const std::exception &e) {
				} catch (...) {
				};

				executive.threadInfo->isSuspended=true;
				executive.threadInfo->isTerminated=true;

				executive.threadInfo->finalizeLocal=true;
				while(executive.threadInfo->finalizeLocal) {
					XThread::sleepOneMillisecond();
				};

				executive.fiberContext.empty();
				executive.threadInfo->returnValueLocal->decReferenceCount();
				executive.threadInfo->messageQueueLocal->empty();
				executive.threadInfo->messageQueueLocal->decReferenceCount();
				executive.threadInfo->receivedMessage.deleteObject();
				return;

			} catch(const Error &e) {
			} catch (const std::exception &e) {
			} catch (...) {
			};

			threadTransfer->threadStarted=true;
		};

#endif

#endif

		int Executive::compileFile(const char *fileName) {
			Parser parser;
			Input input;
			File in;
			int retV;
			char fullFile[4096];

			retV = VmParserError::Compile;


			if(Shell::realpath(fileName,fullFile,4096)) {

				if (in.openRead(fullFile)) {
					if (input.init(&in)) {
						String strSymbol("#");
						strSymbol << fullFile;
						Symbol symbolSource = Context::getSymbol(strSymbol);
						if (parser.init(&input, assembler, symbolSource)) {
							retV = parser.parse();
						};
					};
					in.close();
				} else {
					retV = VmParserError::FileNotFound;
				};

			} else {
				retV = VmParserError::FileNotFound;
			};

			assembler->resetLinks();

			errorInfo.compileError = retV;
			errorInfo.compileFilePos = input.filePos;
			errorInfo.compileLineNumber = input.lineNumber;
			errorInfo.compileLineColumn = input.lineColumn;
			errorInfo.compileFileName = fileName;
			return retV;
		};

		int Executive::includeAndExecuteFile(InstructionContext *context,const char *fileName) {
			ProgramCounter *linkBegin;
			ProgramCounter *linkStart;
			ProgramCounter *linkEnd;
			ProgramCounter *nextProgramCounter_;
			ProgramCounter *currentProgramCounter_;
			int retV;
			linkBegin = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);
			linkStart = assembler->assemble(ParserAsm::Nop, "", 0, 0, 0, 0); // don't use mark, next instruction can be removed
			retV = compileFile(fileName);
			if (retV == VmParserError::None) {

				linkEnd=assembler->assembleProgramCounter(ParserAsm::InstructionListExtractAndDelete,linkBegin,0,0,0,0);
				assembler->linkProgramCounter(linkBegin, linkEnd);
				assembler->linkProgramCounter(linkEnd,context->nextProgramCounter);
				context->nextProgramCounter = linkStart;
				return 0;
			};

			InstructionList instructionList;
			TYList2<InstructionX> *pcBegin=reinterpret_cast<TYList2<InstructionX> *> (linkBegin);
			TYList2<InstructionX> *pcEnd=reinterpret_cast<TYList2<InstructionX> *> (assembler->assemble(ParserAsm::Nop,"", 0, 0, 0, 0));
			context->instructionListExecutive->extractList(pcBegin,pcEnd);
			instructionList.setList(pcBegin,pcEnd);
			return retV;
		};

		int Executive::executeExtension(InstructionContext *context,QuantumScriptExtensionProc extensionProc,void *extensionId) {
			ProgramCounter *linkBegin;
			ProgramCounter *linkStart;
			ProgramCounter *linkEnd;
			ProgramCounter *nextProgramCounter_;
			ProgramCounter *currentProgramCounter_;
			linkBegin = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);
			linkStart = assembler->assemble(ParserAsm::Nop, "", 0, 0, 0, 0); // don't use mark, next instruction can be removed

			try {

				extensionProc(this,extensionId);
				linkEnd=assembler->assembleProgramCounter(ParserAsm::InstructionListExtractAndDelete,linkBegin,0,0,0,0);
				assembler->linkProgramCounter(linkBegin, linkEnd);
				assembler->linkProgramCounter(linkEnd,context->nextProgramCounter);
				context->nextProgramCounter = linkStart;
				return 0;

			} catch(...) {};

			InstructionList instructionList;
			TYList2<InstructionX> *pcBegin=reinterpret_cast<TYList2<InstructionX> *> (linkBegin);
			TYList2<InstructionX> *pcEnd=reinterpret_cast<TYList2<InstructionX> *> (assembler->assemble(ParserAsm::Nop,"", 0, 0, 0, 0));
			context->instructionListExecutive->extractList(pcBegin,pcEnd);
			instructionList.setList(pcBegin,pcEnd);
			return VmParserError::Compile;
		};

		int Executive::compileString(const char *data_) {
			Parser parser;
			Input input;
			MemoryFileRead in;
			int retV;

			retV = VmParserError::Compile;

			if (in.openRead(data_, StringBase::length(data_))) {
				if (input.init(&in)) {
					String strSymbol("@");
					strSymbol << data_;
					Symbol symbolSource = Context::getSymbol(strSymbol);
					if (parser.init(&input, assembler, symbolSource)) {
						retV = parser.parse();
					};
				};
				in.close();
			};
			assembler->resetLinks();

			errorInfo.compileError = retV;
			errorInfo.compileFilePos = input.filePos;
			errorInfo.compileLineNumber = input.lineNumber;
			errorInfo.compileLineColumn = input.lineColumn;
			errorInfo.compileFileName = "";

			return retV;
		};

		int Executive::includeAndExecuteString(InstructionContext *context,const char *data_) {
			ProgramCounter *linkBegin;
			ProgramCounter *linkStart;
			ProgramCounter *linkEnd;
			ProgramCounter *nextProgramCounter_;
			ProgramCounter *currentProgramCounter_;
			int retV;
			linkBegin = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);
			linkStart = assembler->assemble(ParserAsm::Nop, "", 0, 0, 0, 0); // don't use mark, next instruction can be removed

			retV = compileString(data_);
			if (retV == 0) {
				linkEnd=assembler->assembleProgramCounter(ParserAsm::InstructionListExtractAndDelete,linkBegin,0,0,0,0);
				assembler->linkProgramCounter(linkBegin, linkEnd);
				assembler->linkProgramCounter(linkEnd,context->nextProgramCounter);
				context->nextProgramCounter = linkStart;
				return 0;
			};

			InstructionList instructionList;
			TYList2<InstructionX> *pcBegin=reinterpret_cast<TYList2<InstructionX> *> (linkBegin);
			TYList2<InstructionX> *pcEnd=reinterpret_cast<TYList2<InstructionX> *> (assembler->assemble(ParserAsm::Nop,"", 0, 0, 0, 0));
			context->instructionListExecutive->extractList(pcBegin,pcEnd);
			instructionList.setList(pcBegin,pcEnd);
			return retV;
		};


		void Executive::setVmFunction(const char *name, InstructionProcedure procedure, TPointer<Variable> operand) {
			ProgramCounter *linkFunctionBegin;
			ProgramCounter *linkFunctionEnd;
			ProgramCounter *linkFunctionEnter;

			String buf;
			char bufx[2];
			int lk;
			int k;
			int isFirst;

			int isEmpty;
			int retV;
			int level;
			char sLevel[32];

			retV = 0;
			isFirst = 1;
			bufx[1]=0;

			for (k = 0; name[k] != 0; ++k) {
				if (name[k] == '(') {
					break;
				};
				if (name[k] == '.') {

					if (isFirst) {
						isFirst = 0;
						assembler->assemble(ParserAsm::PushSymbol, buf, 0, 0, 0, 0);
					} else {
						assembler->assemble(ParserAsm::Reference, buf, 0, 0, 0, 0);
					};

					buf="";
					continue;
				};
				bufx[0]=name[k];
				buf<<bufx;
			};

			if (isFirst) {
				assembler->assemble(ParserAsm::PushObjectReference, buf, 0, 0, 0, 0);
			} else {
				assembler->assemble(ParserAsm::ReferenceObjectReference, buf, 0, 0, 0, 0);
			};

			linkFunctionBegin = assembler->assembleProgramCounter(ParserAsm::XPushFunction, NULL,0,0,0,0);
			linkFunctionEnd = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);

			assembler->linkProgramCounter(linkFunctionBegin,
						      assembler->assemble(ParserAsm::Mark, "", 0, 0, 0, 0)
						     );

			isEmpty = 0;
			if (name[k] == '(') {
				++k;
				if (name[k] == 0) {
					isEmpty = 1;
				} else if (name[k] == ')') {
					isEmpty = 1;
				};
			} else {
				isEmpty = 1;
			};

			level = 0;

			if (isEmpty) {
			} else {
				buf = "";
				for (; name[k] != 0; ++k) {
					if (name[k] == ')') {

						/* - no need - */

						break;
					};
					if (name[k] == ',') {

						/* - no need - */

						buf = "";
						continue;
					};
					bufx[0]= name[k];
					buf<<bufx;
				};

			};

			assembler->assembleDirect(InstructionVmCallNative, VariableNativeVmFunction::newVariable(procedure, operand));

			assembler->assemble(ParserAsm::PushUndefined, "", 0, 0, 0, 0);
			assembler->linkProgramCounterEnd(linkFunctionBegin,assembler->assemble(ParserAsm::Return, "", 0, 0, 0, 0));


			assembler->linkProgramCounter(linkFunctionEnd,assembler->assemble(ParserAsm::Assign, "", 0, 0, 0, 0));
		};

		int Executive::setVmFunctionFromFile(const char *name, const char *fileName) {
			ProgramCounter *linkFunctionBegin;
			ProgramCounter *linkFunctionEnd;
			ProgramCounter *linkFunctionEnter;

			String buf;
			char bufx[2];
			int lk;
			int k;
			int isFirst;

			int isEmpty;
			int retV;
			int level;
			char sLevel[32];

			retV = 0;
			isFirst = 1;

			bufx[1] = 0;

			for (k = 0; name[k] != 0; ++k) {
				if (name[k] == '(') {
					break;
				};
				if (name[k] == '.') {

					if (isFirst) {
						isFirst = 0;
						assembler->assemble(ParserAsm::PushSymbol, buf, 0, 0, 0, 0);
					} else {
						assembler->assemble(ParserAsm::Reference, buf, 0, 0, 0, 0);
					};

					buf="";
					continue;
				};
				bufx[0]=name[k];
				buf<<bufx;
			};

			if (isFirst) {
				assembler->assemble(ParserAsm::PushObjectReference, buf, 0, 0, 0, 0);
			} else {
				assembler->assemble(ParserAsm::ReferenceObjectReference, buf, 0, 0, 0, 0);
			};

			linkFunctionBegin = assembler->assembleProgramCounter(ParserAsm::XPushFunction, NULL,0,0,0,0);

			linkFunctionEnd = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);

			assembler->linkProgramCounter(linkFunctionBegin,
						      assembler->assemble(ParserAsm::Mark, "", 0, 0, 0, 0)
						     );

			isEmpty = 0;
			if (name[k] == '(') {
				++k;
				if (name[k] == 0) {
					isEmpty = 1;
				} else if (name[k] == ')') {
					isEmpty = 1;
				};
			} else {
				isEmpty = 1;
			};

			level = 0;

			if (isEmpty) {
			} else {
				buf = "";
				for (; name[k] != 0; ++k) {
					if (name[k] == ')') {

						/* - no need - */

						break;
					};
					if (name[k] == ',') {

						/* - no need - */

						buf = "";
						continue;
					};
					bufx[0] = name[k];
					buf<<bufx;
				};

			};

			retV = compileFile(fileName);

			assembler->assemble(ParserAsm::PushUndefined, "", 0, 0, 0, 0);
			assembler->linkProgramCounterEnd(linkFunctionBegin,assembler->assemble(ParserAsm::Return, "", 0, 0, 0, 0));

			assembler->linkProgramCounter(linkFunctionEnd,
						      assembler->assemble(ParserAsm::Assign, "", 0, 0, 0, 0)
						     );

			return retV;
		};

		int Executive::setVmFunctionFromString(const char *name, const char *data) {
			ProgramCounter *linkFunctionBegin;
			ProgramCounter *linkFunctionEnd;
			ProgramCounter *linkFunctionEnter;

			String buf;
			char bufx[2];
			int lk;
			int k;
			int isFirst;

			int isEmpty;
			int retV;
			int level;
			char sLevel[32];

			retV = 0;
			isFirst = 1;
			bufx[1]=0;

			for (k = 0; name[k] != 0; ++k) {
				if (name[k] == '(') {
					break;
				};
				if (name[k] == '.') {

					if (isFirst) {
						isFirst = 0;
						assembler->assemble(ParserAsm::PushSymbol, buf, 0, 0, 0, 0);
					} else {
						assembler->assemble(ParserAsm::Reference, buf, 0, 0, 0, 0);
					};

					buf = "";
					continue;
				};
				bufx[0] = name[k];
				buf<<bufx;
			};

			if (isFirst) {
				assembler->assemble(ParserAsm::PushObjectReference, buf, 0, 0, 0, 0);
			} else {
				assembler->assemble(ParserAsm::ReferenceObjectReference, buf, 0, 0, 0, 0);
			};



			linkFunctionBegin = assembler->assembleProgramCounter(ParserAsm::XPushFunction, NULL,0,0,0,0);

			linkFunctionEnd = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);

			assembler->linkProgramCounter(linkFunctionBegin,
						      assembler->assemble(ParserAsm::Mark, "", 0, 0, 0, 0)
						     );

			isEmpty = 0;
			if (name[k] == '(') {
				++k;
				if (name[k] == 0) {
					isEmpty = 1;
				} else if (name[k] == ')') {
					isEmpty = 1;
				};
			} else {
				isEmpty = 1;
			};

			level = 0;

			if (isEmpty) {
			} else {
				buf = "";
				for (; name[k] != 0; ++k) {
					if (name[k] == ')') {

						/* - no need - */

						break;
					};
					if (name[k] == ',') {

						/* - no need - */

						buf = "";
						continue;
					};
					bufx[0] = name[k];
					buf<<bufx;
				};

			};

			retV = compileString(data);

			assembler->assemble(ParserAsm::PushUndefined, "", 0, 0, 0, 0);
			assembler->linkProgramCounterEnd(linkFunctionBegin,assembler->assemble(ParserAsm::Return, "", 0, 0, 0, 0));

			assembler->linkProgramCounter(linkFunctionEnd,
						      assembler->assemble(ParserAsm::Assign, "", 0, 0, 0, 0)
						     );

			return retV;
		};

		int Executive::setVmFunctionFromFileX(InstructionContext *context,const char *name,const char *fileName) {
			ProgramCounter *linkBegin;
			ProgramCounter *linkStart;
			ProgramCounter *linkEnd;
			ProgramCounter *nextProgramCounter_;
			ProgramCounter *currentProgramCounter_;
			int retV;
			linkBegin = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);
			linkStart = assembler->assemble(ParserAsm::Nop, "", 0, 0, 0, 0); // don't use mark, next instruction can be removed
			retV = setVmFunctionFromFile(name,fileName);
			if (retV == VmParserError::None) {

				linkEnd=assembler->assembleProgramCounter(ParserAsm::InstructionListExtractAndDelete,linkBegin,0,0,0,0);
				assembler->linkProgramCounter(linkBegin, linkEnd);
				assembler->linkProgramCounter(linkEnd,context->nextProgramCounter);
				context->nextProgramCounter = linkStart;
				return 0;
			};

			InstructionList instructionList;
			TYList2<InstructionX> *pcBegin=reinterpret_cast<TYList2<InstructionX> *> (linkBegin);
			TYList2<InstructionX> *pcEnd=reinterpret_cast<TYList2<InstructionX> *> (assembler->assemble(ParserAsm::Nop,"", 0, 0, 0, 0));
			context->instructionListExecutive->extractList(pcBegin,pcEnd);
			instructionList.setList(pcBegin,pcEnd);
			return retV;
		};

		int Executive::setVmFunctionFromStringX(InstructionContext *context,const char *name,const char *fileName) {
			ProgramCounter *linkBegin;
			ProgramCounter *linkStart;
			ProgramCounter *linkEnd;
			ProgramCounter *nextProgramCounter_;
			ProgramCounter *currentProgramCounter_;
			int retV;
			linkBegin = assembler->assembleProgramCounter(ParserAsm::Goto, NULL,0,0,0,0);
			linkStart = assembler->assemble(ParserAsm::Nop, "", 0, 0, 0, 0); // don't use mark, next instruction can be removed
			retV = setVmFunctionFromString(name,fileName);
			if (retV == VmParserError::None) {

				linkEnd=assembler->assembleProgramCounter(ParserAsm::InstructionListExtractAndDelete,linkBegin,0,0,0,0);
				assembler->linkProgramCounter(linkBegin, linkEnd);
				assembler->linkProgramCounter(linkEnd,context->nextProgramCounter);
				context->nextProgramCounter = linkStart;
				return 0;
			};

			InstructionList instructionList;
			TYList2<InstructionX> *pcBegin=reinterpret_cast<TYList2<InstructionX> *> (linkBegin);
			TYList2<InstructionX> *pcEnd=reinterpret_cast<TYList2<InstructionX> *> (assembler->assemble(ParserAsm::Nop,"", 0, 0, 0, 0));
			context->instructionListExecutive->extractList(pcBegin,pcEnd);
			instructionList.setList(pcBegin,pcEnd);
			return retV;
		};

		TPointerOwner<Variable> Executive::callVmFunction(TPointer<Variable> fnProcedure,TPointer<Variable> fnThis,TPointer<Variable> fnParameters) {
			if(currentFiberExit==NULL) {
				return NULL;
			};
			if(fnProcedure) {
				if(fnProcedure->variableType==VariableVmFunction::typeVmFunction) {
					if(fnParameters) {
						if(fnParameters->variableType==VariableUndefined::typeUndefined) {
							fnParameters.setObject(VariableArray::newVariable());
						};
					} else {
						fnParameters.setObject(VariableArray::newVariable());
					};
					if(fnParameters->variableType==VariableArray::typeArray) {
						TPointer<InstructionContext> fnFiber;
						fnFiber.newObject();

						fnFiber->configPrintStackTraceLimit=configPrintStackTraceLimit;
						fnFiber->executiveSuper=this;
						fnFiber->init();
						fnFiber->contextStack.newObject();
						fnFiber->contextStack->enterMaster(fnFiber->functionContext);
						fnFiber->functionContext->this_=Context::getValueUndefined();
						fnFiber->pcContext=fnFiber->functionContext;
						fnFiber->pcContext->pc_=currentFiberExit;
						fnFiber->instructionListExecutive=assembler->instructionList.value();

						fnFiber->fiberInfo->isSuspended=false;
#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
						fnFiber->threadInfo=threadInfo;
#endif

						fnFiber->error = InstructionError::None;
						fnFiber->currentProgramCounter=NULL;
						fnFiber->nextProgramCounter=currentFiberExit;

						TPointer<Variable> functionArguments;
						functionArguments.setObject(VariableArray::newVariable());
						(*(((VariableArray *)(functionArguments.value()))->value))[0]=fnParameters;

						if(fnThis) {
						} else {
							fnThis.setObject(VariableUndefined::newVariable());
						};
						// apply
						fnFiber->pushX(fnProcedure);
						fnFiber->pushX(fnThis);
						fnFiber->pushX(functionArguments);
						InstructionVmXCallThisModeApply(fnFiber,NULL);
						fnFiber->currentProgramCounter=fnFiber->nextProgramCounter;

#ifdef QUANTUM_SCRIPT_SINGLE_FIBER
#else
						fiberCount++;
						fiberContext.pushX(fnFiber);
#endif
						// -- executor
						int error=Executive::execute_(fnFiber.value());
						if(error==InstructionError::None) {
							return fnFiber->fiberInfo->returnValue;
						};
						if(error==InstructionError::Throw) {
							TPointerOwner<Variable> throwValue;
							fnFiber->popX(throwValue);
							// catch, native throw
							if(fnFiber->stackTrace) {
								fnFiber->stackTrace->popEmpty();
							};
							TPointerOwner<VariableStackTrace> stackTrace((VariableStackTrace *)VariableStackTrace::newVariable(fnFiber->stackTrace, fnFiber));
							fnFiber->stackTrace.deleteObject();
							stackTrace->configPrintStackTraceLimit=fnFiber->configPrintStackTraceLimit;
							ExecutiveX::setStackTrace(stackTrace->toString());
							stackTrace.deleteObject();
							throw Error(
								(&throwValue->operatorReference(Context::getSymbol("message")))->toString()
							);
						};

					};
				};
			};
			return NULL;
		};

		void Executive::setFunction(const char *name, Variable *nativeFunction) {
			if(nativeFunction->variableType==VariableVmFunction::typeVmFunction) {
				throw Error("setFunction require native function");
			};

			String buf;
			char bufx[2];
			int lk;
			int k;
			int isFirst;

			int isEmpty;
			int retV;
			int level;
			char sLevel[32];

			retV = 0;
			isFirst = 1;

			bufx[1] = 0;

			for (k = 0; name[k] != 0; ++k) {
				if (name[k] == '(') {
					break;
				};
				if (name[k] == '.') {

					if (isFirst) {
						isFirst = 0;
						assembler->assemble(ParserAsm::PushSymbol, buf, 0, 0, 0, 0);
					} else {
						assembler->assemble(ParserAsm::Reference, buf, 0, 0, 0, 0);
					};

					buf = "";
					continue;
				};
				bufx[0] = name[k];
				buf<<bufx;
			};

			if (isFirst) {
				assembler->assemble(ParserAsm::PushObjectReference, buf, 0, 0, 0, 0);
			} else {
				assembler->assemble(ParserAsm::ReferenceObjectReference, buf, 0, 0, 0, 0);
			};

			nativeFunction->incReferenceCount();
			assembler->assembleDirect(InstructionVmSetFunction, nativeFunction);
		};

		void Executive::compileStringX(const char *source) {
			if(compileString(source)!=0) {
				throw Error("Compile error");
			};
		};

		void Executive::setFunction2(const char *name,FunctionProcedure functionProcedure) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunction::newVariable(NULL,NULL,NULL,functionProcedure,NULL,NULL));
			setFunction(name,functionV);
		};

		void Executive::setFunction3(const char *name,FunctionProcedure functionProcedure,Object *super) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunction::newVariable(NULL,NULL,NULL,functionProcedure,super,NULL));
			setFunction(name,functionV);
		};

		void Executive::setFunction4(const char *name,FunctionProcedure functionProcedure,void *valueSuper) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunction::newVariable(NULL,NULL,NULL,functionProcedure,NULL,valueSuper));
			setFunction(name,functionV);
		};

		void Executive::setFunction5(const char *name,FunctionProcedure functionProcedure,Object *super,void *valueSuper) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunction::newVariable(NULL,NULL,NULL,functionProcedure,super,valueSuper));
			setFunction(name,functionV);
		};

		void Executive::setFunctionWithYield2(const char *name,FunctionProcedureWithYield functionProcedureWithYield) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunctionWithYield::newVariable(NULL,NULL,NULL,functionProcedureWithYield,NULL,NULL));
			setFunction(name,functionV);
		};

		void Executive::setFunctionWithYield3(const char *name,FunctionProcedureWithYield functionProcedureWithYield,Object *super) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunctionWithYield::newVariable(NULL,NULL,NULL,functionProcedureWithYield,super,NULL));
			setFunction(name,functionV);
		};

		void Executive::setFunctionWithYield4(const char *name,FunctionProcedureWithYield functionProcedureWithYield,void *valueSuper) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunctionWithYield::newVariable(NULL,NULL,NULL,functionProcedureWithYield,NULL,valueSuper));
			setFunction(name,functionV);
		};

		void Executive::setFunctionWithYield5(const char *name,FunctionProcedureWithYield functionProcedureWithYield,Object *super,void *valueSuper) {
			TPointer<Variable> functionV;
			functionV.setObject(VariableFunctionWithYield::newVariable(NULL,NULL,NULL,functionProcedureWithYield,NULL,valueSuper));
			setFunction(name,functionV);
		};

		void Executive::compileEnd() {
			assembler->assemble(ParserAsm::PushUndefined, "", 0, 0, 0, 0);
			assembler->assemble(ParserAsm::CurrentFiberExit, "", 0, 0, 0, 0);
		};


		void Executive::memoryInit() {
			//
			// not really required
			//
			Context::memoryInit();
			TMemory<TStack2<String > >::memoryInit();
			TMemory<InstructionContext>::memoryInit();
			TMemory<TStack2<TPointer<InstructionContext> > >::memoryInit();
			TMemory<TStack2<VariableThreadInfo *> >::memoryInit();
			TMemory<InstructionList>::memoryInit();
			Parser::memoryInit();
			TMemory<ArrayIteratorKey>::memoryInit();
			TMemory<ArrayIteratorValue>::memoryInit();
			TMemory<ObjectIteratorKey>::memoryInit();
			TMemory<ObjectIteratorValue>::memoryInit();
			TMemory<Variable>::memoryInit();
			TMemory<VariableNull>::memoryInit();
			TMemory<VariableBoolean>::memoryInit();
			TMemory<VariableNumber>::memoryInit();
			TMemory<VariableString>::memoryInit();
			TMemory<VariableSymbol>::memoryInit();
			TMemory<VariableArray>::memoryInit();
			TMemory<VariableAssociativeArray>::memoryInit();
			TMemory<VariableObject>::memoryInit();
			TMemory<VariableFunction>::memoryInit();
			TMemory<VariableFunctionWithYield>::memoryInit();
			TMemory<VariableResource>::memoryInit();
			TMemory<VariableDateTime>::memoryInit();
			TMemory<VariableArgumentLevel>::memoryInit();
			TMemory<VariableFiberInfo>::memoryInit();
			TMemory<VariableNativeVmFunction>::memoryInit();
			TMemory<VariableOperator21>::memoryInit();
			TMemory<VariableOperator22>::memoryInit();
			TMemory<VariableOperator23>::memoryInit();
			TMemory<VariableOperator31>::memoryInit();
			TMemory<VariableReferenceObject>::memoryInit();
			TMemory<VariableStackTrace>::memoryInit();
			TMemory<VariableThreadInfo>::memoryInit();
			TMemory<VariableVmProgramCounter>::memoryInit();
			TMemory<VariableVmFunction>::memoryInit();
			TMemory<Stack>::memoryInit();
			TMemory<Array>::memoryInit();
			ExecutiveContext::memoryInit();
		};

	};
};




