//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBQUANTUM_SCRIPT_HPP
#define LIBQUANTUM_SCRIPT_HPP

#include "libquantum-script-licence.hpp"
#include "libquantum-script-copyright.hpp"
#ifndef LIBQUANTUM_SCRIPT_NO_VERSION
#include "libquantum-script-version.hpp"
#endif

#include "quantum-script-executivex.hpp"

#endif

