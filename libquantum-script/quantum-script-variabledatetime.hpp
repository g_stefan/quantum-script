﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEDATETIME_HPP
#define QUANTUM_SCRIPT_VARIABLEDATETIME_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableDateTime;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableDateTime>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableDateTime> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableDateTime :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableDateTime);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeDateTime;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeDateTime;

				DateTime value;

				inline VariableDateTime() {
					variableType = typeDateTime;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
