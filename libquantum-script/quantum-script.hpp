//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_HPP
#define QUANTUM_SCRIPT_HPP

#ifndef LIBXYO_XY_HPP
#include "libxyo-xy.hpp"
#endif

#ifndef LIBXYO_XO_HPP
#include "libxyo-xo.hpp"
#endif

#ifndef QUANTUM_SCRIPT__EXPORT_HPP
#include "quantum-script--export.hpp"
#endif

#ifndef QUANTUM_SCRIPT__CONFIG_HPP
#include "quantum-script--config.hpp"
#endif

#ifndef QUANTUM_SCRIPT_DEFAULT_STACK_TRACE_LEVEL
#   define QUANTUM_SCRIPT_DEFAULT_STACK_TRACE_LEVEL 16
#endif

#ifdef QUANTUM_SCRIPT__SINGLE_FIBER
#   ifndef QUANTUM_SCRIPT__SINGLE_THREAD
#       define QUANTUM_SCRIPT__SINGLE_THREAD
#   endif
#endif

#ifdef XYO_SINGLE_THREAD
#   ifndef QUANTUM_SCRIPT__SINGLE_THREAD
#       define QUANTUM_SCRIPT__SINGLE_THREAD
#   endif
#endif

#ifdef QUANTUM_SCRIPT__SINGLE_THREAD
#   ifndef XYO_SINGLE_THREAD
#       define XYO_SINGLE_THREAD
#   endif
#endif

#endif
