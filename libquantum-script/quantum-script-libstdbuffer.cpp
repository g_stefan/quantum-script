﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdbuffer.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablebuffer.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdBuffer {

			using namespace XYO::XY;
			using namespace XYO::XO;


			static TPointerOwner<Variable> getByte(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-get-byte\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				Number x=(arguments->index(0))->toNumber();

				if(isnan(x)||isinf(x)||signbit(x)) {
					return VariableNumber::newVariable(0);
				};
				if(x>=((VariableBuffer *)this_)->length) {
					return VariableNumber::newVariable(0);
				};
				return VariableNumber::newVariable(((VariableBuffer *)this_)->buffer[(int)x]);
			};

			static TPointerOwner<Variable> setByte(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-set-byte\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				Number x=(arguments->index(0))->toNumber();
				Number v=(arguments->index(1))->toNumber();

				if(isnan(x)||isinf(x)||signbit(x)) {
					return Context::getValueUndefined();
				};
				if(x>=((VariableBuffer *)this_)->length) {
					return Context::getValueUndefined();
				};
				if(isnan(v)) {
					v=0;
				};
				if(isinf(v)) {
					v=0xFF;
				};
				if(signbit(v)) {
					v=0;
				};
				((VariableBuffer *)this_)->buffer[(int)x]=(byte)v;
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> xorBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-xor\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				TPointerX<Variable> &buffer(arguments->index(0));

				if(buffer->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				StringBaseByte::xorBuffer(((VariableBuffer *)this_)->buffer,((VariableBuffer *)this_)->length,((VariableBuffer *)buffer.value())->buffer,((VariableBuffer *)buffer.value())->length);
				return this_->getObject();
			};

			static TPointerOwner<Variable> xorAvalancheEncode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-xor\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				StringBaseByte::xorAvalancheBufferEncode(((VariableBuffer *)this_)->buffer,((VariableBuffer *)this_)->length);
				return this_->getObject();
			};


			static TPointerOwner<Variable> xorAvalancheDecode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-xor\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				StringBaseByte::xorAvalancheBufferDecode(((VariableBuffer *)this_)->buffer,((VariableBuffer *)this_)->length);
				return this_->getObject();
			};


			static TPointerOwner<Variable> fromHex(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-from-hex\n");
#endif

				String input=(arguments->index(0))->toString();
				size_t ln=input.length()/2;
				if(ln==0) {
					return Context::getValueUndefined();
				};
				TPointerOwner<Variable> retV(VariableBuffer::newVariable(ln));
				size_t m;
				size_t k;
				unsigned int h1,h2;

				for(k=0,m=0; m<ln*2; m+=2,++k) {
					h1=input[m];
					if(h1>='0'&&h1<='9') {
						h1-='0';
					} else if(h1>='A'&&h1<='F') {
						h1-='A';
						h1+=10;
					} else if(h1>='a'&&h1<='f') {
						h1-='a';
						h1+=10;
					} else {
						h1=0;
					};
					h2=input[m+1];
					if(h2>='0'&&h2<='9') {
						h2-='0';
					} else if(h2>='A'&&h2<='F') {
						h2-='A';
						h2+=10;
					} else if(h2>='a'&&h2<='f') {
						h2-='a';
						h2+=10;
					} else {
						h2=0;
					};

					((VariableBuffer *)retV.value())->buffer[k]=(XYO::byte)(h1<<4|h2);
				};

				((VariableBuffer *)retV.value())->length=ln;

				return retV;
			};

			static TPointerOwner<Variable> toHex(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-to-hex\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};
				String output_(((VariableBuffer *)this_)->length*2+1,0);
				size_t k;
				unsigned int h1;
				unsigned int h2;
				for(k=0; k<((VariableBuffer *)this_)->length; ++k) {
					h1=(((VariableBuffer *)this_)->buffer[k]>>4)&0x0F;
					if(h1>9) {
						h1-=10;
						h1+='a';
					} else {
						h1+='0';
					};
					h2=(((VariableBuffer *)this_)->buffer[k])&0x0F;
					if(h2>9) {
						h2-=10;
						h2+='a';
					} else {
						h2+='0';
					};
					output_[k*2]=(XYO::byte)h1;
					output_[k*2+1]=(XYO::byte)h2;
				};
				output_[k*2]=0;
				output_.setLength(k*2);

				return VariableString::newVariable(output_);
			};


			static TPointerOwner<Variable> setLength(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-set-length\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				Number x=(arguments->index(0))->toNumber();

				if(isnan(x)||isinf(x)||signbit(x)) {
					return VariableNumber::newVariable(0);
				};
				if(x>((VariableBuffer *)this_)->size) {
					return VariableNumber::newVariable(0);
				};

				((VariableBuffer *)this_)->length=x;
				return VariableNumber::newVariable(((VariableBuffer *)this_)->length);
			};

			static TPointerOwner<Variable> toString(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-to-string\n");
#endif

				if(this_->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				String retV(((VariableBuffer *)this_)->length+1,0);
				memcpy(retV.value(),((VariableBuffer *)this_)->buffer,((VariableBuffer *)this_)->length);
				retV[((VariableBuffer *)this_)->length]=0;
				retV.setLength(((VariableBuffer *)this_)->length);

				return VariableString::newVariable(retV);
			};

			static TPointerOwner<Variable> fromString(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- buffer-from-string\n");
#endif
				return VariableBuffer::newObjectVFromString((arguments->index(0))->toString());
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction2("Buffer.prototype.getByte(pos)", getByte);
				executive->setFunction2("Buffer.prototype.setByte(pos,value)", setByte);
				executive->setFunction2("Buffer.prototype.xor(buffer)", xorBuffer);
				executive->setFunction2("Buffer.prototype.fromHex(str)", fromHex);
				executive->setFunction2("Buffer.prototype.toHex()", toHex);
				executive->setFunction2("Buffer.prototype.setLength(ln)", setLength);
				executive->setFunction2("Buffer.prototype.toString()", toString);
				executive->setFunction2("Buffer.prototype.fromString()", fromString);
				executive->setFunction2("Buffer.prototype.xorAvalancheEncode()", xorAvalancheEncode);
				executive->setFunction2("Buffer.prototype.xorAvalancheDecode()", xorAvalancheDecode);
			};

		};
	};
};


