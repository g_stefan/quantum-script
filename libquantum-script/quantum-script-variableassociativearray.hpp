﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEASSOCIATIVEARRAY_HPP
#define QUANTUM_SCRIPT_VARIABLEASSOCIATIVEARRAY_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace XYO {
	namespace XY {

		template<>
		class TComparator<Quantum::Script::Variable> {
			public:

				inline static bool isLessThan(const Quantum::Script::Variable &a,const Quantum::Script::Variable &b) {
					return ((const_cast<Quantum::Script::Variable &>(a)).compare(&(const_cast<Quantum::Script::Variable &>(b)))<0);
				};

				inline static int compare(const Quantum::Script::Variable &a,const Quantum::Script::Variable &b) {
					return (const_cast<Quantum::Script::Variable &>(a)).compare(&(const_cast<Quantum::Script::Variable &>(b)));
				};
		};

	};
};

namespace Quantum {
	namespace Script {

		class VariableAssociativeArray;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableAssociativeArray>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableAssociativeArray> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		typedef TPointerX<TAssociativeArray<TPointerX<Variable>,TPointerX<Variable>,4,TXMemoryPoolActive> >  AssociativeArray;

		class VariableAssociativeArray :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableAssociativeArray);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeAssociativeArray;
				TPointer<Variable> vLength;
			public:
				AssociativeArray value;
				QUANTUM_SCRIPT_EXPORT static const void *typeAssociativeArray;

				inline VariableAssociativeArray() {
					variableType = typeAssociativeArray;
					value.memoryLink(this);
					value.newObject();
				};

				inline void activeDestructor() {
					value->activeDestructor();
					vLength.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceIndex(Variable *variable);
				QUANTUM_SCRIPT_EXPORT bool operatorDeleteIndex(Variable *variable);
				QUANTUM_SCRIPT_EXPORT Variable &operatorIndex2(Variable *variable);

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();
				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorKey();
				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorValue();


				QUANTUM_SCRIPT_EXPORT static void memoryInit();


				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};



#endif
