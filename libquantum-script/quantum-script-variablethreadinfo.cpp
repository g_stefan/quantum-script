﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variablethreadinfo.hpp"
#include "quantum-script-prototype.hpp"
#include "quantum-script-context.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableThreadInfo::typeThreadInfo= "{C8137EEA-5A92-4B2E-9447-36DC6D82D6E6}";
		const char *VariableThreadInfo::strTypeThreadInfo="ThreadInfo";

		String VariableThreadInfo::getType() {
			return strTypeThreadInfo;
		};

		Variable *VariableThreadInfo::newVariable() {
			return (Variable *) TMemory<VariableThreadInfo>::newObject();
		};

		Variable &VariableThreadInfo::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,prototype->prototype);
		};

		Variable *VariableThreadInfo::instancePrototype() {
			return prototype->prototype;
		};

		bool VariableThreadInfo::hasMessage() {
			if(isSendMessage) {
				return true;
			};
			return (!messageQueueLocal->isEmpty());
		};

		TPointerOwner<Variable> VariableThreadInfo::getMessageX() {
			TPointerOwner<Variable> retV;
			if(isSendMessage) {
				typeOfMessage=true;
				return receivedMessage;
			};
			typeOfMessage=false;
			if(messageQueueLocal->popFromTail(retV)) {
				return retV;
			};
			return Context::getValueUndefined();
		};

		bool VariableThreadInfo::toBoolean() {
			return true;
		};

		String VariableThreadInfo::toString() {
			return strTypeThreadInfo;
		};


	};
};


