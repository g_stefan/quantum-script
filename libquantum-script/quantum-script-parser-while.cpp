//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-parser.hpp"
#include "quantum-script-parserasm.hpp"

namespace Quantum {
	namespace Script {


		bool Parser::statementWhile() {
			if (token.isSymbolX("while")) {
				ProgramCounter *linkWhile;
				ProgramCounter *linkWhileIf;
				ProgramCounter *linkWhileEnd;
				ProgramCounter *linkWhileBreak;

				linkWhile = assemble(ParserAsm::Mark);
				if (expressionParentheses()) {

					linkWhileIf = assembleProgramCounter(ParserAsm::IfFalseGoto, NULL);

					if (token.checkIs1("{")) {

						assemble(ParserAsm::EnterContext);

						linkWhileBreak = assembleProgramCounter(ParserAsm::ContextSetBreak, NULL);
						assembleProgramCounter(ParserAsm::ContextSetContinue, linkWhile);

						if (isBlockStatement()) {

							assemble(ParserAsm::LeaveContext);
							assembleProgramCounter(ParserAsm::Goto, linkWhile);
							linkWhileEnd = assemble(ParserAsm::Mark);

							linkProgramCounter(linkWhileBreak, linkWhileEnd);
							linkProgramCounter(linkWhileIf, linkWhileEnd);

							return true;

						};
						error = ParserError::Compile;
						return false;
					};

					if (statementOrExpression()) {
						assembleProgramCounter(ParserAsm::Goto, linkWhile);
						linkWhileEnd = assemble(ParserAsm::Mark);
						linkProgramCounter(linkWhileIf, linkWhileEnd);
						return true;
					};


				};
				error = ParserError::Compile;
				return false;
			};
			return false;
		};

	};

};


