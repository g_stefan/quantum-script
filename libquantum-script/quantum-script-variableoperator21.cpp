﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableoperator21.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableOperator21::typeOperator21="{C271EE0A-5253-4C82-9A70-348075010527}";

		Variable *VariableOperator21::newVariable() {
			return (Variable *) TMemory<VariableOperator21>::newObject();
		};

		bool VariableOperator21::toBoolean() {
			return true;
		};


		String VariableOperator21::toString() {
			return strTypeUndefined;
		};


	};
};


