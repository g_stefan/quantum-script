﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-asm.hpp"
#include "quantum-script-parserasm.hpp"
#include "quantum-script-programcounter.hpp"
#include "quantum-script-instructionx.hpp"

#include "quantum-script-variablesymbol.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-variableargumentlevel.hpp"
#include "quantum-script-variablevmprogramcounter.hpp"
#include "quantum-script-variablevmfunction.hpp"
#include "quantum-script-variableoperator21.hpp"
#include "quantum-script-variableoperator22.hpp"
#include "quantum-script-variableoperator23.hpp"
#include "quantum-script-variableoperator31.hpp"


//#define QUANTUM_SCRIPT_DEBUG_ASM 1

#define QUANTUM_SCRIPT_ASM_INSTRUCTION(Type,Operand) \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.asmType=ParserAsm::Type;\
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.procedure=InstructionVm##Type;\
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.operand=Operand; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourceSymbol=sourceSymbol; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourcePos=sourcePos; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourceLineNumber=sourceLineNumber; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourceLineColumn=sourceLineColumn;

#define QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(Type,Operand) \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.asmType=ParserAsm::Type;\
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.procedure=InstructionVm##Type;\
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.operand=Operand; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourceSymbol=sourceSymbol; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourcePos=sourcePos; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourceLineNumber=sourceLineNumber; \
	reinterpret_cast<TYList2<InstructionX> *>(pc)->value.sourceLineColumn=sourceLineColumn;


namespace XYO {
	namespace XY {

		template<> class TComparator<Quantum::Script::ProgramCounter *> {
			public:
				typedef Quantum::Script::ProgramCounter *T;

				inline static bool isLessThan(const T &a,const T &b) {
					return (compare(a,b)<0);
				};

				inline static int compare(const T &a,const T &b) {
					return ((byte *)(const_cast<T>(a)))-((byte *)const_cast<T>(b));
				};
		};

	};
};


namespace Quantum {
	namespace Script {

		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		Asm::Asm() {
			lastIsMark_ = false;
			asmLink_.empty();
		};

		void Asm::resetLinks() {
			asmLink_.empty();
		};

		ProgramCounter *Asm::assembleDirect(InstructionProcedure procedure, Variable *operand) {
			TYList2<InstructionX> *pc_;
			ProgramCounter *pc;
			if (lastIsMark_) {
				lastIsMark_ = false;
			} else {
				instructionList->pushToTailEmpty();
			};
			pc_ = instructionList->tail();
			pc = reinterpret_cast<ProgramCounter *> (pc_);


			reinterpret_cast<TYList2<InstructionX> *> (pc)->value.procedure = procedure;
			reinterpret_cast<TYList2<InstructionX> *> (pc)->value.operand = operand;
			return optimizeCode(pc);
		};

		ProgramCounter *Asm::assemble(int type_, const char *name_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) {
			TYList2<InstructionX> *pc_;
			ProgramCounter *pc;
			if (lastIsMark_) {
				lastIsMark_ = false;
			} else {
				instructionList->pushToTailEmpty();
			};

			pc_ = instructionList->tail();
			pc = reinterpret_cast<ProgramCounter *> (pc_);

			switch (type_) {
				case ParserAsm::PushUndefined: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushUndefined,NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-undefined\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushSymbol: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushSymbol,VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-symbol %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushBoolean: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushBoolean,VariableBoolean::newVariable(StringBase::isEqual(name_,"true")));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-boolean %s\n", pc, name_);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushNumber: {
						Number valueNumber;
						sscanf(name_, QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &valueNumber);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushNumber,VariableNumber::newVariable(valueNumber));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-number %s\n", pc, name_);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushNaN: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushNumber,VariableNumber::newVariable(NAN));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-nan\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushInfinity: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushNumber,VariableNumber::newVariable(INFINITY));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-infinity\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssign: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssign, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignPlus: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignPlus, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-plus\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignMinus: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignMinus, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-minus\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignMul: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignMul, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-mul\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignDiv: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignDiv, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-div\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignMod: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignMod, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-mod\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignBitwiseOr: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignBitwiseOr, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-bitwise-or\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignBitwiseAnd: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignBitwiseAnd, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-bitwise-and\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignBitwiseLeftShift: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignBitwiseLeftShift, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-bitwise-left-shift\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignBitwiseRightShift: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignBitwiseRightShift, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-bitwise-right-shift\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignBitwiseRightShiftZero: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignBitwiseRightShiftZero, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-bitwise-right-shift-zero\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorAssignBitwiseXor: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignBitwiseXor, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-bitwise-xor\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorEqual: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorEqual, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-equal\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorEqualStrict: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorEqualStrict, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-equal-strict\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorPlus: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorPlus, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-plus\n", pc);
#endif
						return optimizeCode(pc);
					};
					break;
				case ParserAsm::OperatorMinus: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorMinus, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-minus\n", pc);
#endif
						return optimizeCode(pc);
					};
					break;
				case ParserAsm::OperatorUnaryPlus: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorUnaryPlus, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-unary-plus\n", pc);
#endif
						return optimizeCode(pc);
					};
					break;
				case ParserAsm::OperatorMul: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorMul, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-mul\n", pc);
#endif
						return optimizeCode(pc);
					};
					break;
				case ParserAsm::OperatorDiv: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorDiv, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-div\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorMod: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorMod, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-mod\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorNotEqual: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorNotEqual, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-not-equal\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorNotEqualStrict: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorNotEqualStrict, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-not-equal-strict\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorNot: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorNot, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-not\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorLessThanOrEqual: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorLessThanOrEqual, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-less-than-or-equal\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorLessThan: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorLessThan, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-less-than\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorGreaterThanOrEqual: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorGreaterThanOrEqual, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-greater-than-or-equal\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorGreaterThan: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorGreaterThan, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-greater-than\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorLogicalOr: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorLogicalOr, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-logical-or\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseOr: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseOr,NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-or\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorLogicalAnd: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorLogicalAnd, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-logical-and\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseAnd: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseAnd, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-and\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseNot: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseNot, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-not\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseLeftShift: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseLeftShift, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-left-shift\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseRightShift: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseRightShift, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-right-shift\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseRightShiftZero: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseRightShiftZero, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-right-shift-zero\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorBitwiseXor: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorBitwiseXor, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-bitwise-xor\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorIn: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorIn, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-in\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Nop: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Nop, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    nop\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Reference: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Reference, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    reference %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
					break;
				case ParserAsm::EnterContext: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(EnterContext, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    enter-context\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::LeaveContext: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(LeaveContext, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    leave-context\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Mark: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Mark, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    mark\n", pc);
#endif
						lastIsMark_ = true;
						return pc;
					};
					break;
				case ParserAsm::Break: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Break, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    break\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Continue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Continue, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    continue\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorEqual1: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorEqual1, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-equal1\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Pop1: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Pop1, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    pop1\n", pc);
#endif

						return optimizeCode(pc);
					};
					break;
				case ParserAsm::PushNull: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushNull, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-null\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Return: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Return, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    return\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Throw: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Throw, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    throw\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::ContextSetThis: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextSetThis, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-this\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::ContextPushThis: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextPushThis, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-push-this\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushNewObject: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushNewObject, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-new-object\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushString: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushString, VariableString::newVariable(name_));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-string %s\n", pc, name_);
#endif
						return pc;
					};
					break;
				case ParserAsm::EnterFirstContext: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(EnterFirstContext, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    enter-first-context\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::Assign: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Assign,NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    assign\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorReference: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorReference, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-reference\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushNewArray: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushNewArray, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-new-array\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::ArrayPush: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ArrayPush, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    array-push\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorArrayPush: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorArrayPush, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-array-push\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::PushObjectReference: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushObjectReference,VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-object-reference %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
					break;
				case ParserAsm::ReferenceObjectReference: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ReferenceObjectReference, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    reference-object-reference %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorInstanceOf: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorInstanceOf, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-instance-of\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::OperatorTypeOf: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorTypeOf, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-type-of\n", pc);
#endif
						return pc;
					};

				case ParserAsm::OperatorReferenceReference: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorReferenceReference, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-reference-reference\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorArrayPushReference: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorArrayPushReference, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-array-push-reference\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorObjectReferenceValue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorObjectReferenceValue, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-object-reference-value\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorPlusPlusLeft: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorPlusPlusLeft, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-plus-plus-left\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorPlusPlusRight: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorPlusPlusRight, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-plus-plus-right\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorMinusMinusLeft: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorMinusMinusLeft, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-minus-minus-left\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorMinusMinusRight: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorMinusMinusRight, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-minus-minus-right\n", pc);
#endif
						return pc;
					};

				case ParserAsm::XCall: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XCall, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-call\n", pc);
#endif
						return pc;
					};
				case ParserAsm::XCallThis: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XCallThis,NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-call-this\n", pc);
#endif
						return pc;
					};
				case ParserAsm::XCallThisModeCall: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XCallThisModeCall, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-call-this-mode-call\n", pc);
#endif
						return pc;
					};
				case ParserAsm::XCallThisModeApply: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XCallThisModeApply, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-call-this-mode-apply\n", pc);
#endif
						return pc;
					};
				case ParserAsm::XCallWithThisReference: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XCallWithThisReference,VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-call-with-this-reference %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
				case ParserAsm::XCallSymbol: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XCallSymbol, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-call-symbol %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
				case ParserAsm::XArrayPushWithTransfer: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XArrayPushWithTransfer, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-array-push-with-transfer\n", pc);
#endif
						return pc;
					};
				case ParserAsm::AssignReverse: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(AssignReverse, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    assign-reverse\n", pc);
#endif
						return pc;
					};
				case ParserAsm::Duplicate: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Duplicate, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    duplicate\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorAssignXPrototype: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorAssignXPrototype, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-assign-x-prototype\n", pc);
#endif
						return pc;
					};

				case ParserAsm::ArgumentsPushObjectReference: {
						dword symbolId;
						sscanf(name_,QUANTUM_SCRIPT_FORMAT_DWORD, &symbolId);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ArgumentsPushObjectReference, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    arguments-push-object-reference %s\n", pc, name_);
#endif
						return pc;
					};

				case ParserAsm::ArgumentsPushSymbol: {
						dword symbolId;
						sscanf( name_, QUANTUM_SCRIPT_FORMAT_DWORD, &symbolId);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ArgumentsPushSymbol, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    arguments-push-symbol %s\n", pc, name_);
#endif
						return pc;
					};

				case ParserAsm::AssignNewObject: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(AssignNewObject, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    assign-new-object\n", pc);
#endif
						return pc;
					};

				case ParserAsm::Catch: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Catch, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    catch\n", pc);
#endif
						return pc;
					};

				case ParserAsm::LocalVariablesPushObjectReference: {
						dword symbolId;
						sscanf( name_, QUANTUM_SCRIPT_FORMAT_DWORD, &symbolId);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(LocalVariablesPushObjectReference, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    local-variables-push-object-reference %s\n", pc, name_);
#endif
						return pc;
					};

				case ParserAsm::LocalVariablesPushSymbol: {
						dword symbolId;
						sscanf( name_, QUANTUM_SCRIPT_FORMAT_DWORD, &symbolId);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(LocalVariablesPushSymbol, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    local-variables-push-symbol %s\n", pc, name_);
#endif
						return pc;
					};


				case ParserAsm::EndExecution: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(EndExecution, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    end-execution\n", pc);
#endif
						return pc;
					};

				case ParserAsm::Yield: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(Yield,NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    yield\n", pc);
#endif
						return pc;
					};

				case ParserAsm::ContextSetReference: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextSetReference, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-reference\n", pc);
#endif
						return pc;
					};

				case ParserAsm::OperatorSetReferenceIndexKey: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorSetReferenceIndexKey, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-set-reference-index-key\n", pc);
#endif
						return pc;
					};
				case ParserAsm::OperatorNextReferenceIndex: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorNextReferenceIndex, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-next-reference-index\n", pc);
#endif
						return pc;
					};

				case ParserAsm::OperatorSetReferenceIndexValue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorSetReferenceIndexValue, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-set-reference-index-value\n", pc);
#endif
						return pc;
					};

				case ParserAsm::ContextSetRegisterValue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextSetRegisterValue, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-register-value\n", pc);
#endif
						return pc;
					};



				case ParserAsm::ContextPushRegisterValue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextPushRegisterValue, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-push-register-value\n", pc);
#endif
						return pc;
					};

				case ParserAsm::XPushNewArray: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(XPushNewArray, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-push-new-array\n", pc);
#endif
						return pc;
					};

				case ParserAsm::ContextPushSelf: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextPushSelf, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-push-self\n", pc);
#endif
						return pc;
					};
					break;

				case ParserAsm::CurrentFiberExit: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(CurrentFiberExit, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    current-fiber-exit\n", pc);
#endif
						return pc;
					};
					break;

				case ParserAsm::PushArguments: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(PushArguments, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    push-arguments\n", pc);
#endif
						return pc;
					};
					break;

				case ParserAsm::OperatorReferenceDeleteIndex: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorReferenceDeleteIndex, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-reference-delete-index\n", pc);
#endif
						return pc;
					};
					break;

				case ParserAsm::OperatorReferenceDeleteReference: {
						Symbol symbolId = Context::getSymbol(name_);
						QUANTUM_SCRIPT_ASM_INSTRUCTION(OperatorReferenceDeleteReference, VariableSymbol::newVariable(symbolId));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    operator-reference-delete-reference %s : %d\n", pc, name_, symbolId);
#endif
						return pc;
					};
					break;

				case ParserAsm::ContextSetStack: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ContextSetStack, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-stack\n", pc);
#endif
						return pc;
					};
					break;
				case ParserAsm::ClearIncludedFile: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION(ClearIncludedFile, NULL);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    clear-included-file\n", pc);
#endif
						return pc;
					};
					break;


				default:
					break;
			};

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p **  asm-unknown %04X:%s\n", pc, type_, name_);
#endif

			lastIsMark_ = true;
			return NULL;
		};

		ProgramCounter *Asm::assembleX(int type_, const char *name_, const char *nameX_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) {
			TYList2<InstructionX> *pc_;
			ProgramCounter *pc;
			if (lastIsMark_) {
				lastIsMark_ = false;
			} else {
				instructionList->pushToTailEmpty();
			};

			pc_ = instructionList->tail();
			pc = reinterpret_cast<ProgramCounter *> (pc_);

			switch (type_) {

				case ParserAsm::ArgumentsLevelPushObjectReference: {
						int value;
						int level;
						sscanf( name_, "%d", &value);
						sscanf( nameX_, "%d", &level);

						QUANTUM_SCRIPT_ASM_INSTRUCTION(ArgumentsLevelPushObjectReference, VariableArgumentLevel::newVariable(value, level));

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    arguments-level-push-object-reference %s:%s\n", pc, name_, nameX_);
#endif
						return pc;
					};
				case ParserAsm::ArgumentsLevelPushSymbol: {
						int value;
						int level;
						sscanf( name_, "%d", &value);
						sscanf( nameX_, "%d", &level);

						QUANTUM_SCRIPT_ASM_INSTRUCTION(ArgumentsLevelPushSymbol, VariableArgumentLevel::newVariable(value, level));

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    arguments-level-push-symbol %s:%s\n", pc, name_, nameX_);
#endif
						return pc;
					};

				case ParserAsm::LocalVariablesLevelPushObjectReference: {
						int value;
						int level;
						sscanf( name_, "%d", &value);
						sscanf( nameX_, "%d", &level);

						QUANTUM_SCRIPT_ASM_INSTRUCTION(LocalVariablesLevelPushObjectReference, VariableArgumentLevel::newVariable(value, level));

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    local-variables-level-push-object-reference %s:%s\n", pc, name_, nameX_);
#endif
						return pc;
					};
				case ParserAsm::LocalVariablesLevelPushSymbol: {
						int value;
						int level;
						sscanf( name_, "%d", &value);
						sscanf( nameX_, "%d", &level);

						QUANTUM_SCRIPT_ASM_INSTRUCTION(LocalVariablesLevelPushSymbol, VariableArgumentLevel::newVariable(value, level));

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    local-variables-level-push-symbol %s:%s\n", pc, name_, nameX_);
#endif
						return pc;
					};


				case ParserAsm::FunctionHint: {
						ProgramCounter *fn_;
						int hint;
						sscanf(name_, "%p", &fn_);
						sscanf(nameX_, "%d", &hint);

						((VariableVmFunction *)((reinterpret_cast<TYList2<InstructionX> *> (fn_))->value.operand))->functionHint=hint;
						lastIsMark_ = true;

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    function-hint %p:%04X\n", pc, fn_, hint);
#endif
						return pc;
					};




				default:
					break;
			};


#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p **  asm-unknown-x %04X:%s:%s\n", pc, type_, name_, nameX_);
#endif

			lastIsMark_ = true;
			return NULL;
		};

		ProgramCounter *Asm::assembleProgramCounter(int type_, ProgramCounter *value, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) {
			TYList2<InstructionX> *pc_;
			ProgramCounter *pc;
			if (lastIsMark_) {
				lastIsMark_ = false;
			} else {
				instructionList->pushToTailEmpty();
			};

			pc_ = instructionList->tail();
			pc = reinterpret_cast<ProgramCounter *> (pc_);

			switch (type_) {
				case ParserAsm::IfFalseGoto: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(IfFalseGoto, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    if-false-goto %p\n", pc, value);
#endif
						return optimizeCode(pc);
					};
				case ParserAsm::Goto: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(Goto, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    goto %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetBreak: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetBreak, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-break %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetContinue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetContinue, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-continue %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::IfTrueGoto: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(IfTrueGoto, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    if-true-goto %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::XPushFunction: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(XPushFunction,VariableVmFunction::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    x-push-function %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetCatch: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetCatch,VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-catch %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetFinally: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetFinally, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-finally %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetTryBreak: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetTryBreak, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-try-break %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetTryContinue: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetTryContinue, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-try-continue %p\n", pc, value);
#endif
						return pc;
					};
				case ParserAsm::ContextSetTryReturn: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetTryReturn,VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-try-return %p\n", pc, value);
#endif
						return pc;
					};
					break;
				case ParserAsm::ContextSetTryThrow: {
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(ContextSetTryThrow, VariableVmProgramCounter::newVariable(value));
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    context-set-try-throw %p\n", pc, value);
#endif
						return pc;
					};
				default:
				case ParserAsm::InstructionListExtractAndDelete: {
						Variable *operator23=VariableOperator23::newVariable();
						((VariableOperator23 *)operator23)->linkBegin=value;
						QUANTUM_SCRIPT_ASM_INSTRUCTION_PC(InstructionListExtractAndDelete, operator23);
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    instruction-list-extract-and-delete %p\n", pc, value);
#endif
						return pc;
					};
					break;
			};

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p **  asm-unknown-pc %04X:%p\n", pc, type_, value);
#endif
			lastIsMark_ = true;
			return NULL;
		};

		void Asm::linkProgramCounter(ProgramCounter *old_, ProgramCounter *new_) {
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p -- link %p = %p\n", NULL, old_, new_);
#endif
			asmLink_.set(old_,new_);

#ifdef QUANTUM_SCRIPT_DISABLE_ASM_OPTIMIZER
#else
			if(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.asmType==ParserAsm::IfArgumentsSymbolNotEqualNumberGoto) {
				((VariableOperator31 *)(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->pc=new_;
				return;
			};
			if(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.asmType==ParserAsm::IfSymbolNotLessThanNumberGoto) {
				((VariableOperator31 *)(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->pc=new_;
				return;
			};
			if(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.asmType==ParserAsm::IfArgumentsSymbolNotLessThanNumberGoto) {
				((VariableOperator31 *)(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->pc=new_;
				return;
			};
#endif

			if(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.asmType==ParserAsm::InstructionListExtractAndDelete) {
				((VariableOperator23 *) (reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->pc = new_;
				return;
			};

			if(reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand) {

				if((reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand)->variableType==VariableVmProgramCounter::typeVmProgramCounter) {
					((VariableVmProgramCounter *) (reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->value = new_;
					return;
				};

				((VariableVmFunction *) (reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->value = new_;
			};
		};


		void Asm::linkProgramCounterEnd(ProgramCounter *old_, ProgramCounter *new_) {
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p -- link-end %p = %p\n", NULL, old_, new_);
#endif
			((VariableVmFunction *) (reinterpret_cast<TYList2<InstructionX> *> (old_)->value.operand))->valueEnd = new_;
		};

		void Asm::linkProgramCounterSource(ProgramCounter *pc_, dword sourceSymbol, dword sourcePos, dword sourceLineNumber, dword sourceLineColumn) {
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p -- link-source %p - %d:%d:%d:%d\n", NULL, pc_,sourceSymbol,sourcePos,sourceLineNumber,sourceLineColumn);
#endif

			(reinterpret_cast<TYList2<InstructionX> *> (pc_))->value.sourceSymbol=sourceSymbol;
			(reinterpret_cast<TYList2<InstructionX> *> (pc_))->value.sourcePos=sourcePos;
			(reinterpret_cast<TYList2<InstructionX> *> (pc_))->value.sourceLineNumber=sourceLineNumber;
			(reinterpret_cast<TYList2<InstructionX> *> (pc_))->value.sourceLineColumn=sourceLineColumn;

		};


		void Asm::removeProgramCounter(ProgramCounter *pc_) {
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
			printf("%p -- remove %p\n", NULL, pc_);
#endif
			ProgramCounter *next_;
			TYList2<InstructionX> *instruction=reinterpret_cast<TYList2<InstructionX> *> (pc_);
			next_=(ProgramCounter *)(instruction->next);
			instructionList->extractNode(instruction);
			instructionList->nodeMemoryDelete(instruction);
			//
			TYRedBlackTreeNode<ProgramCounter *, ProgramCounter *> *scan;
			for(scan=asmLink_.begin(); scan; scan=scan->succesor()) {
				if(scan->value==pc_) {
					linkProgramCounter(scan->key,next_);
				};
			};
		};

#ifdef QUANTUM_SCRIPT_DISABLE_ASM_OPTIMIZER
#else
		ProgramCounter *Asm::optimizeCode(ProgramCounter *pc) {
			TYList2<InstructionX> *pc_=reinterpret_cast<TYList2<InstructionX> *>(pc);


			if(pc_->value.asmType==ParserAsm::OperatorMinus) {

				//  1. arguments-push-symbol X
				//  2. push-number Y
				//  3. operator-minus
				// - translate to -
				// 1. operator-minus-arguments-symbol-x-number X Y

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::PushNumber) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::ArgumentsPushSymbol) {
								VariableOperator21 *operatorX=TMemory<VariableOperator21>::newObject();
								operatorX->symbol=((VariableSymbol *)(pc_->back->back->value.operand))->value;
								operatorX->value=((VariableNumber *)(pc_->back->value.operand))->value;
								//
								pc_->back->back->value.asmType=ParserAsm::OperatorMinusArgumentsSymbolXNumber;
								pc_->back->back->value.procedure=InstructionVmOperatorMinusArgumentsSymbolXNumber;
								pc_->back->back->value.operand->decReferenceCount();
								pc_->back->back->value.operand=operatorX;
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] operator-minus-arguments-symbol-x-number %d %d\n", pc, operatorX->symbol,operatorX->value);
#endif

								return pc;
							};
						};
					};
				};



				// 1. local-variables-push-symbol Y
				// 2. local-variables-push-symbol X
				// 3. operator-minus
				// - translate to -
				// 1. operator-minus-local-variables-symbol-2 X Y

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {
								VariableOperator22 *operatorX=TMemory<VariableOperator22>::newObject();
								operatorX->symbol2=((VariableSymbol *)(pc_->back->back->value.operand))->value;
								operatorX->symbol1=((VariableSymbol *)(pc_->back->value.operand))->value;
								//
								pc_->back->back->value.asmType=ParserAsm::OperatorMinusLocalVariablesSymbol2;
								pc_->back->back->value.procedure=InstructionVmOperatorMinusLocalVariablesSymbol2;
								pc_->back->back->value.operand->decReferenceCount();
								pc_->back->back->value.operand=operatorX;
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();


#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] operator-minus-local-variables-symbol-2 %d %d\n", pc,operatorX->symbol1,operatorX->symbol2);
#endif

								return pc;
							};
						};
					};
				};


			};


			if(pc_->value.asmType==ParserAsm::OperatorPlus) {

				// 1. local-variables-push-symbol Y
				// 2. local-variables-push-symbol X
				// 3. operator-plus
				// - translate to -
				// 1. operator-plus-local-variables-symbol-2 X Y

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {
								VariableOperator22 *operatorX=TMemory<VariableOperator22>::newObject();
								operatorX->symbol2=((VariableSymbol *)(pc_->back->back->value.operand))->value;
								operatorX->symbol1=((VariableSymbol *)(pc_->back->value.operand))->value;
								//
								pc_->back->back->value.asmType=ParserAsm::OperatorPlusLocalVariablesSymbol2;
								pc_->back->back->value.procedure=InstructionVmOperatorPlusLocalVariablesSymbol2;
								pc_->back->back->value.operand->decReferenceCount();
								pc_->back->back->value.operand=operatorX;
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();


#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] operator-plus-local-variables-symbol-2 %d %d\n", pc,operatorX->symbol1,operatorX->symbol2);
#endif

								return pc;
							};
						};
					};
				};

				// 1. push-number X
				// 2. push-number Y
				// 3. operator-plus
				// - translate to -
				// 1. push-number Z ; (Z=X+Y)

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::PushNumber) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::PushNumber) {
								Number v1;
								Number v2;
								v1=pc_->back->back->value.operand->toNumber();
								v2=pc_->back->value.operand->toNumber();
								//
								pc_->back->back->value.asmType=ParserAsm::PushNumber;
								pc_->back->back->value.procedure=InstructionVmPushNumber;
								pc_->back->back->value.operand->decReferenceCount();
								pc_->back->back->value.operand=VariableNumber::newVariable(v1+v2);
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] push-number " QUANTUM_SCRIPT_FORMAT_NUMBER "\n", pc,(v1+v2));
#endif

								return pc;
							};
						};
					};
				};

			};



			if(pc_->value.asmType==ParserAsm::Pop1) {

				//  1. push-object-reference X
				//  2. operator-plus-plus-left
				//  3. pop1
				//  - translate to -
				//  1. symbol-plus-plus X


				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorPlusPlusLeft) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::PushObjectReference) {
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								dword symbol_=((VariableSymbol *)(pc_->back->back->value.operand))->value;
#endif
								pc_->back->back->value.asmType=ParserAsm::SymbolPlusPlus;
								pc_->back->back->value.procedure=InstructionVmSymbolPlusPlus;
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] symbol-plus-plus %d\n", pc, symbol_);
#endif

								return pc;
							};
						};
					};
				};

				//  1. push-object-reference X
				//  2. operator-plus-plus-right
				//  3. pop1
				//  - translate to -
				//  1. symbol-plus-plus X



				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorPlusPlusRight) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::PushObjectReference) {
#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								dword symbol_=((VariableSymbol *)(pc_->back->back->value.operand))->value;
#endif
								pc_->back->back->value.asmType=ParserAsm::SymbolPlusPlus;
								pc_->back->back->value.procedure=InstructionVmSymbolPlusPlus;
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] symbol-plus-plus %d\n", pc, symbol_);
#endif

								return pc;
							};
						};
					};
				};


				//  1. operator-assign
				//  2. pop1
				//  - translate to -
				//  1. assign

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorAssign) {
						pc_->back->value.asmType=ParserAsm::Assign;
						pc_->back->value.procedure=InstructionVmAssign;
						pc = reinterpret_cast<ProgramCounter *> (pc_->back);
						instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    [1] assign\n", pc);
#endif

						return pc;
					};
				};


				// 1. local-variables-push-symbol X
				// 2. pop1
				// - translate to -
				// nop

				if(pc_->back) {

					if(pc_->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {

						if(pc_->back->back) {

							pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
							instructionList->popFromTailEmpty();
							instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
							printf("%p    [2] nop\n", pc);
#endif

							return pc;
						};
					};

				};


			};


			if(pc_->value.asmType==ParserAsm::IfFalseGoto) {


				// 1. arguments-push-symbol X
				// 2. push-number Y
				// 3. operator-equal
				// 4. if-false-goto Z
				// - translate to -
				// 1. if-arguments-symbol-not-equal-number-goto X Y Z

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorEqual) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::PushNumber) {
								if(pc_->back->back->back) {
									if(pc_->back->back->back->value.asmType==ParserAsm::ArgumentsPushSymbol) {
										VariableOperator31 *operatorX=TMemory<VariableOperator31>::newObject();
										operatorX->symbol=((VariableSymbol *)(pc_->back->back->back->value.operand))->value;
										operatorX->value=((VariableNumber *)(pc_->back->back->value.operand))->value;
										operatorX->pc=((VariableVmProgramCounter *)(pc_->value.operand))->value;
										//
										pc_->back->back->back->value.asmType=ParserAsm::IfArgumentsSymbolNotEqualNumberGoto;
										pc_->back->back->back->value.procedure=InstructionVmIfArgumentsSymbolNotEqualNumberGoto;
										pc_->back->back->back->value.operand->decReferenceCount();
										pc_->back->back->back->value.operand=operatorX;
										pc = reinterpret_cast<ProgramCounter *> (pc_->back->back->back);
										//
										instructionList->popFromTailEmpty();
										instructionList->popFromTailEmpty();
										instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
										printf("%p    [3] if-arguments-symbol-not-equal-number-goto %d %g %p\n", pc,operatorX->symbol,operatorX->value, operatorX->pc);
#endif

										return pc;
									};
								};
							};
						};
					};
				};


				// 1. push-symbol X
				// 2. push-number Y
				// 3. less-than
				// 4. if-false-goto Z
				// - translate to -
				// 1. if-symbol-not-less-than-number-goto X Y Z


				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorLessThan) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::PushNumber) {
								if(pc_->back->back->back) {
									if(pc_->back->back->back->value.asmType==ParserAsm::PushSymbol) {
										VariableOperator31 *operatorX=TMemory<VariableOperator31>::newObject();
										operatorX->symbol=((VariableSymbol *)(pc_->back->back->back->value.operand))->value;
										operatorX->value=((VariableNumber *)(pc_->back->back->value.operand))->value;
										operatorX->pc=((VariableVmProgramCounter *)(pc_->value.operand))->value;
										//
										pc_->back->back->back->value.asmType=ParserAsm::IfSymbolNotLessThanNumberGoto;
										pc_->back->back->back->value.procedure=InstructionVmIfSymbolNotLessThanNumberGoto;
										pc_->back->back->back->value.operand->decReferenceCount();
										pc_->back->back->back->value.operand=operatorX;
										pc = reinterpret_cast<ProgramCounter *> (pc_->back->back->back);
										//
										instructionList->popFromTailEmpty();
										instructionList->popFromTailEmpty();
										instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
										printf("%p    [3] if-symbol-not-less-than-number-goto %d %g %p\n", pc,operatorX->symbol,operatorX->value, operatorX->pc);
#endif

										return pc;
									};
								};
							};
						};
					};
				};


				// 1. arguments-push-symbol X
				// 2. push-number Y
				// 3. less-than
				// 4. if-false-goto Z
				// - translate to -
				// 1. if-arguments-symbol-not-less-than-number-goto X Y Z


				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorLessThan) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::PushNumber) {
								if(pc_->back->back->back) {
									if(pc_->back->back->back->value.asmType==ParserAsm::ArgumentsPushSymbol) {
										VariableOperator31 *operatorX=TMemory<VariableOperator31>::newObject();
										operatorX->symbol=((VariableSymbol *)(pc_->back->back->back->value.operand))->value;
										operatorX->value=((VariableNumber *)(pc_->back->back->value.operand))->value;
										operatorX->pc=((VariableVmProgramCounter *)(pc_->value.operand))->value;
										//
										pc_->back->back->back->value.asmType=ParserAsm::IfArgumentsSymbolNotLessThanNumberGoto;
										pc_->back->back->back->value.procedure=InstructionVmIfArgumentsSymbolNotLessThanNumberGoto;
										pc_->back->back->back->value.operand->decReferenceCount();
										pc_->back->back->back->value.operand=operatorX;
										pc = reinterpret_cast<ProgramCounter *> (pc_->back->back->back);
										//
										instructionList->popFromTailEmpty();
										instructionList->popFromTailEmpty();
										instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
										printf("%p    [3] if-arguments-symbol-not-less-than-number-goto %d %g %p\n", pc,operatorX->symbol,operatorX->value, operatorX->pc);
#endif

										return pc;
									};
								};
							};
						};
					};
				};



			};

			if(pc_->value.asmType==ParserAsm::OperatorMul) {

				// 1. local-variables-push-symbol Y
				// 2. local-variables-push-symbol X
				// 3. operator-mul
				// - translate to -
				// 1. operator-mul-local-variables-symbol-2 X Y

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {
						if(pc_->back->back) {
							if(pc_->back->back->value.asmType==ParserAsm::LocalVariablesPushSymbol) {
								VariableOperator22 *operatorX=TMemory<VariableOperator22>::newObject();
								operatorX->symbol2=((VariableSymbol *)(pc_->back->back->value.operand))->value;
								operatorX->symbol1=((VariableSymbol *)(pc_->back->value.operand))->value;
								//
								pc_->back->back->value.asmType=ParserAsm::OperatorMulLocalVariablesSymbol2;
								pc_->back->back->value.procedure=InstructionVmOperatorMulLocalVariablesSymbol2;
								pc_->back->back->value.operand->decReferenceCount();
								pc_->back->back->value.operand=operatorX;
								pc = reinterpret_cast<ProgramCounter *> (pc_->back->back);
								//
								instructionList->popFromTailEmpty();
								instructionList->popFromTailEmpty();


#ifdef QUANTUM_SCRIPT_DEBUG_ASM
								printf("%p    [2] operator-mul-local-variables-symbol-2 %d %d\n", pc,operatorX->symbol1,operatorX->symbol2);
#endif
								return pc;
							};
						};
					};
				};
			};


			if(pc_->value.asmType==ParserAsm::IfFalseGoto) {

				// 1. greater-than
				// 2. if-false-goto X
				// - translate to -
				// 1. if-not-greater-than X

				if(pc_->back) {
					if(pc_->back->value.asmType==ParserAsm::OperatorGreaterThan) {

						pc_->back->value.asmType=ParserAsm::IfNotGreaterThanGoto;
						pc_->back->value.procedure=InstructionVmIfNotGreaterThanGoto;
						pc_->back->value.operand=pc_->value.operand;
						pc_->value.operand=NULL;
						pc = reinterpret_cast<ProgramCounter *> (pc_->back);
						instructionList->popFromTailEmpty();

#ifdef QUANTUM_SCRIPT_DEBUG_ASM
						printf("%p    [1] if-not-greater-than %p\n", pc, ((VariableVmProgramCounter *)(((TYList2<InstructionX> *)pc)->value.operand))->value);
#endif

						return pc;


					};
				};
			};

			return pc;

		};


#endif



	};
};


