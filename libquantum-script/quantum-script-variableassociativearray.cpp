﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableassociativearray.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-associativearrayiteratorkey.hpp"
#include "quantum-script-associativearrayiteratorvalue.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableAssociativeArray::typeAssociativeArray="{35EFC1B3-6F8B-4604-A31E-95CC449B48E1}";
		const char *VariableAssociativeArray::strTypeAssociativeArray="AssociativeArray";

		Variable *VariableAssociativeArray::newVariable() {
			return (Variable *) TMemory<VariableAssociativeArray>::newObject();
		};

		String VariableAssociativeArray::getType() {
			return strTypeAssociativeArray;
		};

		Variable &VariableAssociativeArray::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolLength()) {
				if(vLength) {
					((VariableNumber *)vLength.value())->value=(Number)value->length();
				} else {
					vLength.setObject(VariableNumber::newVariable((Number)value->length()));
				};
				return *vLength;
			};

			return operatorReferenceX(symbolId,(Context::getPrototypeAssociativeArray())->prototype);
		};

		Variable *VariableAssociativeArray::instancePrototype() {
			return (Context::getPrototypeAssociativeArray())->prototype;
		};

		TPointerOwner<Iterator> VariableAssociativeArray::getIteratorKey() {
			TPointerOwner<Iterator> retV;
			AssociativeArrayIteratorKey *iterator_=TMemory<AssociativeArrayIteratorKey>::newObject();
			iterator_->sourceArray=this;
			iterator_->index=0;
			retV.setObject(iterator_);
			return retV;
		};

		TPointerOwner<Iterator> VariableAssociativeArray::getIteratorValue() {
			TPointerOwner<Iterator> retV;
			AssociativeArrayIteratorValue *iterator_=TMemory<AssociativeArrayIteratorValue>::newObject();
			iterator_->sourceArray=this;
			iterator_->index=0;
			retV.setObject(iterator_);
			return retV;
		};

		TPointerX<Variable> &VariableAssociativeArray::operatorReferenceIndex(Variable *variable) {
			TYRedBlackTreeNode<TPointerX<Variable>, dword> *x;
			x=value->mapKey->find(*variable);
			if(x) {
				return (*(value->arrayValue))[x->value];
			};
			value->mapKey->set(variable, value->length_);
			(*(value->arrayKey))[value->length_].setObjectX(variable);
			(*(value->arrayValue))[value->length_].setObjectX(VariableUndefined::newVariableX());
			++value->length_;
			return (*(value->arrayValue))[value->length_-1];
		};

		bool VariableAssociativeArray::operatorDeleteIndex(Variable *variable) {
			TYRedBlackTreeNode<TPointerX<Variable>, dword> *x;
			x=value->mapKey->find(*variable);
			if(x) {
				dword index;
				index=x->value;
				value->arrayKey->remove(index);
				value->arrayValue->remove(index);
				value->mapKey->removeNode(x);
				for(x=value->mapKey->begin(); x!=NULL; x=x->succesor()) {
					if(x->value>=index) {
						x->value--;
					};
				};
				--value->length_;
			};
			return true;
		};

		Variable &VariableAssociativeArray::operatorIndex2(Variable *variable) {
			return *(operatorReferenceIndex(variable));
		};

		void VariableAssociativeArray::memoryInit() {
			TMemory<Variable>::memoryInit();
			TMemory<AssociativeArray>::memoryInit();
		};

		Variable *VariableAssociativeArray::clone(SymbolList &inSymbolList) {
			dword k;
			VariableAssociativeArray *out=(VariableAssociativeArray *)newVariable();
			Variable *newKey;
			for(k=0; k<value->length(); ++k) {
				newKey=((*(value->arrayKey))[k])->clone(inSymbolList);
				out->value->mapKey->set(newKey,k);
				((*(out->value->arrayKey))[k]).setObject(newKey);
				((*(out->value->arrayValue))[k]).setObject(((*(value->arrayValue))[k])->clone(inSymbolList));
			};
			return out;
		};

		bool VariableAssociativeArray::toBoolean() {
			return true;
		};

		String VariableAssociativeArray::toString() {
			return strTypeAssociativeArray;
		};

	};
};



