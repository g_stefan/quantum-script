﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEFIBERINFO_HPP
#define QUANTUM_SCRIPT_VARIABLEFIBERINFO_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif


namespace Quantum {
	namespace Script {

		class VariableFiberInfo;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableFiberInfo>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableFiberInfo> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableFiberInfo :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableFiberInfo);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeFiberInfo;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeFiberInfo;

				TPointerX<Prototype> prototype;

				TPointerX<Variable> returnValue;

				bool isTerminated;
				bool isSuspended;
				long int sleepMilliseconds;
				long int startMilliseconds;

				TPointerX<TStack2<TPointerX<Variable> > > messageQueue;
				bool messageProcessed;
				bool isSendMessage;
				TPointerX<Variable> receivedMessage;
				bool isBusy;

				inline VariableFiberInfo() {
					returnValue.memoryLink(this);
					prototype.memoryLink(this);
					messageQueue.memoryLink(this);
					receivedMessage.memoryLink(this);
					variableType = typeFiberInfo;
					messageQueue.newObject();
					isSendMessage=false;
					messageProcessed=false;
					isBusy=true;
				};

				inline void activeDestructor() {
					prototype.deleteObject();
					returnValue.deleteObject();
					receivedMessage.deleteObject();
					messageQueue->empty();
					isSendMessage=false;
					messageProcessed=false;
					isBusy=true;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();


				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> getMessageX();
				QUANTUM_SCRIPT_EXPORT bool hasMessage();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
