﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableoperator22.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableOperator22::typeOperator22= "{152BEAAB-FC7B-4F54-846B-15000DE5B937}";

		Variable *VariableOperator22::newVariable() {
			return (Variable *) TMemory<VariableOperator22>::newObject();
		};

		bool VariableOperator22::toBoolean() {
			return true;
		};


		String VariableOperator22::toString() {
			return strTypeUndefined;
		};

	};
};


