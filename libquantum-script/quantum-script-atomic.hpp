//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ATOMIC_HPP
#define QUANTUM_SCRIPT_ATOMIC_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class Atomic {
			public:

#ifndef XYO_SINGLE_THREAD
				XCriticalSection criticalSection;
#endif

				static const int  typeUnknown=0;
				static const int  typeBoolean=1;
				static const int  typeNumber=2;
				static const int  typeString=3;

				int atomicType;
				bool valueBooloean;
				Number valueNumber;
				char *valueString;
				int referenceCount;

				inline Atomic() {
					atomicType=typeUnknown;
					valueBooloean=false;
					valueNumber=0;
					valueString=NULL;
					referenceCount=0;
				};

				inline ~Atomic() {
					if(atomicType==typeString) {
						delete[] valueString;
					};
				};

				inline void decReferenceCount() {
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					--referenceCount;
					if(referenceCount==0) {
#ifndef XYO_SINGLE_THREAD
						criticalSection.leave();
#endif
						delete this;
						return;
					};
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
				};

				inline void incReferenceCount() {
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					++referenceCount;
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
				};

				inline void clear() {
					if(atomicType==typeString) {
						delete[] valueString;
					};
					atomicType=typeUnknown;
				};

				inline void setBoolean(bool value) {
					clear();
					atomicType=typeBoolean;
					valueBooloean=value;
				};

				inline void setNumber(Number value) {
					clear();
					atomicType=typeNumber;
					valueNumber=value;
				};

				inline void setString(String value) {
					clear();
					atomicType=typeString;
					valueString=new char[value.length()+1];
					memcpy(valueString,value.value(),value.length()+1);
				};


		};
	};
};


#endif
