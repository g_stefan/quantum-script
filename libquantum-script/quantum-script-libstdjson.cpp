﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#include <windows.h>
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#else
#include <dlfcn.h>
#endif

#include "quantum-script-libstdjson.hpp"

#include "quantum-script-variablenull.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"

#include "quantum-script-libstdjson.src"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdJSON {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static bool jsonParseObject(Token &token,TPointerOwner<Variable> &result);

			static bool jsonParseArray(Token &token,TPointerOwner<Variable> &result) {
				if (token.is1("[")) {
					dword index=0;
					result=VariableArray::newVariable();
					while (!token.isEof()) {
						token.reset();
						if (token.is1("]")) {
							return true;
						};
						if (token.is1(",")) {
							continue;
						};
						if (token.isString()) {
							(result->operatorIndex(index++)).setObject(VariableString::newVariable(token.value));
							continue;
						};
						if (token.isNumber()) {
							Number valueNumber;
							sscanf( token.value, QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &valueNumber);
							(result->operatorIndex(index++)).setObject(VariableNumber::newVariable(valueNumber));
							continue;
						};
						if (token.isSymbolX("true")) {
							(result->operatorIndex(index++)).setObject(VariableBoolean::newVariable(true));
							continue;
						};
						if (token.isSymbolX("false")) {
							(result->operatorIndex(index++)).setObject(VariableBoolean::newVariable(false));
							continue;
						};
						if (token.isSymbolX("null")) {
							(result->operatorIndex(index++)).setObject(VariableNull::newVariable());
							continue;
						};
						if(token.checkIs1("{")) {
							TPointerOwner<Variable> value_;
							if(jsonParseObject(token,value_)) {
								(result->operatorIndex(index++))=value_;
								continue;
							};
							break;
						};
						if(token.checkIs1("[")) {
							TPointerOwner<Variable> value_;
							if(jsonParseArray(token,value_)) {
								(result->operatorIndex(index++))=value_;
								continue;
							};
							break;
						};
						break;
					};
				};
				return false;
			};


			static bool jsonParseObject(Token &token,TPointerOwner<Variable> &result) {
				if (token.is1("{")) {
					result=VariableObject::newVariable();
					while (!token.isEof()) {
						token.reset();
						if (token.is1("}")) {
							return true;
						};
						if (token.is1(",")) {
							continue;
						};
						if (token.isString()) {
							String key_=token.value;
							if (token.is1(":")) {
								token.reset();
								if (token.isString()) {
									TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
									(result->operatorReferenceIndex(keyV)).setObject(VariableString::newVariable(token.value));
									continue;
								};
								if (token.isNumber()) {
									TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
									Number valueNumber;
									sscanf(token.value, QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &valueNumber);
									(result->operatorReferenceIndex(keyV)).setObject(VariableNumber::newVariable(valueNumber));
									continue;
								};
								if (token.isSymbolX("true")) {
									TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
									(result->operatorReferenceIndex(keyV)).setObject(VariableBoolean::newVariable(true));
									continue;
								};
								if (token.isSymbolX("false")) {
									TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
									(result->operatorReferenceIndex(keyV)).setObject(VariableBoolean::newVariable(false));
									continue;
								};
								if (token.isSymbolX("null")) {
									TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
									(result->operatorReferenceIndex(keyV)).setObject(VariableNull::newVariable());
									continue;
								};
								if(token.checkIs1("{")) {
									TPointerOwner<Variable> value_;
									if(jsonParseObject(token,value_)) {
										TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
										(result->operatorReferenceIndex(keyV))=value_;
										continue;
									};
									break;
								};
								if(token.checkIs1("[")) {
									TPointerOwner<Variable> value_;
									if(jsonParseArray(token,value_)) {
										TPointerOwner<Variable> keyV=VariableString::newVariable(key_);
										(result->operatorReferenceIndex(keyV))=value_;
										continue;
									};
									break;
								};
							};
						};
						break;
					};
				};
				return false;
			};

			static bool jsonDecodeX(Token &token,TPointerOwner<Variable> &result) {
				if (token.read()) {
					if(token.checkIs1("{")) {
						return jsonParseObject(token,result);
					};
					if(token.checkIs1("[")) {
						return jsonParseArray(token,result);
					};
					if (token.isString()) {
						result=VariableString::newVariable(token.value);
						return true;
					};
					if (token.isNumber()) {
						Number valueNumber;
						sscanf(token.value, QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &valueNumber);
						result=VariableNumber::newVariable(valueNumber);
						return true;
					};
					if (token.isSymbolX("true")) {
						result=VariableBoolean::newVariable(true);
						return true;
					};
					if (token.isSymbolX("false")) {
						result=VariableBoolean::newVariable(false);
						return true;
					};
					if (token.isSymbolX("null")) {
						result=VariableNull::newVariable();
						return true;
					};
				};
				return false;
			};

			static TPointerOwner<Variable> jsonDecode_(String data_) {
				Input input;
				Token token;
				MemoryFileRead in;
				dword sourcePos;
				dword sourceLineNumber;
				dword sourceLineColumn;
				TPointerOwner<Variable> retV=Context::getValueUndefined();

				if (in.openRead((char *)data_.value(), data_.length())) {
					if (input.init(&in)) {
						token.input = &input;
						token.sourcePos = &sourcePos;
						token.sourceLineNumber = &sourceLineNumber;
						token.sourceLineColumn = &sourceLineColumn;
						if(!jsonDecodeX(token,retV)) {
							in.close();
							return Context::getValueUndefined();
						};
					};
					in.close();
				};
				return retV;
			};


			static TPointerOwner<Variable> jsonDecode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- json-decode\n");
#endif
				return jsonDecode_((arguments->index(0))->toString());
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var JSON={};");
				executive->setFunction2("JSON.decode(x)", jsonDecode);
				executive->compileStringX((char *)libStdJSONSource);
			};

		};
	};
};


