//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

.solution("libquantum-script",function() {

	.project("libquantum-script","version",function() {
		.option("version","xyo-version", {
			type:"xyo-cpp",
			sourceBegin:

			"#ifndef QUANTUM_SCRIPT__EXPORT_HPP\r\n"+
			"#include \"quantum-script--export.hpp\"\r\n"+
			"#endif\r\n"+
			"\r\n"+
			"namespace Lib {\r\n"+
			"\tnamespace Quantum{\r\n"+
			"\t\tnamespace Script{\r\n",

			sourceEnd:

			"\t\t};\r\n"+
			"\t};\r\n"+
			"};\r\n",

			codeExport:"QUANTUM_SCRIPT_EXPORT",
			lineBegin:"\t\t\t"
		});
	});

	.project("libquantum-script","lib",function() {
		.file("source",["*.hpp","*.cpp"]);
		.dependency("libxyo-xo","libxyo-xo","lib");
		.dependencyOption("install-include","quantum-script");
	});

	.project("libquantum-script","dll",function() {
		.file("source",["*.hpp","*.cpp","*.rc"]);
		.option("define","QUANTUM_SCRIPT_INTERNAL");
		.option("sign","xyo-security");
		.dependency("libxyo-xo","libxyo-xo","dll");
		.dependencyOption("install-include","quantum-script");
	});

	.project("libquantum-script","install",function() {
		.install("include","*.hpp","quantum-script");
	});

});

