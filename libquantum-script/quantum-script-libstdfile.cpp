﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdfile.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablebuffer.hpp"
#include "quantum-script-variablefile.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdFile {

			using namespace XYO::XY;
			using namespace XYO::XO;

			FileContext::FileContext() {
				symbolFunctionFile=0;
				prototypeFile.memoryLink(this);
			};

			FileContext *getContext() {
				return TXSingleton<FileContext>::getValue();
			};

			static TPointerOwner<Variable> functionFile(VariableFunction *function,Variable *this_,VariableArray *arguments) {
				return VariableFile::newVariable();
			};

			static void deleteContext() {
				FileContext *fileContext=getContext();
				fileContext->prototypeFile.deleteObject();
				fileContext->symbolFunctionFile=0;
			};

			static void newContext(Executive *executive,void *extensionId) {
				VariableFunction *defaultPrototypeFunction;

				FileContext *fileContext=getContext();
				executive->setExtensionDeleteContext(extensionId,deleteContext);

				fileContext->symbolFunctionFile=Context::getSymbol("File");
				fileContext->prototypeFile.newObject();

				defaultPrototypeFunction=(VariableFunction *)VariableFunction::newVariableX(NULL,NULL,NULL,functionFile,NULL,NULL);
				((Context::getGlobalObject())->operatorReferenceOwnProperty(fileContext->symbolFunctionFile)).setObjectX(defaultPrototypeFunction);
				fileContext->prototypeFile=defaultPrototypeFunction->prototype;

			};

			static TPointerOwner<Variable> isFile(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- script-is-file\n");
#endif
				return VariableBoolean::newVariable((arguments->index(0))->variableType == VariableFile::typeFile);
			};

			static TPointerOwner<Variable> fileOpenReadOnly(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-read-only\n");
#endif
				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openRead((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> fileOpenWrite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-write\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openWrite((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> fileOpenReadAndWrite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-read-and-write\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openReadAndWrite((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> fileOpenAppend(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-append\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openAppend((arguments->index(0))->toString()));
			};


			static TPointerOwner<Variable> fileOpenStdIn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-stdin\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openStdIn());
			};

			static TPointerOwner<Variable> fileOpenStdOut(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-stdout\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openStdOut());
			};

			static TPointerOwner<Variable> fileOpenStdErr(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-open-stder\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableBoolean::newVariable(((VariableFile *)this_)->value.openStdErr());
			};


			static TPointerOwner<Variable> fileRead(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-read\n");
#endif
				String  retV(1024,1024);  // first 1024, next + 1024 bytes
				Number ln;

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};
				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				if(FileX::read(((VariableFile *) this_)->value,retV,ln)) {
					return VariableString::newVariable(retV);
				};

				return Context::getValueUndefined();
			};


			static TPointerOwner<Variable> fileReadLn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-read-ln\n");
#endif
				String  retV(1024,1024);  // first 1024, next + 1024 bytes
				Number ln;

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				if(FileX::readLn(((VariableFile *) this_)->value,retV,ln)) {
					return VariableString::newVariable(retV);
				};

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> fileWrite(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-write\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable((Number)FileX::write(((VariableFile *) this_)->value,(arguments->index(0))->toString()));
			};


			static TPointerOwner<Variable> fileWriteLn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-write-ln\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable((Number)FileX::writeLn(((VariableFile *) this_)->value,(arguments->index(0))->toString()));
			};


			static TPointerOwner<Variable> fileClose(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-close\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				((VariableFile *) this_)->value.close();

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> releaseOwner(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-release-owner\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				((VariableFile *) this_)->value.releaseOwner();

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> aquireOwner(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-aquire-owner\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				((VariableFile *) this_)->value.aquireOwner();

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> fileFlush(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-flush\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				((VariableFile *) this_)->value.flush();

				return Context::getValueUndefined();
			};


			static TPointerOwner<Variable> fileSeekFromBegin(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-seek-from-begin\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				Number ln;

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				return VariableBoolean::newVariable(
					       ((VariableFile *)this_)->value.seekFromBegin(
						       (Integer)ln
					       )
				       );
			};

			static TPointerOwner<Variable> fileSeek(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-seek\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				Number ln;

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)) {
					return Context::getValueUndefined();
				};
				return VariableBoolean::newVariable(
					       ((VariableFile *)this_)->value.seek(
						       (Integer)ln
					       )
				       );
			};

			static TPointerOwner<Variable> fileSeekFromEnd(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-seek-from-end\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				Number ln;

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				return VariableBoolean::newVariable(
					       ((VariableFile *)this_)->value.seekFromEnd(
						       (Integer)ln
					       )
				       );
			};

			static TPointerOwner<Variable> fileSeekTell(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-seek-tell\n");
#endif


				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(
					       (Number)
					       ((VariableFile *)this_)->value.seekTell()
				       );
			};


			static TPointerOwner<Variable> fileReadToBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-read-to-buffer\n");
#endif
				size_t readLn;
				Number ln;

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				TPointerX<Variable> &buffer(arguments->index(0));

				if(buffer->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				ln=(arguments->index(1))->toNumber();
				if(isnan(ln)||signbit(ln)||ln==0.0) {
					((VariableBuffer *)buffer.value())->length=0;
					return VariableNumber::newVariable(0);
				};
				if(isinf(ln)) {
					ln=((VariableBuffer *)buffer.value())->size;
				};
				if(ln>((VariableBuffer *)buffer.value())->size) {
					ln=((VariableBuffer *)buffer.value())->size;
				};

				readLn=((VariableFile *) this_)->value.read(((VariableBuffer *)buffer.value())->buffer, ln);
				((VariableBuffer *)buffer.value())->length=readLn;
				return VariableNumber::newVariable(readLn);
			};

			static TPointerOwner<Variable> fileWriteFromBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- file-write-from-buffer\n");
#endif

				if(this_->variableType!=VariableFile::typeFile) {
					throw(Error("invalid parameter"));
				};

				TPointerX<Variable> &buffer(arguments->index(0));

				if(buffer->variableType!=VariableBuffer::typeBuffer) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable((Number)(((VariableFile *) this_)->value.write(
						((VariableBuffer *)buffer.value())->buffer,((VariableBuffer *)buffer.value())->length
								   )));
			};


			void initExecutive(Executive *executive,void *extensionId) {

				newContext(executive,extensionId);

				executive->setFunction2("Script.isFile(x)", isFile);
				executive->setFunction2("File.prototype.openReadOnly(file)",  fileOpenReadOnly);
				executive->setFunction2("File.prototype.openWrite(file)",  fileOpenWrite);
				executive->setFunction2("File.prototype.openReadAndWrite(file)",  fileOpenReadAndWrite);
				executive->setFunction2("File.prototype.openAppend(file)",  fileOpenAppend);
				executive->setFunction2("File.prototype.openStdIn()",  fileOpenStdIn);
				executive->setFunction2("File.prototype.openStdOut()",  fileOpenStdOut);
				executive->setFunction2("File.prototype.openStdErr()",  fileOpenStdErr);
				executive->setFunction2("File.prototype.read(size)",  fileRead);
				executive->setFunction2("File.prototype.readLn(size)",  fileReadLn);
				executive->setFunction2("File.prototype.write(str)",  fileWrite);
				executive->setFunction2("File.prototype.writeLn(str)",  fileWriteLn);
				executive->setFunction2("File.prototype.close()",  fileClose);
				executive->setFunction2("File.prototype.releaseOwner()",  releaseOwner);
				executive->setFunction2("File.prototype.aquireOwner()",  aquireOwner);
				executive->setFunction2("File.prototype.flush()",  fileFlush);
				executive->setFunction2("File.prototype.seekFromBegin(pos)",  fileSeekFromBegin);
				executive->setFunction2("File.prototype.seekFromEnd(pos)",  fileSeekFromEnd);
				executive->setFunction2("File.prototype.seek(pos)",  fileSeek);
				executive->setFunction2("File.prototype.seekTell()",  fileSeekTell);
				executive->setFunction2("File.prototype.readToBuffer(buffer)",  fileReadToBuffer);
				executive->setFunction2("File.prototype.writeFromBuffer(buffer)",  fileWriteFromBuffer);
			};

		};
	};
};




