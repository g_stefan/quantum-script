﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdbase64.hpp"

#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablebuffer.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdBase64 {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> encode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- base64-encode\n");
#endif
				return VariableString::newVariable(XO::Base64::encode((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> decode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- base64-decode\n");
#endif
				String result;
				if(XO::Base64::decode((arguments->index(0))->toString(),result)) {
					return VariableString::newVariable(result);
				};
				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> decodeToBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- base64-decode-to-buffer\n");
#endif
				String result;
				if(XO::Base64::decode((arguments->index(0))->toString(),result)) {
					return VariableBuffer::newObjectVFromString(result);
				};
				return Context::getValueUndefined();
			};

			void initExecutive(Executive *executive,void *extensionId) {
				XO::Base64::memoryInit();
				executive->compileStringX(
					"var Base64={};"
				);

				executive->setFunction2("Base64.encode(str)", encode);
				executive->setFunction2("Base64.decode(str)", decode);
				executive->setFunction2("Base64.decodeToBuffer(str)", decodeToBuffer);
			};

		};
	};
};


