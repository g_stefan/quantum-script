//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

Script.requireExtension("ShellFind");

Shell.getFileList_1=function(fileName) {
	var retV;
	var path;
	var k;
	var find;
	retV=[];
	path=Shell.getFilePathX(fileName);
	if(path) {} else {
		path="";
	};
	k=0;

	for(find=ShellFind(fileName); find.isValid(); find.next()) {
		if(find.isDirectory()) {
		} else {
			retV[k]=path+find.name();
			++k;
		};
	};
	return retV;
};

Shell.getFileList=function(fileName) {
	if(fileName.indexOf("*")>=0) {
		return Shell.getFileList_1(fileName);
	};
	if(fileName.indexOf("?")>=0) {
		return Shell.getFileList_1(fileName);
	};
	if(Shell.fileExists(fileName)) {
		return [fileName];
	};
	return [];
};

Shell.getDirList_1=function(fileName) {
	var retV;
	var path;
	var k;
	var find;
	retV=[];
	path=Shell.getFilePathX(fileName);
	if(path) {} else {
		path="";
	};
	k=0;
	for(find=ShellFind(fileName); find.isValid(); find.next()) {
		if(find.isDirectory()) {
			if(find.name()=="..") {
			} else if(find.name()==".") {
			} else {
				retV[k]=path+find.name();
				++k;
			};
		};
	};
	return retV;
};

Shell.getDirList=function(fileName) {
	if(fileName.indexOf("*")>=0) {
		return Shell.getDirList_1(fileName);
	};
	if(fileName.indexOf("?")>=0) {
		return Shell.getDirList_1(fileName);
	};
	if(Shell.directoryExists(fileName)) {
		return [fileName];
	};
	return [];
};

Shell.mkdirRecursively=function(dir) {
	var list;
	var index;
	var k;
	var ldir;
	dir=dir.replace("\\","/");
	if(dir==null) {
		return;
	};
	k=0;
	list=[];
	index=0;
	while(index>=0) {
		index=dir.indexOf("/");
		if(index<0) {
			index=dir.indexOf("\\");
		};
		if(index<0) {
			list[k]=dir;
			++k;
		} else {
			list[k]=dir.substring(0,index);
			dir=dir.substring(index+1);
			++k;
		};
	};
	ldir='';
	for(k=0; k<list.length; ++k) {
		ldir=ldir+list[k];
		Shell.mkdir(ldir);
		ldir=ldir+"/";
	};
};

Shell.removeEmptyDirRecursively=function(dir) {
	var retV;
	var path;
	var found;
	var dirList;
	var k;
	var find_;
	var find;
	dirList=[];
	k=0;
	find_=dir+"/*";
	for(find=ShellFind(find_); find.isValid(); find.next()) {
		if(find.isDirectory()) {
			if(find.name()=="..") {
			} else if(find.name()==".") {
			} else {
				dirList[k]=dir+"/"+find.name();
				++k;
			};
		} else {
			break;
		};
	};
	for(k=0; k<dirList.length; ++k) {
		Shell.removeEmptyDirRecursively(dirList[k]);
	};
	found=false;
	for(find=ShellFind(find_); find.isValid(); find.next()) {
		if(find.isDirectory()) {
			if(find.name()=="..") {
			} else if(find.name()==".") {
			} else {
				found=true;
				break;
			}
		} else {
			found=true;
			break;
		};
	};
	if(found) {
	} else {
		Shell.rmdir(dir);
	};
};

Shell.removeDirRecursively=function(dir) {
	var dirList;
	var fileList;
	var k;
	var m;
	var find;
	var find_;
	dirList=[];
	fileList=[];
	k=0;
	m=0;
	find_=dir+"/*";
	for(find=ShellFind(find_); find.isValid(); find.next()) {
		if(find.isDirectory()) {
			if(find.name()=="..") {
			} else if(find.name()==".") {
			} else {
				dirList[k]=dir+"/"+find.name();
				++k;
			};
		} else {
			fileList[m]=dir+"/"+find.name(resource);
			++m;
		};
	};
	for(k=0; k<dirList.length; ++k) {
		Shell.removeDirRecursively(dirList[k]);
	};
	for(m=0; m<fileList.length; ++m) {
		Shell.remove(fileList[m]);
	};
	Shell.rmdir(dir);
};

Shell.copyDirRecursively_1=function(source,target,base_) {
	var dirList;
	var fileList;
	var k;
	var m;
	var find;
	var find_;
	dirList=[];
	fileList=[];
	k=0;
	m=0;
	find_=source+'/*';
	for(find=ShellFind(find_); find.isValid(); find.next()) {
		if(find.isDirectory()) {
			if(find.name()=="..") {
			} else if(find.name()==".") {
			} else {
				dirList[k]=source+"/"+find.name();
				++k;
			};
		} else {
			fileList[m]=source+"/"+find.name();
			++m;
		};
	};
	for(k=0; k<dirList.length; ++k) {
		Shell.copyDirRecursively_1(dirList[k],target,base_);
	};
	Shell.mkdirRecursively(target+source.substring(base_.length));
	for(m=0; m<fileList.length; ++m) {
		Shell.copy(fileList[m],target+fileList[m].substring(base_.length));
	};
};

Shell.copyDirRecursively=function(source,target) {
	Shell.copyDirRecursively_1(source,target,source);
};

Shell.removeFileRecursively=function(dir,file) {
	var dirList;
	var fileList;
	var k;
	dirList=Shell.getDirList(dir+"/*");
	fileList=Shell.getFileList(dir+"/"+file);
	for(k=0; k<fileList.length; ++k) {
		Shell.remove(fileList[k]);
	};
	for(k=0; k<dirList.length; ++k) {
		Shell.removeFileRecursively(dirList[k],file);
	};
};

Shell.copyFilesToDirectory=function(source,target) {
	var list;
	var k;
	if(target==null) {
		return;
	};
	Shell.mkdirRecursively(target);
	list=Shell.getFileList(source);
	for(k=0; k<list.length; ++k) {
		Shell.copy(list[k],target+"/"+Shell.getFileName(list[k]));
	};
};

Shell.fileReplaceText=function(fileIn,fileOut,textInOut,lineMaxLength) {
	var fileIn_=new File();
	var fileOut_=new File();
	var line;
	var what;
	var retV=false;
	if(Script.isNil(lineMaxLength)) {
		lineMaxLength=16384;
	};
	if(fileIn_.openReadOnly(fileIn)) {
		if(fileOut_.openWrite(fileOut)) {
			for(;;) {
				line=fileIn_.readLn(lineMaxLength);
				if(Script.isUndefined(line)) {
					break;
				};
				for(what of textInOut) {
					line=line.replace(what[0],what[1]);
				};
				fileOut_.writeLn(line);
			};
			retV=true;
			fileOut_.close();
		};
		fileIn_.close();
	};
	return retV;
};

Shell.mkdirRecursivelyIfNotExists=function(path) {
	if(Shell.fileExists(path)) {
		return false;
	};
	if(Shell.directoryExists(path)) {
		return false;
	};
	Shell.mkdirRecursively(path);
	return true;
};

Shell.mkdirFilePath=function(file) {
	var path;
	path=Shell.getFilePath(file);
	if(path) {
		Shell.mkdirRecursivelyIfNotExists(path);
	};
};

Shell.copyFile=function(target,source) {
	Shell.mkdirFilePath(target);
	Shell.copy(source,target);
};

