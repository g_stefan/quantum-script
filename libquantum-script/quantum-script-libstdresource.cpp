//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdresource.hpp"

#include "quantum-script-variableresource.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdResource {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> drop(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- resource-drop\n");
#endif

				if(this_->variableType!=VariableResource::typeResource) {
					throw(Error("invalid parameter"));
				};

				((VariableResource *)( this_ ))->drop();
				return Context::getValueUndefined();;
			};

			static TPointerOwner<Variable> close(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- resource-close\n");
#endif

				if(this_->variableType!=VariableResource::typeResource) {
					throw(Error("invalid parameter"));
				};

				((VariableResource *)( this_ ))->close();
				return Context::getValueUndefined();;
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction2("Resource.prototype.close()", close);
				executive->setFunction2("Resource.prototype.drop()", drop);
			};

		};
	};
};


