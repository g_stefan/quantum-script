﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-context.hpp"
#include "quantum-script-variablerandom.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableRandom::typeRandom="{A2D9B22E-4185-45CE-BA5D-40989BD2947A}";
		const char *VariableRandom::strTypeRandom="Random";

		String VariableRandom::getType() {
			return strTypeRandom;
		};

		Variable *VariableRandom::newVariable() {
			return (Variable *) TMemory<VariableRandom>::newObject();
		};

		Variable &VariableRandom::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(Context::getPrototypeRandom())->prototype);
		};

		Variable *VariableRandom::instancePrototype() {
			return (Context::getPrototypeRandom())->prototype;
		};

		void VariableRandom::activeConstructor() {
			value.seed(0);
		};

		Variable *VariableRandom::clone(SymbolList &inSymbolList) {
			VariableRandom *out=(VariableRandom *)newVariable();
			out->value.copy(value);
			return out;
		};

		bool VariableRandom::toBoolean() {
			return true;
		};

		String VariableRandom::toString() {
			return strTypeRandom;
		};


	};
};


