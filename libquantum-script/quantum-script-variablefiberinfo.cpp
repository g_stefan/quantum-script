﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variablefiberinfo.hpp"
#include "quantum-script-prototype.hpp"
#include "quantum-script-context.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableFiberInfo::typeFiberInfo="{28E23920-7709-4599-8095-598905C1F62F}";
		const char *VariableFiberInfo::strTypeFiberInfo="FiberInfo";

		String VariableFiberInfo::getType() {
			return strTypeFiberInfo;
		};

		Variable *VariableFiberInfo::newVariable() {
			return (Variable *) TMemory<VariableFiberInfo>::newObject();
		};

		Variable &VariableFiberInfo::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,prototype->prototype);
		};

		Variable *VariableFiberInfo::instancePrototype() {
			return prototype->prototype;
		};

		bool VariableFiberInfo::hasMessage() {
			if(isSendMessage) {
				return true;
			};
			return (!messageQueue->isEmpty());
		};

		TPointerOwner<Variable> VariableFiberInfo::getMessageX() {
			TPointerOwner<Variable> retV;
			if(isSendMessage) {
				return receivedMessage;
			};
			if(messageQueue->popFromTail(retV)) {
				return retV;
			};
			return Context::getValueUndefined();
		};

		bool VariableFiberInfo::toBoolean() {
			return true;
		};

		String VariableFiberInfo::toString() {
			return strTypeFiberInfo;
		};


	};
};


