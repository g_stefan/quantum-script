//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-parser.hpp"
#include "quantum-script-parserasm.hpp"

namespace Quantum {
	namespace Script {


		bool Parser::statementFunctionArgument(int level) {
			token.reset();
			if (token.isSymbol()) {
				if (token.is("arguments")) {
					error = ParserError::Compile;
					return false;
				};
				if (token.is1(",")) {
					(functionArguments.head())->value.set(token.value, level);
					if (statementFunctionArgument(level + 1)) {
						return true;
					} else {
						return false;
					};
				};
				if (token.is1(")")) {
					(functionArguments.head())->value.set(token.value, level);
					return true;
				};
			};
			error = ParserError::Compile;
			return false;
		};

		bool Parser::statementFunction() {
			if (token.isSymbolX("function")) {
				ProgramCounter *linkFunctionBegin;
				ProgramCounter *linkFunctionEnd;
				ProgramCounter *linkFunctionDone;

				ProgramCounter *sourceFunctionBegin;
				int isAnonymous;

				dword fnBeginSourceSymbol;
				dword fnBeginSourcePos;
				dword fnBeginSourceLineNumber;
				dword fnBeginSourceLineColumn;

				isAnonymous = 0;
				if (token.checkIs1("(")) {
					isAnonymous = 1;
				} else {
					token.reset();
					if (token.isSymbol()) {

						if(functionLocalVariables.head()) {
							char buf[32];
							(functionLocalVariables.head())->value.set(token.value, (functionLocalVariableLevel.head())->value);
							sprintf(buf,"%d",(functionLocalVariableLevel.head())->value);
							++(functionLocalVariableLevel.head())->value;
							assemble1(ParserAsm::LocalVariablesPushObjectReference, buf);
							(functionHint.head())->value|=ParserFunctionHint::LocalVariables;
						} else {
							assemble1(ParserAsm::PushObjectReference, token.value);
						};
					} else {
						error = ParserError::Compile;
						return false;
					};
				};

				linkFunctionBegin = assembleProgramCounter(ParserAsm::XPushFunction, NULL);

				linkFunctionEnd = assembleProgramCounter(ParserAsm::Goto, NULL);

				sourceFunctionBegin=assemble(ParserAsm::Nop);
				linkProgramCounter(linkFunctionBegin,sourceFunctionBegin);

				fnBeginSourceSymbol=sourceSymbol;
				fnBeginSourcePos=sourcePos-1;
				fnBeginSourceLineNumber=sourceLineNumber;
				fnBeginSourceLineColumn=sourceLineColumn;

				if (token.is1("(")) {
					functionArguments.pushEmpty();
					(functionArguments.head())->value.empty();

					if (token.is1(")")) {
					} else if (statementFunctionArgument(0)) {
					} else {
						error = ParserError::Compile;
						return false;
					};

					functionLocalVariables.pushEmpty();
					(functionLocalVariables.head())->value.empty();
					functionLocalVariableLevel.pushEmpty();
					(functionLocalVariableLevel.head())->value=0;

					functionHint.push(ParserFunctionHint::None);
					functionArgumentsLevelHint.push(0);
					functionVariablesLevelHint.push(0);

					if (isBlockStatement()) {
						char buffer1[32];
						char buffer2[32];

						assemble(ParserAsm::PushUndefined);
						assemble(ParserAsm::Return);

						functionArguments.popEmpty();
						functionLocalVariables.popEmpty();
						functionLocalVariableLevel.popEmpty();

						if (isAnonymous) {

							linkFunctionDone=assemble(ParserAsm::Nop);

							linkProgramCounter(linkFunctionEnd,assemble(ParserAsm::Mark));
							linkProgramCounterEnd(linkFunctionBegin,linkFunctionDone);


						} else {

							linkFunctionDone=assemble(ParserAsm::Nop);
							linkProgramCounter(linkFunctionEnd,assemble(ParserAsm::Assign));
							linkProgramCounterEnd(linkFunctionBegin,linkFunctionDone);

						};

						assembler->linkProgramCounterSource(sourceFunctionBegin, fnBeginSourceSymbol, fnBeginSourcePos, fnBeginSourceLineNumber, fnBeginSourceLineColumn);
						assembler->linkProgramCounterSource(linkFunctionDone, sourceSymbol, sourcePos-1, sourceLineNumber, sourceLineColumn);

						sprintf(buffer1, "%p", linkFunctionBegin);
						sprintf(buffer2, "%d", (functionHint.head())->value);
						assembleX(ParserAsm::FunctionHint, buffer1, buffer2);

						functionHint.popEmpty();

						functionUpdateArgumentsLevelHint();
						functionArgumentsLevelHint.popEmpty();

						functionUpdateVariablesLevelHint();
						functionVariablesLevelHint.popEmpty();

						return true;

					};

				};

				error = ParserError::Compile;
				return false;
			};
			return false;
		};

		void Parser::setArgumentsLevelHint(int level) {
			if(level>(functionArgumentsLevelHint.head())->value) {
				(functionArgumentsLevelHint.head())->value=level;
			};
		};

		void Parser::functionUpdateArgumentsLevelHint() {
			TYList1<int> *hint;
			int level=(functionArgumentsLevelHint.head())->value;
			if(level) {
				for(hint=functionHint.head(); hint; hint=hint->next) {
					--level;
					if(level==0) {
						hint->value|=ParserFunctionHint::Arguments;
						break;
					} else {
						hint->value|=ParserFunctionHint::Arguments;
						hint->value|=ParserFunctionHint::ArgumentsLevel;
					};
				};
			};
		};


		void Parser::setVariablesLevelHint(int level) {
			if(level>(functionVariablesLevelHint.head())->value) {
				(functionVariablesLevelHint.head())->value=level;
			};
		};

		void Parser::functionUpdateVariablesLevelHint() {
			TYList1<int> *hint;
			int level=(functionVariablesLevelHint.head())->value;
			if(level) {
				for(hint=functionHint.head(); hint; hint=hint->next) {
					--level;
					if(level==0) {
						hint->value|=ParserFunctionHint::LocalVariables;
						break;
					} else {
						hint->value|=ParserFunctionHint::LocalVariables;
						hint->value|=ParserFunctionHint::LocalVariablesLevel;
					};
				};
			};
		};


	};
};


