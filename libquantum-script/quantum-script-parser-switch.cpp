//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-parser.hpp"
#include "quantum-script-parserasm.hpp"

namespace Quantum {
	namespace Script {


		bool Parser::statementSwitch() {
			if (token.isSymbolX("switch")) {
				ProgramCounter *linkSwitchBreak;

				if (expressionParentheses()) {

					if (token.is1("{")) {

						assemble(ParserAsm::EnterContext);
						linkSwitchBreak = assembleProgramCounter(ParserAsm::ContextSetBreak, NULL);
						assemble(ParserAsm::ContextSetRegisterValue);

						if (statementSwitchCase()) {
							if (token.is1("}")) {
								assemble(ParserAsm::LeaveContext);
								linkProgramCounter(linkSwitchBreak,
										   assemble(ParserAsm::Mark)
										  );
								return true;
							};
						};
					};
				};

				error = ParserError::Compile;
				return false;
			};
			return false;
		};

		bool Parser::statementSwitchCase() {
			if (token.isSymbolX("case")) {
				ProgramCounter *linkCaseIf;
				ProgramCounter *linkCaseSkip;

				if (expression(0)) {

					if (token.is1(":")) {

						assemble(ParserAsm::ContextPushRegisterValue);
						assemble(ParserAsm::OperatorEqual);
						linkCaseIf = assembleProgramCounter(ParserAsm::IfFalseGoto, NULL);

						while (!token.isEof()) {
							if (token.isSymbolX("case")) {
								linkCaseSkip = assembleProgramCounter(ParserAsm::Goto, NULL);
								linkProgramCounter(linkCaseIf,
										   assemble(ParserAsm::Mark)
										  );
								if (expression(0)) {
									if (token.is1(":")) {
										assemble(ParserAsm::ContextPushRegisterValue);
										assemble(ParserAsm::OperatorEqual);
										linkCaseIf = assembleProgramCounter(ParserAsm::IfFalseGoto, NULL);

										linkProgramCounter(linkCaseSkip,
												   assemble(ParserAsm::Mark)
												  );

										continue;
									};
								};
							};
							if (token.isSymbolX("default")) {
								if (token.is1(":")) {
									if(linkCaseIf!=NULL) {
										linkProgramCounter(linkCaseIf,
												   assemble(ParserAsm::Mark)
												  );
									};
									linkCaseIf = NULL;
									continue;

								};
								error = ParserError::Compile;
								return false;
							};
							if (token.checkIs1("{")) {
								if (isBlockStatement()) {
									continue;
								};
								error = ParserError::Compile;
								return false;
							};
							if (token.checkIs1("}")) {
								if (linkCaseIf != NULL) {
									linkProgramCounter(linkCaseIf,
											   assemble(ParserAsm::Mark)
											  );
								};
								return true;
							};
							if (statementOrExpression()) {
								continue;
							};
							break;
						};
					};
				};

				error = ParserError::Compile;
				return false;
			};
			return false;
		};

	};
};


