﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-instructionx.hpp"
#include "quantum-script-instructioncontext.hpp"
#include "quantum-script-variablevmfunction.hpp"
#include "quantum-script-executivex.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableVmFunction::typeVmFunction= "{341DBCC2-9D58-4E15-B9CA-9F215B8375AD}";
		const char *VariableVmFunction::strTypeVmFunction="Function";

		Variable *VariableVmFunction::newVariable(ProgramCounter *value) {
			VariableVmFunction *retV;
			retV=TMemory<VariableVmFunction>::newObject();
			retV->value = value;
			retV->valueEnd = NULL;
			retV->originalValue=value;
			retV->functionHint=ParserFunctionHint::All;
			return (Variable *) retV;
		};

		String VariableVmFunction::getType() {
			return strTypeVmFunction;
		};

		TPointerOwner<Variable> VariableVmFunction::functionApply(Variable *this_,VariableArray *arguments) {
			return  (ExecutiveX::getExecutive()).callVmFunction(this,this_,arguments);
		};

		TPointerX<Variable> &VariableVmFunction::operatorReferenceOwnProperty(Symbol symbolId) {
			if(symbolId==Context::getSymbolPrototype()) {
				return prototype->prototype;
			};
			throw Error("operatorReferenceOwnProperty");
		};

		Variable &VariableVmFunction::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolPrototype()) {
				return *prototype->prototype;
			};
			return operatorReferenceX(symbolId,(Context::getPrototypeFunction())->prototype);
		};

		Variable *VariableVmFunction::instancePrototype() {
			return (Context::getPrototypeFunction())->prototype;
		};

		bool VariableVmFunction::instanceOfPrototype(Prototype *&out) {
			out=prototype;
			return true;
		};

		String VariableVmFunction::toString() {
			if(valueEnd) {
				String out;
				TYList2<InstructionX> *index=(TYList2<InstructionX> *)value;
				TYList2<InstructionX> *indexEnd=(TYList2<InstructionX> *)valueEnd;

				String symbol;
				symbol=Context::getSymbolMirror(index->value.sourceSymbol);
				if(symbol.length()) {
					File inFile;
					MemoryFileRead inMemory;
					IRead *sourceFile = NULL;
					ISeek *sourceSeek = NULL;
					size_t ln=(indexEnd->value.sourcePos)-(index->value.sourcePos);

					if (symbol[0] == '#') {
						if (inFile.openRead((char *)symbol.index(1))) {
							sourceFile = &inFile;
							sourceSeek = &inFile;
						};
					};
					if (symbol[0] == '@') {
						if (inMemory.openRead((char *)symbol.index(1), symbol.length() - 1)) {
							sourceFile = &inMemory;
							sourceSeek = &inMemory;
						};
					};
					if (sourceFile) {
						String source(ln+1,0);

						sourceSeek->seekFromBegin(index->value.sourcePos);
						if(sourceFile->read(source.index(0),ln)==ln) {
							*(source.index(ln))=0;
							source.setLength(ln);

							out="function";
							out<<source;
							return out;
						};
					};
				};
			};
			throw Error("toString");
		};

		bool VariableVmFunction::toBoolean() {
			return true;
		};


	};
};


