//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_INSTRUCTIONX_HPP
#define QUANTUM_SCRIPT_INSTRUCTIONX_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PARSERASM_HPP
#include "quantum-script-parserasm.hpp"
#endif


namespace Quantum {
	namespace Script {

		class InstructionX;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::InstructionX>:
			public TMemoryObjectPoolActive<Quantum::Script::InstructionX> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class InstructionX;
		class InstructionContext;
		typedef void (*InstructionProcedure)(InstructionContext *context, Variable *operand);
		typedef TStack2<InstructionX> InstructionList;

		class InstructionX {
			public:
				InstructionProcedure procedure;
				Variable *operand;
				int asmType;
				dword sourceSymbol;
				dword sourcePos;
				dword sourceLineNumber;
				dword sourceLineColumn;

				inline InstructionX() {
					procedure = NULL;
					operand = NULL;
					asmType = ParserAsm::Unknown;
					sourceSymbol = 0;
					sourcePos= 0;
					sourceLineNumber = 0;
					sourceLineColumn = 0;
				};

				inline ~InstructionX() {
					if (operand) {
						operand->decReferenceCount();
					};
				};

				inline void activeDestructor() {
					if (operand) {
						operand->decReferenceCount();
						operand=NULL;
					};
				};

				inline InstructionX(InstructionX &value) {
					procedure = value.procedure;
					operand = value.operand;
				};

				inline InstructionX &operator=(InstructionX &value) {
					if (value.operand) {
						value.operand->incReferenceCount();
					};
					if (operand) {
						operand->decReferenceCount();
					};
					procedure = value.procedure;
					operand = value.operand;
					return *this;
				};

		};

	};
};


#endif
