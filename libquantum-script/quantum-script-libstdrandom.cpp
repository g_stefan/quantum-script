﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdrandom.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablerandom.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdRandom {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static Number toNumber_(Variable *value) {
				return ((Number)((static_cast<VariableRandom *>(value))->value.getValue()))/(double)4294967296.0;
			};

			static Number toInteger_(Variable *value) {
				return ((Number)((static_cast<VariableRandom *>(value))->value.getValue()));
			};

			static String toString_(Number value) {
				char buf[128];
				if(isnan(value)) {
					return "NaN";
				};
				if(isinf(value)) {
					if(signbit(value)) {
						return "-Infinity";
					} else {
						return "Infinity";
					};
				};
				if((value>0 && value<1e+11)||(value<0 && value>-1e+11)) {
					Number fractpart, intpart;
					fractpart=modf(value,&intpart);
					if(fractpart==0.0) {
						sprintf(buf, QUANTUM_SCRIPT_FORMAT_NUMBER_INTEGER, value);
					} else {
						sprintf(buf, QUANTUM_SCRIPT_FORMAT_NUMBER, value);
					};
				} else {
					sprintf(buf, QUANTUM_SCRIPT_FORMAT_NUMBER, value);
				};
				return buf;
			};


			static TPointerOwner<Variable> nextRandom(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- random-next-random\n");
#endif

				if(this_->variableType!=VariableRandom::typeRandom) {
					throw(Error("invalid parameter"));
				};

				((VariableRandom *)( this_ ))->value.nextRandom();
				return VariableNumber::newVariable(toNumber_(this_));
			};

			static TPointerOwner<Variable> toInteger(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- random-to-integer\n");
#endif

				if(this_->variableType!=VariableRandom::typeRandom) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(toInteger_(this_));
			};

			static TPointerOwner<Variable> toNumber(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- random-to-number\n");
#endif

				if(this_->variableType!=VariableRandom::typeRandom) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(toNumber_(this_));
			};

			static TPointerOwner<Variable> toString(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- random-to-string\n");
#endif

				if(this_->variableType!=VariableRandom::typeRandom) {
					throw(Error("invalid parameter"));
				};

				return VariableString::newVariable(toString_(toNumber_(this_)));
			};


			static TPointerOwner<Variable> seed(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- random-seed\n");
#endif

				if(this_->variableType!=VariableRandom::typeRandom) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};


				((VariableRandom *)( this_ ))->value.seed((Integer)value);

				return VariableBoolean::newVariable(true);
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction2("Random.prototype.next()",  nextRandom);
				executive->setFunction2("Random.prototype.toInteger()",  toInteger);
				executive->setFunction2("Random.prototype.toNumber()",  toNumber);
				executive->setFunction2("Random.prototype.toString()",  toString);
				executive->setFunction2("Random.prototype.seed(x)",  seed);
			};

		};
	};
};


