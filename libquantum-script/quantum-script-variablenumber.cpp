﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablenull.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableNumber::typeNumber="{14F9D487-6647-4D2E-9C5B-23F8A486F8BD}";
		const char *VariableNumber::strTypeNumber="Number";

		String VariableNumber::getType() {
			return strTypeNumber;
		};

		Variable *VariableNumber::newVariable(Number value) {
			VariableNumber *retV;
			retV=TMemory<VariableNumber>::newObject();
			retV->value = value;
			return (Variable *) retV;
		};

		Variable *VariableNumber::newVariableX(Number value) {
			VariableNumber *retV;
			retV=TMemory<VariableNumber>::newObjectX();
			retV->value = value;
			return (Variable *) retV;
		};

		Variable &VariableNumber::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(Context::getPrototypeNumber())->prototype);
		};

		Variable *VariableNumber::instancePrototype() {
			return (Context::getPrototypeNumber())->prototype;
		};

		Variable *VariableNumber::clone(SymbolList &inSymbolList) {
			return newVariable(value);
		};

		bool VariableNumber::toBoolean() {
			if(isnan(value)) {
				return false;
			};
			if(value) {
				return true;
			};
			return false;
		};

		Number VariableNumber::toNumber() {
			return value;
		};

		String VariableNumber::toString() {
			char buf[128];
			if(isnan(value)) {
				return "NaN";
			};
			if(isinf(value)) {
				if(signbit(value)) {
					return "-Infinity";
				} else {
					return "Infinity";
				};
			};
			if((value>0 && value<1e+11)||(value<0 && value>-1e+11)) {
				Number fractpart, intpart;
				fractpart=modf(value,&intpart);
				if(fractpart==0.0) {
					sprintf(buf, QUANTUM_SCRIPT_FORMAT_NUMBER_INTEGER, value);
				} else {
					sprintf(buf, QUANTUM_SCRIPT_FORMAT_NUMBER, value);
				};
			} else {
				sprintf(buf, QUANTUM_SCRIPT_FORMAT_NUMBER, value);
			};
			return buf;
		};

	};
};


