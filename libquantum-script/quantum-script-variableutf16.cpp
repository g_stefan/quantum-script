﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-variablestring.hpp"
#include "quantum-script-variableutf16.hpp"
#include "quantum-script-variableutf32.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablenull.hpp"
#include "quantum-script-variablesymbol.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableUtf16::typeUtf16="{3B92E0C1-55D6-486B-8B6F-7759AEE35FB5}";
		const char *VariableUtf16::strTypeUtf16="Utf16";

		String VariableUtf16::getType() {
			return strTypeUtf16;
		};

		Variable *VariableUtf16::newVariable(StringWord value) {
			VariableUtf16 *retV;
			retV = TMemory<VariableUtf16>::newObject();
			retV->value=value;
			return (Variable *) retV;
		};

		Variable &VariableUtf16::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolLength()) {
				if(vLength) {
					((VariableNumber *)vLength.value())->value=(Number)value.length();
				} else {
					vLength.setObject(VariableNumber::newVariable((Number)value.length()));
				};
				return *vLength;
			};
			return operatorReferenceX(symbolId,(Context::getPrototypeString())->prototype);
		};

		Variable *VariableUtf16::instancePrototype() {
			return (Context::getPrototypeUtf16())->prototype;
		};

		void VariableUtf16::memoryInit() {
			TMemory<StringWord>::memoryInit();
		};

		Variable *VariableUtf16::clone(SymbolList &inSymbolList) {
			return newVariable(value.value());
		};

		StringWord VariableUtf16::getUtf16(Variable *this_) {
			if(this_->variableType==VariableString::typeString) {
				return UtfX::utf16FromUtf8(((VariableString *)this_)->value);
			};
			if(this_->variableType==VariableUtf16::typeUtf16) {
				return ((VariableUtf16 *)this_)->value;
			};
			if(this_->variableType==VariableUtf32::typeUtf32) {
				return UtfX::utf16FromUtf32(((VariableUtf32 *)this_)->value);
			};
			return UtfX::utf16FromUtf8(this_->toString());
		};


		bool VariableUtf16::hasProperty(Variable *variable) {
			if(variable->variableType==VariableSymbol::typeSymbol) {
				if((static_cast<VariableSymbol *>(variable))->value==Context::getSymbolLength()) {
					return true;
				};
			};
			return (Context::getPrototypeUtf16())->prototype->hasProperty(variable);
		};

		bool VariableUtf16::toBoolean() {
			return (value.length()>0);
		};

		Number VariableUtf16::toNumber() {
			Number retV;
			if(sscanf(toString(), QUANTUM_SCRIPT_FORMAT_NUMBER_INPUT, &retV) == 1) {
				return retV;
			};
			return NAN;
		};

		String VariableUtf16::toString() {
			return UtfX::utf8FromUtf16(value);
		};

		bool VariableUtf16::isString() {
			return true;
		};


	};
};


