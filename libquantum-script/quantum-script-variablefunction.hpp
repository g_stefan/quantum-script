﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEFUNCTION_HPP
#define QUANTUM_SCRIPT_VARIABLEFUNCTION_HPP

#ifndef QUANTUM_SCRIPT_CONTEXT_HPP
#include "quantum-script-context.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROTOTYPE_HPP
#include "quantum-script-prototype.hpp"
#endif

#ifndef QUANTUM_SCRIPT_FUNCTIONPARENT_HPP
#include "quantum-script-functionparent.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEOBJECT_HPP
#include "quantum-script-variableobject.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#include "quantum-script-variablearray.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableFunction;

	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableFunction>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableFunction> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableFunction;
		typedef TPointerOwner<Variable> (*FunctionProcedure)(VariableFunction *function,Variable *this_,VariableArray *arguments);

		class VariableFunction :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableFunction);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeFunction;
				QUANTUM_SCRIPT_EXPORT static const char *strNativeFunction;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeFunction;

				TPointerX<Object> super;
				void *valueSuper;

				TPointerX<Prototype> prototype;

				TPointerX<FunctionParent> functionParent;
				FunctionProcedure functionProcedure;

				inline VariableFunction() {
					variableType = typeFunction;
					super.memoryLink(this);
					prototype.memoryLink(this);
					prototype.newObject();
					prototype->prototype.setObjectX(VariableObject::newVariableX());
					functionParent.memoryLink(this);
					functionProcedure=NULL;
					valueSuper=NULL;
				};

				inline void activeConstructor() {
					valueSuper=NULL;
					prototype.newObject();
					prototype->prototype.setObjectX(VariableObject::newVariableX());
				};

				inline void activeDestructor() {
					functionParent.deleteObject();
					prototype.deleteObject();
				};


				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(FunctionParent *functionParent,VariableArray *parentVariables,VariableArray *parentArguments,FunctionProcedure functionProcedure,Object *super,void *valueSuper);
				QUANTUM_SCRIPT_EXPORT static Variable *newVariableX(FunctionParent *functionParent,VariableArray *parentVariables,VariableArray *parentArguments,FunctionProcedure functionProcedure,Object *super,void *valueSuper);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> functionApply(Variable *this_,VariableArray *arguments);

				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();
				QUANTUM_SCRIPT_EXPORT bool instanceOfPrototype(Prototype *&out);

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};



#endif


