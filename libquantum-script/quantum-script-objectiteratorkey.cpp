﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-objectiteratorkey.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-variableobject.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablesymbol.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		bool ObjectIteratorKey::next(TPointerX<Variable> &out) {
			PropertyNode *next_;
			if(value) {
				out.setObject(VariableSymbol::newVariable(value->key));
				value=value->succesor();
				return true;
			};
			out=Context::getValueUndefined();
			return false;
		};

	};
};



