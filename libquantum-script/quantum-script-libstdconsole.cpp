﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdconsole.hpp"
#include "quantum-script-executivex.hpp"
#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablestring.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdConsole {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> write(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- console-write\n");
#endif

				printf("%s", ((arguments->index(0))->toString()).value());

				return Context::getValueUndefined();
			};


			static TPointerOwner<Variable> writeLn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- console-write-ln\n");
#endif

				printf("%s\r\n", ((arguments->index(0))->toString()).value());

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> readLn(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- console-read-ln\n");
#endif
				String  retV(1024,1024);  // first 1024, next + 1024 bytes
				Number ln;
				size_t readLn;
				size_t readToLn;
				size_t readTotal;
				size_t k;
				char buffer[2];

				buffer[1]=0;

				ln=(arguments->index(0))->toNumber();
				if(isnan(ln)||isinf(ln)||signbit(ln)) {
					return Context::getValueUndefined();
				};

				readToLn=(size_t)(ln);
				readTotal=0;
				if(readToLn<1) {
					return VariableString::newVariable("");
				};
				for(;;) {
					readLn=fread(buffer,sizeof(byte), 1,stdin);
					if(readLn>0) {

						if(buffer[0]=='\r') {
							if(readTotal+1>=readToLn) {
								retV.concatenate("\r",1);
								return VariableString::newVariable(retV);
							};
							readLn=fread(buffer,sizeof(byte), 1,stdin);
							if(readLn>0) {
								if(buffer[0]=='\n') {
									return VariableString::newVariable(retV);
									break;
								};
								retV.concatenate(buffer,1);
								readTotal+=2;
								if(readTotal>=readToLn) {
									return VariableString::newVariable(retV);
								};
								continue;
							};

							retV.concatenate("\r",1);
							//end of file
							return VariableString::newVariable(retV);
						};

						if(buffer[0]=='\n') {
							return VariableString::newVariable(retV);
						};

						retV.concatenate(buffer,1);
						readTotal++;
						if(readTotal>=readToLn) {
							return VariableString::newVariable(retV);
						};
						continue;
					};
					// connection interrupted - 0 to read ...
					if(readTotal==0) {
						break;
					};
					//end of file
					return VariableString::newVariable(retV);
				};

				return Context::getValueUndefined();
			};

			static TPointerOwner<Variable> keyHit(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- console-key-hit\n");
#endif
				return VariableBoolean::newVariable(XO::Console::keyHit());
			};

			static TPointerOwner<Variable> getChar(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- console-get-char\n");
#endif
				char buf[2];
				buf[0]=XO::Console::getChar();
				buf[1]=0;
				return VariableString::newVariable((&buf[0]));
			};

			static TPointerOwner<Variable> getKey(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- console-get-key\n");
#endif
				if(XO::Console::keyHit()) {
					char buf[2];
					buf[0]=XO::Console::getChar();
					buf[1]=0;
					return VariableString::newVariable((&buf[0]));
				};

				return VariableString::newVariable("");
			};

			void initExecutive(Executive *executive,void *extensionId) {
#ifdef XYO_OS_TYPE_UNIX
				setbuf(stdout,NULL);
#endif
				executive->compileStringX("var Console={};");

				executive->setFunction2("Console.write(str)", write);
				executive->setFunction2("Console.writeLn(str)", writeLn);
				executive->setFunction2("Console.readLn(max)", readLn);
				executive->setFunction2("Console.keyHit()", keyHit);
				executive->setFunction2("Console.getChar()", getChar);
				executive->setFunction2("Console.getKey()", getKey);
			};

		};
	};
};


