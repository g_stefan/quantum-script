﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEQUITMESSAGE_HPP
#define QUANTUM_SCRIPT_VARIABLEQUITMESSAGE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableQuitMessage;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableQuitMessage>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableQuitMessage> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableQuitMessage :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableQuitMessage);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeQuitMessage;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeQuitMessage;

				inline VariableQuitMessage() {
					variableType = typeQuitMessage;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};


	};
};


#endif

