//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_OBJECTITERATORVALUE_HPP
#define QUANTUM_SCRIPT_OBJECTITERATORVALUE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLEOBJECT_HPP
#include "quantum-script-variableobject.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {

		class ObjectIteratorValue;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ObjectIteratorValue>:
			public TMemoryObjectPoolActive<Quantum::Script::ObjectIteratorValue> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class ObjectIteratorValue :
			public Iterator {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ObjectIteratorValue);
			public:

				TPointer<VariableObject> value_;
				PropertyNode *value;
				TRedBlackTree<dword, String, TXMemoryPoolActive > *mirrorList;

				inline ObjectIteratorValue() {
				};

				QUANTUM_SCRIPT_EXPORT bool next(TPointerX<Variable> &out);

				inline void activeDestructor() {
					value_.deleteObject();
				};


		};

	};
};


#endif
