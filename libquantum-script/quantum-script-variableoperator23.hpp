﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEOPERATOR23_HPP
#define QUANTUM_SCRIPT_VARIABLEOPERATOR23_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROGRAMCOUNTER_HPP
#include "quantum-script-programcounter.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableOperator23;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableOperator23>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableOperator23> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableOperator23 :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableOperator23);
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeOperator23;

				ProgramCounter *linkBegin;
				ProgramCounter *pc;

				inline VariableOperator23() {
					variableType = typeOperator23;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif

