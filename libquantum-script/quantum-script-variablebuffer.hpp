﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEBUFFER_HPP
#define QUANTUM_SCRIPT_VARIABLEBUFFER_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableBuffer;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableBuffer>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableBuffer> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableBuffer :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableBuffer);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeBuffer;

				TPointer<Variable> vLength;
				TPointer<Variable> vSize;
			public:

				XYO::byte *buffer;
				size_t length;
				size_t size;

				QUANTUM_SCRIPT_EXPORT static const void *typeBuffer;

				inline VariableBuffer() {
					variableType = typeBuffer;
					buffer=NULL;
					length=0;
					size=0;
				};

				inline void activeDestructor() {
					if(buffer) {
						delete[] buffer;
						buffer=NULL;
						length=0;
						size=0;
					};
					vLength.deleteObject();
					vSize.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT  static Variable *newVariable(size_t size);
				QUANTUM_SCRIPT_EXPORT  static Variable *newObjectVFromString(String str_);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT Number toNumber();
				QUANTUM_SCRIPT_EXPORT String toString();
				QUANTUM_SCRIPT_EXPORT bool isString();
		};
	};
};


#endif
