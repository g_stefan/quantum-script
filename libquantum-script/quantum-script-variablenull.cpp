﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variablenull.hpp"
#include "quantum-script-context.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableNull::typeNull="{BD1A147D-2E59-4583-9E4B-C815A3EF5543}";
		const char *VariableNull::strTypeNull="null";

		String VariableNull::getType() {
			return strTypeNull;
		};

		Variable *VariableNull::newVariable() {
			return (Variable *) TMemory<VariableNull>::newObject();
		};

		Variable *VariableNull::clone(SymbolList &inSymbolList) {
			return newVariable();
		};

		bool VariableNull::toBoolean() {
			return false;
		};

		Number VariableNull::toNumber() {
			return 0;
		};

		String VariableNull::toString() {
			return strTypeNull;
		};


	};
};


