//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdfunction.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

#include "quantum-script-libstdobject.src"

namespace Quantum {
	namespace Script {

		namespace LibStdObject {

			using namespace XYO::XY;
			using namespace XYO::XO;

			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX((char *)libStdObjectSource);
			};

		};
	};
};


