﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstddatetime.hpp"

#include "quantum-script-variableboolean.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variabledatetime.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdDateTime {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> getYear(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-year\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getYear());
			};

			static TPointerOwner<Variable> getMonth(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-month\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getMonth());
			};

			static TPointerOwner<Variable> getDay(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-day\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getDay());
			};

			static TPointerOwner<Variable> getDayOfWeek(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-day-of-week\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getDayOfWeek());
			};

			static TPointerOwner<Variable> getHour(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-hour\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getHour());
			};

			static TPointerOwner<Variable> getMinute(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-minute\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getMinute());
			};

			static TPointerOwner<Variable> getSecond(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-second\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getSecond());
			};

			static TPointerOwner<Variable> getMilliseconds(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-get-miliseconds\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.getMilliseconds());
			};

			static TPointerOwner<Variable> setYear(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-year\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setYear((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setMonth(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-month\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setMonth((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setDay(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-day\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setDay((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setDayOfWeek(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-day-of-week\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setDayOfWeek((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setHour(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-hour\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setHour((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setMinute(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-minute\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setMinute((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setSecond(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-second\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setSecond((word)value);

				return VariableBoolean::newVariable(true);
			};

			static TPointerOwner<Variable> setMilliseconds(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-set-miliseconds\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.setMilliseconds((word)value);

				return VariableBoolean::newVariable(true);
			};


			static TPointerOwner<Variable> toUnixTime(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-to-unix-time\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				return VariableNumber::newVariable(((VariableDateTime *)this_)->value.toUnixTime());
			};

			static TPointerOwner<Variable> fromUnixTime(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- datetime-from-unix-time\n");
#endif
				if(this_->variableType!=VariableDateTime::typeDateTime) {
					throw(Error("invalid parameter"));
				};

				Number value=(arguments->index(0))->toNumber();
				if(isnan(value)||isinf(value)||signbit(value)) {
					return VariableBoolean::newVariable(false);
				};

				((VariableDateTime *)this_)->value.fromUnixTime((qword)value);

				return VariableBoolean::newVariable(true);
			};


			void initExecutive(Executive *executive,void *extensionId) {
				executive->setFunction2("DateTime.prototype.getYear()", getYear);
				executive->setFunction2("DateTime.prototype.getMonth()", getMonth);
				executive->setFunction2("DateTime.prototype.getDay()", getDay);
				executive->setFunction2("DateTime.prototype.getDayOfWeek()", getDayOfWeek);
				executive->setFunction2("DateTime.prototype.getHour()", getHour);
				executive->setFunction2("DateTime.prototype.getMinute()", getMinute);
				executive->setFunction2("DateTime.prototype.getSecond()", getSecond);
				executive->setFunction2("DateTime.prototype.getMilliseconds()", getMilliseconds);
				executive->setFunction2("DateTime.prototype.setYear(x)", setYear);
				executive->setFunction2("DateTime.prototype.setMonth(x)", setMonth);
				executive->setFunction2("DateTime.prototype.setDay(x)", setDay);
				executive->setFunction2("DateTime.prototype.setDayOfWeek(x)", setDayOfWeek);
				executive->setFunction2("DateTime.prototype.setHour(x)", setHour);
				executive->setFunction2("DateTime.prototype.setMinute(x)", setMinute);
				executive->setFunction2("DateTime.prototype.setSecond(x)", setSecond);
				executive->setFunction2("DateTime.prototype.setMilliseconds(x)", setMilliseconds);
				executive->setFunction2("DateTime.prototype.toUnixTime()", toUnixTime);
				executive->setFunction2("DateTime.prototype.fromUnixTime(x)", fromUnixTime);
			};

		};
	};
};


