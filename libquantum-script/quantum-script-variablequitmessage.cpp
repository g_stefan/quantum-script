﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variablequitmessage.hpp"
#include "quantum-script-variablenull.hpp"
#include "quantum-script-context.hpp"


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableQuitMessage::typeQuitMessage="{FB65DA32-E55A-40D5-9CA7-21E819017750}";
		const char *VariableQuitMessage::strTypeQuitMessage="QuitMessage";

		String VariableQuitMessage::getType() {
			return strTypeQuitMessage;
		};

		Variable *VariableQuitMessage::newVariable() {
			return (Variable *) TMemory<VariableQuitMessage>::newObject();
		};

		Variable *VariableQuitMessage::clone(SymbolList &inSymbolList) {
			return newVariable();
		};

		bool VariableQuitMessage::toBoolean() {
			return true;
		};

		String VariableQuitMessage::toString() {
			return strTypeQuitMessage;
		};


	};
};


