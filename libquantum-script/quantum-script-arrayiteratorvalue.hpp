//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ARRAYITERATORVALUE_HPP
#define QUANTUM_SCRIPT_ARRAYITERATORVALUE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#include "quantum-script-variablearray.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {
		class ArrayIteratorValue;
	};
};

namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ArrayIteratorValue>:
			public TMemoryObjectPoolActive<Quantum::Script::ArrayIteratorValue> {};
	};
};

namespace Quantum {
	namespace Script {

		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class ArrayIteratorValue :
			public Iterator {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ArrayIteratorValue);
			public:

				Integer index;
				TPointer<VariableArray> sourceArray;

				inline ArrayIteratorValue() {
				};

				QUANTUM_SCRIPT_EXPORT bool next(TPointerX<Variable> &out);

				inline void activeDestructor() {
					sourceArray.deleteObject();
				};
		};

	};
};


#endif
