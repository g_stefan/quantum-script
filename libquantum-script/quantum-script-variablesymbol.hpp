﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLESYMBOL_HPP
#define QUANTUM_SCRIPT_VARIABLESYMBOL_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_SYMBOLLIST_HPP
#include "quantum-script-symbollist.hpp"
#endif


namespace Quantum {
	namespace Script {

		class VariableSymbol;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableSymbol>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableSymbol> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableSymbol :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableSymbol);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeSymbol;

				TPointer<Variable> vLength;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeSymbol;

				Symbol value;

				inline VariableSymbol() {
					variableType = typeSymbol;
				};

				inline void activeDestructor() {
					vLength.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(Symbol value);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT String toString();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT Number toNumber();
				QUANTUM_SCRIPT_EXPORT bool isString();
		};

	};
};


#endif
