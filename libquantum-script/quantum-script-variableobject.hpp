﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEOBJECT_HPP
#define QUANTUM_SCRIPT_VARIABLEOBJECT_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROTOTYPE_HPP
#include "quantum-script-prototype.hpp"
#endif

namespace Quantum {
	namespace Script {


		class VariableObject;

	};
};

namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableObject>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableObject> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableObject :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableObject);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeObject;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeObject;

				TPointerX<Property> value;
				TPointerX<Prototype> prototype;

				inline VariableObject() {
					value.memoryLink(this);
					prototype.memoryLink(this);
					variableType = typeObject;
					value.newObject();
					prototype.newObject();
				};

				inline void activeConstructor() {
					prototype.newObject();
				};

				inline void activeDestructor() {
					prototype.deleteObject();
					value->activeDestructor();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();
				QUANTUM_SCRIPT_EXPORT static Variable *newVariableX();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();
				QUANTUM_SCRIPT_EXPORT bool findOwnProperty(Symbol symbolId,Variable *&out);
				QUANTUM_SCRIPT_EXPORT bool operatorDeleteIndex(Variable *variable);
				QUANTUM_SCRIPT_EXPORT bool operatorDeleteOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable &operatorIndex2(Variable *variable);
				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceIndex(Variable *variable);

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorKey();
				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorValue();

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);
				QUANTUM_SCRIPT_EXPORT bool hasProperty(Variable *variable);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif


