//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-symbollist.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		void SymbolList::memoryInit() {
			TMemory<XSymbolList>::memoryInit();
			TMemory<XSymbolListMirror>::memoryInit();
		};

		SymbolList::SymbolList() {
			lastId = 0;
		};

		Symbol SymbolList::getSymbol(String name) {
			Symbol id;
			if (symbolList.get(name, id)) {
				return id;
			};
			++lastId;
			symbolList.set(name, lastId);
			symbolListMirror.set(lastId, name);
			return lastId;
		};

		String SymbolList::getSymbolMirror(Symbol id) {
			XSymbolListMirror::Node *tmp=symbolListMirror.find(id);
			if(tmp) {
				return tmp->value;
			};
			throw(Error("getSymbolMirror"));
		};

	};
};



