﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variablefunctionwithyield.hpp"
#include "quantum-script-variablearray.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableFunctionWithYield::typeFunctionWithYield="{483FB52F-EEAB-46C5-89B3-850A8F65304F}";
		const char *VariableFunctionWithYield::strTypeFunction="Function";
		const char *VariableFunctionWithYield::strNativeFunction="NativeFunction";

		String VariableFunctionWithYield::getType() {
			return strTypeFunction;
		};

		Variable *VariableFunctionWithYield::newVariable(FunctionParent *functionParent,VariableArray *parentVariables,VariableArray *parentArguments,FunctionProcedureWithYield functionProcedure,Object *super,void *valueSuper) {
			VariableFunctionWithYield *retV;
			retV=TMemory<VariableFunctionWithYield>::newObject();
			if(functionParent||parentVariables||parentArguments) {
				retV->functionParent.newObject();
				retV->functionParent->functionParent=functionParent;
				retV->functionParent->variables=parentVariables;
				retV->functionParent->arguments=parentArguments;
			};
			retV->functionProcedure=functionProcedure;
			retV->super=super;
			retV->valueSuper=valueSuper;
			return (Variable *) retV;
		};


		TPointerOwner<Variable> VariableFunctionWithYield::functionApply(Variable *this_,VariableArray *arguments) {
			if(yieldStep) {
				return (*functionProcedure)(this,this_,arguments);
			};
			yieldVariables.setObject(VariableArray::newVariable());
			TPointerOwner<Variable> retV=(*functionProcedure)(this,this_,arguments);
			if(yieldStep) {
				return retV;
			};
			yieldVariables.setObject(VariableArray::newVariable());
			return retV;
		};

		TPointerX<Variable> &VariableFunctionWithYield::operatorReferenceOwnProperty(Symbol symbolId) {
			if(symbolId==Context::getSymbolPrototype()) {
				return prototype->prototype;
			};
			throw Error("operatorReferenceOwnProperty");
		};

		Variable &VariableFunctionWithYield::operatorReference(Symbol symbolId) {
			if(symbolId==Context::getSymbolPrototype()) {
				return *prototype->prototype;
			};
			return operatorReferenceX(symbolId,(Context::getPrototypeFunction())->prototype);
		};

		Variable *VariableFunctionWithYield::instancePrototype() {
			return (Context::getPrototypeFunction())->prototype;
		};

		bool VariableFunctionWithYield::instanceOfPrototype(Prototype *&out) {
			out=prototype;
			return true;
		};

		void VariableFunctionWithYield::memoryInit() {
			TMemory<Variable>::memoryInit();
			TMemory<Prototype>::memoryInit();
			TMemory<FunctionParent>::memoryInit();
		};

		bool VariableFunctionWithYield::toBoolean() {
			return true;
		};

		String VariableFunctionWithYield::toString() {
			return strTypeFunction;
		};


	};
};


