﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEFUNCTIONWITHYIELD_HPP
#define QUANTUM_SCRIPT_VARIABLEFUNCTIONWITHYIELD_HPP

#ifndef QUANTUM_SCRIPT_CONTEXT_HPP
#include "quantum-script-context.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROTOTYPE_HPP
#include "quantum-script-prototype.hpp"
#endif

#ifndef QUANTUM_SCRIPT_FUNCTIONPARENT_HPP
#include "quantum-script-functionparent.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#include "quantum-script-variablearray.hpp"
#endif

#ifndef QUANTUM_SCRIPT_VARIABLEOBJECT_HPP
#include "quantum-script-variableobject.hpp"
#endif

namespace Quantum {
	namespace Script {


		class VariableFunctionWithYield;

	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableFunctionWithYield>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableFunctionWithYield> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableFunctionWithYield;
		typedef TPointerOwner<Variable> (*FunctionProcedureWithYield)(VariableFunctionWithYield *function,Variable *this_,VariableArray *arguments);

		class VariableFunctionWithYield :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableFunctionWithYield);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeFunction;
				QUANTUM_SCRIPT_EXPORT static const char *strNativeFunction;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeFunctionWithYield;

				TPointerX<Object> super;
				void *valueSuper;

				TPointerX<Prototype> prototype;

				TPointerX<FunctionParent> functionParent;
				FunctionProcedureWithYield functionProcedure;

				dword yieldStep;
				TPointerX<Variable> yieldVariables;


				inline VariableFunctionWithYield() {
					variableType = typeFunctionWithYield;

					super.memoryLink(this);
					prototype.memoryLink(this);
					prototype.newObject();
					prototype->prototype.setObjectX(VariableObject::newVariableX());

					functionParent.memoryLink(this);
					yieldVariables.memoryLink(this);
					functionProcedure=NULL;
					yieldStep=0;
					yieldVariables.setObjectX(VariableArray::newVariableX());
					valueSuper=NULL;

				};

				inline void activeConstructor() {
					valueSuper=NULL;
					prototype.newObject();
					prototype->prototype.setObjectX(VariableObject::newVariableX());
					yieldStep=0;
					yieldVariables.setObjectX(VariableArray::newVariableX());
				};

				inline void activeDestructor() {
					functionParent.deleteObject();
					prototype.deleteObject();
					yieldVariables.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(FunctionParent *functionParent,VariableArray *parentVariables,VariableArray *parentArguments,FunctionProcedureWithYield functionProcedure,Object *super,void *valueSuper);


				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> functionApply(Variable *this_,VariableArray *arguments);

				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceOwnProperty(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT bool instanceOfPrototype(Prototype *&out);

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#define QUANTUM_SCRIPT_FUNCTIONWITHYIELD_BEGIN() \
	switch(function->yieldStep){ \
	case 0:


#define QUANTUM_SCRIPT_FUNCTIONWITHYIELD_YIELD(returnValue) \
	function->yieldStep=__LINE__;\
	return returnValue;\
	break;\
	case __LINE__:

#define QUANTUM_SCRIPT_FUNCTIONWITHYIELD_END(returnValue) \
	};\
	function->yieldStep=0;\
	return returnValue


#endif


