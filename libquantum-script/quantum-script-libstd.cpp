//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstd.hpp"
#include "quantum-script-libstdresource.hpp"
#include "quantum-script-libstdscript.hpp"
#include "quantum-script-libstderror.hpp"
#include "quantum-script-libstdstring.hpp"
#include "quantum-script-libstdarray.hpp"
#include "quantum-script-libstdconsole.hpp"
#include "quantum-script-libstdfiber.hpp"
#include "quantum-script-libstdthread.hpp"
#include "quantum-script-libstdconvert.hpp"
#include "quantum-script-libstdapplication.hpp"
#include "quantum-script-libstdmath.hpp"
#include "quantum-script-libstddatetime.hpp"
#include "quantum-script-libstdbase64.hpp"
#include "quantum-script-libstdmd5.hpp"
#include "quantum-script-libstdrandom.hpp"
#include "quantum-script-libstdsocket.hpp"
#include "quantum-script-libstdfile.hpp"
#include "quantum-script-libstdshellfind.hpp"
#include "quantum-script-libstdshell.hpp"
#include "quantum-script-libstdatomic.hpp"
#include "quantum-script-libstdjson.hpp"
#include "quantum-script-libstdbuffer.hpp"
#include "quantum-script-libstdcsv.hpp"
#include "quantum-script-libstdurl.hpp"
#include "quantum-script-libstdutf16.hpp"
#include "quantum-script-libstdutf32.hpp"
#include "quantum-script-libstdsha256.hpp"
#include "quantum-script-libstdsha512.hpp"
#include "quantum-script-libstdjob.hpp"
#include "quantum-script-libstdhttp.hpp"
#include "quantum-script-libstdfunction.hpp"
#include "quantum-script-libstdobject.hpp"
#include "quantum-script-libstdmake.hpp"
#include "quantum-script-libstdcodec.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStd {

			void initExecutive(Executive *executive,void *extensionId) {
#ifndef QUANTUM_SCRIPT_DISABLE_INIT_EXECUTIVE
				executive->initExtension(LibStdScript::initExecutive);
				executive->initExtension(LibStdError::initExecutive);
				executive->initExtension(LibStdFunction::initExecutive);
				executive->initExtension(LibStdObject::initExecutive);
				executive->initExtension(LibStdConvert::initExecutive);
				executive->initExtension(LibStdMath::initExecutive);
				executive->initExtension(LibStdString::initExecutive);
				executive->initExtension(LibStdArray::initExecutive);
				executive->initExtension(LibStdResource::initExecutive);
				executive->initExtension(LibStdConsole::initExecutive);
				executive->initExtension(LibStdApplication::initExecutive);
				executive->initExtension(LibStdDateTime::initExecutive);
				executive->initExtension(LibStdRandom::initExecutive);
				executive->initExtension(LibStdAtomic::initExecutive);
				executive->initExtension(LibStdJSON::initExecutive);
				executive->initExtension(LibStdBuffer::initExecutive);
				executive->initExtension(LibStdUtf16::initExecutive);
				executive->initExtension(LibStdUtf32::initExecutive);

#ifndef QUANTUM_SCRIPT_SINGLE_FIBER
				executive->initExtension(LibStdFiber::initExecutive);
#endif

#ifndef QUANTUM_SCRIPT_SINGLE_THREAD
				executive->initExtension(LibStdThread::initExecutive);
#endif
				//
				executive->registerInternalExtension("File",LibStdFile::initExecutive);
				executive->registerInternalExtension("Socket",LibStdSocket::initExecutive);
				executive->registerInternalExtension("ShellFind",LibStdShellFind::initExecutive);
				//
				executive->registerInternalExtension("Base64",LibStdBase64::initExecutive);
				executive->registerInternalExtension("MD5",LibStdMD5::initExecutive);
				executive->registerInternalExtension("Shell",LibStdShell::initExecutive);
				executive->registerInternalExtension("CSV",LibStdCSV::initExecutive);
				executive->registerInternalExtension("URL",LibStdURL::initExecutive);
				executive->registerInternalExtension("SHA256",LibStdSHA256::initExecutive);
				executive->registerInternalExtension("SHA512",LibStdSHA512::initExecutive);
				executive->registerInternalExtension("Job",LibStdJob::initExecutive);
				executive->registerInternalExtension("HTTP",LibStdHTTP::initExecutive);
				executive->registerInternalExtension("Make",LibStdMake::initExecutive);
				executive->registerInternalExtension("CoDec",LibStdCoDec::initExecutive);

#endif
			};

		};
	};
};


