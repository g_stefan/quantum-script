﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-instructioncontext.hpp"
#include "quantum-script-variablestring.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		InstructionContext::InstructionContext() {
			error = InstructionError::None;
		};

		bool InstructionContext::init() {

			configPrintStackTraceLimit=QUANTUM_SCRIPT_DEFAULT_STACK_TRACE_LEVEL;

			strLength__ = "length";
			strPrototype__ = "prototype";
			strArguments__ = "arguments";
			strString__ = "String";
			strArray__ = "Array";
			strMessage__ = "message";
			strError__ = "Error";
			strStackTrace__ = "stackTrace";
			strValue__ = "value";


			symbolLength__ = Context::getSymbol(strLength__);
			symbolPrototype__ = Context::getSymbol(strPrototype__);
			symbolArguments__ = Context::getSymbol(strArguments__);
			symbolString__ = Context::getSymbol(strString__);
			symbolArray__ = Context::getSymbol(strArray__);
			symbolMessage__ = Context::getSymbol(strMessage__);
			symbolError__ = Context::getSymbol(strError__);
			symbolStackTrace__ = Context::getSymbol(strStackTrace__);
			symbolValue__ = Context::getSymbol(strValue__);

			fiberInfo.newObject();
			fiberInfo->returnValue.setObject(VariableUndefined::newVariable());
			fiberInfo->isTerminated=false;
			fiberInfo->isSuspended=true;
			fiberCriticalSection=0;
			return true;
		};

		TPointerOwner<Variable> InstructionContext::newError(String str) {
			TPointer<Variable> constructor;
			VariableObject *retV_;
			retV_ = (VariableObject *) VariableObject::newVariable();
			retV_->value->setOwner(symbolMessage__, VariableString::newVariable(str));
			if (getFunctionX(symbolError__, constructor)) {
				retV_->prototype = ((VariableVmFunction *) constructor.value())->prototype;
			};
			return (Variable *) retV_;
		};

		void InstructionContext::memoryInit() {
			TMemory<TStack1<String> >::memoryInit();
			TMemory<TRedBlackTree<String,bool> >::memoryInit();
			TMemory<ExecutiveContext>::memoryInit();
			TMemory<ExecutiveContextPc>::memoryInit();
			TMemory<ExecutiveContextFunction>::memoryInit();
			TMemory<TStack2<InstructionTrace> >::memoryInit();
			TMemory<VariableFiberInfo>::memoryInit();
			TMemory<VariableThreadInfo>::memoryInit();
		};

	};
};


