﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLETHREADINFO_HPP
#define QUANTUM_SCRIPT_VARIABLETHREADINFO_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableThreadInfo;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableThreadInfo>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableThreadInfo> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class ThreadTransfer {
			public:
				Variable *threadFile;
				Variable *threadThis;
				Variable *threadArguments;
				Variable *threadInfo;
				void *super;
				void *this_;
				bool threadStarted;
				bool isFile;
		};

		class VariableThreadInfo :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableThreadInfo);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeThreadInfo;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeThreadInfo;

				Variable *returnValueLocal;
				void     *vmLocal;
				bool        finalizeLocal;
				bool        terminatedLocal;

				TStack2<TPointerX<Variable> > *messageQueueLocal;

				bool messageProcessed;
				bool isSendMessage;
				TPointerX<Variable> receivedMessage;
				Variable *messageToReceive;
				Variable *messageToSend;
				bool isPostMessage;
				bool typeOfMessage;

				TPointerX<Prototype> prototype;
				TPointerX<Variable> returnValueSuper;
				bool isTerminated;
				bool isSuspended;
				bool isBusy;
				bool waitChilds;

				inline VariableThreadInfo() {
					returnValueSuper.memoryLink(this);
					prototype.memoryLink(this);
					receivedMessage.memoryLink(this);
					variableType = typeThreadInfo;
					isSendMessage=false;
					messageProcessed=false;
					messageToReceive=NULL;
					messageToSend=NULL;
					isPostMessage=false;
					messageQueueLocal=NULL;
					typeOfMessage=false;
					isBusy=true;
					terminatedLocal=false;
					finalizeLocal=false;
					waitChilds=false;
				};

				inline void activeDestructor() {
					prototype.deleteObject();
					returnValueSuper.deleteObject();
					receivedMessage.deleteObject();
					isSendMessage=false;
					messageProcessed=false;
					isPostMessage=false;
					messageToReceive=NULL;
					messageToSend=NULL;
					messageQueueLocal=NULL;
					typeOfMessage=false;
					isBusy=true;
					terminatedLocal=false;
					finalizeLocal=false;
					waitChilds=false;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();
				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Variable> getMessageX();
				QUANTUM_SCRIPT_EXPORT bool hasMessage();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
