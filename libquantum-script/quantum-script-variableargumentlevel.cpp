﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableargumentlevel.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableArgumentLevel::typeArgumentLevel= "{E3D25A81-CBD1-4EF4-8E58-7CE3EDB6569B}";
		const char *VariableArgumentLevel::strTypeArgumentLevel="ArgumentLevel";

		String VariableArgumentLevel::getType() {
			return strTypeArgumentLevel;
		};

		Variable *VariableArgumentLevel::newVariable(int value, int level) {
			VariableArgumentLevel *retV;
			retV=TMemory<VariableArgumentLevel>::newObject();
			retV->value = value;
			retV->level = level;
			return (Variable *) retV;
		};

		bool VariableArgumentLevel::toBoolean() {
			return true;
		};

		String VariableArgumentLevel::toString() {
			return strTypeArgumentLevel;
		};


	};
};


