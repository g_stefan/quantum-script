//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_PROGRAMCOUNTER_HPP
#define QUANTUM_SCRIPT_PROGRAMCOUNTER_HPP

#ifndef QUANTUM_SCRIPT_HPP
#include "quantum-script.hpp"
#endif

namespace Quantum {
	namespace Script {


		class ProgramCounter;

	};
};


#endif
