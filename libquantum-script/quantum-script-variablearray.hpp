﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#define QUANTUM_SCRIPT_VARIABLEARRAY_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableArray;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableArray>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableArray> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableArray :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableArray);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeArray;
				TPointer<Variable> vLength;
			public:
				TPointerX<Array> value;

				QUANTUM_SCRIPT_EXPORT static const void *typeArray;

				inline VariableArray() {
					variableType = typeArray;
					value.memoryLink(this);
					value.newObject();
				};

				inline void activeDestructor() {
					value->activeDestructor();
					vLength.deleteObject();
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();
				QUANTUM_SCRIPT_EXPORT static Variable *newVariableX();

				inline static VariableArray *newArray() {
					return TMemory<VariableArray>::newObject();
				};

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorIndex(dword index);
				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);

				inline TPointerX<Variable> &index(dword index_) {
					TPointerX<Variable> &retV=(*value)[index_];
					if(!retV) {
						retV.setObjectX(VariableUndefined::newVariableX());
					};
					return retV;
				};

				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();
				QUANTUM_SCRIPT_EXPORT bool operatorDeleteIndex(Variable *variable);
				QUANTUM_SCRIPT_EXPORT Variable &operatorIndex2(Variable *variable);
				QUANTUM_SCRIPT_EXPORT TPointerX<Variable> &operatorReferenceIndex(Variable *variable);
				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorKey();
				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorValue();
				QUANTUM_SCRIPT_EXPORT bool hasProperty(Variable *variable);

				QUANTUM_SCRIPT_EXPORT static void memoryInit();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();

				QUANTUM_SCRIPT_EXPORT String join(String with_);
		};

	};
};



#endif
