//
// Quantum Script CoDec Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

Script.requireExtension("File");
Script.requireExtension("Shell");
Script.requireExtension("SHA512");
Script.requireExtension("Base64");

function CoDec() {
	.table=[];
	return this;
};

CoDec.prototype.buildTable=function() {
	.table=[];
	var rnd=new Random();
	rnd.seed((new DateTime()).toUnixTime());
	rnd.next();
	for(var k=0; k<1024; ++k) {
		.table[k]=SHA512.hashToBuffer(rnd.toInteger());
		rnd.next();
	};
};

CoDec.prototype.saveTable=function(fileName,key) {
	var table=[];
	for(var k=0; k<.table.length; ++k) {
		table[k]=Base64.encode(.table[k]);
	};
	return Shell.filePutContents(fileName,Buffer.prototype.fromString(JSON.encodeWithIndentation(table,0)).xorAvalancheEncode().xor(key));
};

CoDec.prototype.loadTable=function(fileName,key) {
	var table=JSON.decode(Buffer.prototype.fromString(Shell.fileGetContents(fileName)).xor(key).xorAvalancheDecode());
	.table=[];
	for(var k=0; k<table.length; ++k) {
		.table[k]=Base64.decodeToBuffer(table[k]);
	};
};

CoDec.prototype.encode=function(value) {
	var rnd=new Random();
	rnd.seed((new DateTime()).toUnixTime());
	rnd.next();
	var index=Math.floor(rnd.toInteger()%.table.length);
	rnd.next();
	var key=Base64.encode(SHA512.hashToBuffer(rnd.toInteger()));
	var data=Buffer.prototype.fromString(value);
	data.xor(SHA512.hashToBuffer(key+Base64.encode(.table[index])));
	data=Base64.encode(data.toString());
	var crc=Base64.encode(SHA512.hashToBuffer(SHA512.hash(value)+SHA512.hash(data)));
	return {
		index: index,
		key: key,
		data: data,
		crc: crc
	};
};

CoDec.prototype.encodeJSON=function(value) {
	return JSON.encodeWithIndentation(.encode(value),0);
};

CoDec.prototype.decode=function(value) {
	try {
		var buffer=Base64.decodeToBuffer(value.data);
		buffer.xor(SHA512.hashToBuffer(value.key+Base64.encode(.table[value.index])));
		var retV=buffer.toString();
		var crc=Base64.encode(SHA512.hashToBuffer(SHA512.hash(retV)+SHA512.hash(value.data)));
		if(value.crc==crc) {
			return retV;
		};
	} catch(e) {};
	return null;
};

CoDec.prototype.decodeJSON=function(valueJSON) {
	try {
		return .decode(JSON.decode(valueJSON));
	} catch(e) {};
	return null;
};

