//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

ThreadImplementation.prototype.onFinish=function(fn) {
	CurrentFiber.setInterval(function(fnX) {
		if(this.isTerminated()) {
			fnX(this.getReturnedValue());
			CurrentFiber.exit();
		};
	},1,this,[fn]);
};

ThreadImplementation.prototype.join=function() {
	while(!this.isTerminated()) {
		CurrentFiber.sleep(1);
	};
};

ThreadImplementation.prototype.sendMessage=function(message) {
	while(!this.sendMessageX(message)) {
		CurrentFiber.sleep(1);
	};
	while(!this.isMessageProcessedX()) {
		CurrentFiber.sleep(1);
	};
	return this.getMessageReturnValueX();
};

ThreadImplementation.prototype.sendQuitMessage=function() {
	return this.sendMessage(this.quitMessage());
};

ThreadImplementation.prototype.postQuitMessage=function() {
	return this.postMessage(this.quitMessage());
};

function newThread(fn,this_,parameters) {
	var thread=new ThreadImplementation(0,fn,this_,parameters);
	thread.resume();
	return thread;
};

function newThreadFromFile(file,this_,parameters) {
	var thread=new ThreadImplementation(1,file,this_,parameters);
	thread.resume();
	return thread;
};

function newThreadFromString(code,this_,parameters) {
	var thread=new ThreadImplementation(2,code,this_,parameters);
	thread.resume();
	return thread;
};

function newThreadWorker(fn,this_) {
	return newThread(function(fn) {
		var fn_=Script.execute("return "+fn+";");
		while(CurrentThread.getMessage()) {
			CurrentThread.messageProcessed(fn_.call(this,CurrentThread.message));
		};
	},this_,[Convert.toString(fn)]);
};

CurrentThread.getMessage=function() {
	CurrentThread.setIsBusyX(false);
	while(!CurrentThread.hasMessage()) {
		CurrentFiber.sleep(1);
	};
	CurrentThread.setIsBusyX(true);
	CurrentThread.message=CurrentThread.getMessageX();
	if(Script.isQuitMessage(CurrentThread.message)) {
		CurrentThread.messageProcessed(undefined);
		return false
	};
	return true
};
