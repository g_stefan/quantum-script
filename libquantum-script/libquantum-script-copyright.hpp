//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBQUANTUM_SCRIPT_COPYRIGHT_HPP
#define LIBQUANTUM_SCRIPT_COPYRIGHT_HPP

#define LIBQUANTUM_SCRIPT_COPYRIGHT            "Copyright (C) Grigore Stefan."
#define LIBQUANTUM_SCRIPT_PUBLISHER            "Grigore Stefan"
#define LIBQUANTUM_SCRIPT_COMPANY              LIBQUANTUM_SCRIPT_PUBLISHER
#define LIBQUANTUM_SCRIPT_CONTACT              "g_stefan@yahoo.com"
#define LIBQUANTUM_SCRIPT_FULL_COPYRIGHT       LIBQUANTUM_SCRIPT_COPYRIGHT " <" LIBQUANTUM_SCRIPT_CONTACT ">"

#ifndef XYO_RC

#ifndef QUANTUM_SCRIPT__EXPORT_HPP
#include "quantum-script--export.hpp"
#endif

namespace Lib {
	namespace Quantum {
		namespace Script {

			class Copyright {
				public:
					QUANTUM_SCRIPT_EXPORT static const char *copyright();
					QUANTUM_SCRIPT_EXPORT static const char *publisher();
					QUANTUM_SCRIPT_EXPORT static const char *company();
					QUANTUM_SCRIPT_EXPORT static const char *contact();
					QUANTUM_SCRIPT_EXPORT static const char *fullCopyright();

			};

		};
	};
};

#endif
#endif
