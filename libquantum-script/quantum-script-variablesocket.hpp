﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLESOCKET_HPP
#define QUANTUM_SCRIPT_VARIABLESOCKET_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {


		class VariableSocket;

	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableSocket>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableSocket> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableSocket :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableSocket);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeSocket;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeSocket;

				Socket value;

				inline VariableSocket() {
					variableType = typeSocket;
				};

				QUANTUM_SCRIPT_EXPORT void activeDestructor();

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif

