﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEARGUMENTLEVEL_HPP
#define QUANTUM_SCRIPT_VARIABLEARGUMENTLEVEL_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableArgumentLevel;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableArgumentLevel>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableArgumentLevel> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableArgumentLevel :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableArgumentLevel);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeArgumentLevel;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeArgumentLevel;

				int value;
				int level;

				inline VariableArgumentLevel() {
					variableType = typeArgumentLevel;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(int value, int level);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
