//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ASSOCIATIVEARRAYITERATORVALUE_HPP
#define QUANTUM_SCRIPT_ASSOCIATIVEARRAYITERATORVALUE_HPP

#ifndef QUANTUM_SCRIPT_VARIABLEASSOCIATIVEARRAY_HPP
#include "quantum-script-variableassociativearray.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {

		class AssociativeArrayIteratorValue;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::AssociativeArrayIteratorValue>:
			public TMemoryObjectPoolActive<Quantum::Script::AssociativeArrayIteratorValue> {};
	};
};

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class AssociativeArrayIteratorValue :
			public Iterator {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(AssociativeArrayIteratorValue);
			public:

				Integer index;
				TPointer<VariableAssociativeArray> sourceArray;

				inline AssociativeArrayIteratorValue() {
				};

				QUANTUM_SCRIPT_EXPORT bool next(TPointerX<Variable> &out);

				inline void activeDestructor() {
					sourceArray.deleteObject();
				};

		};

	};
};



#endif
