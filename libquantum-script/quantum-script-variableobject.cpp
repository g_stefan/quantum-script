﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "quantum-script-variableobject.hpp"
#include "quantum-script-context.hpp"
#include "quantum-script-objectiteratorkey.hpp"
#include "quantum-script-objectiteratorvalue.hpp"

#include "quantum-script-variablefunction.hpp"
#include "quantum-script-variablefunctionwithyield.hpp"
#include "quantum-script-variablevmfunction.hpp"
#include "quantum-script-variablenativevmfunction.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableObject::typeObject="{1F81286F-F646-4DCD-B5B5-EEB61C2BFDC0}";
		const char *VariableObject::strTypeObject="Object";

		String VariableObject::getType() {
			return strTypeObject;
		};


		Variable *VariableObject::newVariable() {
			return (Variable *) TMemory<VariableObject>::newObject();
		};

		Variable *VariableObject::newVariableX() {
			return (Variable *) TMemory<VariableObject>::newObjectX();
		};


		TPointerX<Variable> &VariableObject::operatorReferenceOwnProperty(Symbol symbolId) {
			PropertyNode *outX;
			outX = value->find(symbolId);
			if (outX) {
				return outX->value;
			};
			outX=Property::NodeMemory::memoryNew();
			outX->key=symbolId;
			outX->value.memoryLink(this);
			outX->value.setObject(VariableUndefined::newVariable());
			value->insertNode(outX);
			return outX->value;
		};

		bool VariableObject::findOwnProperty(Symbol symbolId,Variable *&out) {
			PropertyNode *outX;
			outX = value->find(symbolId);
			if (outX) {
				out=outX->value;
				return true;
			};
			return false;
		};

		Variable &VariableObject::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,this);
		};

		Variable *VariableObject::instancePrototype() {
			if(prototype->prototype) {
				return prototype->prototype;
			};
			if(((Variable *)this)==(Context::getPrototypeObject())->prototype) {
				return NULL;
			};
			return (Context::getPrototypeObject())->prototype;
		};

		bool VariableObject::operatorDeleteIndex(Variable *variable) {
			PropertyNode *outX;
			Symbol symbolId=Context::getSymbol(variable->toString());
			value->remove(symbolId);
			return true;
		};

		bool VariableObject::operatorDeleteOwnProperty(Symbol symbolId) {
			value->remove(symbolId);
			return true;
		};


		Variable &VariableObject::operatorIndex2(Variable *variable) {
			PropertyNode *outX;
			Symbol symbolId=Context::getSymbol(variable->toString());
			outX = value->find(symbolId);
			if (outX) {
				return  *(outX->value);
			};
			return *(Context::getValueUndefined());
		};

		TPointerX<Variable> &VariableObject::operatorReferenceIndex(Variable *variable) {
			PropertyNode *outX;
			Symbol symbolId=Context::getSymbol(variable->toString());
			outX = value->find(symbolId);
			if (outX) {
				return outX->value;
			};
			outX=Property::NodeMemory::memoryNew();
			outX->key=symbolId;
			outX->value.memoryLink(this);
			outX->value.setObject(VariableUndefined::newVariable());
			value->insertNode(outX);
			return outX->value;
		};

		TPointerOwner<Iterator> VariableObject::getIteratorKey() {
			TPointerOwner<Iterator> retV;
			ObjectIteratorKey *iterator_=TMemory<ObjectIteratorKey>::newObject();
			iterator_->value_=this;
			iterator_->value=value->begin();
			retV.setObject(iterator_);
			return retV;
		};

		TPointerOwner<Iterator> VariableObject::getIteratorValue() {
			TPointerOwner<Iterator> retV;
			ObjectIteratorValue *iterator_=TMemory<ObjectIteratorValue>::newObject();
			iterator_->value_=this;
			iterator_->value=value->begin();
			retV.setObject(iterator_);
			return retV;
		};

		void VariableObject::memoryInit() {
			TMemory<Variable>::memoryInit();
			TMemory<Property>::memoryInit();
			TMemory<Prototype>::memoryInit();
		};

		Variable *VariableObject::clone(SymbolList &inSymbolList) {
			VariableObject *out=(VariableObject *)newVariable();
			TYRedBlackTreeNode<dword, TPointerX<Variable> > *scan;
			Symbol symbolId;
			String symbolString;

			for(scan=value->begin(); scan; scan=scan->succesor()) {
				if(inSymbolList.symbolListMirror.get(scan->key,symbolString)) {
					symbolId=Context::getSymbol(symbolString.value());
					out->value->setOwner(symbolId,scan->value->clone(inSymbolList));
				};
			};
			// don't copy prototype ...
			return out;
		};

		bool VariableObject::hasProperty(Variable *variable) {
			PropertyNode *outX;
			Symbol symbolId=Context::getSymbol(variable->toString());
			outX = value->find(symbolId);
			if (outX) {
				return true;
			};
			Variable *prototype_ = instancePrototype();
			if(prototype_) {
				return prototype_->hasProperty(variable);
			};
			return false;
		};

		bool VariableObject::toBoolean() {
			return true;
		};

		String VariableObject::toString() {
			Variable &variable=operatorReference(Context::getSymbolToString());
			if(

				(variable.variableType == VariableFunction::typeFunction)||
				(variable.variableType == VariableFunctionWithYield::typeFunctionWithYield)||
				(variable.variableType == VariableVmFunction::typeVmFunction)||
				(variable.variableType == VariableNativeVmFunction::typeNativeVmFunction)
			) {
				TPointerOwner<VariableArray> arguments(VariableArray::newArray());
				return (variable.functionApply(this,arguments))->toString();
			};
			return strTypeObject;
		};


	};
};


