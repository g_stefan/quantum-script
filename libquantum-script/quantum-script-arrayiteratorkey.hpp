//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_ARRAYITERATORKEY_HPP
#define QUANTUM_SCRIPT_ARRAYITERATORKEY_HPP

#ifndef QUANTUM_SCRIPT_VARIABLEARRAY_HPP
#include "quantum-script-variablearray.hpp"
#endif

#ifndef QUANTUM_SCRIPT_ITERATOR_HPP
#include "quantum-script-iterator.hpp"
#endif

namespace Quantum {
	namespace Script {
		class ArrayIteratorKey;
	};
};

namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::ArrayIteratorKey>:
			public TMemoryObjectPoolActive<Quantum::Script::ArrayIteratorKey> {};
	};
};

namespace Quantum {
	namespace Script {

		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class ArrayIteratorKey :
			public Iterator {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(ArrayIteratorKey);
			public:

				Integer index;
				TPointer<VariableArray> sourceArray;

				inline ArrayIteratorKey() {
				};

				QUANTUM_SCRIPT_EXPORT bool next(TPointerX<Variable> &out);

				inline void activeDestructor() {
					sourceArray.deleteObject();
				};

		};

	};
};

#endif

