//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_PARSERASM_HPP
#define QUANTUM_SCRIPT_PARSERASM_HPP

namespace Quantum {
	namespace Script {


		class ParserAsm {
			public:

				enum {
					Unknown,
					Nop,
					Mark,
					PushUndefined,
					PushSymbol,
					PushNumber,
					PushNaN,
					PushInfinity,
					OperatorAssign,
					OperatorEqual,
					OperatorEqualStrict,
					OperatorPlus,
					OperatorMinus,
					OperatorUnaryPlus,

					OperatorMul,
					OperatorDiv,
					OperatorMod,
					OperatorNotEqual,
					OperatorNotEqualStrict,
					OperatorNot,
					OperatorLessThanOrEqual,
					OperatorLessThan,
					OperatorGreaterThanOrEqual,
					OperatorGreaterThan,
					OperatorLogicalOr,
					OperatorBitwiseOr,
					OperatorLogicalAnd,
					OperatorBitwiseAnd,
					OperatorBitwiseNot,
					OperatorBitwiseLeftShift,
					OperatorBitwiseRightShift,
					OperatorBitwiseRightShiftZero,
					OperatorBitwiseXor,

					OperatorAssignPlus,
					OperatorAssignMinus,
					OperatorAssignMod,
					OperatorAssignDiv,
					OperatorAssignMul,
					OperatorAssignBitwiseOr,
					OperatorAssignBitwiseAnd,
					OperatorAssignBitwiseLeftShift,
					OperatorAssignBitwiseRightShift,
					OperatorAssignBitwiseRightShiftZero,
					OperatorAssignBitwiseXor,

					OperatorConditional,
					OperatorIn,

					Reference,
					EnterContext,
					LeaveContext,
					IfFalseGoto,
					Goto,
					ContextSetBreak,
					ContextSetContinue,
					Break,
					Continue,
					IfTrueGoto,
					OperatorEqual1,
					Pop1,
					PushNull,
					XPushFunction,
					Return,
					ContextSetCatch,
					ContextSetFinally,
					Throw,
					ContextSetThis,
					ContextPushThis,
					PushNewObject,
					PushString,
					ContextSetTryBreak,
					ContextSetTryContinue,
					ContextSetTryReturn,
					ContextSetTryThrow,
					EnterFirstContext,
					Assign,
					OperatorReference,
					PushNewArray,
					ArrayPush,
					OperatorArrayPush,
					PushObjectReference,
					ReferenceObjectReference,
					OperatorInstanceOf,
					OperatorTypeOf,

					OperatorReferenceReference,
					OperatorArrayPushReference,
					OperatorObjectReferenceValue,
					OperatorPlusPlusLeft,
					OperatorPlusPlusRight,
					OperatorMinusMinusLeft,
					OperatorMinusMinusRight,

					XPushNewArray,
					XArrayPushWithTransfer,
					XCall,
					XCallThis,
					XCallThisModeCall,
					XCallThisModeApply,
					XCallWithThisReference,
					XCallSymbol,
					AssignReverse,

					Duplicate,
					PushBoolean,

					OperatorAssignXPrototype,
					ArgumentsPushObjectReference,
					ArgumentsPushSymbol,

					AssignNewObject,
					Catch,

					ArgumentsLevelPushObjectReference,
					ArgumentsLevelPushSymbol,

					LocalVariablesPushObjectReference,
					LocalVariablesPushSymbol,
					LocalVariablesLevelPushObjectReference,
					LocalVariablesLevelPushSymbol,
					EndExecution,
					Yield,

					ContextSetReference,
					OperatorSetReferenceIndexKey,
					OperatorNextReferenceIndex,
					OperatorSetReferenceIndexValue,
					ContextSetRegisterValue,
					ContextPushRegisterValue,

					FunctionHint,
					ContextPushSelf,
					CurrentFiberExit,
					PushArguments,
					InstructionListExtractAndDelete,
					OperatorReferenceDeleteIndex,
					OperatorReferenceDeleteReference,

					//--- optimization
					SymbolPlusPlus,
					OperatorMinusArgumentsSymbolXNumber,
					OperatorMinusLocalVariablesSymbol2,
					OperatorPlusLocalVariablesSymbol2,
					IfArgumentsSymbolNotEqualNumberGoto,
					IfSymbolNotLessThanNumberGoto,
					IfArgumentsSymbolNotLessThanNumberGoto,
					OperatorMulLocalVariablesSymbol2,
					IfNotGreaterThanGoto,
					//--- stack garbage collect on try/throw/catch/finally
					ContextSetStack,
					//---
					ClearIncludedFile,
					End
				};
		};

	};
};


#endif
