﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLERANDOM_HPP
#define QUANTUM_SCRIPT_VARIABLERANDOM_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {


		class VariableRandom;

	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableRandom>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableRandom> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableRandom :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableRandom);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeRandom;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeRandom;

				RandomMT value;

				inline VariableRandom() {
					variableType = typeRandom;
				};

				QUANTUM_SCRIPT_EXPORT void activeConstructor();

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT Variable *clone(SymbolList &inSymbolList);

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};


	};
};


#endif

