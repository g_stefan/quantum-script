﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLESHELLFIND_HPP
#define QUANTUM_SCRIPT_VARIABLESHELLFIND_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

namespace Quantum {
	namespace Script {


		class VariableShellFind;

	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableShellFind>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableShellFind> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		class VariableShellFind :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableShellFind);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeShellFind;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeShellFind;

				ShellFind value;

				inline VariableShellFind() {
					variableType = typeShellFind;
				};

				QUANTUM_SCRIPT_EXPORT void activeDestructor();

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable();

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT Variable &operatorReference(Symbol symbolId);
				QUANTUM_SCRIPT_EXPORT Variable *instancePrototype();

				QUANTUM_SCRIPT_EXPORT TPointerOwner<Iterator> getIteratorValue();


				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};


	};
};


#endif

