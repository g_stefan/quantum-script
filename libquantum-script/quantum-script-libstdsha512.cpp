﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdsha512.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-variablebuffer.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdSHA512 {

			using namespace XYO::XY;
			using namespace XYO::XO;

			static TPointerOwner<Variable> hash(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- sha512-hash\n");
#endif
				return VariableString::newVariable(XO::SHA512Hash::getHashString((arguments->index(0))->toString()));
			};

			static TPointerOwner<Variable> hashToBuffer(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- sha512-hash-to-buffer\n");
#endif
				TPointerOwner<Variable> retV(VariableBuffer::newVariable(64));
				((VariableBuffer *)retV.value())->length=64;
				XO::SHA512Hash::hashStringToBytes((arguments->index(0))->toString(),((VariableBuffer *)retV.value())->buffer);
				return retV;
			};

			void initExecutive(Executive *executive,void *extensionId) {
				XO::SHA512Hash::memoryInit();
				executive->compileStringX("var SHA512={};");
				executive->setFunction2("SHA512.hash(str)", hash);
				executive->setFunction2("SHA512.hashToBuffer(str)", hashToBuffer);
			};

		};
	};
};


