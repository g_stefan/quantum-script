﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "quantum-script-libstdcsv.hpp"
#include "quantum-script-variablestring.hpp"

//#define QUANTUM_SCRIPT_DEBUG_RUNTIME

namespace Quantum {
	namespace Script {

		namespace LibStdCSV {

			using namespace XYO::XY;
			using namespace XYO::XO;


			static TPointerOwner<Variable> decode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- csv-decode\n");
#endif
				String in=(arguments->index(0))->toString();
				Variable *out=VariableArray::newVariable();
				String out_;
				size_t k;
				size_t ln;

				out_="";
				ln=0;
				for(k=0; k<in.length(); ++k) {
					if(in[k]==',') {
						(out->operatorIndex(ln)).setObject(VariableString::newVariable(out_));
						ln++;
						out_="";
						continue;
					};
					if(in[k]=='\"') {
						++k;
						for(; k<in.length(); ++k) {
							if(in[k]=='\"') {
								if(k+1<in.length()) {
									if(in[k+1]=='\"') {
										out_<<'\"';
										++k;
										continue;
									};
								};
								break;
							};
							out_<<in[k];
						};
						continue;
					};
					out_<<in[k];
				};
				(out->operatorIndex(ln)).setObject(VariableString::newVariable(out_));

				return out;
			};

			static TPointerOwner<Variable> encode(VariableFunction *function,Variable *this_,VariableArray *arguments) {
#ifdef QUANTUM_SCRIPT_DEBUG_RUNTIME
				printf("- csv-encode\n");
#endif

				Variable *in=(arguments->index(0)).value();
				String out;

				String out_;
				bool useComa;
				bool useEscape;

				if(in->variableType!=VariableArray::typeArray) {
					throw(Error("invalid parameter"));
				};

				size_t k;
				size_t sz;

				useComa=false;
				for(k=0; k<((VariableArray *)in)->value->length(); ++k) {
					if(useComa) {
						out<<',';
					};
					useEscape=false;
					out_=(in->operatorIndex(k))->toString();
					if(StringX::indexOf(out_," ",0,sz)) {
						useEscape=true;
					};
					if(StringX::indexOf(out_,"\t",0,sz)) {
						useEscape=true;
					};
					if(StringX::indexOf(out_,",",0,sz)) {
						useEscape=true;
					};
					if(StringX::indexOf(out_,"\"",0,sz)) {
						useEscape=true;
						out_=StringX::replace(out_,"\"","\"\"");
					};
					if(useEscape) {
						out<<'"';
					};
					out<<out_;
					if(useEscape) {
						out<<'"';
					};
					useComa=true;
				};

				return VariableString::newVariable(out);
			};

			void initExecutive(Executive *executive,void *extensionId) {
				executive->compileStringX("var CSV={};");
				executive->setFunction2("CSV.decode(str)", decode);
				executive->setFunction2("CSV.encode(value)", encode);
			};

		};
	};
};


