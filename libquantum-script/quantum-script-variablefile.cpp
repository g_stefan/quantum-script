﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "quantum-script-context.hpp"
#include "quantum-script-variablefile.hpp"
#include "quantum-script-variablenumber.hpp"
#include "quantum-script-variablestring.hpp"
#include "quantum-script-libstdfile.hpp"

namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;

		const void *VariableFile::typeFile="{81FA9545-8011-4BE0-9268-90383DDF8793}";
		const char *VariableFile::strTypeFile="File";

		String VariableFile::getType() {
			return strTypeFile;
		};

		Variable *VariableFile::newVariable() {
			return (Variable *) TMemory<VariableFile>::newObject();
		};

		Variable &VariableFile::operatorReference(Symbol symbolId) {
			return operatorReferenceX(symbolId,(LibStdFile::getContext())->prototypeFile->prototype);
		};

		Variable *VariableFile::instancePrototype() {
			return (LibStdFile::getContext())->prototypeFile->prototype;
		};

		void VariableFile::activeDestructor() {
			value.close();
		};

		Variable *VariableFile::clone(SymbolList &inSymbolList) {
			VariableFile *out=(VariableFile *)newVariable();
			if(out->value.becomeOwner(value)) {
				return out;
			};

			out->decReferenceCount();
			return VariableUndefined::newVariable();
		};

		bool VariableFile::toBoolean() {
			return true;
		};

		String VariableFile::toString() {
			return strTypeFile;
		};

	};
};


