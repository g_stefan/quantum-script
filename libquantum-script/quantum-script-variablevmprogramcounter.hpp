﻿//
// Quantum Script Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef QUANTUM_SCRIPT_VARIABLEVMPROGRAMCOUNTER_HPP
#define QUANTUM_SCRIPT_VARIABLEVMPROGRAMCOUNTER_HPP

#ifndef QUANTUM_SCRIPT_VARIABLE_HPP
#include "quantum-script-variable.hpp"
#endif

#ifndef QUANTUM_SCRIPT_PROGRAMCOUNTER_HPP
#include "quantum-script-programcounter.hpp"
#endif

namespace Quantum {
	namespace Script {

		class VariableVmProgramCounter;
	};
};


namespace XYO {
	namespace XY {
		template<>
		class TMemoryObject<Quantum::Script::VariableVmProgramCounter>:
			public TMemoryObjectPoolActive<Quantum::Script::VariableVmProgramCounter> {};
	};
};


namespace Quantum {
	namespace Script {


		using namespace XYO;
		using namespace XYO::XY;
		using namespace XYO::XO;


		class VariableVmProgramCounter :
			public Variable {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(VariableVmProgramCounter);
			protected:
				QUANTUM_SCRIPT_EXPORT static const char *strTypeVmProgramCounter;
			public:
				QUANTUM_SCRIPT_EXPORT static const void *typeVmProgramCounter;

				ProgramCounter *value;

				inline VariableVmProgramCounter() {
					variableType = typeVmProgramCounter;
				};

				QUANTUM_SCRIPT_EXPORT static Variable *newVariable(ProgramCounter *value);

				QUANTUM_SCRIPT_EXPORT String getType();

				QUANTUM_SCRIPT_EXPORT bool toBoolean();
				QUANTUM_SCRIPT_EXPORT String toString();
		};

	};
};


#endif
