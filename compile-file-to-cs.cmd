@echo off                        
set SRC=

set XLIB= file-to-cs.src\

set SRC=%SRC% %XLIB%file-to-cs.cpp
set SRC=%SRC% %XLIB%file-to-cs-copyright.cpp
set SRC=%SRC% %XLIB%file-to-cs-licence.cpp

set INC=
set INC= %INC% /Ifile-to-cs

set DEF=
set DEF= %DEF% /DXYO_OS_TYPE_WIN
set DEF= %DEF% /DXYO_COMPILER_MSVC
set DEF= %DEF% /DXYO_MACHINE_32BIT
set DEF= %DEF% /DFILE_TO_CS_NO_VERSION

set DEF= %DEF% /D_CRT_SECURE_NO_WARNINGS

cl /MT /O2 /Ox /Oy /GS- /GL /GA /EHsc /GR- /TP  %DEF%  %INC% %SRC% /link ws2_32.lib user32.lib

del *.ilk
del *.obj
del *.pdb
