@echo off                        
@call compile-file-to-cs.cmd

set XLIB= libquantum-script\

file-to-cs libStdErrorSource  		%XLIB%quantum-script-libstderror.js 		%XLIB%quantum-script-libstderror.src
file-to-cs libStdScriptSource 		%XLIB%quantum-script-libstdscript.js 		%XLIB%quantum-script-libstdscript.src
file-to-cs libStdArraySource  		%XLIB%quantum-script-libstdarray.js 		%XLIB%quantum-script-libstdarray.src
file-to-cs libStdFiberSource  		%XLIB%quantum-script-libstdfiber.js 		%XLIB%quantum-script-libstdfiber.src
file-to-cs libStdThreadSource 		%XLIB%quantum-script-libstdthread.js 		%XLIB%quantum-script-libstdthread.src
file-to-cs libStdApplicationSource  	%XLIB%quantum-script-libstdapplication.js 	%XLIB%quantum-script-libstdapplication.src
file-to-cs libStdMathSource 		%XLIB%quantum-script-libstdmath.js 		%XLIB%quantum-script-libstdmath.src
file-to-cs libStdShellSource 		%XLIB%quantum-script-libstdshell.js 		%XLIB%quantum-script-libstdshell.src
file-to-cs libStdJSONSource 		%XLIB%quantum-script-libstdjson.js 		%XLIB%quantum-script-libstdjson.src
file-to-cs libStdJobSource 	 	%XLIB%quantum-script-libstdjob.js 		%XLIB%quantum-script-libstdjob.src
file-to-cs libStdHTTPSource 		%XLIB%quantum-script-libstdhttp.js 		%XLIB%quantum-script-libstdhttp.src
file-to-cs libStdFunctionSource		%XLIB%quantum-script-libstdfunction.js 		%XLIB%quantum-script-libstdfunction.src
file-to-cs libStdObjectSource		%XLIB%quantum-script-libstdobject.js 		%XLIB%quantum-script-libstdobject.src
file-to-cs libStdMakeSource		%XLIB%quantum-script-libstdmake.js 		%XLIB%quantum-script-libstdmake.src
file-to-cs libStdCoDecSource		%XLIB%quantum-script-libstdcodec.js 		%XLIB%quantum-script-libstdcodec.src

set SRC=

set SRC=%SRC% quantum-script.cpp
set SRC=%SRC% quantum-script-copyright.cpp
set SRC=%SRC% quantum-script-licence.cpp

set INC=
set INC= %INC% /Ilibquantum-script
set INC= %INC% /Ilibxyo-xy
set INC= %INC% /Ilibxyo-xo

set DEF=
set DEF= %DEF% /DXYO_OS_TYPE_WIN
set DEF= %DEF% /DXYO_COMPILER_MSVC
set DEF= %DEF% /DXYO_MACHINE_32BIT
set DEF= %DEF% /DXYO_XY_INTERNAL
set DEF= %DEF% /DXYO_XO_INTERNAL
set DEF= %DEF% /DQUANTUM_SCRIPT_INTERNAL
set DEF= %DEF% /DLIBXYO_XY_NO_VERSION
set DEF= %DEF% /DLIBXYO_XO_NO_VERSION
set DEF= %DEF% /DLIBQUANTUM_SCRIPT_NO_VERSION
set DEF= %DEF% /DQUANTUM_SCRIPT_NO_VERSION
set DEF= %DEF% /DQUANTUM_SCRIPT_AMALGAM

set DEF= %DEF% /D_CRT_SECURE_NO_WARNINGS

rem set DEF= %DEF% /DXYO_MEMORY_LEAK_DETECTOR
rem set DEF= %DEF% /DXYO_MEMORYPOOL_FORCE_ACTIVE_MEMORY_AS_SYSTEM
rem set DEF= %DEF% /DXYO_MEMORYPOOL_FORCE_ACTIVE_MEMORY_PROCESS_AS_SYSTEM
rem set DEF= %DEF% /DQUANTUM_SCRIPT_DEBUG_ASM
rem set DEF= %DEF% /DQUANTUM_SCRIPT_DEBUG_RUNTIME
rem set DEF= %DEF% /DQUANTUM_SCRIPT_DISABLE_ASM_OPTIMIZER
rem set DEF= %DEF% /DXYO_SINGLE_THREAD
rem set DEF= %DEF% /DQUANTUM_SCRIPT_SINGLE_FIBER 
rem set DEF= %DEF% /DQUANTUM_SCRIPT_SINGLE_THREAD
rem set DEF= %DEF% /DQUANTUM_SCRIPT_DISABLE_CLOSURE
rem set DEF= %DEF% /DQUANTUM_SCRIPT_DEBUG_LIBSTD_INIT

rem cl /Zi /MDd /EHsc %DEF% %INC% %SRC% /link ws2_32.lib user32.lib
cl /MT /O2 /Ox /Oy /GS- /GL /GA /EHsc /GR- /TP  %DEF%  %INC% %SRC% /link ws2_32.lib user32.lib
rem cl /RTCs /RTCu /MD  /Oy /GS /GL /GA /EHsc /GR /TP  %DEF%  %INC% %SRC% /link ws2_32.lib user32.lib

del *.ilk
del *.obj
del *.pdb
