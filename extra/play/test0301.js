// Public domain
// http://unlicense.org/

var fiber1;
var fiber2;
var fiber3;
var fiber4;

fiber1=CurrentFiber.setInterval(function() {

	Console.write("  Tick -     \r");
	yield;
	Console.write("       - Tack\r");

},1000);

fiber2=CurrentFiber.setInterval(function() {

	Console.write("/\r");
	yield;
	Console.write("-\r");
	yield;
	Console.write("\\\r");
	yield;
	Console.write("|\r");

},100);

fiber4=CurrentFiber.setTimeout(function() {
	fiber1.terminate();
	fiber2.terminate();
	fiber3.terminate();
	Console.writeLn("*  Done.   ");
},20000);

fiber3=CurrentFiber.setInterval(function() {
	if(Console.keyHit()) {
		if(Console.getChar().toUpperCaseAscii()=="Q") {
			fiber1.terminate();
			fiber2.terminate();
			fiber4.terminate();
			Console.writeLn("*  Interupted.   ");
			fiber3.terminate();
		};
	};
},100);
