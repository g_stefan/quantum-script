#!/bin/sh

printf "" > conformance-quantum-script.txt
for i in *.js
do
    printf "$i\n" >>conformance-quantum-script.txt
    time -p ../../quantum-script --execution-time $i 1>>conformance-quantum-script.txt 2>&1
done

