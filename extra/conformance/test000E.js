// Public domain
// http://unlicense.org/

var o = {
	a: 2,
	m:
	function(b) {
		return .a + 1;
	}
};


if(o.m()==3) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

