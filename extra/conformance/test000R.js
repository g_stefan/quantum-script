// Public domain
// http://unlicense.org/

var undef;
var undef2;

var isOk=true;
if(undef) {
	Console.writeLn("Fail - 1!");
	isOk=false;
};
if(!undef) {
} else {
	Console.writeLn("Fail - 2!");
	isOk=false;
};
if(undef==null) {
} else {
	Console.writeLn("Fail - 3!");
	isOk=false;
};
if(null==undef) {
} else {
	Console.writeLn("Fail - 4!");
	isOk=false;
};
if(undef2==undef) {
} else {
	Console.writeLn("Fail - 5!");
	isOk=false;
};
if(1==undef) {
	Console.writeLn("Fail - 6!");
	isOk=false;
};
if(undef==1) {
	Console.writeLn("Fail - 7!");
	isOk=false;
};
if(1.2==undef) {
	Console.writeLn("Fail - 8!");
	isOk=false;
};
if(undef==1.2) {
	Console.writeLn("Fail - 9!");
	isOk=false;
};
if(1==null) {
	Console.writeLn("Fail - 10!");
	isOk=false;
};
if(null==1) {
	Console.writeLn("Fail - 11!");
	isOk=false;
};
if(1.2==null) {
	Console.writeLn("Fail - 12!");
	isOk=false;
};
if(null==1.2) {
	Console.writeLn("Fail - 13!");
	isOk=false;
};
if("x"==null) {
	Console.writeLn("Fail - 14!");
	isOk=false;
};
if(null=="x") {
	Console.writeLn("Fail - 15!");
	isOk=false;
};
if("x"==undef) {
	Console.writeLn("Fail - 16!");
	isOk=false;
};
if(undef=="x") {
	Console.writeLn("Fail - 17!");
	isOk=false;
};
if([]==undef) {
	Console.writeLn("Fail - 18!");
	isOk=false;
};
if(undef==[]) {
	Console.writeLn("Fail - 19!");
	isOk=false;
};
if([]==null) {
	Console.writeLn("Fail - 20!");
	isOk=false;
};
if(null==[]) {
	Console.writeLn("Fail - 21!");
	isOk=false;
};
if({}==undef) {
	Console.writeLn("Fail - 22!");
	isOk=false;
};
if(undef== {}) {
	Console.writeLn("Fail - 23!");
	isOk=false;
};
if({}==null) {
	Console.writeLn("Fail - 24!");
	isOk=false;
};
if(null== {}) {
	Console.writeLn("Fail - 25!");
	isOk=false;
};
if(null!=null) {
	Console.writeLn("Fail - 26!");
	isOk=false;
};

if(isOk) {
	Console.writeLn("Pass");
};


