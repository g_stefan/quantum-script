// Public domain
// http://unlicense.org/

function loop(x) {
	if (x >= 10) {
		return x;
	}
	return loop(x + 1);
};

if(loop(0)==10) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

