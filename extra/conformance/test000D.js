// Public domain
// http://unlicense.org/

function outside() {
	var x = 10;
	function inside(x) {
		return x;
	};
	return inside;
};
result = outside()(20);


if(result==20) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

