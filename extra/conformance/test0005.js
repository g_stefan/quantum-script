// Public domain
// http://unlicense.org/

function testX(a,b,c) {
	return a+b+c;
};

if(testX(1,2,3)==6) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

