// Public domain
// http://unlicense.org/

function fibR(n) {
	if (n < 2) {
		return n;
	}
	return (fibR(n-2) + fibR(n-1));
}

if(fibR(8)==21) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};


