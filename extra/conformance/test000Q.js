// Public domain
// http://unlicense.org/

function inc(n) {
	var q;
	q=n;
	++q;
	return q;
};

var x=10;
var z;

z=inc(x);

if(z==11&&x==10) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

