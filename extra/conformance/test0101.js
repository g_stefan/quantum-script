// Public domain
// http://unlicense.org/

function testYield() {
	try {
		yield;
		yield;
		throw("Pass");
	} catch(e) {
		Console.writeLn(e.message);
	};
};

testYield();
testYield();
testYield();



