// Public domain
// http://unlicense.org/

function Test() {
	var pass = "Pass";
	function displayPass() {
		Console.writeLn(pass);
	};
	return displayPass;
};

var doTest=new Test();
doTest();

