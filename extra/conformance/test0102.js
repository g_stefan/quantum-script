// Public domain
// http://unlicense.org/

function testYield() {
	var k=0;
	while(true) {
		++k;
		yield k;
	};
};

if(testYield()==1 && testYield()==2 && testYield()==3 && testYield()==4) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

