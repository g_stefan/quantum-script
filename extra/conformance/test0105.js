// Public domain
// http://unlicense.org/

var thread1=newThread(function() {

	if(
		(.a==1)&&
		(.b==2)&&
		(.c==3)&&
		(arguments[0]==101)&&
		(arguments[1]==102)&&
		(arguments[2]==103)
	) {
		return "Pass";
	};

	return "Fail";

}, {a:1,b:2,c:3},[101,102,103]);

thread1.onFinish(function(returnValue) {
	Console.writeLn(returnValue);
});

