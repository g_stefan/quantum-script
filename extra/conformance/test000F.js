// Public domain
// http://unlicense.org/

function fibI(n) {
	var last = 0;
	var cur = 1;
	n = n - 1;
	while(n) {
		--n;
		var tmp = cur;
		cur = last + cur;
		last = tmp;
	}
	return cur;
}

if(fibI(43)==433494437) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

