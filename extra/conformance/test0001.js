// Public domain
// http://unlicense.org/

function With(this_,function_) {
	return function_.call(this_);
};

With("Pass",function() {
	Console.writeLn(this);
});
