// Public domain
// http://unlicense.org/


function inc(n) {
	++n;
};

var x=10;

inc(x);

if(x==10) {
	Console.writeLn("Pass");
} else {
	Console.writeLn("Fail");
};

