// Public domain
// http://unlicense.org/

Script.requireExtension("Example");

Example.print(Example.process(function(x) {
	return x+" world!\r\n";
}));

Console.writeLn(JSON.encodeWithIndentation(Script.getExtensionList()));

