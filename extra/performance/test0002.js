// Public domain
// http://unlicense.org/

function factor(n) {
	if(n==0) {
		return 1;
	};
	return n*factor(n-1);
};

for(l=0; l<1024; ++l) {
	for(m=0; m<1024; ++m) {
		factor(10);
	};
};

Console.writeLn("Pass");

