@echo off
@set QUANTUM_SCRIPT= ..\..\quantum-script.exe
@rem set QUANTUM_SCRIPT=quantum-script.exe
@set PERFORMANCE_CMD= %QUANTUM_SCRIPT% --execution-time
@echo -- %1 -- >> performance-quantum-script.txt
@%PERFORMANCE_CMD% %1 >>performance-quantum-script.txt
@rem %PERFORMANCE_CMD% %1