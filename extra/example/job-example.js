
var job=new Job(8,15);

job.onStart=function(process){
	Console.writeLn("START: "+process.info);
};

job.onEnd=function(process){
	Console.writeLn("END: "+process.info);
};

job.onTimedout=function(process){
	Console.writeLn("TIMEDOUT: "+process.info);
};

job.addProcess(Application.getExecutable()+" job-example-sub-10.js","10");
job.addProcess(Application.getExecutable()+" job-example-sub-20.js","20");
job.addProcess(Application.getExecutable()+" job-example-sub-10.js","30-sync","30");
job.addProcess(Application.getExecutable()+" job-example-sub-10.js","31-sync","30");
job.addProcess(Application.getExecutable()+" job-example-sub-10.js","40-sync","40");
job.addProcess(Application.getExecutable()+" job-example-sub-10.js","41-sync","40");

job.addThread(function(){
	CurrentFiber.sleep(10*1000);
},null,null,"50");

job.addThread(function(){
	CurrentFiber.sleep(20*1000);
},null,null,"60");

job.addThread(function(){
	CurrentFiber.sleep(10*1000);
},null,null,"50-sync","50");

job.addThread(function(){
	CurrentFiber.sleep(10*1000);
},null,null,"51-sync","50");

job.process();

Console.writeLn("DONE");

