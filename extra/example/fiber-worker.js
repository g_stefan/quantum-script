// Public domain
// http://unlicense.org/

var worker=newFiberWorker(function(message) {
	Console.writeLn("Message is #1:"+message);
	yield "bar 1";
	Console.writeLn("Message is #2:"+message);
	return "bar2";
});


Console.writeLn(worker.sendMessage("foo 1"));
Console.writeLn(worker.sendMessage("foo 2"));
worker.postMessage("foo 3");
worker.postMessage("foo 4");
worker.postMessage("foo 5");
Console.writeLn(worker.sendMessage("foo 6"));
// end work
worker.postQuitMessage();
Console.writeLn("Done.");

