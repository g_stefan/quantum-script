//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TSTACK1_X_TPOINTER_HPP
#define XYO_XY_TSTACK1_X_TPOINTER_HPP

#ifndef XYO_XY_TSTACK1_HPP
#include "xyo-xy-tstack1.hpp"
#endif

namespace XYO {
	namespace XY {

		template <typename T_, template <typename U> class TMemory>
		class TStack1<TPointer<T_>, TMemory> :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TStack1);

				typedef TPointer<T_> T;
			protected:
				TYList1<T> *head_;
			public:

				typedef TYList1<T> Node;
				typedef TMemory<TYList1<T> > NodeMemory;

				inline TStack1() {
					head_ = nullptr;
				};

				inline ~TStack1() {
					empty();
				};

				inline void empty() {
					TYList1<T> *element;
					while (head_) {
						element = head_;
						head_ = head_->next;
						TMemory<TYList1<T> >::memoryDelete(element);
					};
					head_ = nullptr;
				};

				inline void push(T value) {
					pushX(value);
				};

				inline void pushX(T &value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					element->value=value;
					head_ = element;
				};

				inline void pushEmpty() {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					head_ = element;
				};

				inline bool peek(T &out) {
					if (head_) {
						out = head_->value;
						return true;
					};
					return false;
				};

				inline bool pop(T &out) {
					TYList1<T> *element;
					if (head_) {
						element = head_;
						head_ = head_->next;
						out = element->value;
						TMemory<TYList1<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool popEmpty() {
					TYList1<T> *element;
					if (head_) {
						element = head_;
						head_ = head_->next;
						TMemory<TYList1<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool isEmpty() {
					return head_ == nullptr;
				};

				inline operator TYList1<T> *() {
					return head_;
				};

				inline TYList1<T> *head() {
					return head_;
				};

				inline bool popElement(TYList1<T> *&element) {
					if (head_) {
						element=head_;
						head_ = head_->next;
						return true;
					};
					return false;
				};

				inline void pushElement(TYList1<T> *element) {
					element->next = head_;
					head_=element;
				};

				static inline TYList1<T> *nodeMemoryNew() {
					return TMemory<TYList1<T> >::memoryNew();
				};

				static inline void nodeMemoryDelete(TYList1<T> *node) {
					TMemory<TYList1<T> >::memoryDelete(node);
				};

				inline void activeDestructor() {
					empty();
				};

				inline void duplicate() {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					if (head_) {
						element->value = head_->value;
					};
					head_ = element;
				};

				//
				// Other
				//

				inline void pushOwner(T_ *value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					element->value.setObject(value);
					head_ = element;
				};

				inline void pushX(const TPointerOwner<T_> &value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					element->value=value;
					head_ = element;
				};

				inline void pushX(TPointerOwner<T_> &&value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					element->value=value;
					head_ = element;
				};

				inline void pushX(const TPointerX<T_> &value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					element->value=value;
					head_ = element;
				};


				inline void pushX(TPointerX<T_> &&value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					element->next = head_;
					element->value=value;
					head_ = element;
				};

				inline void popXUnsafe(T &out) {
					TYList1<T> *element;
					element = head_;
					head_ = head_->next;
					out = element->value;
					TMemory<TYList1<T> >::memoryDelete(element);
				};


				inline void popXUnsafe(TPointerOwner<T_> &out) {
					TYList1<T> *element;
					element = head_;
					head_ = head_->next;
					out = element->value;
					TMemory<TYList1<T> >::memoryDelete(element);
				};

				inline void popXUnsafe(TPointerX<T_> &out) {
					TYList1<T> *element;
					element = head_;
					head_ = head_->next;
					out = element->value;
					TMemory<TYList1<T> >::memoryDelete(element);
				};

				inline void popOwnerUnsafe(T_ *&out) {
					TYList1<T> *element;
					element = head_;
					head_ = head_->next;
					out = element->value.getObject();
					TMemory<TYList1<T> >::memoryDelete(element);
				};

				inline bool popX(TPointerOwner<T_> &out) {
					TYList1<T> *element;
					if (head_) {
						element = head_;
						head_ = head_->next;
						out = element->value;
						TMemory<TYList1<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool popX(TPointerX<T_> &out) {
					TYList1<T> *element;
					if (head_) {
						element = head_;
						head_ = head_->next;
						out = element->value;
						TMemory<TYList1<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool swap() {
					T tmp;
					if (head_) {
						if(head_->next) {
							tmp=head_->next->value;
							head_->next->value=head_->value;
							head_->value=tmp;
							return true;
						};
					};
					return false;
				};

				inline void peekUnsafe(T_ *&out) {
					out = head_->value;
				};

				inline static void memoryInit() {
					TMemory<TYList1<T> >::memoryInit();
				};


		};

	};
};

#endif

