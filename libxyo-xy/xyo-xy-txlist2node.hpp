//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXLIST2NODE_HPP
#define XYO_XY_TXLIST2NODE_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode>
		class TXList2Node {
			public:
				TNode *back;
				TNode *next;

				inline void extract() {
					if(next) {
						next->back=back;
					};
					if(back) {
						back->next=next;
					};
				};
		};

	};
};


#endif

