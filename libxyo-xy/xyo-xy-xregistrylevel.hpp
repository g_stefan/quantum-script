//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XREGISTRYLEVEL_HPP
#define XYO_XY_XREGISTRYLEVEL_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {
		namespace XRegistryLevel {

			enum {
				System = 0,
				Static = 1,
				Active = 2,
				Singleton = 3
			};

		};
	};
};

#endif

