//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#define XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP

#include <new>

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_XREGISTRY_HPP
#include "xyo-xy-xregistry.hpp"
#endif

#ifndef XYO_XY_TIFHASMEMORYINIT_HPP
#include "xyo-xy-tifhasmemoryinit.hpp"
#endif

#ifndef XYO_XY_XCRITICALSECTION_HPP
#include "xyo-xy-xcriticalsection.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<size_t sizeOfT>
		class TXMemoryPoolUnifiedImplementProcess {
			protected:
#ifndef XYO_SINGLE_THREAD
				XCriticalSection criticalSection;
#endif
			public:

				typedef struct SLink {
					struct SLink *next;
				} Link;

				typedef struct SMemory {
					struct SMemory *next;
				} Memory;

				Link *poolFree;
				Memory *poolMemory;

				inline TXMemoryPoolUnifiedImplementProcess() {
					poolFree=nullptr;
					poolMemory=nullptr;
				};

				inline ~TXMemoryPoolUnifiedImplementProcess() {
					Memory *link;
					Memory *next;

					for (link = poolMemory; link; link = next) {
						next = link->next;
						delete[] (byte *)link;
					};
				};

				inline void grow() {
					size_t totalSize;
					size_t k;
					byte *scan;
					Link *link;
					Memory *memory;

					totalSize = (sizeOfT + sizeof (Link)) * 8 + sizeof (Memory);
					scan = new byte[totalSize];
					memory = (Memory *)scan;
					memory->next = poolMemory;
					poolMemory = memory;
					scan += sizeof (Memory);
					for (k = 0; k < 8; ++k) {
						link = (Link *)scan;
						scan += sizeof (Link) + sizeOfT;
						link->next = poolFree;
						poolFree = link;
					};
				};

				inline void *memoryNew() {
					void *retV;
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					if(!poolFree) {
						grow();
					};
					retV=(void *)(((byte *)poolFree)+sizeof (Link));
					poolFree=poolFree->next;
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
					return retV;
				};

				inline void memoryDelete(void *value) {
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					value=(void *)(((byte *)value)-sizeof(Link));
					((Link *)value)->next = poolFree;
					poolFree =(Link *)value;
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
				};


				static TXMemoryPoolUnifiedImplementProcess *memoryPool;
				static const char *registryKey();
				static void resourceDelete(void *);
		};

		template<size_t sizeOfT>
		const char   *TXMemoryPoolUnifiedImplementProcess<sizeOfT>::registryKey() {
			return XYO_FUNCTION;
		};

		template<size_t sizeOfT>
		TXMemoryPoolUnifiedImplementProcess<sizeOfT> *TXMemoryPoolUnifiedImplementProcess<sizeOfT>::memoryPool=nullptr;

		template<size_t sizeOfT>
		void TXMemoryPoolUnifiedImplementProcess<sizeOfT>::resourceDelete(void *this_) {
			delete (TXMemoryPoolUnifiedImplementProcess<sizeOfT> *)this_;
		};

		template<typename T>
		class TXMemoryPoolUnifiedProcess {
			public:

				inline static T *memoryNew() {
					if(!TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::memoryPool) {
						memoryInit();
					};
					T *retV=new(
						(
							TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::memoryPool
						)->memoryNew()
					) T();
					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline static void memoryDelete(T *this_) {
					TIfHasActiveDestructor<T>::callActiveDestructor(this_);
					this_->~T();
					(
						TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::memoryPool
					)->memoryDelete(this_);
				};

				inline static void memoryInit() {
					XRegistry::registryInit();
					TIfHasMemoryInit<T>::callMemoryInit();
					void *registryLink;

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalEnter();
#endif

					if(XRegistryProcess::registerKey(
						   TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::registryKey(),
						   &registryLink)) {
						TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::memoryPool=new TXMemoryPoolUnifiedImplementProcess<sizeof(T)>();

						XRegistryProcess::setValue(
							registryLink,
							XRegistryLevel::Static,
							TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::memoryPool,
							TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::resourceDelete);
					} else {
						TXMemoryPoolUnifiedImplementProcess<sizeof(T)>::memoryPool=(TXMemoryPoolUnifiedImplementProcess<sizeof(T)> *)XRegistryProcess::getValue(registryLink);
					};

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalLeave();
#endif

				};

		};

	};
};

#endif

