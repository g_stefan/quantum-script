//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXSINGLETONTHREAD_HPP
#define XYO_XY_TXSINGLETONTHREAD_HPP

#ifdef XYO_SINGLE_THREAD

#ifndef XYO_XY_TXSINGLETONPROCESS_HPP
#include "xyo-xy-txsingletonprocess.hpp"
#endif

namespace XYO {
	namespace XY {
		template<typename T> class TXSingletonThread: public TXSingletonProcess<T> {};
	};
};

#else

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TIFHASMEMORYINIT_HPP
#include "xyo-xy-tifhasmemoryinit.hpp"
#endif

#ifndef XYO_XY_XREGISTRYTHREAD_HPP
#include "xyo-xy-xregistrythread.hpp"
#endif

#ifndef XYO_XY_TXMEMORYTHREAD_HPP
#include "xyo-xy-txmemoryhread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TXSingletonThread {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TXSingletonThread);
			protected:
				inline TXSingletonThread() {
				};
			public:
				static size_t registryLink;
				static const char *registryKey();
				static void resourceDelete(void *);

				inline static T *getValue() {
					T *retV=(T *)XRegistryThread::getValue(registryLink);
					if(!retV) {
						memoryInit();
						retV=(T *)XRegistryThread::getValue(registryLink);
					};
					return retV;
				};

				inline static void memoryInit() {
					TXMemoryThread<T>::memoryInit();

					XRegistryThread::criticalEnter();
					if(XRegistryThread::registerKey(
						   registryKey(),
						   &registryLink)) {

						XRegistryThread::setValue(
							registryLink,
							XRegistryLevel::Singleton,
							TXMemoryThread<T>::memoryNew(),
							resourceDelete);
					};
					XRegistryThread::criticalLeave();
				};

		};

		template<typename T>
		const char *TXSingletonThread<T>::registryKey() {
			return XYO_FUNCTION;
		};

		template<typename T>
		size_t TXSingletonThread<T>::registryLink=0;

		template<typename T>
		void TXSingletonThread<T>::resourceDelete(void *this_) {
			TXMemory<T>::memoryDelete((T *)this_);
		};

	};
};

#endif

#endif
