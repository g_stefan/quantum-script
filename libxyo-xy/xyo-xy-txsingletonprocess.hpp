//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXSINGLETONPROCESS_HPP
#define XYO_XY_TXSINGLETONPROCESS_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TIFHASMEMORYINIT_HPP
#include "xyo-xy-tifhasmemoryinit.hpp"
#endif

#ifndef XYO_XY_XREGISTRYPROCESS_HPP
#include "xyo-xy-xregistryprocess.hpp"
#endif

#ifndef XYO_XY_TXMEMORYPROCESS_HPP
#include "xyo-xy-txmemoryprocess.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TXSingletonProcess {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TXSingletonProcess);
			protected:
				inline TXSingletonProcess() {
				};
			public:
				static T *singletonLink;
				static const char *registryKey();
				static void resourceDelete(void *);

				inline static T *getValue() {
					if(!singletonLink) {
						memoryInit();
					};
					return singletonLink;
				};

				inline static void memoryInit() {
					TXMemoryProcess<T>::memoryInit();

					void *registryLink;

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalEnter();
#endif
					if(XRegistryProcess::registerKey(
						   registryKey(),
						   &registryLink)) {

						singletonLink=TXMemoryProcess<T>::memoryNew();
						XRegistryProcess::setValue(
							registryLink,
							XRegistryLevel::Singleton,
							singletonLink,
							resourceDelete);
					} else {
						singletonLink=(T *)XRegistryProcess::getValue(registryLink);
					};
#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalLeave();
#endif
				};

		};

		template<typename T>
		const char *TXSingletonProcess<T>::registryKey() {
			return XYO_FUNCTION;
		};

		template<typename T>
		T *TXSingletonProcess<T>::singletonLink=nullptr;

		template<typename T>
		void TXSingletonProcess<T>::resourceDelete(void *this_) {
			TXMemoryProcess<T>::memoryDelete((T *)this_);
		};

	};
};

#endif

