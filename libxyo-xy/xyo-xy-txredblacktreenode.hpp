//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXREDBLACKTREENODE_HPP
#define XYO_XY_TXREDBLACKTREENODE_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TKey>
		class TXRedBlackTreeNode {
			public:

				typedef TKey Key;

				enum Color : bool {
					Black = false,
					Red = true
				};

				TNode *parent;
				TNode *left;
				TNode *right;
				Color   color;
				Key     key;

				inline TNode *minimum() {
					TNode *x=static_cast<TNode *>(this);
					while (x->left) {
						x = x->left;
					};
					return x;
				};

				inline TNode *maximum() {
					TNode *x=static_cast<TNode *>(this);
					while (x->right) {
						x = x->right;
					};
					return x;
				};

				inline TNode *succesor() {
					TNode *y;
					TNode *x=static_cast<TNode *>(this);

					if (x->right) {
						return x->right->minimum();
					};
					y = x->parent;
					while (y) {
						if (x == y->right) {
							x = y;
							y = y->parent;
						} else {
							break;
						};
					};
					return y;
				};

				inline TNode *predecesor() {
					TNode *y;
					TNode *x=static_cast<TNode *>(this);

					if (x->left) {
						return x->left->maximum();
					};
					y = x->parent;
					while (y) {
						if (x == y->left) {
							x = y;
							y = y->parent;
						} else {
							break;
						};
					};
					return y;
				};
		};

	};
};


#endif

