//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY__EXPORT_HPP
#define XYO_XY__EXPORT_HPP

#ifdef XYO_DYNAMIC_LINK
#   ifndef XYO_EXPORT
#       ifdef XYO_OS_TYPE_WIN
#           define XYO_EXPORT __declspec(dllexport)
#       endif
#       ifdef XYO_OS_TYPE_UNIX
#           define XYO_EXPORT __attribute__ ((dllexport))
#       endif
#
#   endif
#   ifndef XYO_IMPORT
#       ifdef XYO_OS_TYPE_WIN
#           define XYO_IMPORT __declspec(dllimport)
#       endif
#       ifdef XYO_OS_TYPE_UNIX
#           define XYO_IMPORT __attribute__ ((dllimport))
#       endif
#   endif
#
#else
#   ifndef XYO_EXPORT
#       define XYO_EXPORT
#   endif
#   ifndef XYO_IMPORT
#       define XYO_IMPORT
#   endif
#endif

#ifdef  XYO_XY_INTERNAL
#   define XYO_XY_EXPORT XYO_EXPORT
#else
#   define XYO_XY_EXPORT XYO_IMPORT
#endif

#endif

