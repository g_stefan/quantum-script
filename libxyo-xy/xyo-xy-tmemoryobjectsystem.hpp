//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTSYSTEM_HPP
#define XYO_XY_TMEMORYOBJECTSYSTEM_HPP

#ifndef XYO_XY_TXMEMORYSYSTEM_HPP
#include "xyo-xy-txmemorysystem.hpp"
#endif

#ifndef XYO_XY_OBJECT_HPP
#include "xyo-xy-object.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TMemoryObjectSystem {
			public:
				static inline T *newObject() {
					T *retV;
					retV = TXMemorySystem<T>::memoryNew();
					retV->setDynamic_((Object::PMemoryDelete)TXMemorySystem<T>::memoryDelete,retV);
					retV->incReferenceCount();
					return retV;
				};

				static inline void deleteObject(T *this_) {
					this_->decReferenceCount();
				};

				static inline T *newObjectX() {
					T *retV;
					retV = TXMemorySystem<T>::memoryNew();
					retV->setDynamic_((Object::PMemoryDelete)TXMemorySystem<T>::memoryDelete,retV);
					return retV;
				};

				static inline void deleteObjectX(T *this_) {
					TXMemorySystem<T>::memoryDelete(this_);
				};

				inline static void memoryInit() {
					TXMemorySystem<T>::memoryInit();
				};
		};
	};
};

#endif

