//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIEDTHREAD_HPP
#define XYO_XY_TMEMORYOBJECTPOOLUNIFIEDTHREAD_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-txmemorypoolunifiedthread.hpp"
#endif

#ifndef XYO_XY_OBJECT_HPP
#include "xyo-xy-object.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TMemoryObjectPoolUnifiedThread {
			public:
				static inline T *newObject() {
					T *retV;
					retV = TXMemoryPoolUnifiedThread<T>::memoryNew();
					retV->setDynamic_((Object::PMemoryDelete)TXMemoryPoolUnifiedThread<T>::memoryDelete,retV);
					retV->incReferenceCount();
					return retV;
				};

				static inline void deleteObject(T *this_) {
					this_->decReferenceCount();
				};

				static inline T *newObjectX() {
					T *retV;
					retV = TXMemoryPoolUnifiedThread<T>::memoryNew();
					retV->setDynamic_((Object::PMemoryDelete)TXMemoryPoolUnifiedThread<T>::memoryDelete,retV);
					return retV;
				};

				static inline void deleteObjectX(T *this_) {
					TXMemoryPoolUnifiedThread<T>::memoryDelete(this_);
				};

				inline static void memoryInit() {
					TXMemoryPoolUnifiedThread<T>::memoryInit();
				};
		};
	};
};

#endif



