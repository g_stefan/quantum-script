//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLACTIVEPROCESS_HPP
#define XYO_XY_TMEMORYPOOLACTIVEPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLACTIVEPROCESS_HPP
#include "xyo-xy-txmemorypoolactiveprocess.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVEPROCESS_HPP
#include "xyo-xy-tmemoryobjectpoolactiveprocess.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolActiveProcessImplementation: public TXMemoryPoolActiveProcess<T> {};

		template<typename T>
		class TMemoryPoolActiveProcessImplementation<T,true>: public TMemoryObjectPoolActiveProcess<T> {};

		template<typename T>
		class TMemoryPoolActiveProcess: public TMemoryPoolActiveProcessImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

