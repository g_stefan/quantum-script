//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLACTIVE_HPP
#define XYO_XY_TMEMORYPOOLACTIVE_HPP

#ifndef XYO_XY_TXMEMORYPOOLACTIVE_HPP
#include "xyo-xy-txmemorypoolactive.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVE_HPP
#include "xyo-xy-tmemoryobjectpoolactive.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolActiveImplementation: public TXMemoryPoolActive<T> {};

		template<typename T>
		class TMemoryPoolActiveImplementation<T,true>: public TMemoryObjectPoolActive<T> {};

		template<typename T>
		class TMemoryPoolActive: public TMemoryPoolActiveImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

