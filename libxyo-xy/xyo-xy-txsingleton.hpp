//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXSINGLETON_HPP
#define XYO_XY_TXSINGLETON_HPP

#ifndef XYO_XY_TXSINGLETONTHREAD_HPP
#include "xyo-xy-txsingletonthread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXSingleton: public TXSingletonThread<T> {};

	};
};

#endif

