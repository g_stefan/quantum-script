//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIEDPROCESS_HPP
#define XYO_XY_TMEMORYOBJECTPOOLUNIFIEDPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

#ifndef XYO_XY_OBJECT_HPP
#include "xyo-xy-object.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T>
		class TMemoryObjectPoolUnifiedProcess {
			public:
				static inline T *newObject() {
					T *retV;
					retV = TXMemoryPoolUnifiedProcess<T>::memoryNew();
					retV->setDynamic_((Object::PMemoryDelete)TXMemoryPoolUnifiedProcess<T>::memoryDelete,retV);
					retV->incReferenceCount();
					return retV;
				};

				static inline void deleteObject(T *this_) {
					this_->decReferenceCount();
				};

				static inline T *newObjectX() {
					T *retV;
					retV = TXMemoryPoolUnifiedProcess<T>::memoryNew();
					retV->setDynamic_((Object::PMemoryDelete)TXMemoryPoolUnifiedProcess<T>::memoryDelete,retV);
					return retV;
				};

				static inline void deleteObjectX(T *this_) {
					TXMemoryPoolUnifiedProcess<T>::memoryDelete(this_);
				};

				inline static void memoryInit() {
					TXMemoryPoolUnifiedProcess<T>::memoryInit();
				};
		};
	};
};

#endif

