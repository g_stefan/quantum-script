
XYO XY Library
==============

Memory manager built around multi-threading suport in "isolation", every thread has its own
memory pool. This allows for nonblocking garbage collector.

Full garbage-collector based on executive-dynamic reference counting with mark-and-sweep algorithm.	
	
Containers based on this memory model.
