//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TYREDBLACKTREENODEONE_HPP
#define XYO_XY_TYREDBLACKTREENODEONE_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TXREDBLACKTREE_HPP
#include "xyo-xy-txredblacktree.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TYRedBlackTreeNodeOne:
			public TXRedBlackTreeNode<TYRedBlackTreeNodeOne<T>,T> {
			public:
				inline void activeConstructor() {
					TIfHasActiveConstructor<T>::callActiveConstructor(&key);
				};

				inline void activeDestructor() {
					TIfHasActiveDestructor<T>::callActiveDestructor(&key);
				};

				inline void activeReset() {
					TIfHasActiveReset<T>::callActiveReset(&key);
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

		};

	};
};

#endif

