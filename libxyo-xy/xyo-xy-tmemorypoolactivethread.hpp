//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLACTIVETHREAD_HPP
#define XYO_XY_TMEMORYPOOLACTIVETHREAD_HPP

#ifndef XYO_XY_TXMEMORYPOOLACTIVETHREAD_HPP
#include "xyo-xy-txmemorypoolactivethread.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVETHREAD_HPP
#include "xyo-xy-tmemoryobjectpoolactivethread.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolActiveThreadImplementation: public TXMemoryPoolActiveThread<T> {};

		template<typename T>
		class TMemoryPoolActiveThreadImplementation<T,true>: public TMemoryObjectPoolActiveThread<T> {};

		template<typename T>
		class TMemoryPoolActiveThread: public TMemoryPoolActiveThreadImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

