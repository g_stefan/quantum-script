//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYTHREAD_HPP
#define XYO_XY_TMEMORYTHREAD_HPP

#ifndef XYO_XY_TXMEMORYTHREAD_HPP
#include "xyo-xy-txmemorythread.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTTHREAD_HPP
#include "xyo-xy-tmemoryobjectthread.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryThreadImplementation: public TXMemoryThread<T> {};

		template<typename T>
		class TMemoryThreadImplementation<T,true>: public TMemoryObjectThread<T> {};

		template<typename T>
		class TMemoryThread: public TMemoryThreadImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

