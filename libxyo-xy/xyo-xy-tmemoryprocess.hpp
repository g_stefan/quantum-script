//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPROCESS_HPP
#define XYO_XY_TMEMORYPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPROCESS_HPP
#include "xyo-xy-txmemoryprocess.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPROCESS_HPP
#include "xyo-xy-tmemoryobjectprocess.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryProcessImplementation: public TXMemoryProcess<T> {};

		template<typename T>
		class TMemoryProcessImplementation<T,true>: public TMemoryObjectProcess<T> {};

		template<typename T>
		class TMemoryProcess: public TMemoryProcessImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

