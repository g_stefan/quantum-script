//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY__CONFIG_HPP
#define XYO_XY__CONFIG_HPP

//#define XYO_OS_TYPE_WIN
//#define XYO_OS_TYPE_UNIX

//#define XYO_DYNAMIC_LINK
//#define XYO_SINGLE_THREAD

#endif
