//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVETHREAD_HPP
#define XYO_XY_TMEMORYOBJECTPOOLACTIVETHREAD_HPP


#ifdef XYO_SINGLE_THREAD

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVEPROCESS_HPP
#include "xyo-xy-tmemoryobjectpoolactiveprocess.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectPoolActiveThread: public TMemoryObjectPoolActiveProcess<T> {};
	};
};

#else

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-txmemorypoolunifiedthread.hpp"
#endif

namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryObjectPoolActiveMemoryThread: public TXMemoryPoolUnifiedThread<T> {};
	};
};


#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasyactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TMemoryObjectPoolActiveThread;

		template<typename T>
		class TMemoryObjectPoolActiveImplementThread {
			public:

				typedef struct SLink {
					struct SLink *next;
					T value;
				} Link;

				Link *poolFree;
				size_t  linkValue;

				inline TMemoryObjectPoolActiveImplementThread() {
					poolFree=nullptr;
				};

				inline ~TMemoryObjectPoolActiveImplementThread() {
					Link *element;
					while (poolFree) {
						element = poolFree;
						poolFree = poolFree->next;
						TXMemoryObjectPoolActiveMemoryThread<Link>::memoryDelete(element);
					};
				};

				inline void grow() {
					size_t k;
					Link *item;
					for (k=0; k<8; ++k) {
						item = TXMemoryObjectPoolActiveMemoryThread<Link>::memoryNew();
						item->next = poolFree;
						item->value.setDynamic_((Object::PMemoryDelete)memoryDelete_,&item->value);
						poolFree = item;
					};
					linkValue=reinterpret_cast<byte *>(&item->value)-reinterpret_cast<byte *>(item);
				};

				inline T *memoryNew() {
					T *retV;
					if(!poolFree) {
						grow();
					};
					retV=&poolFree->value;
					poolFree=poolFree->next;
					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline void memoryDelete(T *value) {
					TIfHasActiveDestructor<T>::callActiveDestructor(value);
					value=reinterpret_cast<T *>( (reinterpret_cast<byte *>(value)) - linkValue );
					(reinterpret_cast<Link *>(value))->next = poolFree;
					poolFree = (reinterpret_cast<Link *>(value));
				};

				static inline void memoryDelete_(T *this_);

				static void memoryInit() {
					TXMemoryObjectPoolActiveMemoryThread<Link>::memoryInit();
				};

				static size_t registryLink;
				static const char *registryKey();
				static void resourceDelete(void *);
		};

		template<typename T>
		const char   *TMemoryObjectPoolActiveImplementThread<T>::registryKey() {
			return XYO_FUNCTION;
		};

		template<typename T>
		size_t TMemoryObjectPoolActiveImplementThread<T>::registryLink=0;

		template<typename T>
		void TMemoryObjectPoolActiveImplementThread<T>::resourceDelete(void *this_) {
			delete (TMemoryObjectPoolActiveImplementThread<T> *)this_;
		};

		template<typename T>
		void TMemoryObjectPoolActiveImplementThread<T>::memoryDelete_(T *this_) {
			(
				(TMemoryObjectPoolActiveImplementThread<T> *)
				XRegistryThread::getValue(TMemoryObjectPoolActiveImplementThread<T>::registryLink)
			)->memoryDelete(this_);
		};

		template<typename T>
		class TMemoryObjectPoolActiveThread {
			public:


				static inline T *newObject() {
					T *retV=reinterpret_cast<T *>(XRegistryThread::getValue(TMemoryObjectPoolActiveImplementThread<T>::registryLink));
					if(!retV) {
						memoryInit();
						retV=reinterpret_cast<T *>(XRegistryThread::getValue(TMemoryObjectPoolActiveImplementThread<T>::registryLink));
					};
					retV = (
						       (TMemoryObjectPoolActiveImplementThread<T> *)
						       retV
					       )->memoryNew();
					retV->incReferenceCount();
					return retV;
				};

				static inline void deleteObject(T *this_) {
					this_->decReferenceCount();
				};

				static inline T *newObjectX() {
					T *retV=reinterpret_cast<T *>(XRegistryThread::getValue(TMemoryObjectPoolActiveImplementThread<T>::registryLink));
					if(!retV) {
						memoryInit();
						retV=reinterpret_cast<T *>(XRegistryThread::getValue(TMemoryObjectPoolActiveImplementThread<T>::registryLink));
					};
					retV = (
						       (TMemoryObjectPoolActiveImplementThread<T> *)
						       retV
					       )->memoryNew();
					return retV;
				};

				static inline void deleteObjectX(T *this_) {
					TMemoryObjectPoolActiveImplementThread<T>::memoryDelete_(this_);
				};

				inline static void memoryInit() {
					XRegistry::registryInit();
					TIfHasMemoryInit<T>::callMemoryInit();
					TMemoryObjectPoolActiveImplementThread<T>::memoryInit();

					XRegistryThread::criticalEnter();
					if(XRegistryThread::registerKey(
						   TMemoryObjectPoolActiveImplementThread<T>::registryKey(),
						   &TMemoryObjectPoolActiveImplementThread<T>::registryLink
					   )) {

						XRegistryThread::setValue(
							TMemoryObjectPoolActiveImplementThread<T>::registryLink,
							XRegistryLevel::Active,
							new TMemoryObjectPoolActiveImplementThread<T>(),
							TMemoryObjectPoolActiveImplementThread<T>::resourceDelete
						);
					};
					XRegistryThread::criticalLeave();

				};

		};

	};
};

#endif

#endif


