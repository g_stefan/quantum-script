//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_OBJECT_HPP
#define XYO_XY_OBJECT_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_POINTERXBASE_HPP
#include "xyo-xy-pointerxbase.hpp"
#endif

namespace XYO {
	namespace XY {

		class Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(Object);
			public:
				typedef void (*PMemoryDelete)(void *);

			protected:

				size_t referenceCounter_;

				size_t referenceLoopDetectorMark_;

				PMemoryDelete memoryDelete_;
				void *memoryThis_;

				PointerXBase *referencePointerXHead_;

				enum {
					ReferenceTypeIsNone_,
					ReferenceTypeIsExecutive_,
					ReferenceTypeIsDynamic_
				};

				void XYO_XY_EXPORT referenceCountLoopDetector_();
				int XYO_XY_EXPORT referenceCountLoopDetectorX_(size_t mark);

			public:

				inline bool isDynamic() {
					return (memoryDelete_ != nullptr);
				};

				inline void incReferenceCount() {
					++referenceCounter_;
				};

				void XYO_XY_EXPORT decReferenceCount();

				inline void setDynamic_(PMemoryDelete memoryDelete, void *memoryThis) {
					memoryDelete_ = memoryDelete;
					memoryThis_ = memoryThis;
				};

				XYO_XY_EXPORT Object();

				void XYO_XY_EXPORT unregisterPointerXAndCount_(PointerXBase *value);
				void XYO_XY_EXPORT registerPointerXAndCount_(PointerXBase *value);

				inline void decReferenceCountExecutive_() {
					--referenceCounter_;
				};
		};

	};
};

#endif
