//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIED_HPP
#define XYO_XY_TMEMORYOBJECTPOOLUNIFIED_HPP

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-tmemoryobjectpoolunifiedthread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectPoolUnified: public TMemoryObjectPoolUnifiedThread<T> {};

	};
};

#endif

