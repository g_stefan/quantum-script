//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXLIST3_HPP
#define XYO_XY_TXLIST3_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TXLIST2NODE_HPP
#include "xyo-xy-txlist2node.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TNodeMemory>
		class TXList3 {
			public:

				TNode *head;
				TNode *tail;

				inline TXList3() {
					head=nullptr;
					tail=nullptr;
				};

				inline void rootDestructor() {
					TNode *this_;
					while (head) {
						this_ = head;
						head = head->next;
						TNodeMemory::memoryDelete(this_);
					};
				};

				inline void empty() {
					rootDestructor();
					head=nullptr;
					tail=nullptr;
				};

				inline void pushHead(TNode *node) {
					node->back = nullptr;
					node->next = head;
					if (head) {
						head->back = node;
					} else {
						tail = node;
					};
					head = node;
				};

				inline TNode *popHead() {
					if (head) {
						TNode *node=head;
						if (head->next) {
							head->next->back = nullptr;
						};
						if (head == tail) {
							tail = nullptr;
						};
						head = head->next;
						return node;
					};
					return nullptr;
				};

				inline TNode *popHeadUnsafe() {
					TNode *node=head;
					if (head->next) {
						head->next->back = nullptr;
					};
					if (head == tail) {
						tail = nullptr;
					};
					head = head->next;
					return node;
				};

				inline void pushTail(TNode *node) {
					node->back = tail;
					node->next = null;
					if (tail) {
						tail->next = node;
					} else {
						head = node;
					};
					tail = node;
				};

				inline TNode *popTail() {
					if(tail) {
						TNode *node=tail;
						if (tail->back) {
							tail->back->next = nullptr;
						};
						if (tail == head) {
							head = nullptr;
						};
						tail = tail->back;
						return node;
					};
					return nullptr;
				};

				inline TNode *popTailUnsafe() {
					TNode *node=tail;
					if (tail->back) {
						tail->back->next = nullptr;
					};
					if (tail == head) {
						head = nullptr;
					};
					tail = tail->back;
					return node;
				};

				inline void extract(TNode *node) {
					if(head==node) {
						return popHeadUnsafe();
					};
					if(tail==node) {
						return popTailUnsafe();
					};
					node->extract();
				};

				inline void extractList(TXList3 &list3) {
					if(head==list3.head) {
						head=list3.tail->next;
					};
					if(tail==list3.tail) {
						tail=list3.head->back;
					};
					if(list3.head->back) {
						list3.head->back->next=list3.tail->next;
					};
					if(list3.tail->next) {
						list3.tail->next->back=list3.head->back;
					};
				};

		};

	};
};

#endif

