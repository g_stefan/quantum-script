//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOL_HPP
#define XYO_XY_TXMEMORYPOOL_HPP

#ifndef XYO_XY_TXMEMORYPOOLTHREAD_HPP
#include "xyo-xy-txmemorypoolthread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXMemoryPool: public TXMemoryPoolThread<T> {};

	};
};

#endif

