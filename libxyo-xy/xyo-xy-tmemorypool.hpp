//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOL_HPP
#define XYO_XY_TMEMORYPOOL_HPP

#ifndef XYO_XY_TXMEMORYPOOL_HPP
#include "xyo-xy-txmemorypool.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOL_HPP
#include "xyo-xy-tmemoryobjectpool.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolImplementation: public TXMemoryPool<T> {};

		template<typename T>
		class TMemoryPoolImplementation<T,true>: public TMemoryObjectPool<T> {};

		template<typename T>
		class TMemoryPool: public TMemoryPoolImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

