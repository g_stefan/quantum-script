//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLPROCESS_HPP
#define XYO_XY_TMEMORYPOOLPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLPROCESS_HPP
#include "xyo-xy-txmemorypoolprocess.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLPROCESS_HPP
#include "xyo-xy-tmemoryobjectpoolprocess.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolProcessImplementation: public TXMemoryPoolProcess<T> {};

		template<typename T>
		class TMemoryPoolProcessImplementation<T,true>: public TMemoryObjectPoolProcess<T> {};

		template<typename T>
		class TMemoryPoolProcess: public TMemoryPoolProcessImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

