//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLACTIVE_HPP
#define XYO_XY_TXMEMORYPOOLACTIVE_HPP

#ifndef XYO_XY_TXMEMORYPOOLACTIVETHREAD_HPP
#include "xyo-xy-txmemorypoolactivethread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXMemoryPoolActive: public TXMemoryPoolActiveThread<T> {};

	};
};

#endif

