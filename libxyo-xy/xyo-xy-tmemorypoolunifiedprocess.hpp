//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLUNIFIEDPROCESS_HPP
#define XYO_XY_TMEMORYPOOLUNIFIEDPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-tmemoryobjectpoolunifiedprocess.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolUnifiedProcessImplementation: public TXMemoryPoolUnifiedProcess<T> {};

		template<typename T>
		class TMemoryPoolUnifiedProcessImplementation<T,true>: public TMemoryObjectPoolUnifiedProcess<T> {};

		template<typename T>
		class TMemoryPoolUnifiedProcess: public TMemoryPoolUnifiedProcessImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

