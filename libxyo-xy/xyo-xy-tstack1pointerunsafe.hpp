//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TSTACK1POINTERUNSAFE_HPP
#define XYO_XY_TSTACK1POINTERUNSAFE_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

namespace XYO {
	namespace XY {

		//
		// Non null stack element
		//

		template <typename T, template <typename U> class TMemory=TXMemory>
		class TStack1PointerUnsafe:
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TStack1PointerUnsafe);
			protected:
				TYList1<T *> *head_;
			public:

				typedef TYList1<T *> Node;
				typedef TMemory<TYList1<T *> > NodeMemory;

				inline TStack1PointerUnsafe() {
					head_ = nullptr;
				};

				inline ~TStack1PointerUnsafe() {
					empty();
				};

				inline void empty() {
					TYList1<T *> *element;
					while (head_) {
						element = head_;
						head_ = head_->next;
						element->value->decReferenceCount();
						TMemory<TYList1<T *> >::memoryDelete(element);
					};
					head_ = nullptr;
				};

				inline void push(TPointer<T> value) {
					pushX(value);
				};

				inline void pushX(TPointer<T> &value) {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value= value.getObject();
					head_ = element;
				};

				inline void pushEmpty() {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value=nullptr;
					head_ = element;
				};

				inline void peek(TPointer<T> &out) {
					out = head_->value;
				};

				inline void pop(TPointer<T> &out) {
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					out.setObject(element->value);
					TMemory<TYList1<T *> >::memoryDelete(element);
				};

				inline void popEmpty() {
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					element->value->decReferenceCount();
					TMemory<TYList1<T *> >::memoryDelete(element);
				};

				inline bool isEmpty() {
					return head_ == nullptr;
				};

				inline operator TYList1<T *> *() {
					return head_;
				};

				inline TYList1<T *> *head() {
					return head_;
				};

				inline void popElement(TYList1<T *> *&element) {
					element=head_;
					head_ = head_->next;
				};

				inline void pushElement(TYList1<T *> *element) {
					element->next = head_;
					head_=element;
				};

				static inline TYList1<T *> *nodeMemoryNew() {
					return TMemory<TYList1<T *> >::memoryNew();
				};

				static inline void nodeMemoryDelete(TYList1<T *> *node) {
					TMemory<TYList1<T *> >::memoryDelete(node);
				};

				inline void activeDestructor() {
					empty();
				};

				inline void duplicate() {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value = head_->value;
					element->value->incReferenceCount();
					head_ = element;
				};

				inline T *popOwnerX() {
					T *retV;
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					retV = element->value;
					TMemory<TYList1<T *> >::memoryDelete(element);
					return retV;
				};

				inline void pushOwner(T *value) {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value=value;
					head_ = element;
				};

				//
				// Other
				//

				inline void pushX(const TPointer<T> &value) {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value=(const_cast<TPointer<T> &>(value)).getObject();
					head_ = element;
				};


				inline void pushX(const TPointerOwner<T> &value) {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value=(const_cast<TPointerOwner<T> &>(value)).getObjectOwnerTransfer();
					head_ = element;
				};

				inline void pushX(TPointerOwner<T> &&value) {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value=value.getObjectOwnerTransfer();
					head_ = element;
				};

				inline void pushX(const TPointerX<T> &value) {
					TYList1<T *> *element;
					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value = (const_cast<TPointerX<T> &>(value)).getObject();
					head_ = element;
				};


				inline void pushX(T &value) {
					TYList1<T *> *element;

					value.incReferenceCount();

					element = TMemory<TYList1<T *> >::memoryNew();
					element->next = head_;
					element->value = &value;
					head_ = element;
				};


				inline void popX(TPointer<T> &out) {
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					out.setObject(element->value);
					TMemory<TYList1<T *> >::memoryDelete(element);
				};


				inline void popOwner(T *&out) {
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					out = element->value;
					TMemory<TYList1<T *> >::memoryDelete(element);
				};


				inline void popX(TPointerOwner<T> &out) {
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					out.setObject(element->value);
					TMemory<TYList1<T *> >::memoryDelete(element);
				};

				inline void popX(TPointerX<T> &out) {
					TYList1<T *> *element;
					element = head_;
					head_ = head_->next;
					out.setObject(element->value);
					TMemory<TYList1<T *> >::memoryDelete(element);
				};

				inline void swap() {
					T *tmp;
					tmp=head_->next->value;
					head_->next->value=head_->value;
					head_->value=tmp;
				};

				inline void peek(T *&out) {
					out = head_->value;
				};

				inline static void memoryInit() {
					TMemory<TYList1<T *> >::memoryInit();
				};

		};

	};
};

#endif

