//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLACTIVEPROCESS_HPP
#define XYO_XY_TXMEMORYPOOLACTIVEPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryPoolActiveMemoryProcess: public TXMemoryPoolUnifiedProcess<T> {};
	};
};

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TXMemoryPoolActiveImplementProcess {
			protected:
#ifndef XYO_SINGLE_THREAD
				XCriticalSection criticalSection;
#endif
			public:

				typedef struct SLink {
					struct SLink *next;
					T value;
				} Link;

				Link *poolFree;
				size_t  linkValue;

				inline TXMemoryPoolActiveImplementProcess() {
					poolFree=nullptr;
				};

				inline ~TXMemoryPoolActiveImplementProcess() {
					Link *element;
					while (poolFree) {
						element = poolFree;
						poolFree = poolFree->next;
						TXMemoryPoolActiveMemoryProcess<Link>::memoryDelete(element);
					};
				};

				inline void grow() {
					size_t k;
					Link *item;
					for (k=0; k<8; ++k) {
						item = TXMemoryPoolActiveMemoryProcess<Link>::memoryNew();
						item->next = poolFree;
						poolFree = item;
					};
					linkValue=reinterpret_cast<byte *>(&item->value)-reinterpret_cast<byte *>(item);
				};

				inline T *memoryNew() {
					T *retV;
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					if(!poolFree) {
						grow();
					};
					retV=&poolFree->value;
					poolFree=poolFree->next;
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline void memoryDelete(T *value) {
					TIfHasActiveDestructor<T>::callActiveDestructor(value);
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					value=reinterpret_cast<T *>( (reinterpret_cast<byte *>(value)) - linkValue );
					(reinterpret_cast<Link *>(value))->next = poolFree;
					poolFree = (reinterpret_cast<Link *>(value));
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
				};

				static void memoryInit() {
					TXMemoryPoolActiveMemoryProcess<Link>::memoryInit();
				};

				static TXMemoryPoolActiveImplementProcess *memoryPool;
				static const char *registryKey();
				static void resourceDelete(void *);
		};

		template<typename T>
		const char   *TXMemoryPoolActiveImplementProcess<T>::registryKey() {
			return XYO_FUNCTION;
		};

		template<typename T>
		TXMemoryPoolActiveImplementProcess<T> *TXMemoryPoolActiveImplementProcess<T>::memoryPool=nullptr;

		template<typename T>
		void TXMemoryPoolActiveImplementProcess<T>::resourceDelete(void *this_) {
			delete (TXMemoryPoolActiveImplementProcess<T> *)this_;
		};

		template<typename T>
		class TXMemoryPoolActiveProcess {
			public:

				inline static T *memoryNew() {
					if(!TXMemoryPoolActiveImplementProcess<T>::memoryPool) {
						memoryInit();
					};
					return (
						       TXMemoryPoolActiveImplementProcess<T>::memoryPool
					       )->memoryNew();
				};

				inline static void memoryDelete(T *this_) {
					(
						TXMemoryPoolActiveImplementProcess<T>::memoryPool
					)->memoryDelete(this_);
				};

				inline static void memoryInit() {
					XRegistry::registryInit();
					TIfHasMemoryInit<T>::callMemoryInit();
					TXMemoryPoolActiveImplementProcess<T>::memoryInit();
					void *registryLink;

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalEnter();
#endif
					if(XRegistryProcess::registerKey(
						   TXMemoryPoolActiveImplementProcess<T>::registryKey(),
						   &registryLink
					   )) {
						TXMemoryPoolActiveImplementProcess<T>::memoryPool=new TXMemoryPoolActiveImplementProcess<T>();

						XRegistryProcess::setValue(
							registryLink,
							XRegistryLevel::Active,
							TXMemoryPoolActiveImplementProcess<T>::memoryPool,
							TXMemoryPoolActiveImplementProcess<T>::resourceDelete
						);

					} else {
						TXMemoryPoolActiveImplementProcess<T>::memoryPool=(TXMemoryPoolActiveImplementProcess<T> *)XRegistryProcess::getValue(registryLink);
					};

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalLeave();
#endif

				};

		};

	};
};

#endif

