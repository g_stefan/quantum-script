//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXLIST1_HPP
#define XYO_XY_TXLIST1_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TXLIST1NODE_HPP
#include "xyo-xy-txlist1node.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TNodeMemory>
		class TXList1 {
			public:
				TNode *root;

				inline TXList1() {
					root=nullptr;
				};

				inline ~TXList1() {
					rootDestructor();
				};

				inline void rootDestructor() {
					TNode *this_;
					while (root) {
						this_ = root;
						root = root->next;
						TNodeMemory::memoryDelete(this_);
					};
				};

				inline void empty() {
					rootDestructor();
					root = nullptr;
				};

				inline void push(TNode *node) {
					node->next=root;
					root=node;
				};

				inline TNode *pop() {
					if(root) {
						TNode *node=root;
						root=root->next;
						return node;
					};
					return nullptr;
				};

				inline TNode *popUnsafe() {
					TNode *node=root;
					root=root->next;
					return node;
				};

		};

	};
};

#endif

