//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xy-xregistrykey.hpp"
#include "xyo-xy-xregistrydata.hpp"
#include "xyo-xy-txlist2.hpp"
#include "xyo-xy-xcriticalsection.hpp"
#include "xyo-xy-xregistrythread.hpp"
#include "xyo-xy-xregistryprocess.hpp"

namespace XYO {
	namespace XY {
		namespace XRegistryProcess {

#ifndef XYO_SINGLE_THREAD
			static XCriticalSection criticalSection;
#endif
			static XRegistryData::List2 data[4];


			void processBegin() {
				XRegistryKey::processBegin();

#ifndef XYO_SINGLE_THREAD
				XRegistryThread::processBegin();
#endif
			};

			void processEnd() {
#ifndef XYO_SINGLE_THREAD
				XRegistryThread::processEnd();
#endif

				data[XRegistryLevel::Singleton].empty();
				data[XRegistryLevel::Active].empty();
				data[XRegistryLevel::Static].empty();
				data[XRegistryLevel::System].empty();

				XRegistryKey::processEnd();
			};

			bool registerKey(const char *registryKey,void **registryLink) {
				XRegistryKey::Node *node=XRegistryKey::registerKey(registryKey);
#ifndef XYO_SINGLE_THREAD
				XRegistryThread::checkFastTrack(node);
#endif
				*registryLink=node;
				return node->processValue==nullptr;
			};

			void setValue(void *registryLink,size_t categoryLevel,void *resourceValue,ResourceDelete resourceDelete) {
				XRegistryData::Node *node=XRegistryData::NodeMemory::memoryNew();
				data[categoryLevel].push(node);
				node->resourceValue=resourceValue;
				node->resourceDelete=resourceDelete;
				(reinterpret_cast<XRegistryKey::Node *>(registryLink))->processValue=resourceValue;
			};

			void *getValue(void *registryLink) {
				return (reinterpret_cast<XRegistryKey::Node *>(registryLink))->processValue;
			};

#ifndef XYO_SINGLE_THREAD
			void criticalEnter() {
				criticalSection.enter();
			};

			void criticalLeave() {
				criticalSection.leave();
			};
#endif


		};
	};
};

