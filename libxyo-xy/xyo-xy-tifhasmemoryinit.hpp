//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TIFHASMEMORYINIT_HPP
#define XYO_XY_TIFHASMEMORYINIT_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class THasMemoryInit {
			protected:
				template <typename U, void (*)()> struct CheckMember;

				template <typename U>
				static bool testMember(CheckMember<U, &U::memoryInit > *);

				template <typename U>
				static int testMember(...);

			public:
				static const bool value = sizeof(testMember<T>(nullptr))==sizeof(char);
		};

		template<typename T, bool hasMemoryInit>
		class TIfHasMemoryInitBase {
			public:
				static inline void callMemoryInit() {
				};
		};

		template<typename T>
		class TIfHasMemoryInitBase<T, true > {
			public:
				static inline void callMemoryInit() {
					T::memoryInit();
				};
		};

		template<typename T>
		class TIfHasMemoryInit :
			public TIfHasMemoryInitBase<T, THasMemoryInit<T>::value> {
		};

	};
};

#endif

