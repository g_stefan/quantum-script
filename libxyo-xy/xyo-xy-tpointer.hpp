//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TPOINTER_HPP
#define XYO_XY_TPOINTER_HPP

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TPointerOwner;

		template<typename T>
		class TPointerX;

		//
		// Executive link - part of return/function parameter
		//
		template<typename T>
		class TPointer {

				template<typename Type > friend class TPointerX;
				template<typename Type > friend class TPointerOwner;

			protected:

				T *object;

			public:

				inline TPointer() {
					object = nullptr;
				};

				inline ~TPointer() {
					if (object) {
						object->decReferenceCount();
					};
				};

				inline TPointer(T *value) {
					object = value;
					if (object) {
						object->incReferenceCount();
					};
				};

				inline TPointer(const TPointer &value) {
					object = value.object;
					if (object) {
						object->incReferenceCount();
					};
				};

				inline TPointer(TPointer &&value) {
					object = value.object;
					value.object=nullptr;
				};

				inline TPointer(const TPointerX<T> &value) {
					object = (T *) value.object;
					if (object) {
						object->incReferenceCount();
					};
				};

				inline TPointer(TPointerX<T> &&value) {
					object = (T *) value.object;
					if (object) {
						object->incReferenceCount();
					};
				};

				inline TPointer(const TPointerOwner<T> &value) {
					object = value.object;
					(const_cast<TPointerOwner<T> &>(value)).object = nullptr;
				};

				inline TPointer(TPointerOwner<T> &&value) {
					object = value.object;
					value.object = nullptr;
				};

				inline TPointer &operator=(T *value) {
					if (value) {
						value->incReferenceCount();
					};
					if (object) {
						object->decReferenceCount();
					};
					object = value;
					return *this;
				};

				inline TPointer &operator=(const TPointer &value) {
					return operator=(value.object);
				};

				inline TPointer &operator=(TPointer &&value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					value.object=nullptr;
					return *this;
				};

				inline TPointer &operator=(const TPointerX<T> &value) {
					return operator=((T *) value.object);
				};

				inline TPointer &operator=(TPointerX<T> &&value) {
					return operator=((T *) value.object);
				};

				inline TPointer &operator=(const TPointerOwner<T> &value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					(const_cast<TPointerOwner<T> &>(value)).object = nullptr;
					return *this;
				};

				inline TPointer &operator=(TPointerOwner<T> &&value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					value.object = nullptr;
					return *this;
				};


				inline bool operator==(const TPointer &value) const {
					return object == value.object;
				};

				inline bool operator==(const TPointerX<T> &value) const {
					return object == (T *) value.object;
				};

				inline bool operator==(const TPointerOwner<T> &value) const {
					return object == value.object;
				};

				inline bool operator==(const T *value) const {
					return object == value;
				};

				inline bool operator!=(const TPointer &value) const {
					return object != value.object;
				};

				inline bool operator!=(const TPointerX<T> &value) const {
					return object != (T *) value.object;
				};

				inline bool operator!=(const TPointerOwner<T> &value) const {
					return object != value.object;
				};

				inline bool operator!=(const T *value) const {
					return object != value;
				};

				inline operator bool() {
					return (object);
				};

				inline T *operator->() {
					return object;
				};

				inline T &operator*() {
					return *object;
				};

				inline operator T *() {
					return object;
				};

				inline operator T &() {
					return *object;
				};

				inline T *value() {
					return object;
				};

				inline T &reference() {
					return *object;
				};

				inline bool isValid() {
					return object?true:false;
				};

				inline void newObject() {
					if (object) {
						object->decReferenceCount();
					};
					object = TMemoryObject<T>::newObject();
				};

				inline void deleteObject() {
					if (object) {
						object->decReferenceCount();
						object = nullptr;
					};
				};

				inline void setObject(T *value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value;
				};

				inline T *getObject() {
					if (object) {
						object->incReferenceCount();
					};
					return object;
				};

				inline T *getObjectOwnerTransfer() {
					T *retV=object;
					object=nullptr;
					return retV;
				};

				inline void setObjectWithOwnerTransfer(const TPointer &value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					(const_cast<TPointer &>(value)).object=nullptr;
				};

				inline void setObjectWithOwnerTransfer(TPointer &&value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					value.object=nullptr;
				};

				inline void activeDestructor() {
					if (object) {
						object->decReferenceCount();
						object = nullptr;
					};
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

		};

		template<typename T_>
		class TComparator<TPointer<T_> > {
			public:
				typedef TPointer<T_> T;

				inline static bool isEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isNotEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return TComparator<T_>::isLessThan(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return TComparator<T_>::isGreaterThan(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isLessThanOrEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isGreaterThanOrEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static int compare(const T &a, const T &b) {
					return TComparator<T_>::compare(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};
		};


	};
};

#endif

