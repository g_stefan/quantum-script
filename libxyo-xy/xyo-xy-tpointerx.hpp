//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TPOINTERX_HPP
#define XYO_XY_TPOINTERX_HPP

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_POINTERXBASE_HPP
#include "xyo-xy-pointerxbase.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TPointer;

		template<typename T>
		class TPointerOwner;

		//
		// Dynamic link - part of an object
		//
		template<typename T>
		class TPointerX :
			public PointerXBase {
				template<typename Type > friend class TPointer;
				template<typename Type > friend class TPointerOwner;

			private:
				inline TPointerX(const TPointerX &value) {};
				inline TPointerX(TPointerX &&value) {};
			public:

				inline TPointerX() {
					link = nullptr;
					object = nullptr;
				};

				inline ~TPointerX() {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
				};

				inline TPointerX &operator=(T *value) {
					if (value) {
						value->incReferenceCount();
						if (object) {
							((T *) object)->unregisterPointerXAndCount_(this);
						};
						object = value;
						((T *) object)->registerPointerXAndCount_(this);
						((T *) object)->decReferenceCount();
						return *this;
					};
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
						object = nullptr;
					};
					return *this;
				};

				inline TPointerX &operator=(const TPointerX &value) {
					if (object) {
						if (value.object) {
							((T *) (value.object))->incReferenceCount();
							if (object) {
								((T *) object)->unregisterPointerXAndCount_(this);
							};
							object = value.object;
							((T *) object)->registerPointerXAndCount_(this);
							((T *) object)->decReferenceCount();
							return *this;
						};
						if (object) {
							((T *) object)->unregisterPointerXAndCount_(this);
							object = nullptr;
						};
						return *this;
					};
					object = value.object;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
					};
					return *this;
				};

				inline TPointerX &operator=(TPointerX &&value) {
					if (object) {
						if (value.object) {
							((T *) (value.object))->incReferenceCount();
							if (object) {
								((T *) object)->unregisterPointerXAndCount_(this);
							};
							object = value.object;
							((T *) object)->registerPointerXAndCount_(this);
							((T *) object)->decReferenceCount();
							return *this;
						};
						if (object) {
							((T *) object)->unregisterPointerXAndCount_(this);
							object = nullptr;
						};
						return *this;
					};
					object = value.object;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
					};
					return *this;
				};


				inline TPointerX &operator=(const TPointer<T> &value) {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object = const_cast<TPointer<T> &>(value).object;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
					};
					return *this;
				};

				inline TPointerX &operator=(TPointer<T> &&value) {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object = value.object;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
					};
					return *this;
				};

				inline TPointerX &operator=(const TPointerOwner<T> &value) {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object = const_cast<TPointerOwner<T> &>(value).object;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
						((T *) object)->decReferenceCount();
					};
					const_cast<TPointerOwner<T> &>(value).object = nullptr;
					return *this;
				};

				inline TPointerX &operator=(TPointerOwner<T> &&value) {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object = value.object;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
						((T *) object)->decReferenceCount();
					};
					value.object = nullptr;
					return *this;
				};


				inline bool operator==(const TPointerX &value) const {
					return object == value.object;
				};

				inline bool operator==(const TPointer<T> &value) const {
					return object == value.object;
				};

				inline bool operator==(const TPointerOwner<T> &value) const {
					return object == value.object;
				};

				inline bool operator==(const T *value) const {
					return object == value;
				};

				inline bool operator!=(const TPointerX &value) const {
					return object != value.object;
				};

				inline bool operator!=(const TPointer<T> &value) const {
					return object != value.object;
				};

				inline bool operator!=(const TPointerOwner<T> &value) const {
					return object != value.object;
				};

				inline bool operator!=(const T *value) const {
					return object != value;
				};


				inline operator bool() {
					return object?true:false;
				};

				inline T *operator->() {
					return (T *) object;
				};

				inline T &operator*() {
					return *((T *) object);
				};

				inline operator T *() {
					return (T *) object;
				};

				inline operator T &() {
					return *((T *) object);
				};

				inline T *value() {
					return (T *) object;
				};

				inline T &reference() {
					return *((T *) object);
				};

				inline bool isValid() {
					return (object);
				};

				inline void newObject() {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object = TMemoryObject<T>::newObjectX();
					((T *) object)->registerPointerXAndCount_(this);
				};

				inline void deleteObject() {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
						object = nullptr;
					};
				};

				inline void setObject(T *value) {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object = value;
					if (object) {
						((T *) object)->registerPointerXAndCount_(this);
						((T *) object)->decReferenceCount();
					};
				};

				inline T *getObject() {
					if (object) {
						((T *) object)->incReferenceCount();
					};
					return (T *) object;
				};

				inline void memoryLink(Object *link_) {
					link = link_;
				};

				inline void setObjectX(T *value_) {
					if(object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object=value_;
					if(object) {
						((T *) object)->registerPointerXAndCount_(this);
					};
				};

				inline void setObjectXExecutive(T *value_) {
					if(object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object=value_;
					if(object) {
						((T *) object)->registerPointerXAndCount_(this);
						((T *) object)->decReferenceCountExecutive_();
					};
				};

				inline void activeDestructor() {
					if (object) {
						((T *) object)->unregisterPointerXAndCount_(this);
						object = nullptr;
					};
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

				inline void setObjectXExecutive(TPointerOwner<T> &value_) {
					if(object) {
						((T *) object)->unregisterPointerXAndCount_(this);
					};
					object=value_.getObjectOwnerTransfer();
					if(object) {
						((T *) object)->registerPointerXAndCount_(this);
						((T *) object)->decReferenceCountExecutive_();
					};
				};

		};

		template<typename T_>
		class TComparator<TPointerX<T_> > {
			public:
				typedef TPointerX<T_> T;

				inline static bool isEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isNotEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return TComparator<T_>::isLessThan(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return TComparator<T_>::isGreaterThan(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isLessThanOrEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isGreaterThanOrEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static int compare(const T &a, const T &b) {
					return TComparator<T_>::compare(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};
		};


	};
};

#endif

