//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_SINGLE_THREAD
#ifdef XYO_OS_TYPE_UNIX

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

#include "xyo-xy-xcriticalsection.hpp"

namespace XYO {
	namespace XY {


		struct SXCriticalSection_ {
			pthread_mutex_t section;
		};

		XCriticalSection::XCriticalSection() {
			criticalSection=new XCriticalSection_();
			pthread_mutex_init(&criticalSection->section,nullptr);
		};

		XCriticalSection::~XCriticalSection() {
			pthread_mutex_destroy(&criticalSection->section);
			delete criticalSection;
		};

		void XCriticalSection::enter() {
			pthread_mutex_lock(&criticalSection->section);
		};

		void XCriticalSection::leave() {
			pthread_mutex_unlock(&criticalSection->section);
		};

	};
};

#endif
#endif

