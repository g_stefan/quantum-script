#ifndef LIBXYO_XY_VERSION_HPP
#define LIBXYO_XY_VERSION_HPP

#define LIBXYO_XY_VERSION_ABCD      5,2,0,64
#define LIBXYO_XY_VERSION_A         5
#define LIBXYO_XY_VERSION_B         2
#define LIBXYO_XY_VERSION_C         0
#define LIBXYO_XY_VERSION_D         64
#define LIBXYO_XY_VERSION_STR_ABCD  "5.2.0.64"
#define LIBXYO_XY_VERSION_STR       "5.2.0"
#define LIBXYO_XY_VERSION_STR_BUILD "64"
#define LIBXYO_XY_VERSION_BUILD     64
#define LIBXYO_XY_VERSION_HOUR      18
#define LIBXYO_XY_VERSION_MINUTE    44
#define LIBXYO_XY_VERSION_SECOND    56
#define LIBXYO_XY_VERSION_DAY       14
#define LIBXYO_XY_VERSION_MONTH     6
#define LIBXYO_XY_VERSION_YEAR      2015
#define LIBXYO_XY_VERSION_STR_DATETIME "2015-06-14 18:44:56"

#ifndef XYO_RC

#ifndef XYO_XY__EXPORT_HPP
#include "xyo-xy--export.hpp"
#endif

namespace Lib {
	namespace XYO {
		namespace XY {

			class Version {
				public:
					XYO_XY_EXPORT static const char *getABCD();
					XYO_XY_EXPORT static const char *getA();
					XYO_XY_EXPORT static const char *getB();
					XYO_XY_EXPORT static const char *getC();
					XYO_XY_EXPORT static const char *getD();
					XYO_XY_EXPORT static const char *getVersion();
					XYO_XY_EXPORT static const char *getBuild();
					XYO_XY_EXPORT static const char *getHour();
					XYO_XY_EXPORT static const char *getMinute();
					XYO_XY_EXPORT static const char *getSecond();
					XYO_XY_EXPORT static const char *getDay();
					XYO_XY_EXPORT static const char *getMonth();
					XYO_XY_EXPORT static const char *getYear();
					XYO_XY_EXPORT static const char *getDatetime();
			};

		};
	};
};

#endif
#endif

