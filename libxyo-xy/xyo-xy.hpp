//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_HPP
#define XYO_XY_HPP

#include <cstddef>
#include <cstdint>

#ifndef XYO_XY__EXPORT_HPP
#include "xyo-xy--export.hpp"
#endif

#ifndef XYO_XY__CONFIG_HPP
#include "xyo-xy--config.hpp"
#endif

namespace XYO {
	typedef uint8_t  byte;
	typedef uint16_t word;
	typedef uint32_t dword;
	typedef uint64_t qword;
};

#define XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(T) \
	private:\
	inline T(const T&) = delete;\
	inline T(T&&) = delete;\
	inline T& operator =(const T&) = delete;\
	inline T& operator =(T&&) = delete


#define XYO_XY_INTERFACE(T) \
	protected:\
	inline T() = default;\
	inline ~T() = default;\
	XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(T)


#ifdef XYO_COMPILER_MSVC
#define XYO_FUNCTION __FUNCTION__
#else
#ifdef XYO_COMPILER_GNU
#define XYO_FUNCTION __PRETTY_FUNCTION__
#else
#define XYO_FUNCTION __func__
#endif
#endif

#ifdef XYO_MACHINE_64BIT
#define XYO_FORMAT_DWORD "%u"
#define XYO_FORMAT_SIZET_HEX "%16lX"
#define XYO_FORMAT_SIZET "%ld"
#else
#define XYO_FORMAT_DWORD "%lu"
#define XYO_FORMAT_SIZET_HEX "%08X"
#define XYO_FORMAT_SIZET "%d"
#endif

#ifndef XYO_SINGLE_THREAD
#       ifdef XYO_OS_TYPE_WIN
#               ifdef XYO_COMPILER_MSVC
#                       define XYO_THREAD_LOCAL __declspec(thread)
#               else
#                       ifdef XYO_COMPILER_GNU
#                               define XYO_THREAD_LOCAL __thread
#                       endif
#               endif
#       endif
#       ifdef XYO_OS_TYPE_UNIX
#               define XYO_THREAD_LOCAL __thread
#       endif
#else
#       define XYO_THREAD_LOCAL
#endif

#endif
