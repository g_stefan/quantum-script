//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TREDBLACKTREE_X_TPOINTERX_VALUE_HPP
#define XYO_XY_TREDBLACKTREE_X_TPOINTERX_VALUE_HPP

#ifndef XYO_XY_TREDBLACKTREE_HPP
#include "xyo-xy-tredblacktree.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TKey, typename TValue_, template <typename U> class TMemory>
		class TRedBlackTree<TKey, TPointerX<TValue_>, TMemory> :
			public Object,
			public TXRedBlackTree<TYRedBlackTreeNode<TKey, TPointerX<TValue_> >,TMemory<TYRedBlackTreeNode<TKey, TPointerX<TValue_> > > > {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TRedBlackTree);
			public:
				typedef TPointerX<TValue_> TValue;
				typedef TYRedBlackTreeNode<TKey, TValue> Node;
				typedef TMemory<Node> NodeMemory;

				inline TRedBlackTree() {
				};

				inline ~TRedBlackTree() {
				};

				inline void set(const TKey &key, const TValue &value) {
					Node *node=find(key);
					if (node) {
						node->value=value;
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					TIfHasObjectMemoryLink<TValue>::callObjectMemoryLink(&node->value,this);
					node->key = key;
					node->value = value;
					insertNode(node);
				};

				inline void set(const TKey &key, const TValue_ &value) {
					Node *node=find(key);
					if (node) {
						*(node->value)=value;
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					TIfHasObjectMemoryLink<TValue>::callObjectMemoryLink(&node->value,this);
					node->key = key;
					node->value.newObject();
					*(node->value)=value;
					insertNode(node);
				};

				inline void setOwner(const TKey &key, TValue_ *value) {
					Node *node=find(key);
					if (node) {
						node->value.setObjectXExecutive(value);
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					TIfHasObjectMemoryLink<TValue>::callObjectMemoryLink(&node->value,this);
					node->key = key;
					node->value.setObjectXExecutive(value);
					insertNode(node);
				};

				inline bool get(const TKey &key, TValue_ &value) {
					Node *x=find(key);
					if (x) {
						value = *(x->value);
						return true;
					};
					return false;
				};

				inline TValue_ getValue(const TKey &key, TValue_ value) {
					Node *x=find(key);
					if (x) {
						return *(x->value);
					};
					return value;
				};

				inline void activeDestructor() {
					empty();
				};

				inline static void memoryInit() {
					NodeMemory::memoryInit();
				};

		};

	};
};


#endif

