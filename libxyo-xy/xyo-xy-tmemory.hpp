//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORY_HPP
#define XYO_XY_TMEMORY_HPP

#ifndef XYO_XY_TXMEMORY_HPP
#include "xyo-xy-txmemory.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECT_HPP
#include "xyo-xy-tmemoryobject.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryImplementation: public TXMemory<T> {};

		template<typename T>
		class TMemoryImplementation<T,true>: public TMemoryObject<T> {};

		template<typename T>
		class TMemory: public TMemoryImplementation<T, TIsDerivedFrom<T,Object>::value> {};
	};
};

#endif

