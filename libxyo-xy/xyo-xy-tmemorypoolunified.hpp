//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLUNIFIED_HPP
#define XYO_XY_TMEMORYPOOLUNIFIED_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIED_HPP
#include "xyo-xy-txmemorypoolunified.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIED_HPP
#include "xyo-xy-tmemoryobjectpoolunified.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolUnifiedImplementation: public TXMemoryPoolUnified<T> {};

		template<typename T>
		class TMemoryPoolUnifiedImplementation<T,true>: public TMemoryObjectPoolUnified<T> {};

		template<typename T>
		class TMemoryPoolUnified: public TMemoryPoolUnifiedImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

