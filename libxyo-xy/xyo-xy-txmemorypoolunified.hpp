//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLUNIFIED_HPP
#define XYO_XY_TXMEMORYPOOLUNIFIED_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-txmemorypoolunifiedthread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXMemoryPoolUnified: public TXMemoryPoolUnifiedThread<T> {};

	};
};

#endif

