//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xy-object.hpp"

namespace XYO {
	namespace XY {

		static size_t XYO_THREAD_LOCAL referenceCountMark_ = 0;

		namespace Object_ {

			void registryThreadBegin() {
				referenceCountMark_=0;
			};

		};

		Object::Object() {
			referenceCounter_ = 0;
			memoryDelete_ = nullptr;
			referencePointerXHead_ = nullptr;
			referenceLoopDetectorMark_ = 0;
		};

		void Object::decReferenceCount() {
			--referenceCounter_;
			if (referenceCounter_ == 0) {
				if (referencePointerXHead_ == nullptr) {
					if (memoryDelete_) {
						(*memoryDelete_)(memoryThis_);
					};
					return;
				};
				referenceCountLoopDetector_();
				return;
			};
		};

		void Object::unregisterPointerXAndCount_(PointerXBase *value) {
			if (value == referencePointerXHead_) {
				referencePointerXHead_ = referencePointerXHead_->next;
				if (referencePointerXHead_) {
					referencePointerXHead_->back = nullptr;
				};
			} else {
				if (value->next) {
					value->next->back = value->back;
				};
				if (value->back) {
					value->back->next = value->next;
				};
			};


			if (referencePointerXHead_ == nullptr) {
				if (referenceCounter_ == 0) {
					if (memoryDelete_) {
						(*memoryDelete_)(memoryThis_);
					};
				};
				return;
			};
			if (referenceCounter_ == 0) {
				referenceCountLoopDetector_();
			};
		};

		void Object::registerPointerXAndCount_(PointerXBase *value) {
			value->next = referencePointerXHead_;
			value->back = nullptr;
			if (referencePointerXHead_) {
				referencePointerXHead_->back = value;
			};
			referencePointerXHead_ = value;
		};

		void Object::referenceCountLoopDetector_() {
			referenceLoopDetectorMark_ = ++referenceCountMark_;
			if(referenceCountLoopDetectorX_(referenceLoopDetectorMark_)==ReferenceTypeIsDynamic_) {
				//
				// All pointers are dynamic - reclaim
				//
				PointerXBase *index;
				for (index = referencePointerXHead_; index; index = index->next) {
					index->object = nullptr;
				};
				referencePointerXHead_ = nullptr;
				referenceCounter_ = 0;
				if (memoryDelete_) {
					(*memoryDelete_)(memoryThis_);
				};
			};
		};

		int Object::referenceCountLoopDetectorX_(size_t mark) {
			PointerXBase *index;
			int retV;

			referenceLoopDetectorMark_=mark;
			for (index = referencePointerXHead_; index; index = index->next) {
				if (index->link) {
					if(!index->link->memoryDelete_) {
						return ReferenceTypeIsExecutive_;
					};
					if (index->link->referenceCounter_) {
						return ReferenceTypeIsExecutive_;
					};
					if (index->link->referenceLoopDetectorMark_ == referenceLoopDetectorMark_) {
						continue;
					};
					retV = index->link->referenceCountLoopDetectorX_(mark);
					if (retV == ReferenceTypeIsExecutive_) {
						return retV;
					};
				};
			};
			return ReferenceTypeIsDynamic_;
		};

	};
};

