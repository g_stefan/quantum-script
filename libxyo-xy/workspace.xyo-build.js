//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

.solution("libxyo-xy",function() {

	if(Platform.is("win")) {
		.option("define","XYO_OS_TYPE_WIN");
		.dependencyOption("define","XYO_OS_TYPE_WIN");
	};
	if(Platform.is("unix")) {
		.option("define","XYO_OS_TYPE_UNIX");
		.dependencyOption("define","XYO_OS_TYPE_UNIX");
	};
	if(Platform.isCompiler("msvc")) {
		.option("define","XYO_COMPILER_MSVC");
		.dependencyOption("define","XYO_COMPILER_MSVC");
	};
	if(Platform.is("win32")) {
		.option("define","XYO_MACHINE_32BIT");
		.dependencyOption("define","XYO_MACHINE_32BIT");
	};
	if(Platform.is("win64")) {
		.option("define","XYO_MACHINE_64BIT");
		.dependencyOption("define","XYO_MACHINE_64BIT");
	};

	.option("licence","mit");

	.project("libxyo-xy","version",function() {
		.option("version","xyo-version", {
			type:"xyo-cpp",
			sourceBegin:
			"#ifndef XYO_XY__EXPORT_HPP\r\n"+
			"#include \"xyo-xy--export.hpp\"\r\n"+
			"#endif\r\n"+
			"\r\n"+
			"namespace Lib {\r\n"+
			"\tnamespace XYO{\r\n"+
			"\t\tnamespace XY{\r\n",
			sourceEnd:
			"\t\t};\r\n"+
			"\t};\r\n"+
			"};\r\n",
			codeExport:"XYO_XY_EXPORT",
			lineBegin:"\t\t\t"
		});
	});

	.project("libxyo-xy","lib",function() {
		.file("source",["*.hpp","*.cpp"]);

		.option("crt","type","static");
		.option("cpp","exceptions",true);
		.option("cpp","rtti",true);

		.dependencyOption("crt","type","static");
		.dependencyOption("cpp","exceptions",true);
		.dependencyOption("cpp","rtti",true);
		.dependencyOption("install-include","xyo-xy");
	});

	.project("libxyo-xy","dll",function() {
		.file("source",["*.hpp","*.cpp","*.rc"]);

		.option("define","XYO_XY_INTERNAL");
		.option("define","XYO_DYNAMIC_LINK");
		.option("sign","xyo-security");
		.option("crt","type","dynamic");
		.option("cpp","exceptions",true);
		.option("cpp","rtti",true);

		.dependencyOption("define","XYO_DYNAMIC_LINK");
		.dependencyOption("crt","type","dynamic");
		.dependencyOption("cpp","exceptions",true);
		.dependencyOption("cpp","rtti",true);
		.dependencyOption("install-include","xyo-xy");
	});

	.project("libxyo-xy","install",function() {
		.install("include","*.hpp","xyo-xy");
	});

});

