//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XREGISTRYPROCESS_HPP
#define XYO_XY_XREGISTRYPROCESS_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_XREGISTRYLEVEL_HPP
#include "xyo-xy-xregistrylevel.hpp"
#endif

#ifndef XYO_XY_XREGISTRY_HPP
#include "xyo-xy-xregistry.hpp"
#endif

namespace XYO {
	namespace XY {
		namespace XRegistryProcess {

			typedef void (*ResourceDelete)(void *);

			XYO_XY_EXPORT void processBegin();
			XYO_XY_EXPORT void processEnd();
			XYO_XY_EXPORT bool registerKey(const char *registryKey,void **registryLink);
			XYO_XY_EXPORT void setValue(void *registryLink,size_t categoryLevel,void *resourceValue,ResourceDelete resourceDelete);
			XYO_XY_EXPORT void *getValue(void *registryLink);
#ifndef XYO_SINGLE_THREAD
			XYO_XY_EXPORT void criticalEnter();
			XYO_XY_EXPORT void criticalLeave();
#endif

		};
	};
};

#endif
