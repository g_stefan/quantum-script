//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPROCESS_HPP
#define XYO_XY_TXMEMORYPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLPROCESS_HPP
#include "xyo-xy-txmemorypoolprocess.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXMemoryProcess: public TXMemoryPoolProcess<T> {};

	};
};

#endif

