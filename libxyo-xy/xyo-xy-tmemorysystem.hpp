//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYSYSTEM_HPP
#define XYO_XY_TMEMORYSYSTEM_HPP

#ifndef XYO_XY_TXMEMORYSYSTEM_HPP
#include "xyo-xy-txmemorysystem.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTSYSTEM_HPP
#include "xyo-xy-tmemoryobjectsystem.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemorySystemImplementation: public TXMemorySystem<T> {};

		template<typename T>
		class TMemorySystemImplementation<T,true>: public TMemoryObjectSystem<T> {};

		template<typename T>
		class TMemorySystem: public TMemorySystemImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

