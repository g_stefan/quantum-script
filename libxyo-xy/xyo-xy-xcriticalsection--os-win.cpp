//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_SINGLE_THREAD
#ifdef XYO_OS_TYPE_WIN

#include <stdio.h>
#include <windows.h>

#include "xyo-xy-xcriticalsection.hpp"

namespace XYO {
	namespace XY {

		struct SXCriticalSection_ {
			CRITICAL_SECTION section;
		};

		XCriticalSection::XCriticalSection() {
			criticalSection=new XCriticalSection_();
			InitializeCriticalSection(&criticalSection->section);
		};

		XCriticalSection::~XCriticalSection() {
			DeleteCriticalSection(&criticalSection->section);
			delete criticalSection;
		};

		void XCriticalSection::enter() {
			EnterCriticalSection(&criticalSection->section);
		};

		void XCriticalSection::leave() {
			LeaveCriticalSection(&criticalSection->section);
		};

	};
};

#endif
#endif

