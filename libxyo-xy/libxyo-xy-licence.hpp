//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBXYO_XY_LICENCE_HPP
#define LIBXYO_XY_LICENCE_HPP

#ifndef XYO_XY__EXPORT_HPP
#include "xyo-xy--export.hpp"
#endif

namespace Lib {
	namespace XYO {
		namespace XY {

			class Licence {
				public:
					XYO_XY_EXPORT static const char *content();
					XYO_XY_EXPORT static const char *shortContent();
			};

		};
	};
};

#endif
