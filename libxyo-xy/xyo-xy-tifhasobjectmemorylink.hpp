//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TIFHASOBJECTMEMORYLINK_HPP
#define XYO_XY_TIFHASOBJECTMEMORYLINK_HPP

#ifndef XYO_XY_OBJECT_HPP
#include "xyo-xy-object.hpp"
#endif

#ifndef XYO_XY_TGETCLASSOFMEMBER_HPP
#include "xyo-xy-tgetclassofmember.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class THasObjectMemoryLink {
			protected:
				template <typename U, void (U::*)(Object *value_)> struct CheckMember;

				template <typename U>
				static char testMember(CheckMember<U, &U::memoryLink > *);

				template <typename U>
				static int testMember(...);

				template<typename N>
				class THasMember {
					public:
						static const bool value = sizeof(testMember<N>(nullptr))==sizeof(char);
				};

				template <typename U>
				static char testBaseMember(decltype(TGetClassOfMember(&U::memoryLink)) *);

				template <typename U>
				static int testBaseMember(...);

				template<typename N>
				class THasBaseMember {
					public:
						static const bool value = sizeof(testBaseMember<N>(nullptr))==sizeof(char);
				};

				template<typename N,bool hasBase>
				class TProcessBaseMember {
					public:
						static const bool value = false;
				};

				template<typename N>
				class TProcessBaseMember<N, true> {
					public:
						static const bool value = THasMember<decltype(TGetClassOfMember(&N::memoryLink))>::value;
				};

			public:
				static const bool value = THasMember<T>::value|TProcessBaseMember<T,THasBaseMember<T>::value>::value;
		};

		template<typename T, bool hasObjectMemoryLink>
		class TIfHasObjectMemoryLinkBase {
			public:

				static inline void callObjectMemoryLink(T *,Object *) {
				};

				static inline void callObjectMemoryArray(T *,Object *,size_t length) {
				};

		};

		template<typename T>
		class TIfHasObjectMemoryLinkBase<T, true > {
			public:

				static inline void callObjectMemoryLink(T *this_,Object *value_) {
					this_->memoryLink(value_);
				};

				static inline void callObjectMemoryLinkArray(T *this_,Object *value_,size_t length) {
					size_t k;
					for(k=0; k<length; ++k) {
						this_[k].memoryLink(value_);
					};
				};

		};

		template<typename T>
		class TIfHasObjectMemoryLink :
			public TIfHasObjectMemoryLinkBase<T, THasObjectMemoryLink<T>::value> {
		};

	};
};

#endif

