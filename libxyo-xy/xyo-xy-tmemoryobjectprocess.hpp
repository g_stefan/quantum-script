//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPROCESS_HPP
#define XYO_XY_TMEMORYOBJECTPROCESS_HPP

#ifndef XYO_XY_TMEMORYOBJECTPOOLPROCESS_HPP
#include "xyo-xy-tmemoryobjectpoolprocess.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectProcess: public TMemoryObjectPoolProcess<T> {};

	};
};

#endif

