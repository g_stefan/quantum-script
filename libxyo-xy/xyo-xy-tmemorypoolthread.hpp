//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLTHREAD_HPP
#define XYO_XY_TMEMORYPOOLTHREAD_HPP

#ifndef XYO_XY_TXMEMORYPOOLTHREAD_HPP
#include "xyo-xy-txmemorypoolthread.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLTHREAD_HPP
#include "xyo-xy-tmemoryobjectpoolthread.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolThreadImplementation: public TXMemoryPoolThread<T> {};

		template<typename T>
		class TMemoryPoolThreadImplementation<T,true>: public TMemoryObjectPoolThread<T> {};

		template<typename T>
		class TMemoryPoolThread: public TMemoryPoolThreadImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

