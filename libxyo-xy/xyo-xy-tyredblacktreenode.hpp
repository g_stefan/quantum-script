//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TYREDBLACKTREENODE_HPP
#define XYO_XY_TYREDBLACKTREENODE_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TXREDBLACKTREEB_HPP
#include "xyo-xy-txredblacktree.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TKey, typename TValue>
		class TYRedBlackTreeNode:
			public TXRedBlackTreeNode<TYRedBlackTreeNode<TKey,TValue>,TKey> {
			public:
				TValue value;

				inline void activeConstructor() {
					TIfHasActiveConstructor<TKey>::callActiveConstructor(&key);
					TIfHasActiveConstructor<TValue>::callActiveConstructor(&value);
				};

				inline void activeDestructor() {
					TIfHasActiveDestructor<TKey>::callActiveDestructor(&key);
					TIfHasActiveDestructor<TValue>::callActiveDestructor(&value);
				};

				inline void activeReset() {
					TIfHasActiveReset<TKey>::callActiveReset(&key);
					TIfHasActiveReset<TValue>::callActiveReset(&value);
				};

				inline static void memoryInit() {
					TMemory<TKey>::memoryInit();
					TMemory<TValue>::memoryInit();
				};
		};

	};
};

#endif

