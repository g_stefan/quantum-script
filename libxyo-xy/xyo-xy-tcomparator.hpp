//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TCOMPARATOR_HPP
#define XYO_XY_TCOMPARATOR_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TComparator {
			public:

				inline static bool isEqualTo(const T &a, const T &b) {
					return a == b;
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return a != b;
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return a < b;
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return a > b;
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return a <= b;
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return a >= b;
				};

				inline static int compare(const T &a, const T &b) {
					if(a<b) {
						return -1;
					};
					if(a==b) {
						return 0;
					};
					return 1;
				};

		};

	};
};

#endif
