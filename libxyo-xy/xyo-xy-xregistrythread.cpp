//
// XYO XY Library
//
// Copyright (c) 2015 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#ifndef XYO_SINGLE_THREAD

#include "xyo-xy-xregistrykey.hpp"
#include "xyo-xy-xregistrydata.hpp"
#include "xyo-xy-xcriticalsection.hpp"
#include "xyo-xy-xregistrythread.hpp"

namespace XYO {
	namespace XY {

		namespace Object_ {
			void registryThreadBegin();
		};

		namespace XRegistryThread {

			class Node:
				public TXList2Node<Node> {
				public:

					XRegistryData::List2 data[4];
					void ** *fastTrack;
			};

			class NodeMemory {
				public:
					static inline Node *memoryNew() {
						return new Node();
					};

					static void memoryDelete(Node *this_) {
						delete this_;
					};
			};

			typedef TXList2<Node,NodeMemory> List2;

			//
			// To be able to init registry before main
			//
			void *fastTrackInit[]= {
				nullptr
			};

			static XCriticalSection criticalSection;
			static List2 root;
			static size_t fastTrackSize;
			static XYO_THREAD_LOCAL Node *thisThread;
			static XYO_THREAD_LOCAL void **fastTrack=&fastTrackInit[0];

			static void resizeFastTrack() {
				Node *scan;
				size_t oldSize;
				void **fastTrackOld_;
				void **fastTrackNew_;
				oldSize=fastTrackSize;
				fastTrackSize+=256;
				for(scan=root.root; scan; scan=scan->next) {
					fastTrackOld_=*scan->fastTrack;
					fastTrackNew_=new void *[fastTrackSize];
					memset(fastTrackNew_,0,sizeof(void *)*fastTrackSize);
					memcpy(fastTrackNew_,fastTrackOld_,sizeof(void *)*oldSize);
					*scan->fastTrack=fastTrackNew_;
					delete[] fastTrackOld_;
				};
			};



			void processBegin() {
				thisThread=nullptr;
				fastTrack=nullptr;
				fastTrackSize=256;

				threadBegin();

			};

			void XRegistryThread::processEnd() {

				threadEnd();

				root.empty();
			};

			void threadBegin() {
				Node *this_=NodeMemory::memoryNew();

				fastTrack=new void *[fastTrackSize];
				memset(fastTrack,0,sizeof(void *)*fastTrackSize);
				this_->fastTrack=&fastTrack;

				criticalSection.enter();
				root.push(this_);
				criticalSection.leave();
				thisThread=this_;

				Object_::registryThreadBegin();
			};

			void threadEnd() {
				Node *this_=thisThread;

				this_->data[XRegistryLevel::Singleton].empty();
				this_->data[XRegistryLevel::Active].empty();
				this_->data[XRegistryLevel::Static].empty();
				this_->data[XRegistryLevel::System].empty();

				thisThread=nullptr;
				delete[] fastTrack;
				criticalSection.enter();
				root.extract(this_);
				criticalSection.leave();
				NodeMemory::memoryDelete(this_);
			};

			bool registerKey(const char *registryKey,size_t *registryLink) {
				XRegistryKey::Node *node=XRegistryKey::registerKey(registryKey);
				checkFastTrack(node);
				*registryLink=node->threadValue;
				return fastTrack[node->threadValue]==nullptr;
			};

			void setValue(size_t registryLink,size_t categoryLevel,void *resourceValue,ResourceDelete resourceDelete) {
				XRegistryData::Node *node=XRegistryData::NodeMemory::memoryNew();
				thisThread->data[categoryLevel].push(node);
				node->resourceValue=resourceValue;
				node->resourceDelete=resourceDelete;
				fastTrack[registryLink]=resourceValue;
			};

			void *getValue(size_t registryLink) {
				return fastTrack[registryLink];
			};

			void checkFastTrack(void *registryNode) {
				if(((XRegistryKey::Node *)registryNode)->threadValue>fastTrackSize) {
					resizeFastTrack();
				};
			};

			void criticalEnter() {
				criticalSection.enter();
			};

			void criticalLeave() {
				criticalSection.leave();
			};


		};
	};
};

#endif

