//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <stdio.h>
#include <windows.h>

#include "xyo-xy-xregistrythread.hpp"
#include "xyo-xy-xthread.hpp"

#ifdef XYO_SINGLE_THREAD

namespace XYO {
	namespace XY {
		namespace XThread {

			void sleepOneMillisecond() {
				Sleep(1);
			};

		};
	};
};

#else

namespace XYO {
	namespace XY {
		namespace XThread {

			typedef struct SLink Link;
			struct SLink {
				XThread::Procedure procedure;
				void *this_;

				DWORD id;
				HANDLE thread;
			};

			static DWORD WINAPI procedureRun(Link *link) {
				XRegistryThread::threadBegin();
				(*link->procedure)(link->this_);
				XRegistryThread::threadEnd();
				delete link;
				return 0L;
			};

			static DWORD WINAPI procedureRunDirect(Link *link) {
				(*link->procedure)(link->this_);
				delete link;
				return 0L;
			};

			bool start(Procedure procedure, void *this_) {
				Link *link;
				link=new Link();
				link->procedure=procedure;
				link->this_=this_;


				link->thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) procedureRun, link, 0, &link->id);
				if (link->thread != NULL) {
					return true;
				};

				delete link;
				return false;
			};

			bool startDirect(Procedure procedure, void *this_) {
				Link *link;
				link=new Link();
				link->procedure=procedure;
				link->this_=this_;


				link->thread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE) procedureRunDirect, link, 0, &link->id);
				if (link->thread != nullptr) {
					return true;
				};

				delete link;
				return false;
			};

			typedef struct SLinkNotify LinkNotify;
			struct SLinkNotify {
				Procedure procedure;
				void *this_;

				Procedure procedureNew;
				void *thisNew_;

				Procedure procedureDelete;
				void *thisDelete_;

				DWORD id;
				HANDLE thread;
			};

			static DWORD WINAPI procedureRunWithNotify(LinkNotify *link) {
				XRegistryThread::threadBegin();
				(*link->procedureNew)(link->thisNew_);
				(*link->procedure)(link->this_);
				(*link->procedureDelete)(link->thisDelete_);
				XRegistryThread::threadEnd();
				delete link;
				return 0L;
			};

			bool startWithNotify(
				Procedure procedure,void *this_,
				Procedure procedureNew,void *thisNew_,
				Procedure procedureDelete,void *thisDelete_
			) {

				LinkNotify *link;
				link=new LinkNotify();
				link->procedure=procedure;
				link->this_=this_;

				link->procedureNew=procedureNew;
				link->thisNew_=thisNew_;

				link->procedureDelete=procedureDelete;
				link->thisDelete_=thisDelete_;

				link->thread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE) procedureRunWithNotify, link, 0, &link->id);
				if (link->thread != nullptr) {
					return true;
				};

				delete link;
				return false;

			};

			void sleepOneMillisecond() {
				Sleep(1);
			};

		};
	};
};

#endif

#endif

