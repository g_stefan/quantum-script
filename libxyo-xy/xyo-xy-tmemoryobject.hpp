//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECT_HPP
#define XYO_XY_TMEMORYOBJECT_HPP

#ifndef XYO_XY_TMEMORYOBJECTTHREAD_HPP
#include "xyo-xy-tmemoryobjectthread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObject: public TMemoryObjectThread<T> {};

	};
};

#endif

