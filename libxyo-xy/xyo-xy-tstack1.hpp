//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TSTACK1_HPP
#define XYO_XY_TSTACK1_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TXMEMORY_HPP
#include "xyo-xy-txmemory.hpp"
#endif

#ifndef XYO_XY_TYLIST1_HPP
#include "xyo-xy-tylist1.hpp"
#endif

#ifndef XYO_XY_TIFHASOBJECTMEMORYLINK_HPP
#include "xyo-xy-tifhasobjectmemorylink.hpp"
#endif

namespace XYO {
	namespace XY {

		template <typename T, template <typename U> class TMemory=TXMemory>
		class TStack1 :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TStack1);
			protected:
				TYList1<T> *head_;
			public:

				typedef TYList1<T> Node;
				typedef TMemory<TYList1<T> > NodeMemory;

				inline TStack1() {
					head_ = nullptr;
				};

				inline ~TStack1() {
					empty();
				};

				inline void empty() {
					TYList1<T> *element;
					while (head_) {
						element = head_;
						head_ = head_->next;
						TMemory<TYList1<T> >::memoryDelete(element);
					};
					head_ = nullptr;
				};

				inline void push(T value) {
					pushX(value);
				};

				inline void pushX(T &value) {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					TIfHasObjectMemoryLink<T>::callObjectMemoryLink(&element->value,this);
					element->next = head_;
					element->value=value;
					head_ = element;
				};

				inline void pushEmpty() {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					TIfHasObjectMemoryLink<T>::callObjectMemoryLink(&element->value,this);
					element->next = head_;
					head_ = element;
				};

				inline bool peek(T &out) {
					if (head_) {
						out = head_->value;
						return true;
					};
					return false;
				};

				inline bool pop(T &out) {
					TYList1<T> *element;
					if (head_) {
						element = head_;
						head_ = head_->next;
						out = element->value;
						TMemory<TYList1<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool popEmpty() {
					TYList1<T> *element;
					if (head_) {
						element = head_;
						head_ = head_->next;
						TMemory<TYList1<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool isEmpty() {
					return head_ == nullptr;
				};

				inline operator TYList1<T> *() {
					return head_;
				};

				inline TYList1<T> *head() {
					return head_;
				};

				inline bool popElement(TYList1<T> *&element) {
					if (head_) {
						element=head_;
						head_ = head_->next;
						TIfHasObjectMemoryLink<T>::callObjectMemoryLink(&element->value,nullptr);
						return true;
					};
					return false;
				};

				inline void pushElement(TYList1<T> *element) {
					element->next = head_;
					head_=element;
					TIfHasObjectMemoryLink<T>::callObjectMemoryLink(&element->value,this);
				};

				static inline TYList1<T> *nodeMemoryNew() {
					return TMemory<TYList1<T> >::memoryNew();
				};

				static inline void nodeMemoryDelete(TYList1<T> *node) {
					TMemory<TYList1<T> >::memoryDelete(node);
				};

				inline void activeDestructor() {
					empty();
				};

				inline void duplicate() {
					TYList1<T> *element;
					element = TMemory<TYList1<T> >::memoryNew();
					TIfHasObjectMemoryLink<T>::callObjectMemoryLink(&element->value,this);
					element->next = head_;
					if (head_) {
						element->value = head_->value;
					};
					head_ = element;
				};

				inline bool swap() {
					T tmp;
					if (head_) {
						if(head_->next) {
							tmp=head_->next->value;
							head_->next->value=head_->value;
							head_->value=tmp;
							return true;
						};
					};
					return false;
				};

				inline static void memoryInit() {
					TMemory<TYList1<T> >::memoryInit();
				};

		};

	};
};

#endif

