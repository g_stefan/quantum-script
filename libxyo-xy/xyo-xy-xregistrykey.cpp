//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xy-xregistrykey.hpp"

namespace XYO {
	namespace XY {
		namespace XRegistryKey {

			class NodeMemory {
				public:
					static inline Node *memoryNew() {
						return new Node();
					};
					static inline void memoryDelete(Node *this_) {
						delete this_;
					};
			};

			typedef TXRedBlackTree<Node,NodeMemory> RBTree;

			static RBTree root;
			static size_t threadKey;

			Node *XRegistryKey::registerKey(const char *key) {
				Node *this_=root.find(key);
				if(this_) {
					return this_;
				};

				this_=NodeMemory::memoryNew();

				this_->key=key;
				this_->processValue=nullptr;
				this_->threadValue=threadKey;

				threadKey++;

				root.insertNode(this_);

				return this_;
			};

			void XRegistryKey::processBegin() {
				threadKey=1;
			};

			void XRegistryKey::processEnd() {
				root.empty();
			};


		};
	};
};

