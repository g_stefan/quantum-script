//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XTHREAD_HPP
#define XYO_XY_XTHREAD_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifdef XYO_SINGLE_THREAD

namespace XYO {
	namespace XY {
		namespace XThread {

			XYO_XY_EXPORT void sleepOneMillisecond();

		};
	};
};

#else

namespace XYO {
	namespace XY {
		namespace XThread {

			typedef void (*Procedure)(void *);

			XYO_XY_EXPORT bool start(Procedure procedure,void *this_);
			XYO_XY_EXPORT bool startWithNotify(
				Procedure procedure,void *this_,
				Procedure procedureNew,void *thisNew_,
				Procedure procedureDelete,void *thisDelete_
			);
			XYO_XY_EXPORT bool startDirect(Procedure procedure,void *this_);
			XYO_XY_EXPORT void sleepOneMillisecond();

		};
	};
};

#endif

#endif

