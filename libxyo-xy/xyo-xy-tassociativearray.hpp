//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TASSOCIATIVEARRAY_HPP
#define XYO_XY_TASSOCIATIVEARRAY_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TREDBLACKTREE_HPP
#include "xyo-xy-tredblacktree.hpp"
#endif

#ifndef XYO_XY_TDYNAMICARRAY_HPP
#include "xyo-xy-tdynamicarray.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename Key, typename Value, dword dataSize2Pow, template <typename U> class TMemory=TXMemory>
		class TAssociativeArray :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TAssociativeArray);
			public:
				TPointerX<TRedBlackTree<Key,dword,TMemory> > mapKey;
				TPointerX<TDynamicArray<Key,dataSize2Pow,TMemory> > arrayKey;
				TPointerX<TDynamicArray<Value,dataSize2Pow,TMemory> > arrayValue;
				dword length_;

				inline TAssociativeArray() {
					mapKey.memoryLink(this);
					arrayKey.memoryLink(this);
					arrayValue.memoryLink(this);
					mapKey.newObject();
					arrayKey.newObject();
					arrayValue.newObject();
					length_=0;
				};

				inline ~TAssociativeArray() {
					mapKey->empty();
					arrayKey->empty();
					arrayValue->empty();
				};

				inline void empty() {
					mapKey->empty();
					arrayKey->empty();
					arrayValue->empty();
					length_=0;
				};

				inline void activeDestructor() {
					empty();
				};

				inline bool get(Key key, Value &value) {
					return getX(key, value);
				};

				inline bool getX(Key &key, Value &value) {
					TYRedBlackTreeNode<Key, dword> *x;
					x = mapKey->findX(key);
					if (x) {
						return arrayValue->get(x->value,value);
					};
					return false;
				};

				inline void set(Key key, Value value) {
					setX(key, value);
				};

				inline void setX(Key &key, Value &value) {
					TYRedBlackTreeNode<Key, dword> *x;
					x = mapKey->findX(key);
					if (x) {
						arrayValue->setX(x->value,value);
						return;
					};
					mapKey->insertX(key, length_);
					arrayKey->setX(length_,value);
					arrayValue->setX(length_,value);
					++length_;
				};

				inline dword length() {
					return length_;
				};

				inline Value &get2(Key key,Value valueDefault) {
					return get2X(key,valueDefault);
				};

				inline Value &get2X(Key &key,Value &valueDefault) {
					TYRedBlackTreeNode<Key, dword> *x;
					x = mapKey->findX(key);
					if (x) {
						return (*arrayValue)[x->value];
					};
					mapKey->insertX(key, length_);
					arrayKey->setX(length_,valueDefault);
					arrayValue->setX(length_,valueDefault);
					++length_;
					return (*arrayValue)[length_-1];
				};

				inline static void memoryInit() {
					TPointerX<TRedBlackTree<Key,dword,TMemory> >::memoryInit();
					TPointerX<TDynamicArray<Key,dataSize2Pow,TMemory> >::memoryInit();
					TPointerX<TDynamicArray<Value,dataSize2Pow,TMemory> >::memoryInit();
				};

		};

	};
};

#endif
