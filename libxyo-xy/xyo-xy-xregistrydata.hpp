//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XREGISTRYDATA_HPP
#define XYO_XY_XREGISTRYDATA_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TXLIST2_HPP
#include "xyo-xy-txlist2.hpp"
#endif

namespace XYO {
	namespace XY {

		class XRegistryData {
			public:

				typedef void (*ResourceDelete)(void *);

				class Node:
					public TXList2Node<Node> {
					public:
						void *resourceValue;
						ResourceDelete resourceDelete;

						inline ~Node() {
							if(resourceValue) {
								if(resourceDelete) {
									(*resourceDelete)(resourceValue);
								};
							};
						};
				};

				class NodeMemory {
					public:
						static inline Node *memoryNew() {
							return new Node();
						};

						static inline void memoryDelete(Node *this_) {
							delete this_;
						};
				};

				typedef TXList2<Node,NodeMemory> List2;
		};


	};
};

#endif

