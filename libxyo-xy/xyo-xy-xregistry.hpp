//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XREGISTRY_HPP
#define XYO_XY_XREGISTRY_HPP

#ifndef XYO_XY_XREGISTRYPROCESS_HPP
#include "xyo-xy-xregistryprocess.hpp"
#endif

#ifndef XYO_XY_XREGISTRYTHREAD_HPP
#include "xyo-xy-xregistrythread.hpp"
#endif

namespace XYO {
	namespace XY {

		class XRegistry {
			public:
				XYO_XY_EXPORT static void *registryInit();
		};

	};
};

#endif
