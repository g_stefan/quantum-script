//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#define XYO_XY_TISDERIVEDFROM_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		template <typename Derived, typename Base>
		class TIsDerivedFrom {
			private:

				template <typename T>
				static char isDerivedFrom__(T *);

				template <typename T>
				static int isDerivedFrom__(...);

			public:
				static const bool value = (sizeof (isDerivedFrom__<Base>(static_cast<Derived *> (0))) == sizeof (char));
		};

	};
};

#endif
