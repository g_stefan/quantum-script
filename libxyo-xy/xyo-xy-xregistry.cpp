//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#include <string.h>
#include <stdio.h>

#ifdef XYO_OS_TYPE_WIN
#ifdef XYO_MEMORY_LEAK_DETECTOR
#include "vld.h"
#endif
#endif

#include "xyo-xy-xregistry.hpp"

namespace XYO {
	namespace XY {

		class XRegistryInit {
			public:
				XRegistryInit();
				~XRegistryInit();
		};

		XRegistryInit::XRegistryInit() {
			XRegistryProcess::processBegin();
		};

		XRegistryInit::~XRegistryInit() {
			XRegistryProcess::processEnd();
		};

		void *XRegistry::registryInit() {
			static XRegistryInit registryInit_;
			return &registryInit_;
		};


	};
};

