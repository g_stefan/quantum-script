//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP
#define XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP

#ifdef XYO_SINGLE_THREAD

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryPoolUnifiedThread: public TXMemoryPoolUnifiedProcess<T> {};
	};
};

#else

#include <new>

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_XREGISTRY_HPP
#include "xyo-xy-xregistry.hpp"
#endif

#ifndef XYO_XY_TIFHASMEMORYINIT_HPP
#include "xyo-xy-tifhasmemoryinit.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<size_t sizeOfT>
		class TXMemoryPoolUnifiedImplementThread {
			public:

				typedef struct SLink {
					struct SLink *next;
				} Link;

				typedef struct SMemory {
					struct SMemory *next;
				} Memory;

				Link *poolFree;
				Memory *poolMemory;

				inline TXMemoryPoolUnifiedImplementThread() {
					poolFree=nullptr;
					poolMemory=nullptr;
				};

				inline ~TXMemoryPoolUnifiedImplementThread() {
					Memory *link;
					Memory *next;

					for (link = poolMemory; link; link = next) {
						next = link->next;
						delete[] (byte *)link;
					};
				};

				inline void grow() {
					size_t totalSize;
					size_t k;
					byte *scan;
					Link *link;
					Memory *memory;

					totalSize = (sizeOfT + sizeof (Link)) * 8 + sizeof (Memory);
					scan = new byte[totalSize];
					memory = (Memory *)scan;
					memory->next = poolMemory;
					poolMemory = memory;
					scan += sizeof (Memory);
					for (k = 0; k < 8; ++k) {
						link = (Link *)scan;
						scan += sizeof (Link) + sizeOfT;
						link->next = poolFree;
						poolFree = link;
					};
				};

				inline void *memoryNew() {
					void *retV;
					if(!poolFree) {
						grow();
					};
					retV=(void *)(((byte *)poolFree)+sizeof (Link));
					poolFree=poolFree->next;
					return retV;
				};

				inline void memoryDelete(void *value) {
					value=(void *)(((byte *)value)-sizeof(Link));
					((Link *)value)->next = poolFree;
					poolFree =(Link *)value;
				};


				static size_t registryLink;
				static const char *registryKey();
				static void resourceDelete(void *);
		};

		template<size_t sizeOfT>
		const char   *TXMemoryPoolUnifiedImplementThread<sizeOfT>::registryKey() {
			return XYO_FUNCTION;
		};

		template<size_t sizeOfT>
		size_t TXMemoryPoolUnifiedImplementThread<sizeOfT>::registryLink=0;

		template<size_t sizeOfT>
		void TXMemoryPoolUnifiedImplementThread<sizeOfT>::resourceDelete(void *this_) {
			delete (TXMemoryPoolUnifiedImplementThread<sizeOfT> *)this_;
		};

		template<typename T>
		class TXMemoryPoolUnifiedThread {
			public:

				inline static T *memoryNew() {
					T *retV=reinterpret_cast<T *>(XRegistryThread::getValue(TXMemoryPoolUnifiedImplementThread<sizeof(T)>::registryLink));
					if(!retV) {
						memoryInit();
						retV=reinterpret_cast<T *>(XRegistryThread::getValue(TXMemoryPoolUnifiedImplementThread<sizeof(T)>::registryLink));
					};
					retV=new(
						(
							(TXMemoryPoolUnifiedImplementThread<sizeof(T)> *)
							retV
						)->memoryNew()
					) T();
					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline static void memoryDelete(T *this_) {
					TIfHasActiveDestructor<T>::callActiveDestructor(this_);
					this_->~T();
					(
						(TXMemoryPoolUnifiedImplementThread<sizeof(T)> *)
						XRegistryThread::getValue(TXMemoryPoolUnifiedImplementThread<sizeof(T)>::registryLink)
					)->memoryDelete(this_);
				};

				inline static void memoryInit() {
					XRegistry::registryInit();
					TIfHasMemoryInit<T>::callMemoryInit();

					XRegistryThread::criticalEnter();
					if(XRegistryThread::registerKey(
						   TXMemoryPoolUnifiedImplementThread<sizeof(T)>::registryKey(),
						   &TXMemoryPoolUnifiedImplementThread<sizeof(T)>::registryLink)) {

						XRegistryThread::setValue(
							TXMemoryPoolUnifiedImplementThread<sizeof(T)>::registryLink,
							XRegistryLevel::Static,
							new TXMemoryPoolUnifiedImplementThread<sizeof(T)>(),
							TXMemoryPoolUnifiedImplementThread<sizeof(T)>::resourceDelete
						);
					};
					XRegistryThread::criticalLeave();
				};

		};

	};
};

#endif

#endif

