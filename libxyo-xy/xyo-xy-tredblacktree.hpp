//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TREDBLACKTREE_HPP
#define XYO_XY_TREDBLACKTREE_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TXMEMORY_HPP
#include "xyo-xy-txmemory.hpp"
#endif

#ifndef XYO_XY_TYREDBLACKTREENODE_HPP
#include "xyo-xy-tyredblacktreenode.hpp"
#endif

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

#ifndef XYO_XY_TIFHASOBJECTMEMORYLINK_HPP
#include "xyo-xy-tifhasobjectmemorylink.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TKey, typename TValue, template <typename U> class TMemory=TXMemory>
		class TRedBlackTree :
			public Object,
			public TXRedBlackTree<TYRedBlackTreeNode<TKey, TValue>,TMemory<TYRedBlackTreeNode<TKey, TValue> > > {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TRedBlackTree);
			public:

				typedef TYRedBlackTreeNode<TKey, TValue> Node;
				typedef TMemory<Node> NodeMemory;


				inline TRedBlackTree() {
				};

				inline ~TRedBlackTree() {
				};

				inline void set(const TKey &key, const TValue &value) {
					Node *node=find(key);
					if (node) {
						node->value=value;
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					TIfHasObjectMemoryLink<TValue>::callObjectMemoryLink(&node->value,this);
					node->key = key;
					node->value = value;
					insertNode(node);
				};

				inline bool get(const TKey &key, TValue &value) {
					Node *x=find(key);
					if (x) {
						value = x->value;
						return true;
					};
					return false;
				};

				inline TValue getValue(const TKey &key, TValue value) {
					Node *x=find(key);
					if (x) {
						return x->value;
					};
					return value;
				};

				inline void activeDestructor() {
					empty();
				};

				inline static void memoryInit() {
					NodeMemory::memoryInit();
				};

		};

	};
};


#endif

