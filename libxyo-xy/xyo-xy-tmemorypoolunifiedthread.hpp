//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYPOOLUNIFIEDTHREAD_HPP
#define XYO_XY_TMEMORYPOOLUNIFIEDTHREAD_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-txmemorypoolunifiedthread.hpp"
#endif

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-tmemoryobjectpoolunifiedthread.hpp"
#endif

#ifndef XYO_XY_TISDERIVEDFROM_HPP
#include "xyo-xy-tisderivedfrom.hpp"
#endif


namespace XYO {
	namespace XY {

		template<typename T,bool isObject>
		class TMemoryPoolUnifiedThreadImplementation: public TXMemoryPoolUnifiedThread<T> {};

		template<typename T>
		class TMemoryPoolUnifiedThreadImplementation<T,true>: public TMemoryObjectPoolUnifiedThread<T> {};

		template<typename T>
		class TMemoryPoolUnifiedThread: public TMemoryPoolUnifiedThreadImplementation<T, TIsDerivedFrom<T,Object>::value> {};

	};
};

#endif

