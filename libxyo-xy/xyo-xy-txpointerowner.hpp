//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXPOINTEROWNER_HPP
#define XYO_XY_TXPOINTEROWNER_HPP

#ifndef XYO_XY_TXMEMORY_HPP
#include "xyo-xy-txmemory.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TXPointerOwner {
			protected:

				T *object;

			public:

				inline TXPointerOwner() {
					object=nullptr;
				};

				inline ~TXPointerOwner() {
					if (object) {
						TXMemory<T>::memoryDelete(object);
					};
				};

				inline TXPointerOwner(T *value) {
					object = value;
				};

				inline TXPointerOwner(const TXPointerOwner &value) {
					object = value.object;
					(const_cast<TXPointerOwner &>(value)).object = nullptr;
				};

				inline TXPointerOwner(TXPointerOwner &&value) {
					object = value.object;
					value.object = nullptr;
				};

				inline TXPointerOwner &operator=(const T *value) {
					if(object) {
						TXMemory<T>::memoryDelete(object);
					};
					object = const_cast<T *>(value);
					return *this;
				};

				inline TXPointerOwner &operator=(const TXPointerOwner &value) {
					if(object) {
						TXMemory<T>::memoryDelete(object);
					};
					object = value.object;
					(const_cast<TXPointerOwner &>(value)).object = nullptr;
					return *this;
				};

				inline TXPointerOwner &operator=(TXPointerOwner &&value) {
					if(object) {
						TXMemory<T>::memoryDelete(object);
					};
					object = value.object;
					value.object = nullptr;
					return *this;
				};

				inline bool operator==(const T *value) const {
					return object == value;
				};

				inline bool operator!=(const T *value) const {
					return object != value;
				};

				inline operator bool() {
					return (object != nullptr);
				};

				inline T *operator->() {
					return object;
				};

				inline T &operator*() {
					return *object;
				};

				inline operator T *() {
					return object;
				};

				inline operator T &() {
					return *object;
				};

				inline T *value() {
					return object;
				};

				inline T &reference() {
					return *object;
				};

				inline bool isValid() {
					return object != nullptr;
				};

				inline void newObject() {
					if(object) {
						TXMemory<T>::memoryDelete(object);
					};
					object = TXMemory<T>::memoryNew();
				};

				inline void deleteObject() {
					if (object) {
						TXMemory<T>::memoryDelete(object);
						object = nullptr;
					};
				};

				inline void setObject(T *value) {
					if(object) {
						TXMemory<T>::memoryDelete(object);
					};
					object = value;
				};

				inline void ownerTransfer() {
					object = nullptr;
				};

				inline T *getObject() {
					return object;
				};

				inline void activeDestructor() {
					if (object) {
						TXMemory<T>::memoryDelete(object);
						object = nullptr;
					};
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

		};
	};
};

#endif

