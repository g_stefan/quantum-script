//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYSYSTEM_HPP
#define XYO_XY_TXMEMORYSYSTEM_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TIFHASMEMORYINIT_HPP
#include "xyo-xy-tifhasmemoryinit.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TXMemorySystem {
			public:
				inline static T *memoryNew() {
					T *retV=new T();
					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline static void memoryDelete(T *this_) {
					TIfHasActiveDestructor<T>::callActiveDestructor(this_);
					delete this_;
				};

				inline static void memoryInit() {
					TIfHasMemoryInit<T>::callMemoryInit();
				};
		};

	};
};

#endif

