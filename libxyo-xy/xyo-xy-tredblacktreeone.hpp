//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TREDBLACKTREEONE_HPP
#define XYO_XY_TREDBLACKTREEONE_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TXMEMORY_HPP
#include "xyo-xy-txmemory.hpp"
#endif

#ifndef XYO_XY_TYREDBLACKTREENODEONE_HPP
#include "xyo-xy-tyredblacktreenodeone.hpp"
#endif

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

#ifndef XYO_XY_TIFHASOBJECTMEMORYLINK_HPP
#include "xyo-xy-tifhasobjectmemorylink.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TKey, template <typename U> class TMemory=TXMemory>
		class TRedBlackTreeOne :
			public Object,
			public TXRedBlackTree<TYRedBlackTreeNodeOne<TKey>,TMemory<TYRedBlackTreeNodeOne<TKey> > > {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TRedBlackTreeOne);
			public:

				typedef TYRedBlackTreeNodeOne<TKey> Node;
				typedef TMemory<Node> NodeMemory;

				inline TRedBlackTreeOne() {
				};

				inline ~TRedBlackTreeOne() {
				};

				inline void set(const TKey &key) {
					Node *node=find(key);
					if (node) {
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					node->key = key;
					insertNode(node);
				};

				inline void activeDestructor() {
					empty();
				};

				inline static void memoryInit() {
					NodeMemory::memoryInit();
				};

		};

	};
};


#endif

