//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TCOMPARATOR_X_CHAR_POINTER_HPP
#define XYO_XY_TCOMPARATOR_X_CHAR_POINTER_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

namespace XYO {
	namespace XY {

		template<>
		class TComparator<const char *> {
			public:

				inline static bool isEqualTo(const char *a,const char *b) {
					return (strcmp(a,b)==0);
				};

				inline static bool isNotEqualTo(const char *a,const char *b) {
					return (strcmp(a,b)!=0);
				};

				inline static bool isLessThan(const char *a,const char *b) {
					return (strcmp(a,b)<0);
				};

				inline static bool isGreaterThan(const char *a,const char *b) {
					return (strcmp(a,b)>0);
				};

				inline static bool isLessThanOrEqualTo(const char *a,const char *b) {
					return (strcmp(a,b)<=0);
				};

				inline static bool isGreaterThanOrEqualTo(const char *a,const char *b) {
					return (strcmp(a,b)>=0);
				};

				inline static int compare(const char *a,const char *b) {
					return strcmp(a,b);
				};

		};

		template<>
		class TComparator<char *>:public TComparator<const char *> {};

	};
};

#endif
