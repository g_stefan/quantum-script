//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_UNIX

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

#include "xyo-xy-xregistrythread.hpp"
#include "xyo-xy-xthread.hpp"

#ifdef XYO_SINGLE_THREAD

namespace XYO {
	namespace XY {
		namespace XThread {

			sleepOneMillisecond() {
				usleep(1);
			};


		};
	};
};

#else


namespace XYO {
	namespace XY {
		namespace XThread {

			typedef struct SLink Link;
			struct SLink {
				Procedure procedure;
				void *this_;

				pthread_t thread;
			};

			static void *procedureRun(Link *link) {
				XRegistryThread::threadBegin();
				(*link->procedure)(link->this_);
				XRegistryThread::threadEnd();
				delete link;
				return nullptr;
			};

			static void *procedureRunDirect(Link *link) {
				(*link->procedure)(link->this_);
				delete link;
				return nullptr;
			};

			bool start(Procedure procedure, void *this_) {
				Link *link;
				link=new Link();
				link->procedure=procedure;
				link->this_=this_;

				if(pthread_create(&link->thread, nullptr, (void *( *)(void *))procedureRun, link)==0) {
					return true;
				};

				delete link;
				return false;
			};

			bool startDirect(Procedure procedure, void *this_) {
				Link *link;
				link=new Link();
				link->procedure=procedure;
				link->this_=this_;

				if(pthread_create(&link->thread, nullptr, (void *( *)(void *))procedureRunDirect, link)==0) {
					return true;
				};

				delete link;
				return false;
			};


			typedef struct SLinkNotify LinkNotify;
			struct SLinkNotify {
				Procedure procedure;
				void *this_;

				Procedure procedureNew;
				void *thisNew_;

				Procedure procedureDelete;
				void *thisDelete_;

				pthread_t thread;
			};

			static void *procedureRunWithNotify(LinkNotify *link) {
				XRegistryThread::threadBegin();
				(*link->procedureNew)(link->thisNew_);
				(*link->procedure)(link->this_);
				(*link->procedureDelete)(link->thisDelete_);
				XRegistryThread::threadEnd();
				delete link;
				return nullptr;
			};

			bool startWithNotify(
				Procedure procedure,void *this_,
				Procedure procedureNew,void *thisNew_,
				Procedure procedureDelete,void *thisDelete_
			) {

				LinkNotify *link;
				link=new LinkNotify();
				link->procedure=procedure;
				link->this_=this_;

				link->procedureNew=procedureNew;
				link->thisNew_=thisNew_;

				link->procedureDelete=procedureDelete;
				link->thisDelete_=thisDelete_;

				if(pthread_create(&link->thread, nullptr, (void *( *)(void *))procedureRunWithNotify, link)==0) {
					return true;
				};
				delete link;
				return false;

			};

			void sleepOneMillisecond() {
				usleep(1);
			};


		};
	};
};

#endif

#endif

