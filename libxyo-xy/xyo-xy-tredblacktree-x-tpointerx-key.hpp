//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TREDBLACKTREE_X_TPOINTERX_KEY_HPP
#define XYO_XY_TREDBLACKTREE_X_TPOINTERX_KEY_HPP

#ifndef XYO_XY_TREDBLACKTREE_HPP
#include "xyo-xy-tredblacktree.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TKey_, typename TValue, template <typename U> class TMemory>
		class TRedBlackTree<TPointerX<TKey_>, TValue, TMemory> :
			public Object,
			public TXRedBlackTree<TYRedBlackTreeNode<TPointerX<TKey_>, TValue>,TMemory<TYRedBlackTreeNode<TPointerX<TKey_>, TValue> > > {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TRedBlackTree);
			public:
				typedef TPointerX<TKey_> TKey;
				typedef TYRedBlackTreeNode<TKey, TValue> Node;
				typedef TMemory<Node> NodeMemory;

				inline TRedBlackTree() {
				};

				inline ~TRedBlackTree() {
				};

				inline Node *find(const TKey_ &key) {
					Node *x;
					int compare;
					for(x=root; x;) {
						compare = TComparator<TKey_>::compare(key, x->key);
						if (compare == 0) {
							return x;
						};
						if (compare < 0) {
							x = x->left;
						} else {
							x = x->right;
						};
					};
					return x;
				};

				inline bool remove(const TKey_ &key) {
					Node *x = find(key);
					if (x) {
						extractNode(x);
						NodeMemory::memoryDelete(x);
						return true;
					};
					return false;
				};

				inline void set(const TKey &key, const TValue &value) {
					Node *node=find(key);
					if (node) {
						node->value=value;
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					TIfHasObjectMemoryLink<TValue>::callObjectMemoryLink(&node->value,this);
					node->key = key;
					node->value = value;
					insertNode(node);
				};

				inline void set(TKey_ *key, const TValue &value) {
					Node *node=find(*key);
					if (node) {
						node->value=value;
						return;
					};
					node = NodeMemory::memoryNew();
					TIfHasObjectMemoryLink<TKey>::callObjectMemoryLink(&node->key,this);
					TIfHasObjectMemoryLink<TValue>::callObjectMemoryLink(&node->value,this);
					node->key.setObjectX(key);
					node->value = value;
					insertNode(node);
				};

				inline bool get(const TKey &key, TValue &value) {
					Node *x=find(key);
					if (x) {
						value = x->value;
						return true;
					};
					return false;
				};

				inline bool get(const TKey_ &key, TValue &value) {
					Node *x=find(key);
					if (x) {
						value = x->value;
						return true;
					};
					return false;
				};

				inline TValue getValue(const TKey &key, TValue value) {
					Node *x=find(key);
					if (x) {
						return x->value;
					};
					return value;
				};

				inline TValue getValue(const TKey_ &key, TValue value) {
					Node *x=find(key);
					if (x) {
						return x->value;
					};
					return value;
				};

				inline void activeDestructor() {
					empty();
				};

				inline static void memoryInit() {
					NodeMemory::memoryInit();
				};

		};

	};
};


#endif

