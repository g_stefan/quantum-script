//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TDYNAMICARRAY_HPP
#define XYO_XY_TDYNAMICARRAY_HPP

#ifndef XYO_XY_OBJECT_HPP
#include "xyo-xy-object.hpp"
#endif

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TIFHASOBJECTMEMORYLINK_HPP
#include "xyo-xy-tifhasobjectmemorylink.hpp"
#endif

#ifndef XYO_XY_TYDYNAMICARRAYNODE_HPP
#include "xyo-xy-tydynamicarraynode.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T, dword dataSize2Pow, template <typename U> class TMemory=TXMemory>
		class TDynamicArray :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TDynamicArray);
			protected:
				static const size_t dataSize = (1 << dataSize2Pow);
				static const size_t dataMask = ((1 << dataSize2Pow) - 1);

				size_t indexSize;
				size_t itemSize;
				size_t length_;

				TYDynamicArrayNode<T, dataSize> **value;
			public:

				typedef TYDynamicArrayNode<T, dataSize> Node;
				typedef TMemory<TYDynamicArrayNode<T, dataSize> > NodeMemory;

				inline TDynamicArray(int indexSize_ = 1) {
					size_t k;
					indexSize = indexSize_;
					itemSize = indexSize*dataSize;
					length_ = 0;
					value = new Node *[indexSize];
					for (k = 0; k < indexSize; ++k) {
						value[k] = NodeMemory::memoryNew();
						TIfHasObjectMemoryLink<T>::callObjectMemoryLinkArray(&(value[k]->value[0]),this,dataSize);
					};
				};

				inline ~TDynamicArray() {
					size_t k;
					for (k = 0; k < indexSize; ++k) {
						NodeMemory::memoryDelete(value[k]);
					};
					delete[] value;
				};

				inline void empty() {
					int indexHigh;
					int indexScanHigh;
					if(length_==0) {
						return;
					};
					indexHigh=length_>>dataSize2Pow;
					for(indexScanHigh=0; indexScanHigh<indexHigh; ++indexScanHigh) {
						value[indexScanHigh]->empty(dataSize);
					};
					value[indexHigh]->empty(length_&dataMask);
					length_=0;
				};

				inline void growWith(int count_) {
					size_t k;
					size_t newIndexSize = indexSize + count_;
					TYDynamicArrayNode<T, dataSize> **newValue;
					TYDynamicArrayNode<T, dataSize> **oldValue;
					newValue = new Node *[newIndexSize];
					memcpy(newValue, value, sizeof (Node *) * indexSize);
					for (k = indexSize; k < newIndexSize; ++k) {
						newValue[k] = NodeMemory::memoryNew();
						TIfHasObjectMemoryLink<T>::callObjectMemoryLinkArray(&(newValue[k]->value[0]),this,dataSize);
					};
					indexSize = newIndexSize;
					itemSize = indexSize*dataSize;
					oldValue = value;
					value = newValue;
					delete[] oldValue;
				};

				inline T &operator [](int index) {
					if ((size_t)index >= length_) {
						length_ = (size_t)index + 1;
						if ((size_t)index < itemSize) {
						} else {
							growWith(((size_t)index >> dataSize2Pow) - indexSize + 1);
						};
					};
					return value[(size_t)index >> dataSize2Pow]->value[(size_t)index & dataMask];
				};

				inline bool get(size_t index, T &object) {
					if (index >= length_) {
						return false;
					};
					object = value[index >> dataSize2Pow]->value[index & dataMask];
					return true;
				};

				inline T &getX(size_t index,T &default_) {
					if (index >= length_) {
						length_ = index + 1;
						if (index < itemSize) {
						} else {
							growWith((index >> dataSize2Pow) - indexSize + 1);
						};
						T &retV=value[index >> dataSize2Pow]->value[index & dataMask];
						retV=default_;
						return retV;
					};
					return value[index >> dataSize2Pow]->value[index & dataMask];
				};

				inline void set(size_t index, T object) {
					setX(index, object);
				};

				inline void setX(size_t index, T &object) {
					if (index >= length_) {
						length_ = index + 1;
						if (index >= itemSize) {
							growWith((index >> dataSize2Pow) - indexSize + 1);
						};
					};
					value[index >> dataSize2Pow]->value[index & dataMask] = object;
				};

				inline size_t length() {
					return length_;
				};

				inline size_t arraySize() {
					return itemSize;
				};

				inline void setLength(size_t newLength) {
					if (newLength <= length_) {
						length_ = newLength;
						return;
					};
					length_ = newLength;
					if (newLength < itemSize) {
					} else {
						growWith((newLength >> dataSize2Pow) - indexSize + 1);
					};
				};

				inline void clearLength() {
					length_ = 0;
				};

				inline void activeDestructor() {
					empty();
				};

				inline static void memoryInit() {
					NodeMemory::memoryInit();
				};

				inline bool shift(T &out) {
					size_t indexHigh;
					size_t indexLow;
					size_t indexScanHigh;
					size_t indexScanLow;

					if(length_==0) {
						return false;
					};
					out=value[0]->value[0];
					--length_;
					if(length_>0) {
						indexHigh=length_>>dataSize2Pow;
						indexLow=length_ & dataMask;
						for(indexScanHigh=0; indexScanHigh<indexHigh; ++indexScanHigh) {
							for(indexScanLow=0; indexScanLow<dataSize-1; ++indexScanLow) {
								value[indexScanHigh]->value[indexScanLow]=value[indexScanHigh]->value[indexScanLow+1];
							};
							value[indexScanHigh]->value[dataSize-1]=value[indexScanHigh+1]->value[0];
						};
						for(indexScanLow=0; indexScanLow<dataSize-1; ++indexScanLow) {
							value[indexHigh]->value[indexScanLow]=value[indexHigh]->value[indexScanLow+1];
						};
					};
					return true;
				};

				inline void shiftEmpty() {
					size_t indexHigh;
					size_t indexLow;
					size_t indexScanHigh;
					size_t indexScanLow;

					if(length_==0) {
						return;
					};
					--length_;
					if(length_>0) {
						indexHigh=length_>>dataSize2Pow;
						indexLow=length_ & dataMask;
						for(indexScanHigh=0; indexScanHigh<indexHigh; ++indexScanHigh) {
							for(indexScanLow=0; indexScanLow<dataSize-1; ++indexScanLow) {
								value[indexScanHigh]->value[indexScanLow]=value[indexScanHigh]->value[indexScanLow+1];
							};
							value[indexScanHigh]->value[dataSize-1]=value[indexScanHigh+1]->value[0];
						};
						for(indexScanLow=0; indexScanLow<dataSize-1; ++indexScanLow) {
							value[indexHigh]->value[indexScanLow]=value[indexHigh]->value[indexScanLow+1];
						};
					};
				};


				inline bool remove(size_t index) {
					size_t indexHighX;
					size_t indexLowX;
					size_t indexHigh;
					size_t indexLow;
					size_t indexScanHigh;
					size_t indexScanLow;

					if(length_==0) {
						return false;
					};
					--length_;
					if(length_>0) {
						indexHighX=index >>dataSize2Pow;
						indexLowX= index & dataMask;
						indexHigh=length_>>dataSize2Pow;
						indexLow=length_ & dataMask;
						for(indexScanHigh=indexHighX; indexScanHigh<indexHigh; ++indexScanHigh) {
							for(indexScanLow=indexLowX; indexScanLow<dataSize-1; ++indexScanLow) {
								value[indexScanHigh]->value[indexScanLow]=value[indexScanHigh]->value[indexScanLow+1];
							};
							value[indexScanHigh]->value[dataSize-1]=value[indexScanHigh+1]->value[0];
							for(indexScanLow=0; indexScanLow<indexLowX; ++indexScanLow) {
								value[indexScanHigh]->value[indexScanLow]=value[indexScanHigh]->value[indexScanLow+1];
							};
						};
						for(indexScanLow=indexLowX; indexScanLow<dataSize-1; ++indexScanLow) {
							value[indexHigh]->value[indexScanLow]=value[indexHigh]->value[indexScanLow+1];
						};
					};
					return true;
				};


		};

	};
};

#endif
