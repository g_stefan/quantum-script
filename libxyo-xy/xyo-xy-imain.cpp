//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifdef XYO_OS_TYPE_WIN

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "xyo-xy-imain.hpp"

namespace XYO {
	namespace XY {

		static void mainArgsFilter__(char *cmdX) {
			char *result;
			char *scan;
			char *check;
			result=cmdX;
			scan=cmdX;
			while(*scan!='\0') {
				if(*scan=='\\') {
					check=scan+1;
					while(*check!='\0') {
						if(*check=='\\') {
							++check;
							continue;
						};
						break;
					};
					if(*check=='"') {

						while(*scan!='\0') {
							if(*scan=='\\') {
								++scan;
								*result=*scan;
								++result;
								++scan;
								continue;
							};
							break;
						};
						continue;
					};
					while(*scan!='\0') {
						if(*scan=='\\') {
							*result=*scan;
							++result;
							++scan;
							continue;
						};
						break;
					};
					continue;
				};
				if(*scan=='"') {
					++scan;
					continue;
				};
				*result=*scan;
				++result;
				++scan;
			};
			*result='\0';
		};

		static void mainArgsParse__(bool commit,char *cmdLine,int &cmdN,char ** &cmdS) {
			char *cmdLineScan;
			char *cmdLastLineScan;
			int cmdSize;

			// exe name is first
			cmdN=1;

			cmdLineScan=cmdLine;
			// ignore first spaces
			while(*cmdLineScan!='\0') {
				if(*cmdLineScan==' '||*cmdLineScan=='\t') {
					while(*cmdLineScan==' '||*cmdLineScan=='\t') {
						++cmdLineScan;
						if(*cmdLineScan=='\0') {
							break;
						};
					};
					if(*cmdLineScan=='\0') {
						break;
					};
					continue;
				};
				break;
			};
			//
			cmdLastLineScan=cmdLineScan;
			cmdSize=0;
			while(*cmdLineScan!='\0') {
				if(*cmdLineScan==' '||*cmdLineScan=='\t') {
					if(cmdSize>0) {
						if(commit) {
							cmdS[cmdN]=new char[cmdSize+1];
							memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
							cmdS[cmdN][cmdSize]='\0';
						};
						++cmdN;
					};
					while(*cmdLineScan==' '||*cmdLineScan=='\t') {
						++cmdLineScan;
						if(*cmdLineScan=='\0') {
							break;
						};
					};
					cmdLastLineScan=cmdLineScan;
					cmdSize=0;
					if(*cmdLineScan=='\0') {
						break;
					};
					continue;
				};
				if(*cmdLineScan=='\\') {
					++cmdSize;
					++cmdLineScan;
					if(*cmdLineScan!='\0') {
						++cmdSize;
						++cmdLineScan;
					};
					continue;
				};
				if(*cmdLineScan=='\"') {
					if(cmdSize==0) {
						cmdLastLineScan=cmdLineScan;
						++cmdSize;
						++cmdLineScan;
						while(*cmdLineScan!='\0') {
							if(*cmdLineScan=='\\') {
								++cmdSize;
								++cmdLineScan;
								if(*cmdLineScan!='\0') {
									++cmdSize;
									++cmdLineScan;
								};
								continue;
							};
							if(*cmdLineScan=='\"') {
								++cmdSize;
								++cmdLineScan;
								break;
							};
							++cmdSize;
							++cmdLineScan;
						};

						if(commit) {
							cmdS[cmdN]=new char[cmdSize+1];
							memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
							cmdS[cmdN][cmdSize]='\0';
						};
						++cmdN;

						cmdLastLineScan=cmdLineScan;
						cmdSize=0;
						continue;
					} else {
						++cmdSize;
						++cmdLineScan;
						while(*cmdLineScan!='\0') {
							if(*cmdLineScan=='\\') {
								++cmdSize;
								++cmdLineScan;
								if(*cmdLineScan!='\0') {
									++cmdSize;
									++cmdLineScan;
								};
								continue;
							};
							if(*cmdLineScan=='\"') {
								++cmdSize;
								++cmdLineScan;
								break;
							};
							++cmdSize;
							++cmdLineScan;
						};
						if(*cmdLineScan=='\0') {
							if(commit) {
								cmdS[cmdN]=new char[cmdSize+1];
								memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
								cmdS[cmdN][cmdSize]='\0';
							};
							++cmdN;

							cmdLastLineScan=cmdLineScan;
							cmdSize=0;

							break;
						};
						continue;
					};
				};
				++cmdSize;
				++cmdLineScan;
				if(*cmdLineScan=='\0') {
					if(commit) {
						cmdS[cmdN]=new char[cmdSize+1];
						memcpy(cmdS[cmdN],cmdLastLineScan,cmdSize);
						cmdS[cmdN][cmdSize]='\0';
					};
					++cmdN;
					break;
				};
			};
		};

		void mainArgsSet__(int &cmdN,char ** &cmdS) {
			char exeName[MAX_PATH];
			char *cmdLine;
			int cmdNX;
			int cmdNZ;
			int cmdSize;
			int k;

			GetModuleFileName(nullptr,exeName,MAX_PATH);
			cmdLine=GetCommandLineA();
			mainArgsParse__(false,cmdLine,cmdN,cmdS);
			//
			cmdS=new char *[cmdN];
			cmdSize=strlen(exeName);
			cmdS[0]=new char[cmdSize+1];
			memcpy(cmdS[0],exeName,cmdSize);
			cmdS[0][cmdSize]='\0';
			//
			mainArgsParse__(true,cmdLine,cmdN,cmdS);

			for(k=0; k<cmdN; ++k) {
				mainArgsFilter__(cmdS[k]);
			};

			if(cmdN>1) {
				if(strcmp(cmdS[0],cmdS[1])==0) {
					delete[] cmdS[0];
					--cmdN;
					memcpy(cmdS,cmdS+1,cmdN*sizeof(char *));
					cmdS[cmdN]=nullptr;
				} else {
					char *fullPath1;
					char *fullPath2;
					fullPath1=_fullpath(nullptr,cmdS[0],_MAX_PATH);
					fullPath2=_fullpath(nullptr,cmdS[1],_MAX_PATH);
					if(fullPath1) {
						if(fullPath2) {
							if(strcmpi(fullPath1,fullPath2)==0) {
								delete[] cmdS[0];
								--cmdN;
								memcpy(cmdS,cmdS+1,cmdN*sizeof(char *));
								cmdS[cmdN]=nullptr;
							};
							free(fullPath2);
						};
						free(fullPath1);
					};
				};
			};
		};

		void mainArgsDelete__(int cmdN,char **cmdS) {
			int k;
			for(k=0; k<cmdN; ++k) {
				delete[] cmdS[k];
			};
			delete[] cmdS;
		};

	};
};

#endif

