//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TSTACK2_X_TPOINTERX_HPP
#define XYO_XY_TSTACK2_X_TPOINTERX_HPP

#ifndef XYO_XY_TSTACK2_HPP
#include "xyo-xy-tstack2.hpp"
#endif

namespace XYO {
	namespace XY {

		template <typename T_, template <typename U> class TMemory>
		class TStack2<TPointerX<T_>, TMemory> :
			public Object {
				XYO_XY_DISALLOW_COPY_ASSIGN_MOVE(TStack2);

				typedef TPointerX<T_> T;
			protected:
				TYList2<T> *head_;
				TYList2<T> *tail_;
			public:

				typedef TYList2<T> Node;
				typedef TMemory<TYList2<T> > NodeMemory;

				inline TStack2() {
					head_ = nullptr;
					tail_ = nullptr;
				};

				inline ~TStack2() {
					empty();
				};

				inline void empty() {
					TYList2<T> *element;
					while (head_) {
						if (head_->next) {
							head_->next->back = nullptr;
						};
						if (head_ == tail_) {
							TMemory<TYList2<T> >::memoryDelete(head_);
							break;
						} else {
							element = head_;
							head_ = head_->next;
							TMemory<TYList2<T> >::memoryDelete(element);
						};
					};
					head_ = nullptr;
					tail_ = nullptr;
				};

				inline void push(T value) {
					pushX(value);
				};

				inline void pushX(T &value) {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->back = nullptr;
					element->next = head_;
					element->value = value;
					if (head_) {
						head_->back = element;
					} else {
						tail_ = element;
					};

					head_ = element;
				};

				inline void pushEmpty() {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->back = nullptr;
					element->next = head_;
					if (head_) {
						head_->back = element;
					} else {
						tail_ = element;
					};

					head_ = element;
				};

				inline bool pop(T &out) {
					TYList2<T> *element(head_);
					if (head_) {
						if (head_->next) {
							head_->next->back = nullptr;
						};
						if (head_ == tail_) {
							tail_ = nullptr;
						};
						head_ = head_->next;
						out = element->value;
						TMemory<TYList2<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool popEmpty() {
					TYList2<T> *element(head_);
					if (head_) {
						if (head_->next) {
							head_->next->back = nullptr;
						};
						if (head_ == tail_) {
							tail_ = nullptr;
						};
						head_ = head_->next;
						TMemory<TYList2<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline void pushToTail(T value) {
					pushToTailX(value);
				};

				inline void pushToTailX(T &value) {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->next = nullptr;
					element->back = tail_;
					element->value = value;
					if (tail_) {
						tail_->next = element;
					} else {
						head_ = element;
					};
					tail_ = element;
				};

				inline void pushToTailEmpty() {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->next = nullptr;
					element->back = tail_;
					if (tail_) {
						tail_->next = element;
					} else {
						head_ = element;
					};
					tail_ = element;
				};

				inline bool popFromTail(T &out) {
					TYList2<T> *element = tail_;
					if (tail_) {
						if (tail_->back) {
							tail_->back->next = nullptr;
						};
						if (tail_ == head_) {
							head_ = nullptr;
						};
						tail_ = tail_->back;
						out = element->value;
						TMemory<TYList2<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool popFromTailEmpty() {
					TYList2<T> *element = tail_;
					if (tail_) {
						if (tail_->back) {
							tail_->back->next = nullptr;
						};
						if (tail_ == head_) {
							head_ = nullptr;
						};
						tail_ = tail_->back;
						TMemory<TYList2<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline bool peek(T &out) {
					if (head_) {
						out = head_->value;
						return true;
					};
					return false;
				};

				inline bool peekFromTail(T &out) {
					if (head_) {
						out = head_->value;
						return true;
					};
					return false;
				};

				inline bool isEmpty() {
					return head_ == nullptr;
				};

				inline operator TYList2<T> *() {
					return head_;
				};

				inline TYList2<T> *head() {
					return head_;
				};

				inline TYList2<T> *tail() {
					return tail_;
				};

				inline void extractNode(TYList2<T> *value) {
					if(value==head_) {
						if(head_->next) {
							head_->next->back=nullptr;
						};
						if(head_==tail_) {
							tail_=nullptr;
						};
						head_=head_->next;
						if(head_) {
							head_->back=nullptr;
						};
						value->next=nullptr;
						value->back=nullptr;
						value->value.memoryLink(nullptr);
						return;
					};
					if(value==tail_) {
						if(tail_->back) {
							tail_->back->next=nullptr;
						};
						tail_=tail_->back;
						if(tail_) {
							tail_->next=nullptr;
						};
						value->next=nullptr;
						value->back=nullptr;
						value->value.memoryLink(nullptr);
						return;
					};
					if(value->next) {
						value->next->back=value->back;
					};
					if(value->back) {
						value->back->next=value->next;
					};
					value->next=nullptr;
					value->back=nullptr;
					value->value.memoryLink(nullptr);
				};

				inline void pushNode(TYList2<T> *element) {
					element->back = nullptr;
					element->next = head_;
					if (head_) {
						head_->back = element;
					} else {
						tail_ = element;
					};
					head_ = element;
					element->value.memoryLink(this);
				};

				inline TYList2<T> *pushN(T value) {
					return pushXN(value);
				};

				inline TYList2<T> *pushXN(T &value) {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->back = nullptr;
					element->next = head_;
					element->value = value;
					if (head_) {
						head_->back = element;
					} else {
						tail_ = element;
					};
					head_ = element;
					return element;
				};


				inline void extractList(TYList2<T> *xHead,TYList2<T> *xTail) {
					if(xHead==head_) {
						head_=xTail->next;
					};
					if(xTail==tail_) {
						tail_=xHead->back;
					};
					if(xHead->back) {
						xHead->back->next=xTail->next;
					};
					if(xTail->next) {
						xTail->next->back=xHead->back;
					};
					xHead->back=nullptr;
					xTail->next=nullptr;
					if(THasObjectMemoryLink<T>::value) {
						for(; xHead; xHead=xHead->next) {
							xHead->value.memoryLink(nullptr);
						};
					};
				};

				inline void setList(TYList2<T> *xHead,TYList2<T> *xTail) {
					empty();
					head_=xHead;
					tail_=xTail;
					if(THasObjectMemoryLink<T>::value) {
						for(; xHead; xHead=xHead->next) {
							xHead->value.memoryLink(this);
						};
					};
				};

				static inline TYList2<T> *nodeMemoryNew() {
					return TMemory<TYList2<T> >::memoryNew();
				};

				static inline void nodeMemoryDelete(TYList2<T> *node) {
					TMemory<TYList2<T> >::memoryDelete(node);
				};

				inline void activeDestructor() {
					empty();
				};

				//
				// Other
				//

				inline void pushX(const TPointerOwner<T_> &value) {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->back = nullptr;
					element->next = head_;
					element->value = value;
					if (head_) {
						head_->back = element;
					} else {
						tail_ = element;
					};

					head_ = element;
				};

				inline void pushX(TPointerOwner<T_> &&value) {
					TYList2<T> *element;
					element = TMemory<TYList2<T> >::memoryNew();
					element->value.memoryLink(this);
					element->back = nullptr;
					element->next = head_;
					element->value = value;
					if (head_) {
						head_->back = element;
					} else {
						tail_ = element;
					};

					head_ = element;
				};

				inline bool popFromTail(TPointerOwner<T_> &out) {
					TYList2<T> *element = tail_;
					if (tail_) {
						if (tail_->back) {
							tail_->back->next = nullptr;
						};
						if (tail_ == head_) {
							head_ = nullptr;
						};
						tail_ = tail_->back;
						out = element->value;
						TMemory<TYList2<T> >::memoryDelete(element);
						return true;
					};
					return false;
				};

				inline static void memoryInit() {
					TMemory<TYList2<T> >::memoryInit();
				};

		};


	};
};

#endif

