//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXREDBLACKTREE_HPP
#define XYO_XY_TXREDBLACKTREE_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

#ifndef XYO_XY_TXREDBLACKTREENODE_HPP
#include "xyo-xy-txredblacktreenode.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TNodeMemory>
		class TXRedBlackTree {
			public:

				TNode *root;

				inline TXRedBlackTree() {
					root=nullptr;
				};

				inline ~TXRedBlackTree() {
					nodeDestructor(root);
				};

				static inline void nodeDestructor(TNode *node) {
					if (node) {
						nodeDestructor(node->left);
						nodeDestructor(node->right);
						TNodeMemory::memoryDelete(node);
					};
				};

				static inline bool isBlack(TNode *node) {
					if (node) {
						return node->color == TNode::Black;
					};
					return true;
				};

				static inline bool isRed(TNode *node) {
					if (node) {
						return node->color == TNode::Red;
					};
					return false;
				};

				inline void leftRotate(TNode *x) {
					TNode *y;
					if (x) {
						y = x->right;
						if (y) {
							x->right = y->left;
							if (y->left) {
								y->left->parent = x;
							};
							y->parent = x->parent;
							if (!x->parent) {
								root = y;
							} else {
								if (x == x->parent->left) {
									x->parent->left = y;
								} else {
									x->parent->right = y;
								};
							};

							y->left = x;
							x->parent = y;
						};
					};
				};

				inline void rightRotate(TNode *x) {
					TNode *y;
					if (x) {
						y = x->left;
						if (y) {
							x->left = y->right;
							if (y->right) {
								y->right->parent = x;
							};
							y->parent = x->parent;
							if (!x->parent) {
								root = y;
							} else {
								if (x == x->parent->right) {
									x->parent->right = y;
								} else {
									x->parent->left = y;
								};
							};
							y->right = x;
							x->parent = y;
						};
					};
				};

				inline void insertNode(TNode *z) {
					TNode *y;
					TNode *x;
					y = nullptr;
					x = root;
					while (x) {
						y = x;
						if (TComparator<typename TNode::Key>::isLessThan(z->key,x->key)) {
							x = x->left;
						} else {
							x = x->right;
						};
					};
					z->parent = y;
					if (!y) {
						root = z;
					} else {
						if (TComparator<typename TNode::Key>::isLessThan(z->key,y->key)) {
							y->left = z;
						} else {
							y->right = z;
						};
					};
					z->left = nullptr;
					z->right = nullptr;
					z->color = TNode::Red;
					while (isRed(z->parent)) {
						if (!z->parent->parent) {
							break;
						};
						if (z->parent == z->parent->parent->left) {
							y = z->parent->parent->right;
							if (isRed(y)) {
								z->parent->color = TNode::Black;
								y->color = TNode::Black;
								z->parent->parent->color = TNode::Red;
								z = z->parent->parent;
							} else {
								if (z == z->parent->right) {
									z = z->parent;
									leftRotate(z);
								};
								z->parent->color = TNode::Black;
								z->parent->parent->color = TNode::Red;
								rightRotate(z->parent->parent);
							};
						} else {
							y = z->parent->parent->left;
							if (isRed(y)) {
								z->parent->color = TNode::Black;
								y->color = TNode::Black;
								z->parent->parent->color = TNode::Red;
								z = z->parent->parent;
							} else {
								if (z == z->parent->left) {
									z = z->parent;
									rightRotate(z);
								};
								z->parent->color = TNode::Black;
								z->parent->parent->color = TNode::Red;
								leftRotate(z->parent->parent);
							};
						};
					};

					root->color = TNode::Black;
				};

				inline void transplant(TNode *x, TNode *y) {
					if (!x->parent) {
						root = y;
					} else {
						if (x == x->parent->left) {
							x->parent->left = y;
						} else {
							x->parent->right = y;
						};
					};
					y->parent = x->parent;
				};

				inline void extractNode(TNode *z) {
					TNode *y;
					TNode *x;
					TNode *w;
					typename TNode::Color yOriginalColor;
					y = z;
					yOriginalColor = y->color;
					if (!z->left) {
						x = z->right;
						transplant(z, z->right);
					} else if (!z->right) {
						x = z->left;
						transplant(z, z->left);
					} else {
						y = z->right->minimum();
						yOriginalColor = y->color;
						x = y->right;
						if (y->parent == z) {
							if (x) {
								x->parent = y;
							};
						} else {
							transplant(y, y->right);
							y->right = z->right;
							y->right->parent = y;
						};
						transplant(z, y);
						y->left = z->left;
						y->left->parent = y;
						y->color = z->color;
					};
					if (yOriginalColor == TNode::Black) {
						while ((x != root) && (x->color == TNode::Black)) {
							if (x == x->parent->left) {
								w = x->parent->right;
								if (isRed(w)) {
									w->color = TNode::Black;
									x->parent->color = TNode::Red;
									leftRotate(x->parent);
									w = x->parent->right;
								};
								if (isBlack(w->left) &&
								    isBlack(w->right)) {
									w->color = TNode::Red;
									x = x->parent;
								} else if (isBlack(w->right)) {
									w->left->color = TNode::Black;
									w->color = TNode::Red;
									leftRotate(w);
									w = x->parent->right;
								};
								if (!x->parent) {
									w->color = TNode::Black;
								} else {
									w->color = x->parent->color;
								};
								if (!x->parent) {
									x->parent->color = TNode::Black;
								};
								if (w->right) {
									w->right->color = TNode::Black;
								};
								leftRotate(x->parent);
								x = root;
							} else {
								w = x->parent->left;
								if (isRed(w)) {
									w->color = TNode::Black;
									x->parent->color = TNode::Red;
									rightRotate(x->parent);
									w = x->parent->left;
								};
								if (isBlack(w->right) &&
								    isBlack(w->left)) {
									w->color = TNode::Red;
									x = x->parent;
								} else if (isBlack(w->left)) {
									w->right->color = TNode::Black;
									w->color = TNode::Red;
									rightRotate(w);
									w = x->parent->left;
								};
								if (x->parent) {
									w->color = x->parent->color;
								} else {
									w->color = TNode::Black;
								};
								if (x->parent) {
									x->parent->color = TNode::Black;
								};
								if (w->right) {
									w->right->color = TNode::Black;
								};
								rightRotate(x->parent);
								x = root;
							};
						};
						x->color = TNode::Black;
					};
				};

				inline TNode *find(const typename TNode::Key &key) {
					TNode *x;
					int compare;
					for(x=root; x;) {
						compare = TComparator<typename TNode::Key>::compare(key,x->key);
						if (compare == 0) {
							return x;
						};
						if (compare < 0) {
							x = x->left;
						} else {
							x = x->right;
						};
					};
					return x;
				};

				inline bool remove(const typename TNode::Key &key) {
					TNode *x = find(key);
					if (x) {
						extractNode(x);
						TNodeMemory::memoryDelete(x);
						return true;
					};
					return false;
				};

				inline void removeNode(TNode *node) {
					extractNode(node);
					TNodeMemory::memoryDelete(node);
				};

				inline TNode *begin() {
					if (root) {
						return root->minimum();
					};
					return nullptr;
				};

				inline TNode *end() {
					if (root) {
						return root->maximum();
					};
					return nullptr;
				};

				inline void empty() {
					nodeDestructor(root);
					root = nullptr;
				};

		};

	};
};


#endif

