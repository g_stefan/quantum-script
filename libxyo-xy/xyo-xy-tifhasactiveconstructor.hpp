//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#define XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TGETCLASSOFMEMBER_HPP
#include "xyo-xy-tgetclassofmember.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class THasActiveConstructor {
			protected:
				template <typename U, void (U::*)()> struct CheckMember;

				template <typename U>
				static char testMember(CheckMember<U, &U::activeConstructor > *);

				template <typename U>
				static int testMember(...);

				template<typename N>
				class THasMember {
					public:
						static const bool value = sizeof(testMember<N>(nullptr))==sizeof(char);
				};

				template <typename U>
				static char testBaseMember(decltype(TGetClassOfMember(&U::activeConstructor)) *);

				template <typename U>
				static int testBaseMember(...);

				template<typename N>
				class THasBaseMember {
					public:
						static const bool value = sizeof(testBaseMember<N>(nullptr))==sizeof(char);
				};

				template<typename N,bool hasBase>
				class TProcessBaseMember {
					public:
						static const bool value = false;
				};

				template<typename N>
				class TProcessBaseMember<N, true> {
					public:
						static const bool value = THasMember<decltype(TGetClassOfMember(&N::activeConstructor))>::value;
				};

			public:
				static const bool value = THasMember<T>::value|TProcessBaseMember<T,THasBaseMember<T>::value>::value;
		};


		template<typename T, bool hasActiveConstructor>
		class TIfHasActiveConstructorBase {
			public:

				static inline void callActiveConstructor(T *) {
				};

				static inline void callActiveConstructorArray(T *,size_t length) {
				};
		};

		template<typename T>
		class TIfHasActiveConstructorBase<T, true > {
			public:

				static inline void callActiveConstructor(T *this_) {
					this_->activeConstructor();
				};

				static inline void callActiveConstructorArray(T *this_,size_t length) {
					size_t k;
					for(k=0; k<length; ++k) {
						this_[k].activeConstructor();
					};
				};

		};

		template<typename T>
		class TIfHasActiveConstructor :
			public TIfHasActiveConstructorBase<T, THasActiveConstructor<T>::value> {
		};

	};
};

#endif

