//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TYDYNAMICARRAYNODE_HPP
#define XYO_XY_TYDYNAMICARRAYNODE_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVERESET_HPP
#include "xyo-xy-tifhasactivereset.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T, dword dataSize>
		class TYDynamicArrayNode {
			public:
				T value[dataSize];

				inline void activeConstructor() {
					TIfHasActiveConstructor<T>::callActiveConstructorArray(&value[0],dataSize);
				};

				inline void activeDestructor() {
					TIfHasActiveDestructor<T>::callActiveDestructorArray(&value[0],dataSize);
				};

				inline void activeReset() {
					TIfHasActiveReset<T>::callActiveResetArray(&value[0],dataSize);
				};

				inline void empty(int count_) {
					TIfHasActiveReset<T>::callActiveResetArray(&value[0],count_);
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

		};

	};
};

#endif

