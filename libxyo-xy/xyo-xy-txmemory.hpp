//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORY_HPP
#define XYO_XY_TXMEMORY_HPP

#ifndef XYO_XY_TXMEMORYTHREAD_HPP
#include "xyo-xy-txmemorythread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXMemory: public TXMemoryThread<T> {};

	};
};

#endif

