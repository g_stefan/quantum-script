//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XREGISTRYTHREAD_HPP
#define XYO_XY_XREGISTRYTHREAD_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_XREGISTRYLEVEL_HPP
#include "xyo-xy-xregistrylevel.hpp"
#endif

#ifndef XYO_XY_XREGISTRY_HPP
#include "xyo-xy-xregistry.hpp"
#endif

#ifndef XYO_SINGLE_THREAD
namespace XYO {
	namespace XY {
		namespace XRegistryThread {

			typedef void (*ResourceDelete)(void *);

			XYO_XY_EXPORT void processBegin();
			XYO_XY_EXPORT void processEnd();
			XYO_XY_EXPORT void threadBegin();
			XYO_XY_EXPORT void threadEnd();
			XYO_XY_EXPORT bool registerKey(const char *registryKey,size_t *registryLink);
			XYO_XY_EXPORT void setValue(size_t registryLink,size_t categoryLevel,void *resourceValue,ResourceDelete resourceDelete);
			XYO_XY_EXPORT void *getValue(size_t registryLink);
			XYO_XY_EXPORT void checkFastTrack(void *registryNode);
			XYO_XY_EXPORT void criticalEnter();
			XYO_XY_EXPORT void criticalLeave();

		};
	};
};
#endif


#endif
