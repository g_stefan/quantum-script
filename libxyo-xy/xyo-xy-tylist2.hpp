//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TYLIST2_HPP
#define XYO_XY_TYLIST2_HPP

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

namespace XYO {
	namespace XY {

		template <typename T>
		class TYList2 {
			public:
				TYList2<T> *back;
				TYList2<T> *next;
				T value;

				inline void activeConstructor() {
					TIfHasActiveConstructor<T>::callActiveConstructor(&value);
				};

				inline void activeDestructor() {
					TIfHasActiveDestructor<T>::callActiveDestructor(&value);
				};

				inline void activeReset() {
					TIfHasActiveReset<T>::callActiveReset(&value);
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

		};

	};
};

#endif
