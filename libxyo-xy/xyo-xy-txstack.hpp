//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXSTACK_HPP
#define XYO_XY_TXSTACK_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TXLIST1_HPP
#include "xyo-xy-txlist1.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TNodeMemory>
		class TXStack:
			public TXList1<TNode,TNodeMemory> {
		};

	};
};

#endif

