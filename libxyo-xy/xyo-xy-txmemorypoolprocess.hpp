//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLPROCESS_HPP
#define XYO_XY_TXMEMORYPOOLPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TXMemoryPoolProcess: public TXMemoryPoolUnifiedProcess<T> {};

	};
};

#endif

