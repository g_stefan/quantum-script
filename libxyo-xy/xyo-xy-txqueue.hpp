//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXQUEUE_HPP
#define XYO_XY_TXQUEUE_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TXLIST3_HPP
#include "xyo-xy-txlist3.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TNodeMemory>
		class TXQueue:
			public TXList3<TNode,TNodeMemory> {
			public:

				inline void push(TNode *node) {
					pushHead(node);
				};

				inline TNode *pop() {
					return popTail(node);
				};

				static inline TNode *popUnsafe() {
					return popTailUnsafe();
				};
		};

	};
};

#endif

