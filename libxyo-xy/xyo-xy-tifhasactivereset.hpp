//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TIFHASACTIVERESET_HPP
#define XYO_XY_TIFHASACTIVERESET_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class THasActiveReset {
			protected:
				template <typename U, void (U::*)()> struct CheckMember;

				template <typename U>
				static char testMember(CheckMember<U, &U::activeReset > *);

				template <typename U>
				static int testMember(...);

				template<typename N>
				class THasMember {
					public:
						static const bool value = sizeof(testMember<N>(nullptr))==sizeof(char);
				};

				template <typename U>
				static char testBaseMember(decltype(TGetClassOfMember(&U::activeReset)) *);

				template <typename U>
				static int testBaseMember(...);

				template<typename N>
				class THasBaseMember {
					public:
						static const bool value = sizeof(testBaseMember<N>(nullptr))==sizeof(char);
				};

				template<typename N,bool hasBase>
				class TProcessBaseMember {
					public:
						static const bool value = false;
				};

				template<typename N>
				class TProcessBaseMember<N, true> {
					public:
						static const bool value = THasMember<decltype(TGetClassOfMember(&N::activeReset))>::value;
				};

			public:
				static const bool value = THasMember<T>::value|TProcessBaseMember<T,THasBaseMember<T>::value>::value;
		};

		template<typename T, bool hasActiveReset>
		class TIfHasActiveResetBase {
			public:

				static inline void callActiveReset(T *this_) {
					TIfHasActiveDestructor<T>::callActiveDestructor(this_);
					TIfHasActiveConstructor<T>::callActiveConstructor(this_);
				};

				static inline void callActiveResetArray(T *this_,size_t length) {
					TIfHasActiveDestructor<T>::callActiveDestructorArray(this_,length);
					TIfHasActiveConstructor<T>::callActiveConstructorArray(this_,length);
				};
		};

		template<typename T>
		class TIfHasActiveResetBase<T, true > {
			public:

				static inline void callActiveReset(T *this_) {
					this_->activeReset();
				};

				static inline void callActiveResetArray(T *this_,size_t length) {
					size_t k;
					for(k=0; k<length; ++k) {
						this_[k].activeReset();
					};
				};

		};

		template<typename T>
		class TIfHasActiveReset :
			public TIfHasActiveResetBase<T, THasActiveReset<T>::value> {
		};

	};
};

#endif

