//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef LIBXYO_XY_COPYRIGHT_HPP
#define LIBXYO_XY_COPYRIGHT_HPP

#define LIBXYO_XY_COPYRIGHT            "Copyright (C) Grigore Stefan."
#define LIBXYO_XY_PUBLISHER            "Grigore Stefan"
#define LIBXYO_XY_COMPANY              LIBXYO_XY_PUBLISHER
#define LIBXYO_XY_CONTACT              "g_stefan@yahoo.com"
#define LIBXYO_XY_FULL_COPYRIGHT       LIBXYO_XY_COPYRIGHT " <" LIBXYO_XY_CONTACT ">"

#ifndef XYO_RC

#ifndef XYO_XY__EXPORT_HPP
#include "xyo-xy--export.hpp"
#endif

namespace Lib {
	namespace XYO {
		namespace XY {

			class Copyright {
				public:
					XYO_XY_EXPORT static const char *copyright();
					XYO_XY_EXPORT static const char *publisher();
					XYO_XY_EXPORT static const char *company();
					XYO_XY_EXPORT static const char *contact();
					XYO_XY_EXPORT static const char *fullCopyright();

			};

		};
	};
};

#endif
#endif
