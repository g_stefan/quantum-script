//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLPROCESS_HPP
#define XYO_XY_TMEMORYOBJECTPOOLPROCESS_HPP

#ifndef XYO_XY_TMEMORYOBJECTPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-tmemoryobjectpoolunifiedprocess.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectPoolProcess: public TMemoryObjectPoolUnifiedProcess<T> {};

	};
};

#endif

