//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVEPROCESS_HPP
#define XYO_XY_TMEMORYOBJECTPOOLACTIVEPROCESS_HPP

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#endif

#ifdef XYO_MEMORYPOOL_FORCE_ACTIVE_MEMORY_PROCESS_AS_SYSTEM
#       ifndef XYO_XY_TXMEMORYSYSTEM_HPP
#               include "xyo-xy-txmemorysystem.hpp"
#       endif
namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryObjectPoolActiveMemoryProcess: public TXMemorySystem<T> {};
	};
};
#else
#       ifndef XYO_XY_TXMEMORYPOOLUNIFIEDPROCESS_HPP
#               include "xyo-xy-txmemorypoolunifiedprocess.hpp"
#       endif
namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryObjectPoolActiveMemoryProcess: public TXMemoryPoolUnifiedProcess<T> {};
	};
};
#endif

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TMemoryObjectPoolActiveImplementProcess {
			protected:
#ifndef XYO_SINGLE_THREAD
				XCriticalSection criticalSection;
#endif
			public:

				typedef struct SLink {
					struct SLink *next;
					T value;
				} Link;

				Link *poolFree;
				size_t  linkValue;

				inline TMemoryObjectPoolActiveImplementProcess() {
					poolFree=nullptr;
				};

				inline ~TMemoryObjectPoolActiveImplementProcess() {
					Link *element;
					while (poolFree) {
						element = poolFree;
						poolFree = poolFree->next;
						TXMemoryObjectPoolActiveMemoryProcess<Link>::memoryDelete(element);
					};
				};

				inline void grow() {
					size_t k;
					Link *item;
					for (k=0; k<8; ++k) {
						item = TXMemoryObjectPoolActiveMemoryProcess<Link>::memoryNew();
						item->next = poolFree;
						item->value.setDynamic_((Object::PMemoryDelete)memoryDelete_,&item->value);
						poolFree = item;
					};
					linkValue=reinterpret_cast<byte *>(&item->value)-reinterpret_cast<byte *>(item);
				};

				inline T *memoryNew() {
					T *retV;
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					if(!poolFree) {
						grow();
					};
					retV=&poolFree->value;
					poolFree=poolFree->next;
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif

					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline void memoryDelete(T *value) {
					TIfHasActiveDestructor<T>::callActiveDestructor(value);
#ifndef XYO_SINGLE_THREAD
					criticalSection.enter();
#endif
					value=reinterpret_cast<T *>( (reinterpret_cast<byte *>(value)) - linkValue );
					(reinterpret_cast<Link *>(value))->next = poolFree;
					poolFree = (reinterpret_cast<Link *>(value));
#ifndef XYO_SINGLE_THREAD
					criticalSection.leave();
#endif
				};

				static void memoryDelete_(T *this_);

				static void memoryInit() {
					TXMemoryObjectPoolActiveMemoryProcess<Link>::memoryInit();
				};

				static TMemoryObjectPoolActiveImplementProcess *memoryPool;
				static const char *registryKey();
				static void resourceDelete(void *);
		};

		template<typename T>
		const char   *TMemoryObjectPoolActiveImplementProcess<T>::registryKey() {
			return XYO_FUNCTION;
		};

		template<typename T>
		TMemoryObjectPoolActiveImplementProcess<T> *TMemoryObjectPoolActiveImplementProcess<T>::memoryPool=nullptr;

		template<typename T>
		void TMemoryObjectPoolActiveImplementProcess<T>::resourceDelete(void *this_) {
			delete (TMemoryObjectPoolActiveImplementProcess<T> *)this_;
		};

		template<typename T>
		void TMemoryObjectPoolActiveImplementProcess<T>::memoryDelete_(T *this_) {
			(
				TMemoryObjectPoolActiveImplementProcess<T>::memoryPool
			)->memoryDelete(this_);
		};

		template<typename T>
		class TMemoryObjectPoolActiveProcess {
			public:

				static inline T *newObject() {
					T *retV;
					if(!TMemoryObjectPoolActiveImplementProcess<T>::memoryPool) {
						memoryInit();
					};
					retV = (
						       TMemoryObjectPoolActiveImplementProcess<T>::memoryPool
					       )->memoryNew();
					retV->incReferenceCount();
					return retV;
				};

				static inline void deleteObject(T *this_) {
					this_->decReferenceCount();
				};

				static inline T *newObjectX() {
					if(!TMemoryObjectPoolActiveImplementProcess<T>::memoryPool) {
						memoryInit();
					};
					return (
						       TMemoryObjectPoolActiveImplementProcess<T>::memoryPool
					       )->memoryNew();
				};

				static inline void deleteObjectX(T *this_) {
					TMemoryObjectPoolActiveImplementProcess<T>::memoryDelete_(this_);
				};

				inline static void memoryInit() {
					XRegistry::registryInit();
					TIfHasMemoryInit<T>::callMemoryInit();
					TMemoryObjectPoolActiveImplementProcess<T>::memoryInit();
					void *registryLink;

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalEnter();
#endif

					if(XRegistryProcess::registerKey(
						   TMemoryObjectPoolActiveImplementProcess<T>::registryKey(),
						   &registryLink
					   )) {
						TMemoryObjectPoolActiveImplementProcess<T>::memoryPool=new TMemoryObjectPoolActiveImplementProcess<T>();

						XRegistryProcess::setValue(
							registryLink,
							XRegistryLevel::Active,
							TMemoryObjectPoolActiveImplementProcess<T>::memoryPool,
							TMemoryObjectPoolActiveImplementProcess<T>::resourceDelete
						);

					} else {
						TMemoryObjectPoolActiveImplementProcess<T>::memoryPool=(TMemoryObjectPoolActiveImplementProcess<T> *)XRegistryProcess::getValue(registryLink);
					};

#ifndef XYO_SINGLE_THREAD
					XRegistryProcess::criticalLeave();
#endif

				};

		};

	};
};

#endif

