//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXLIST2_HPP
#define XYO_XY_TXLIST2_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TXLIST2NODE_HPP
#include "xyo-xy-txlist2node.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename TNode,typename TNodeMemory>
		class TXList2 {
			public:
				TNode *root;

				inline TXList2() {
					root=nullptr;
				};

				inline ~TXList2() {
					rootDestructor();
				};

				inline void rootDestructor() {
					TNode *this_;
					while (root) {
						this_ = root;
						root = root->next;
						TNodeMemory::memoryDelete(this_);
					};
				};

				inline void empty() {
					rootDestructor();
					root = nullptr;
				};

				inline void push(TNode *node) {
					node->back = nullptr;
					node->next = root;
					if (root) {
						root->back = node;
					};
					root = node;
				};

				inline TNode *pop() {
					if (root) {
						TNode *node=root;
						if (root->next) {
							root->next->back = nullptr;
						};
						root = root->next;
						return node;
					};
					return nullptr
				};

				inline TNode *popUnsafe() {
					TNode *node=root;
					if (root->next) {
						root->next->back = nullptr;
					};
					root = root->next;
					return node;
				};

				inline void extract(TNode *node) {
					if(root==node) {
						if(node->next) {
							node->next->back=nullptr;
						};
						root=node->next;
						return;
					};
					node->extract();
				};
		};

	};
};

#endif

