//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_SINGLE_THREAD
#ifndef XYO_XY_XCRITICALSECTION_HPP
#define XYO_XY_XCRITICALSECTION_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		typedef struct SXCriticalSection_ XCriticalSection_;

		class XCriticalSection {
			protected:
				XCriticalSection_ *criticalSection;
			public:

				XYO_XY_EXPORT XCriticalSection();
				XYO_XY_EXPORT ~XCriticalSection();

				XYO_XY_EXPORT void enter();
				XYO_XY_EXPORT void leave();
		};

	};
};

#endif
#endif

