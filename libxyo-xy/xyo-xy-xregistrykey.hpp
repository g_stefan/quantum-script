//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_XREGISTRYKEY_HPP
#define XYO_XY_XREGISTRYKEY_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

#ifndef XYO_XY_TCOMPARATOR_X_CHAR_POINTER_HPP
#include "xyo-xy-tcomparator-x-char-pointer.hpp"
#endif

#ifndef XYO_XY_TXREDBLACKTREE_HPP
#include "xyo-xy-txredblacktree.hpp"
#endif

namespace XYO {
	namespace XY {
		namespace XRegistryKey {

			class Node:
				public TXRedBlackTreeNode<Node,const char *> {
				public:

					void *processValue;
					size_t threadValue;
			};

			Node *registerKey(const char *key);

			void processBegin();
			void processEnd();

		};
	};
};

#endif

