//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TXMEMORYPOOLACTIVETHREAD_HPP
#define XYO_XY_TXMEMORYPOOLACTIVETHREAD_HPP

#ifdef XYO_SINGLE_THREAD

#ifndef XYO_XY_TXMEMORYPOOLACTIVEPROCESS_HPP
#include "xyo-xy-txmemorypoolactiveprocess.hpp"
#endif

namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryPoolActiveThread: public TXMemoryPoolActiveProcess<T> {};
	};
};

#else

#ifndef XYO_XY_TXMEMORYPOOLUNIFIEDTHREAD_HPP
#include "xyo-xy-txmemorypoolunifiedthread.hpp"
#endif

namespace XYO {
	namespace XY {
		template<typename T>
		class TXMemoryPoolActiveMemoryThread: public TXMemoryPoolUnifiedThread<T> {};
	};
};

#ifndef XYO_XY_TIFHASACTIVECONSTRUCTOR_HPP
#include "xyo-xy-tifhasactiveconstructor.hpp"
#endif

#ifndef XYO_XY_TIFHASACTIVEDESTRUCTOR_HPP
#include "xyo-xy-tifhasactivedestructor.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TXMemoryPoolActiveImplementThread {
			public:

				typedef struct SLink {
					struct SLink *next;
					T value;
				} Link;

				Link *poolFree;
				size_t  linkValue;

				inline TXMemoryPoolActiveImplementThread() {
					poolFree=nullptr;
				};

				inline ~TXMemoryPoolActiveImplementThread() {
					Link *element;
					while (poolFree) {
						element = poolFree;
						poolFree = poolFree->next;
						TXMemoryPoolActiveMemoryThread<Link>::memoryDelete(element);
					};
				};

				inline void grow() {
					size_t k;
					Link *item;
					for (k=0; k<8; ++k) {
						item = TXMemoryPoolActiveMemoryThread<Link>::memoryNew();
						item->next = poolFree;
						poolFree = item;
					};
					linkValue=reinterpret_cast<byte *>(&item->value)-reinterpret_cast<byte *>(item);
				};

				inline T *memoryNew() {
					T *retV;
					if(!poolFree) {
						grow();
					};
					retV=&poolFree->value;
					poolFree=poolFree->next;
					TIfHasActiveConstructor<T>::callActiveConstructor(retV);
					return retV;
				};

				inline void memoryDelete(T *value) {
					TIfHasActiveDestructor<T>::callActiveDestructor(value);
					value=reinterpret_cast<T *>( (reinterpret_cast<byte *>(value)) - linkValue );
					(reinterpret_cast<Link *>(value))->next = poolFree;
					poolFree = (reinterpret_cast<Link *>(value));
				};

				static void memoryInit() {
					TXMemoryPoolActiveMemoryThread<Link>::memoryInit();
				};

				static size_t registryLink;
				static const char *registryKey();
				static void resourceDelete(void *);
		};

		template<typename T>
		const char   *TXMemoryPoolActiveImplementThread<T>::registryKey() {
			return XYO_FUNCTION;
		};

		template<typename T>
		size_t TXMemoryPoolActiveImplementThread<T>::registryLink=0;

		template<typename T>
		void TXMemoryPoolActiveImplementThread<T>::resourceDelete(void *this_) {
			delete (TXMemoryPoolActiveImplementThread<T> *)this_;
		};

		template<typename T>
		class TXMemoryPoolActiveThread {
			public:

				inline static T *memoryNew() {
					T *retV=reinterpret_cast<T *>(XRegistryThread::getValue(TXMemoryPoolActiveImplementThread<T>::registryLink));
					if(!retV) {
						memoryInit();
						retV=reinterpret_cast<T *>(XRegistryThread::getValue(TXMemoryPoolActiveImplementThread<T>::registryLink));
					};
					return (
						       (TXMemoryPoolActiveImplementThread<T> *)
						       retV
					       )->memoryNew();
				};

				inline static void memoryDelete(T *this_) {
					(
						(TXMemoryPoolActiveImplementThread<T> *)
						XRegistryThread::getValue(TXMemoryPoolActiveImplementThread<T>::registryLink)
					)->memoryDelete(this_);
				};

				inline static void memoryInit() {
					XRegistry::registryInit();
					TIfHasMemoryInit<T>::callMemoryInit();
					TXMemoryPoolActiveImplementThread<T>::memoryInit();

					XRegistryThread::criticalEnter();
					if(XRegistryThread::registerKey(
						   TXMemoryPoolActiveImplementThread<T>::registryKey(),
						   &TXMemoryPoolActiveImplementThread<T>::registryLink
					   )) {

						XRegistryThread::setValue(
							TXMemoryPoolActiveImplementThread<T>::registryLink,
							XRegistryLevel::Active,
							new TXMemoryPoolActiveImplementThread<T>(),
							TXMemoryPoolActiveImplementThread<T>::resourceDelete
						);
					};
					XRegistryThread::criticalLeave();

				};

		};

	};
};

#endif

#endif


