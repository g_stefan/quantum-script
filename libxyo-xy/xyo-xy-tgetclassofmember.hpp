//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TGETCLASSOFMEMBER_HPP
#define XYO_XY_TGETCLASSOFMEMBER_HPP

#ifndef XYO_XY_HPP
#include "xyo-xy.hpp"
#endif

namespace XYO {
	namespace XY {

		template<class U,class V>
		U TGetClassOfMember(V U::*);

	};
};

#endif

