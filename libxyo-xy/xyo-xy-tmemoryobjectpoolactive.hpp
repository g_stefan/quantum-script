//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVE_HPP
#define XYO_XY_TMEMORYOBJECTPOOLACTIVE_HPP

#ifdef XYO_MEMORYPOOL_FORCE_ACTIVE_MEMORY_AS_SYSTEM

#ifndef XYO_XY_TMEMORYOBJECTSYSTEM_HPP
#include "xyo-xy-tmemoryobjectsystem.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectPoolActive: public TMemoryObjectSystem<T> {};

	};
};


#else

#ifndef XYO_XY_TMEMORYOBJECTPOOLACTIVETHREAD_HPP
#include "xyo-xy-tmemoryobjectpoolactivethread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectPoolActive: public TMemoryObjectPoolActiveThread<T> {};

	};
};

#endif
#endif



