//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TMEMORYOBJECTPOOL_HPP
#define XYO_XY_TMEMORYOBJECTPOOL_HPP

#ifndef XYO_XY_TMEMORYOBJECTPOOLTHREAD_HPP
#include "xyo-xy-tmemoryobjectpoolthread.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T> class TMemoryObjectPool: public TMemoryObjectPoolThread<T> {};

	};
};

#endif

