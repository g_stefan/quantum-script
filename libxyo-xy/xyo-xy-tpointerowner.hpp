//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_TPOINTEROWNER_HPP
#define XYO_XY_TPOINTEROWNER_HPP

#ifndef XYO_XY_TCOMPARATOR_HPP
#include "xyo-xy-tcomparator.hpp"
#endif

#ifndef XYO_XY_TMEMORY_HPP
#include "xyo-xy-tmemory.hpp"
#endif

namespace XYO {
	namespace XY {

		template<typename T>
		class TPointer;

		template<typename T>
		class TPointerX;

		//
		// Executive link - part of return/function parameter
		//
		template<typename T>
		class TPointerOwner {
				template<typename Type > friend class TPointer;
				template<typename Type > friend class TPointerX;
			protected:

				T *object;

			public:

				inline TPointerOwner() {
					object=nullptr;
				};

				inline ~TPointerOwner() {
					if (object) {
						object->decReferenceCount();
					};
				};

				inline TPointerOwner(T *value) {
					object = value;
				};

				inline TPointerOwner(const TPointerOwner &value) {
					object = value.object;
					(const_cast<TPointerOwner &>(value)).object = nullptr;
				};

				inline TPointerOwner(TPointerOwner &&value) {
					object = value.object;
					value.object = nullptr;
				};

				inline TPointerOwner(const TPointer<T> &value) {
					object = const_cast<TPointer<T> &>(value).getObject();
				};

				inline TPointerOwner(TPointer<T> &&value) {
					object = value.getObject();
				};

				inline TPointerOwner(const TPointerX<T> &value) {
					object = (const_cast<TPointerX<T> &>(value)).getObject();
				};

				inline TPointerOwner(TPointerX<T> &&value) {
					object = value.getObject();
				};

				inline TPointerOwner &operator=(T *value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value;
					return *this;
				};

				inline TPointerOwner &operator=(const TPointerOwner &value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					(const_cast<TPointerOwner &>(value)).object = nullptr;
					return *this;
				};

				inline TPointerOwner &operator=(TPointerOwner &&value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					value.object = nullptr;
					return *this;
				};

				inline TPointerOwner &operator=(const TPointer<T> &value) {
					if (object) {
						object->decReferenceCount();
					};
					object = (const_cast<TPointer<T> &>(value)).getObject();
					return *this;
				};

				inline TPointerOwner &operator=(TPointer<T> &&value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.object;
					value.object=nullptr;
					return *this;
				};

				inline TPointerOwner &operator=(const TPointerX<T> &value) {
					if (object) {
						object->decReferenceCount();
					};
					object = (const_cast<TPointerX<T> &>(value)).getObject();
					return *this;
				};

				inline TPointerOwner &operator=(TPointerX<T> &&value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value.getObject();
					return *this;
				};


				inline bool operator==(const TPointerOwner &value) const {
					return object == value.object;
				};

				inline bool operator==(const TPointer<T> &value) const {
					return object == value.object;
				};

				inline bool operator==(const TPointerX<T> &value) const {
					return object == (T *) value.object;
				};

				inline bool operator==(const T *value) const {
					return object == value;
				};

				inline bool operator==(T *value) const {
					return object == value;
				};

				inline bool operator!=(const TPointerOwner &value) const {
					return object != value.object;
				};

				inline bool operator!=(const TPointer<T> &value) const {
					return object != value.object;
				};

				inline bool operator!=(const TPointerX<T> &value) const {
					return object != (T *) value.object;
				};

				inline bool operator!=(const T *value) const {
					return object != value;
				};

				inline bool operator!=(T *value) const {
					return object != value;
				};

				inline operator bool() {
					return (object != nullptr);
				};

				inline T *operator->() {
					return object;
				};

				inline T &operator*() {
					return *object;
				};

				inline operator T *() {
					return object;
				};

				inline operator T &() {
					return *object;
				};

				inline T *value() {
					return object;
				};

				inline T &reference() {
					return *object;
				};

				inline bool isValid() {
					return object != nullptr;
				};

				inline void newObject() {
					if (object) {
						object->decReferenceCount();
					};
					object = TMemoryObject<T>::newObject();
				};

				inline void deleteObject() {
					if (object) {
						object->decReferenceCount();
						object = nullptr;
					};
				};

				inline void setObject(T *value) {
					if (object) {
						object->decReferenceCount();
					};
					object = value;
				};

				inline void ownerTransfer() {
					object = nullptr;
				};

				inline T *getObject() {
					if (object) {
						object->incReferenceCount();
					};
					return object;
				};

				inline T *getObjectOwnerTransfer() {
					T *retV=object;
					object=nullptr;
					return retV;
				};

				inline void activeDestructor() {
					if (object) {
						object->decReferenceCount();
						object = nullptr;
					};
				};

				inline static void memoryInit() {
					TMemory<T>::memoryInit();
				};

		};

		template<typename T_>
		class TComparator<TPointerOwner<T_> > {
			public:
				typedef TPointerOwner<T_> T;

				inline static bool isEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isNotEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isNotEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isLessThan(const T &a, const T &b) {
					return TComparator<T_>::isLessThan(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isGreaterThan(const T &a, const T &b) {
					return TComparator<T_>::isGreaterThan(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isLessThanOrEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isLessThanOrEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static bool isGreaterThanOrEqualTo(const T &a, const T &b) {
					return TComparator<T_>::isGreaterThanOrEqualTo(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};

				inline static int compare(const T &a, const T &b) {
					return TComparator<T_>::compare(*(const_cast<T &>(a)),*(const_cast<T &>(b)));
				};
		};


	};
};

#endif

