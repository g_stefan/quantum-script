//
// XYO XY Library
//
// Copyright (c) 2014 Grigore Stefan, <g_stefan@yahoo.com>
// Created by Grigore Stefan <g_stefan@yahoo.com>
//
// The MIT License (MIT) <http://opensource.org/licenses/MIT>
//

#ifndef XYO_XY_IMAIN_HPP
#define XYO_XY_IMAIN_HPP

#ifndef XYO_XY_OBJECT_HPP
#include "xyo-xy-object.hpp"
#endif

namespace XYO {
	namespace XY {

		class IMain :
			public virtual Object {
			public:
				virtual int main(int cmdN, char *cmdS[]) = 0;

				XYO_XY_INTERFACE(IMain);
		};

#ifdef XYO_OS_TYPE_WIN
		XYO_XY_EXPORT void mainArgsSet__(int &cmdN,char ** &cmdS);
		XYO_XY_EXPORT void mainArgsDelete__(int cmdN,char **cmdS);
#endif

#define XYO_XY_MAIN_STD(T) \
	static int xyo_xy_main_(int cmdN,char *cmdS[]){\
		T application;\
		return (static_cast<XYO::XY::IMain *>(&application))->main(cmdN,cmdS);\
	};\
	int main(int cmdN,char *cmdS[]){\
		int retV;\
		TIfHasMemoryInit<T>::callMemoryInit();\
		retV=xyo_xy_main_(cmdN,cmdS);\
		return retV;\
	}

#define XYO_XY_MAIN_CSTD(applicationMain) \
	int main(int cmdN,char *cmdS[]){\
		return applicationMain(cmdN,cmdS);\
	}


#ifdef XYO_OS_TYPE_WIN
#define XYO_XY_WINMAIN_STD(T) \
	static int xyo_xy_main_(int cmdN,char *cmdS[]){\
		T application;\
		return (static_cast<XYO::XY::IMain *>(&application))->main(cmdN,cmdS);\
	};\
	int __stdcall WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR cmdLine,int cmdShow){\
		int    cmdN;\
		char** cmdS;\
		int retV;\
		TIfHasMemoryInit<T>::callMemoryInit();\
		XYO::XY::mainArgsSet__(cmdN,cmdS);\
		retV=xyo_xy_main_(cmdN,cmdS);\
		XYO::XY::mainArgsDelete__(cmdN,cmdS);\
		return retV;\
	}
#endif

#define XYO_XY_WINMAIN_CSTD(applicationMain) \
	int __stdcall WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR cmdLine,int cmdShow){\
		int    cmdN;\
		char** cmdS;\
		int retV;\
		XYO::XY::mainArgsSet__(cmdN,cmdS);\
		retV=applicationMain(cmdN,cmdS);\
		XYO::XY::mainArgsDelete__(cmdN,cmdS);\
		return retV;\
	}

	};
};

#endif

