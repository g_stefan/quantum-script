#ifndef QUANTUM_SCRIPT_GUI_VERSION_HPP
#define QUANTUM_SCRIPT_GUI_VERSION_HPP

#define QUANTUM_SCRIPT_GUI_VERSION_ABCD      5,2,0,65
#define QUANTUM_SCRIPT_GUI_VERSION_A         5
#define QUANTUM_SCRIPT_GUI_VERSION_B         2
#define QUANTUM_SCRIPT_GUI_VERSION_C         0
#define QUANTUM_SCRIPT_GUI_VERSION_D         65
#define QUANTUM_SCRIPT_GUI_VERSION_STR_ABCD  "5.2.0.65"
#define QUANTUM_SCRIPT_GUI_VERSION_STR       "5.2.0"
#define QUANTUM_SCRIPT_GUI_VERSION_STR_BUILD "65"
#define QUANTUM_SCRIPT_GUI_VERSION_BUILD     65
#define QUANTUM_SCRIPT_GUI_VERSION_HOUR      18
#define QUANTUM_SCRIPT_GUI_VERSION_MINUTE    44
#define QUANTUM_SCRIPT_GUI_VERSION_SECOND    52
#define QUANTUM_SCRIPT_GUI_VERSION_DAY       14
#define QUANTUM_SCRIPT_GUI_VERSION_MONTH     6
#define QUANTUM_SCRIPT_GUI_VERSION_YEAR      2015
#define QUANTUM_SCRIPT_GUI_VERSION_STR_DATETIME "2015-06-14 18:44:52"

#ifndef XYO_RC

namespace Quantum {
	namespace Script {

		class Version {
			public:
				static const char *getABCD();
				static const char *getA();
				static const char *getB();
				static const char *getC();
				static const char *getD();
				static const char *getVersion();
				static const char *getBuild();
				static const char *getHour();
				static const char *getMinute();
				static const char *getSecond();
				static const char *getDay();
				static const char *getMonth();
				static const char *getYear();
				static const char *getDatetime();
		};

	};
};
#endif
#endif

